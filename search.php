<?
$indexloaded=1;
include("config/config.php");
include("$dir[func]/global.php");
include("$dir[func]/loginforms.php");
switch($action){
    case "search":
    do_search($find,$type,$order);
    break;
    default:
    search();
    break;
}

function search(){
    global $url, $file, $dir, $out, $site;
    $tablehead=table_head("show","400","","left");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    <br>
    <form method='post' action='$url[base]/$file[search]'>
    $tablehead
    &nbsp; &nbsp;<strong><font class='catfont'>Search $site[longname]</font></strong><br>
    <hr class='catfont' size='1'>
    <center>
    <input type='text' name='find' maxlength='50' class='span3'>
    <select name='type'>
    <option value='1'>Clanes</option>
    <option value='2'>Jugadores</option>
    </select>
    <input type='hidden' name='action' value='search'>
    <button type='submit' name='' value='Search' class='btn btn-mini btn-primary'>Buscar</button>
    </center>
    $tablefoot
    </form>
    </center>";
    include("$dir[curtheme]");
}

function do_search($find,$type,$order){
    global $url, $file, $dir, $out, $site, $misc;
    if(!$find){
        include("$dir[func]/error.php");
        display_error("You must enter something to search for.<br>");
    }

    if(!$type){
        include("$dir[func]/error.php");
        display_error("Invalid search type.<br>");
    }

    $maxresults="25";
    $altcolora="#000033";
    $altcolorb="#000020";
    include("$dir[func]/checkdata.php");
    $find=change_charecters($find);
    $findid=change_numbersonly($find);
    $find="%".$find."%";
    $tablehead=table_head("show","100%","","left");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    &nbsp; &nbsp;<strong><font class='catfont'>Search Results</font></strong><br>
    <hr class='catfont' size='1'>
    <table width='100%' border='0' bordercolor='#000000' cellspacing='1' cellpadding='1'>
    ";
    if($order == "alias_down"){
        $alias = "alias_up";
    }else{

        $alias = "alias_down";
    }

    if($order == "id_down"){
        $id = "id_up";
    }else{

        $id = "id_down";
    }

    if($order == "joindate_down"){
        $joindate = "joindate_up";
    }else{

        $joindate = "joindate_down";
    }

    if($order == "email_down"){
        $email = "email_up";
    }else{

        $email = "email_down";
    }

    if($order == "icq_down"){
        $icq = "icq_up";
    }else{

        $icq = "icq_down";
    }

    if($order == "aim_down"){
        $aim = "aim_up";
    }else{

        $aim = "aim_down";
    }

    if($order == "msn_down"){
        $msn = "msn_up";
    }else{

        $msn = "msn_down";
    }

    if($order == "website_down"){
        $website = "website_up";
    }else{

        $website = "website_down";
    }

    if($order == "country_down"){
        $country = "country_up";
    }else{

        $country = "country_down";
    }

    //PLAYER SEARCH
    if($type=="2"){
        $out[body]=$out[body]."
        <tr class='altcolor'>
        <td width='25%' valign='center' align='left'><strong><a href='$url[base]/$file[search]?action=search&order=$alias&find=$find&type=$type'>Player Name</a></strong></td>
        <td width='5%' valign='center' align='center'><strong><a href='$url[base]/$file[search]?action=search&order=$id&find=$find&type=$type'>ID</a></strong></td>
        <td width='20%' valign='center' align='center'><strong><a href='$url[base]/$file[search]?action=search&order=$joindate&find=$find&type=$type'>Joindate</a></strong></td>
        <td width='10%' valign='center' align='center'><strong><a href='$url[base]/$file[search]?action=search&order=$email&find=$find&type=$type'>Email</a></strong></td>
        <td width='10%' valign='center' align='center'><strong><a href='$url[base]/$file[search]?action=search&order=$icq&find=$find&type=$type'>Icq</a></strong></td>
        <td width='10%' valign='center' align='center'><strong><a href='$url[base]/$file[search]?action=search&order=$aim&find=$find&type=$type'>Aim</a></strong></td>
        <td width='10%' valign='center' align='center'><strong><a href='$url[base]/$file[search]?action=search&order=$msn&find=$find&type=$type'>MSN</a></strong></td>
        <td width='10%' valign='center' align='center'><strong><a href='$url[base]/$file[search]?action=search&order=$website&find=$find&type=$type'>Website</a></strong></td>";
        if($misc[flags] == "yes"){
            $out[body]=$out[body]."
            <td width='8%' valign='center' align='center'><strong><a href='$url[base]/$file[search]?action=search&order=$country&find=$find&type=$type''>Flag</a></strong></td>
            ";
        }

        if($order == "id_up"){
            $order = "id";
        }else if ($order == "id_down"){

            $order = "id desc";
        }else if ($order == "joindate_down"){

            $order = "joindate desc";
        }else if ($order == "joindate_up"){

            $order = "joindate";
        }else if ($order == "alias_down"){

            $order = "alias desc";
        }else if ($order == "alias_up"){

            $order = "alias";
        }else if ($order == "email_down"){

            $order = "email desc";
        }else if ($order == "email_up"){

            $order = "email";
        }else if ($order == "icq_down"){

            $order = "icq desc";
        }else if ($order == "icq_up"){

            $order = "icq";
        }else if ($order == "aim_down"){

            $order = "aim desc";
        }else if ($order == "aim_up"){

            $order = "aim";
        }else if ($order == "msn_down"){

            $order = "msn desc";
        }else if ($order == "msn_up"){

            $order = "msn";
        }else if ($order == "website_down"){

            $order = "website desc";
        }else if ($order == "website_up"){

            $order = "website";
        }else if ($order == "country_down"){

            $order = "country desc";
        }else if ($order == "country_up"){

            $order = "country";
        }else{

            $order = "alias";
        }

        $playerinfo=mysql_query("SELECT id,alias,email,icq,aim,msn,website,country,DATE_FORMAT(joindate, '%M %d'),ipaddress FROM users WHERE id='$findid' OR alias LIKE'$find' ORDER BY $order LIMIT $maxresults");
        while(list($pid,$alias,$email,$icq,$aim,$msn,$website,$country,$joindate,$ip)=mysql_fetch_row($playerinfo)) {
            if($altladrow=="$altcolora"){
                $altladrow="$altcolorb";
            }else{

                $altladrow="$altcolora";
            }

            if($icq > 0){
                $icq="<a href='http://wwp.icq.com/scripts/search.dll?to=$icq'><img src='$url[themes]/images/icq_addme.gif' border='0'></a>";
            }else{

                $icq="";
            }

            if($aim){
                $aim="<a href='aim:addbuddy?screenname=$aim'><img src='$url[themes]/images/aim.gif' border='0'></a>";
            }else{

                $aim="";
            }

            if($msn){
                $msn="";
            }else{

                $msn="";
            }

            if($website){
                $website="<a href='$website' target='top'><img src='$url[themes]/images/home.gif' border='0'></a>";
            }else{

                $website="";
            }

            if($country){
                $country="<img width='20' height='14'  src='$url[themes]/images/flags/$country[0]$country[1]_small.gif'>";
            }else{

                $country="";
            }

            if($ip){
                $ip="<img src='$url[themes]/images/ip.gif' border='0' alt='$ip' style='cursor:hand;'>";
            }else{

                $ip="?";
            }

            $out[body]=$out[body]."
            <tr bgcolor='$altladrow'>
            <td width='' valign='center' align='left'><a href='$url[base]/$file[players]?playerid=$pid'>$alias</a></td>
            <td width='' valign='center' align='center'>$pid</td>
            <td width='' valign='center' align='center'>$joindate</td>
            <td width='' valign='center' align='center'><a href='mailto:$email'><img src='$url[themes]/images/email.gif' border='0'></a></td>
            <td width='' valign='center' align='center'>$icq</td>
            <td width='' valign='center' align='center'>$aim</td>
            <td width='' valign='center' align='center'>$msn</td>
            <td width='' valign='center' align='center'>$website</td>";
            if($misc[flags] == "yes"){
                $out[body]=$out[body]."
                <td width='' valign='center' align='center'>$country</td>
                ";
            }

        }

    }

    // TEAM SEARCH
    if($type=="1"){
        if($order == "teamname_down"){
            $teamname = "teamname_up";
        }else{

            $teamname = "teamname_down";
        }

        if($order == "id_down"){
            $id = "id_up";
        }else{

            $id = "id_down";
        }

        if($order == "tag_down"){
            $tag = "tag_up";
        }else{

            $tag = "tag_down";
        }

        if($order == "joindate_down"){
            $joindate = "joindate_up";
        }else{

            $joindate = "joindate_down";
        }

        if($order == "teamemail_down"){
            $email = "email_up";
        }else{

            $email = "email_down";
        }

        if($order == "type_down"){
            $typea = "type_up";
        }else{

            $typea = "type_down";
        }

        if($order == "website_down"){
            $website = "website_up";
        }else{

            $website = "website_down";
        }

        $out[body]=$out[body]."
        <tr class='altcolor'>
        <td width='' valign='center' align='left'><strong><a href='$url[base]/$file[search]?action=search&order=$teamname&find=$find&type=$type'>Team Name</a></strong></td>
        <td width='' valign='center' align='center'><strong><a href='$url[base]/$file[search]?action=search&order=$id&find=$find&type=$type'>Team ID</a></strong></td>
        <td width='' valign='center' align='center'><strong><a href='$url[base]/$file[search]?action=search&order=$tag&find=$find&type=$type'>Tag</a></strong></td>
        <td width='' valign='center' align='center'><strong><a href='$url[base]/$file[search]?action=search&order=$joindate&find=$find&type=$type'>Joindate</a></strong></td>
        <td width='' valign='center' align='center'><strong><a href='$url[base]/$file[search]?action=search&order=$email&find=$find&type=$type'>Email</a></strong></td>
        <td width='' valign='center' align='center'><strong><a href='$url[base]/$file[search]?action=search&order=$website&find=$find&type=$type'>Website</a></strong></td>
        <td width='' valign='center' align='center'><strong><a href='$url[base]/$file[search]?action=search&order=$typea&find=$find&type=$type'>Type</a></strong></td>";
        if($misc[flags] == "yes"){
            $out[body]=$out[body]."<td width='' valign='center' align='center'><strong>Country</strong></td>";
        }

        if($order == "id_up"){
            $order = "id";
        }else if ($order == "id_down"){

            $order = "id desc";
        }else if ($order == "joindate_down"){

            $order = "joindate desc";
        }else if ($order == "joindate_up"){

            $order = "joindate";
        }else if ($order == "teamname_down"){

            $order = "teamname desc";
        }else if ($order == "teamname_up"){

            $order = "teamname";
        }else if ($order == "email_down"){

            $order = "teamemail desc";
        }else if ($order == "email_up"){

            $order = "teamemail";
        }else if ($order == "tag_down"){

            $order = "tag desc";
        }else if ($order == "tag_up"){

            $order = "tag";
        }else if ($order == "type_up"){

            $order = "type";
        }else if ($order == "type_down"){

            $order = "type desc";
        }else{

            $order = "teamname";
        }

        $result=mysql_query("SELECT id,idcreate,teamname,teamemail,tag,website,DATE_FORMAT(joindate, '%M %d'),type from teams WHERE (id='$findid' OR teamname LIKE'$find') AND type='3' ORDER BY $order LIMIT $maxresults");
        while(list($tid,$idcreate,$teamname,$teamemail,$tag,$website,$joindate,$ttype)=mysql_fetch_row($result)) {
            if($altladrow=="$altcolora"){
                $altladrow="$altcolorb";
            }else{

                $altladrow="$altcolora";
            }

            if($ttype == '1'){
                $teamtype = "Singles";
            }else if($ttype == '3'){

                $teamtype = "Team";
            }else{

                $teamtype = "";
            }

            $flag = mysql_fetch_array(mysql_query("SELECT country FROM users WHERE id='$idcreate'"));
            if($flag[0] == ""){
                $country = "";
            }else{

                $country="<img width='20' height='14'  src='$url[themes]/images/flags/$flag[0]_small.gif'>";
            }

            if($website){
                $website="<a href='$website' target='top'><img src='$url[themes]/images/home.gif' border='0'></a>";
            }else{

                $website="";
            }

            $out[body]=$out[body]."
            <tr bgcolor='$altladrow'>
            <td width='' valign='center' align='left'><a href='$url[base]/$file[teams]?teamid=$tid'>$teamname</a></td>
            <td width='' valign='center' align='center'>$tid</td>
            <td width='' valign='center' align='center'>$tag</td>
            <td width='' valign='center' align='center'>$joindate</td>
            <td width='' valign='center' align='center'><a href='mailto:$teamemail'><img src='$url[themes]/images/email.gif' border='0'></a></td>
            <td width='' valign='center' align='center'>$website</td>
            <td width='' valign='center' align='center'>$teamtype</td>";
            if($misc[flags] == "yes"){
                $out[body]=$out[body]."<td width='' valign='center' align='center'>$country</td>";
            }

        }

    }

    //FOOT
    $out[body]=$out[body]."
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

?>
