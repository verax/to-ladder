<?
$indexloaded=1;
include("config/config.php");
include("$dir[func]/global.php");
switch($action){
    case "alogin":
    admin_login($adminid,$adminpass);
    break;
    case "login":
    player_login($playerid,$playerpass);
    break;
    case "logout":
    player_logout();
    break;
    case "adminlogin":
    admin_login();
    break;
    case "adminlogout":
    admin_logout();
    break;
    default:
    header("Location: $url[base]/$file[main]");
    break;
}

function player_login($playerid, $playerpass){
    global $dir, $url, $file, $misc;
    include("$dir[func]/checkdata.php");
    if($misc[passrequests] != 'no'){
        $lostpasslink = "<a href='$url[base]/$file[join]?action=lostpass'>Olvidaste tu contrase&nacute;a? Click aqu&iacute;.</a><br>";
    }else{

        $lostpasslink="";
    }

    //CHANGED TO ALLOW NAMES LOGIN
    //$playerid=change_numbersonly($playerid);
    $playerid=change_charecters($playerid);
    $playerpass=change_charecters($playerpass);
    $errormessage=check_validlength($errormessage, $playerid, "1", "50", "Player ID no es v&aacute;lido.<br><br>$lostpasslink");
    $errormessage=check_validlength($errormessage, $playerpass, "2", "25", "Contrase&nacute;a no es v&aacute;lida<br><br>$lostpasslink");
    $errormessage=check_ban($errormessage, $alias, $email, $ip);
    error_check($errormessage);
    $playerinfo=mysql_query("SELECT id,pass FROM users WHERE id='$playerid' AND pass='$playerpass' OR alias='$playerid' AND pass='$playerpass'");
    $user=mysql_fetch_array($playerinfo);
    if(!$user[id]){
        error_check("Player ID o Contrase&nacute;a incorrecta.<br><br>$lostpasslink");
    }

    $tday=date("Y-m-d H:i:s");
    mysql_query("UPDATE users SET lastlogin='$tday' WHERE id='$user[id]'");
    //SET PLAYER COOKIE
    setcookie("PID","");
    setcookie("PPW","");
    $cexpire="never"; //Time before cookie expires, set to session to limit it to a user session.
    cookie_crisp("PID", $user[id], $ccode, $cexpire);
    cookie_crisp("PPW", $user[pass], "yes", $cexpire);
    $ref = getenv ("HTTP_REFERER");
    header("Location: $ref");
}

function cookie_crisp($cname, $cvalue, $ccode, $cexpire){
    global $timeout;
    if(($cname) && ($cvalue)){
        setcookie("$cname");
        if($ccode){
            $cvalue=base64_encode($cvalue);
        }

        if(!$cexpire){
            $cexpire="session";
        }

        if($cexpire=="never"){
            $cexpire=(time()+60*60*24*365);
        }

        if($cexpire=="session"){
            $cexpire="";
        }

        $cpath="/";
        $cdomain="";
        setcookie("$cname","$cvalue",$cexpire);
    }

}

function player_logout(){
    global $url, $file;
    setcookie("PID","");
    setcookie("PPW","");
    header("Location: $url[base]/$file[main]");
}

function admin_login($adminid, $adminpass){
    global $dir, $url, $file;
    include("$dir[func]/checkdata.php");
    $adminid=change_numbersonly($adminid);
    $adminpass=change_charecters($adminpass);
    $errormessage=check_validlength($errormessage, $adminid, "1", "10", "Staff ID es inv&aacute;lido");
    $errormessage=check_validlength($errormessage, $adminpass, "2", "25", "Contrase&nacute;a Incorrecta");
    $errormessage=check_ban($errormessage, $alias, $email, $ip);
    error_check($errormessage);
    $staffinfo=mysql_query("SELECT id,pass,access FROM staff WHERE id='$adminid' AND pass='$adminpass'");
    $staff=mysql_fetch_array($staffinfo);
    if((!$staff[id]) || (!$staff[pass])){
        error_check("Staff ID o contrase&nacute;a incorrecta.<br>");
    }

    if($staff[access] < 1){
        error_check("No estas autorizado para loguearte como Staff.<br>");
    }

    $tday=date("Y-m-d H:i:s");
    mysql_query("UPDATE staff SET lastlogin='$tday' WHERE id='$staff[id]'");
    //Delete old admin cookie
    setcookie("AID","");
    setcookie("APW","");
    //Set admin cookie
    $cexpire="100";
    cookie_crisp(AID,$staff[id],$ccode,$cexpire);
    cookie_crisp(APW,$staff[pass],yes,$cexpire);
    header("Location: $url[base]/$file[admin]");
}

function admin_logout(){
    global $url, $file, $admn;
    setcookie("AID","");
    setcookie("APW","");
    header("Location: $url[base]/$file[main]");
}

?>
