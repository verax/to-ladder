<?
function match_database($page,$order){
    global $dir, $file, $url, $out, $misc, $site, $admn;
    $result=mysql_query("select COUNT(*) from matchdb");
    $result=mysql_fetch_array($result);
    $totalusers="$result[0]";
    $maxresults="25";
    $stop="$maxresults";
    if(!$page){
        $start="1";
    }else{

        $start="$page";
    }

    if($start > $totalusers){
        $start=0;
    }

    $page="$start";
    $start=($start-1);
    $altcolora="#000033";
    $altcolorb="#000020";
    if($order == "matchid_up"){
        $order = "matchid";
    }else if ($order == "matchid_down"){

        $order = "matchid desc";
    }else if ($order == "winneralias_down"){

        $order = "winneralias desc";
    }else if ($order == "winneralias_up"){

        $order = "winneralias";
    }else if ($order == "loseralias_down"){

        $order = "loseralias desc";
    }else if ($order == "loseralias_up"){

        $order = "loseralias";
    }else if ($order == "laddername_down"){

        $order = "laddername desc";
    }else if ($order == "laddername_up"){

        $order = "laddername";
    }else if ($order == "reportdate_up"){

        $order = "reportdate";
    }else{

        $order = "reportdate desc";
    }

    $result=mysql_query("SELECT matchid,winnerid,loserid,winneralias,loseralias,ladderid,laddername,DATE_FORMAT(reportdate, '%M %d, %l:%i %p'),reporterip,confirmerip from matchdb ORDER BY $order LIMIT $start,$stop");
    while(list($matchid,$winnerid,$loserid,$winneralias,$loseralias,$ladderid,$laddername,$reportdate,$reporterip,$confirmerip)=mysql_fetch_row($result)) {
        if($altladrow=="$altcolora"){
            $altladrow="$altcolorb";
        }else{

            $altladrow="$altcolora";
        }

        if($reporterip){
            $reporterip="<img src='$url[themes]/images/ip.gif' border='0' alt='$reporterip' style='cursor:hand;'>";
        }else{

            $reporterip="?";
        }

        if($confirmerip){
            $confirmerip="<img src='$url[themes]/images/ip.gif' border='0' alt='$confirmerip' style='cursor:hand;'>";
        }else{

            $confirmerip="?";
        }

        $playerlist=$playerlist."
        <tr bgcolor='$altladrow'>
        <td width='' valign='center' align='left'><a href='$url[base]/$file[teams]?teamid=$winnerid'>$winneralias</a></td>
        <td width='' valign='center' align='center'><a href='$url[base]/$file[teams]?teamid=$loserid'>$loseralias</a></td>
        <td width='' valign='center' align='center'>$reportdate</td>
        <td width='' valign='center' align='center'><a href='$url[base]/$file[match]?matchid=$matchid'><img src='$url[themes]/images/stats.gif' border='0'></a></td>
        <td width='' valign='center' align='center'><a href='$url[base]/$file[game]?ladderid=$ladderid'>$laddername</a></td>";
        if($admn[id]){
            $ipmatchcolor="";
            if($confirmerip==$reporterip){
                $ipmatchcolor="bgcolor='#336699'";
            }

            $playerlist=$playerlist."
            <td width='' valign='center' align='center' $ipmatchcolor>$confirmerip</td>
            <td width='' valign='center' align='center' $ipmatchcolor>$reporterip</td>";
        }

        $playerlist=$playerlist."</tr>";
        $counter++;
    }

    if($order == "winneralias desc"){
        $winner = "winneralias_up";
    }else{

        $winner = "winneralias_down";
    }

    if($order == "loseralias desc"){
        $loser = "loseralias_up";
    }else{

        $loser = "loseralias_down";
    }

    if($order == "reportdate desc"){
        $matchdate = "reportdate_up";
    }else{

        $matchdate = "reportdate_down";
    }

    if($order == "laddername desc"){
        $ladder = "laddername_up";
    }else{

        $ladder = "laddername_down";
    }

    $tablehead=table_head("show","100%","","left");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    <br>
    <center>
    $tablehead
    &nbsp; &nbsp;<strong><font class='catfont'>$site[shortname] Match Database</font></strong><br>
    <hr class='catfont' size='1'>
    <table width='100%' border='0' bordercolor='#000000' cellspacing='1' cellpadding='1'>
    <tr class='altcolor'>
    <td width='' valign='center' align='left'><strong><a href='$url[base]/$file[stats]?action=matchdb&order=$winner'>Winner</a></strong></td>
    <td width='' valign='center' align='center'><strong><a href='$url[base]/$file[stats]?action=matchdb&order=$loser'>Loser</a></strong></td>
    <td width='' valign='center' align='center'><strong><a href='$url[base]/$file[stats]?action=matchdb&order=$matchdate'>Match Date</a></strong></td>
    <td width='' valign='center' align='center'><strong>Stats</strong></td>
    <td width='' valign='center' align='center'><strong><a href='$url[base]/$file[stats]?action=matchdb&order=$ladder'>Ladder</a></strong></td>";
    if($admn[id]){
        $out[body]=$out[body]."
        <td width='' valign='center' align='center'><strong>Wip</strong></td>
        <td width='' valign='center' align='center'><strong>Lip</strong></td>";
    }

    $out[body]=$out[body]."
    </tr>
    $playerlist
    ";
    $pagenumb=1;
    if ($totalusers > $maxresults) {
        $totalpages=($totalusers / $maxresults);
        while($pagenum < $totalpages){
            $pagenum++;
            if($pagenumb=="$page"){
                $pages=$pages."[<a href='$url[base]/$file[stats]?action=matchdb&page=$pagenumb'>$pagenum</a>] ";
            }else{

                $pages=$pages."<a href='$url[base]/$file[stats]?action=matchdb&page=$pagenumb'>$pagenum</a> ";
            }

            $pagenumb=($maxresults + $pagenumb);
        }

    }

    if($pages){
        $out[body]=$out[body]."
        <tr class='altcolor'>
        <td width='100%' valign='center' align='center' colspan='8'><strong>$pages</strong></td>
        </tr>";
    }

    $out[body]=$out[body]."
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function checkl(){
    global $dir, $file, $url, $out, $misc, $site, $admn;
    $tday=date("Y-m-d H:i:s");
    $op='Q29tYm8gU3lzdGVtIFZlcnNpb24gNy4wICgxMS8yOC8wNSkgPGJyPlNvZnR3YXJlIGJ5IGh0dHA6Ly93d3cubXlnYW1pbmdsYWRkZXIuY29tPGJyPlRoaXMgaXMgbGljZW5zZWQgYW5kIGNvcHlyaWdodGVkIHNvZnR3YXJlIGFuZCBpdHMgdXNlIHdpdGhvdXQgYSB2YWxpZCBsaWNlbnNlIGlzIGFnYWluc3QgdGhlIGxhdyBhbmQgaXMgc3RyaWN0bHkgbm90IHBlcm1pdHRlZC4=';
    $end = base64_decode("$op");
    include("$dir[func]/error.php");
    display_error("$tday<br><br>$end");
}

?>
