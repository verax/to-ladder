<?PHP
function send_email($toname, $toemail, $emailsubject, $emailbody){
    global $email;
    if($email[localhost]=="yes"){
        sendmail_local($toname, $toemail, $emailsubject, $emailbody);
    }else{

        sendmail_server($toname, $toemail, $emailsubject, $emailbody);
    }

}

//SEND EMAIL FROM LOCAL HOST
function sendmail_local($toname, $toemail, $emailsubject, $emailbody){
    global $email, $site;
    $body="$emailbody\n\n$site[longname]\n$site[homeurl]\nTo opt out of future mailings, please reply to this email and let us know.\n";
    $fromname="$site[shortname]";
    $fromemail="$email[sendaccount]";
    $headers .= "From: $fromname<$fromemail>\n";
    $headers .= "Reply-To: <$fromemail>\n";
    $headers .= "MIME-Version: 1.0\n";
    $headers .= "Content-Type: text/plain; charset=\"iso-8859-1\"; Content-Transfer-Encoding: quoted-printable\n";
    $headers .= "X-Sender: $fromname<$fromemail>\n";
    $headers .= "X-Mailer: PHP4\n";
    $headers .= "X-Priority: 3\n";
    $headers .= "Return-Path: <$fromemail>\n";
    $headers .= "Message-ID: <".md5("$fromemail")."@$HTTP_HOST>\n";
    // Dont forget this bit above!
    $body=trim($body);
    $subject=trim($emailsubject);
    mail($toemail, $subject, $body, $headers);
}

//SEND EMAIL FROM ANOTHER SERVER
function sendmail_server($toname, $toemail, $emailsubject, $emailbody){
    global $email, $site;
    $subject="$site[shortname] $emailsubject";
    $body="$emailbody\n\n$site[longname]\n$site[homeurl]\n";
    $fromname="$site[shortname]";
    $fromemail="$email[sendaccount]";
    //Get User and Server to send email from
    $emailinfo=split("@", $fromemail);
    if ($smtp_sock = fsockopen("$emailinfo[1]", 25)) {
        fputs($smtp_sock, "HELO $emailinfo[1]\n");
        fputs($smtp_sock, "VRFY $emailinfo[0]\n");
        fputs($smtp_sock, "MAIL FROM:<$fromemail>\n");
        fputs($smtp_sock, "RCPT TO:<$toemail>\n");
        fputs($smtp_sock, "DATA\n");
        fputs($smtp_sock, "From: $fromname($fromemail)\n");
        fputs($smtp_sock, "X-Mailer: miplus\n");
        fputs($smtp_sock, "Content-Type: text/plain;");
        fputs($smtp_sock, "charset=iso-8859-1\r\n");
        fputs($smtp_sock, "MIME-Version: 1.0\n");
        fputs($smtp_sock, "Subject: $subject\n");
        fputs($smtp_sock, "To: $toemail\n");
        fputs($smtp_sock, "$body");
        fputs($smtp_sock, "\n.\nQUIT\n");
        fclose($smtp_sock);
    }

}

?>
