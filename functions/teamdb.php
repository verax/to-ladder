<?
function team_database($page,$orderby){
    global $dir, $file, $url, $out, $misc, $site, $admn;
    $result=mysql_query("select COUNT(*) from teams WHERE type='3'");
    $result=mysql_fetch_array($result);
    $totalusers="$result[0]";
    $maxresults=$misc[teamdb];
    $stop="$maxresults";
    if(!$page){
        $start="1";
    }else{

        $start="$page";
    }

    if($start > $totalusers){
        $start=0;
    }

    $page="$start";
    $start=($start-1);
    $altcolora="#000033";
    $altcolorb="#000020";
    $altcolora="' class='altcolora";
    $altcolorb="' class='altcolorb";
    if($orderby == "id_up"){
        $order = "id";
    }else if ($orderby == "id_down"){

        $order = "id desc";
    }else if ($orderby == "joindate_down"){

        $order = "joindate desc";
    }else if ($orderby == "joindate_up"){

        $order = "joindate";
    }else if ($orderby == "teamname_down"){

        $order = "teamname desc";
    }else if ($orderby == "teamname_up"){

        $order = "teamname";
    }else if ($orderby == "email_down"){

        $order = "teamemail desc";
    }else if ($orderby == "email_up"){

        $order = "teamemail";
    }else if ($orderby == "tag_down"){

        $order = "tag desc";
    }else if ($orderby == "tag_up"){

        $order = "tag";
    }else if ($orderby == "type_up"){

        $order = "type";
    }else if ($orderby == "type_down"){

        $order = "type desc";
    }else{

        $order = "teamname";
    }

    $result=mysql_query("SELECT id,idcreate,teamname,teamemail,type,tag,website,DATE_FORMAT(joindate, '%M %d'),ipcreate from teams WHERE type='3'ORDER BY $order LIMIT $start,$stop");
    while(list($tid,$idcreate,$teamname,$teamemail,$ttype,$tag,$website,$joindate,$ip)=mysql_fetch_row($result)) {
        if($altladrow=="$altcolora"){
            $altladrow="$altcolorb";
        }else{

            $altladrow="$altcolora";
        }

        if($ttype == '1'){
            $teamtype = "Singles";
        }else if($ttype == '3'){

            $teamtype = "Team";
        }else{

            $teamtype = "";
        }

        $flag = mysql_fetch_array(mysql_query("SELECT country FROM users WHERE id='$idcreate'"));
        if($flag[0] == ""){
            $country = "";
        }else{

            $country="<img width='20' height='14' src='$url[themes]/images/flags/$flag[0]_small.gif'>";
        }

        if($website){
            $website="<a href='$website' target='top'><img src='$url[themes]/images/home.gif' border='0'></a>";
        }else{

            $website="";
        }

        if($ip){
            $ip="<img src='$url[themes]/images/ip.gif' border='0' alt='$ip' style='cursor:hand;'>";
        }else{

            $ip="?";
        }

        $playerlist=$playerlist."
        <tr bgcolor='$altladrow'>
        <td width='' valign='center' align='left'><a href='$url[base]/$file[teams]?teamid=$tid'>$teamname</a></td>
        <td width='' valign='center' align='center'>$tid</td>
        <td width='' valign='center' align='center'>$tag</td>
        <td width='' valign='center' align='center'>$joindate</td>
        <td width='' valign='center' align='center'><a href='mailto:$teamemail'><img src='$url[themes]/images/email.gif' border='0'></a></td>
        <td width='' valign='center' align='center'>$website</td>";

        if($misc[flags] == "yes"){
            $playerlist=$playerlist."<td width='' valign='center' align='center'>$country</td>";
        }

        if($admn[id]){
            $playerlist=$playerlist."<td width='' valign='center' align='center'>$ip</td>";
        }

        $playerlist=$playerlist."</tr>";
        $counter++;
    }

    if($order == "teamname desc"){
        $teamname = "teamname_up";
    }else{

        $teamname = "teamname_down";
    }

    if($order == "id desc"){
        $id = "id_up";
    }else{

        $id = "id_down";
    }

    if($order == "tag desc"){
        $tag = "tag_up";
    }else{

        $tag = "tag_down";
    }

    if($order == "joindate desc"){
        $joindate = "joindate_up";
    }else{

        $joindate = "joindate_down";
    }

    if($order == "teamemail desc"){
        $email = "email_up";
    }else{

        $email = "email_down";
    }

    if($order == "type desc"){
        $type = "type_up";
    }else{

        $type = "type_down";
    }

    if($order == "website desc"){
        $website = "website_up";
    }else{

        $website = "website_down";
    }
	
    $tablehead=table_head("show","100%","","left");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    <br>
    <center>
    $tablehead
    &nbsp; &nbsp;<strong><font class='catfont'>$site[shortname] Team Database</font></strong><br>
    <hr class='catfont' size='1'>
    <table width='100%' border='0' bordercolor='#000000' cellspacing='1' cellpadding='1'>
    <tr class='altcolor'>
    <td width='' valign='center' align='left'><strong><a href='$url[base]/$file[stats]?action=teamdb&orderby=$teamname'> Team Name </a></strong></td>
    <td width='' valign='center' align='center'><strong><a href='$url[base]/$file[stats]?action=teamdb&orderby=$id'>Team ID </a></strong></td>
    <td width='' valign='center' align='center'><strong><a href='$url[base]/$file[stats]?action=teamdb&orderby=$tag'>Tag </a></strong></td>
    <td width='' valign='center' align='center'><strong><a href='$url[base]/$file[stats]?action=teamdb&orderby=$joindate'>Joindate </a></strong></td>
    <td width='' valign='center' align='center'><strong><a href='$url[base]/$file[stats]?action=teamdb&orderby=$email'>Email </a></strong></td>
    <td width='' valign='center' align='center'><strong><a href='$url[base]/$file[stats]?action=teamdb&orderby=$website'>Website </a></strong></td>";
    if($misc[flags] == "yes"){
        $out[body]=$out[body]."<td width='' valign='center' align='center'><strong>Country</strong></td>";
    }

    if($admn[id]){
        $out[body]=$out[body]."<td width='' valign='center' align='center'><strong>Ip</strong></td>";
    }
    
    $out[body]=$out[body]."</tr>
    $playerlist
    ";
    $pagenumb=1;
    if ($totalusers > $maxresults) {
        $totalpages=($totalusers / $maxresults);
        while($pagenum < $totalpages){
            $pagenum++;
            if($pagenumb=="$page"){
                $pages=$pages."[<a href='$url[base]/$file[stats]?action=teamdb&orderby=$orderby&page=$pagenumb'>$pagenum</a>] ";
            }else{

                $pages=$pages."<a href='$url[base]/$file[stats]?action=teamdb&orderby=$orderby&page=$pagenumb'>$pagenum</a> ";
            }

            $pagenumb=($maxresults + $pagenumb);
        }

    }

    if($pages){
        $out[body]=$out[body]."
        <tr class='altcolor'>
        <td width='100%' valign='center' align='center' colspan='8'><strong>$pages</strong></td>
        </tr>";
    }

    $out[body]=$out[body]."
    </table>
    $tablefoot";
    $tablehead=table_head("show","400","","left");
    $tablefoot=table_foot("show");
    $out[body]=$out[body]."
    <form method='post' action='$url[base]/$file[search]'>
    $tablehead
    &nbsp; &nbsp;<strong><font class='catfont'>Search $site[longname]</font></strong><br>
    <hr class='catfont' size='1'>
    <center>
    <input type='text' name='find' value='' size='30' maxlength='50'>
    <select name='type'>
    <option value='1'>Teams</option>
    <option value='2'>Players</option>
    </select>
    <input type='hidden' name='action' value='search'>
    <input type='submit' name='' value='Search'>
    </center>
    $tablefoot
    </form>
    </center>";
    include("$dir[curtheme]");
}

?>
