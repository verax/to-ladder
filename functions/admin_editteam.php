<?
if(!IsSet($indexloaded)){
    header("Location: ./index.php");
}

//CHECK ADMIN ACCESS LEVEL
if($admn[access] < 15){
    include("$dir[func]/error.php");
    display_error("You are not allowed to perform this function.<br>");
}

function admin_editateam(){
    global $dir, $url, $out, $site, $admn;
    //GET LADDERS
    $ladderslist=mysql_query("SELECT id,laddername  FROM ladders ORDER by laddername");
    while(list($id,$name)=mysql_fetch_row($ladderslist)){
        $theladders=$theladders."<option value='$id'>$name</option>";
        $foundladders=1;
    }

    if(!$foundladders){
        include("$dir[func]/error.php");
        display_error("There are no ladders.<br>");
    }

    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <form method='post'>
    <td width='100%' valign='top' align='center'>
    <strong>Edit a Team</strong><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <br>
    Select a ladder<br>
    <select name='ladderid'>$theladders</select><br>
    <br></td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='action' value='editateamb'>
    <input type='submit' name='' value='Find Team'>
    </td>
    </form>
    </tr>
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function admin_editateamb($ladderid){
    global $dir, $url, $out, $site, $admn;
    if(!$ladderid){
        include("$dir[func]/error.php");
        display_error("Invalid Ladder ID.<br>");
    }

    if($admn[access] < 99){
        if (mysql_num_rows(mysql_query("SELECT staffid FROM staffaccess WHERE staffid='$admn[id]' AND ladderid='$ladderid'")) < 1){
            include("$dir[func]/error.php");
            display_error("You are not allowed to manage teams on this laddder.<br>");
        }

    }

    //GET TEAMS
    $teamslist=mysql_query("SELECT id FROM ladder_$ladderid");
    while(list($id)=mysql_fetch_row($teamslist)){
        $name=mysql_fetch_array(mysql_query("SELECT teamname FROM teams WHERE id='$id' ORDER BY teamname"));
        $theteams=$theteams."<option value='$id'>$name[teamname]</option>";
        $foundladders=1;
    }

    if(!$foundladders){
        include("$dir[func]/error.php");
        display_error("There are no teams on this ladder.<br>");
    }

    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <form method='post'>
    <td width='100%' valign='top' align='center'>
    <strong>Edit a Team</strong><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <br>
    <select name='teamid'>$theteams</select><br>
    <br></td>
    </tr>
    <script language='javascript'>var confirmdelete='Are you sure you want to DELETE this team';</script>
    <script language='javascript'>var confirmleave='Are you sure you want to REMOVE this team from this ladder';</script>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='action' value='editateamc'>
    <input type='hidden' name='ladderid' value='$ladderid'>
    <input type='submit' name='todo' value='Edit Team'>
    <input type='submit' name='todo' value='Manage Members'> ";
    if($admn[access] >= 20){
        $out[body]=$out[body]."
        <input type='submit' name='todo' value='Delete Team' onClick='return confirm(confirmdelete);'>
        <input type='submit' name='todo' value='Remove From Ladder' onClick='return confirm(confirmleave);'>";
    }

    $out[body]=$out[body]."</td>
    </form>
    </tr>
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function admin_editateamc($ladderid,$teamid,$todo){
    global $dir, $url, $out, $site, $admn;
    if(!$ladderid){
        include("$dir[func]/error.php");
        display_error("Invalid Ladder ID.<br>");
    }

    if(!$teamid){
        include("$dir[func]/error.php");
        display_error("Invalid Team ID.<br>");
    }

    if($todo=="Edit Team"){
        edit_team($ladderid,$teamid);
    }

    else if($todo=="Manage Members"){
        manage_teammembers($ladderid,$teamid);
    }

    else if($todo=="Delete Team"){
        delete_team($teamid);
    }

    else if($todo=="Remove From Ladder"){
        leave_ladder($ladderid,$teamid);
        include("$dir[func]/admin_finishmessage.php");
        display_message("The team was removed from the ladder.<br>");
    }

    else{
        include("$dir[func]/error.php");
        display_error("Unknwon Command.<br>");
    }

}

function edit_team($ladderid,$teamid){
    global $dir, $url, $out, $site, $admn;
    $teaminfo=mysql_query("SELECT * FROM teams WHERE id='$teamid'");
    $tinfo=mysql_fetch_array($teaminfo);
    if(!$tinfo[teamname]){
        include("$dir[func]/error.php");
        display_error("Unknown Team.<br>");
    }

    $tladderinfo=mysql_query("SELECT * FROM ladder_$ladderid WHERE id='$teamid'");
    $tlinfo=mysql_fetch_array($tladderinfo);
    if($tlinfo[id]!="$teamid"){
        include("$dir[func]/error.php");
        display_error("Team is missing from the ladder.<br>Delete the team then restore it.<br>");
    }

    //TABLE ATERNATING COLORS
    $altcolora="#000033";
    $altcolorb="#000020";
    if($tinfo[tagplace]=="0"){
        $checkeda="CHECKED";
        $checkedb="";
    }else{

        $checkeda="";
        $checkedb="CHECKED";
    }

    //CHANGE SPECIAL CHARECTERS BACK
    include("$dir[func]/checkdata.php");
    $tinfo[teamname]=unchange_charecters($tinfo[teamname]);
    $tinfo[tag]=unchange_charecters($tinfo[tag]);
    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <font class='catfont'><strong>Edit a team</strong></font></td>
    </tr>
    <tr>
    <td width='100%' valign='top' align='left'>";
    $iconurl="$url[themes]/icons/";
    $out[body]=$out[body]."
    <SCRIPT type='text/javascript'>
    <!--
    function changeicon() {
        if (!document.images)
        return
        document.images.iconspot.src='$iconurl' + document.teamedit.t_icon.options[document.teamedit.t_icon.selectedIndex].value
    }

    //-->
    </SCRIPT>
    <form method='post' name='teamedit'>
    <table width='100%' border='0' cellspacing='0' cellpadding='0' valign='top' align='center'>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>* Team Name</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='t_alias' value='$tinfo[teamname]' size='30' maxlength='25'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>* Team Email</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='t_email' value='$tinfo[teamemail]' size='30' maxlength='50'></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Tag</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='t_tag' value='$tinfo[tag]' size='30' maxlength='10'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Tag Placement</font></td>
    <td width='50%' valign='center' align='center'>
    Before Name: <input type='radio' name='t_tagplace' value='0' $checkeda>
    After: <input type='radio' name='t_tagplace' value='1' $checkedb></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Team Website</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='t_url' value='$tinfo[website]' size='30' maxlength='100'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Team Logo</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='t_logo' value='$tinfo[logo]' size='30' maxlength='100'></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Contact Method (Irc Channel@Server or Icq)</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='t_contact' value='$tinfo[contact]' size='30' maxlength='50'></td>
    </tr>
    </table>
    <table width='100%' border='0' cellspacing='0' cellpadding='2' valign='top' align='center'>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center' colspan='2' >
    <font class='catfont'><strong>Ladder #$ladderid Stats</strong></font></td>
    </tr>
    <table width='100%' border='0' cellspacing='0' cellpadding='0' valign='top' align='center'>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Current Rank</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='t_rank' value='$tlinfo[rank]' size='30' maxlength='5'></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Last Rank</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='t_lrank' value='$tlinfo[lastrank]' size='30' maxlength='5'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Best Rank</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='t_brank' value='$tlinfo[bestrank]' size='30' maxlength='5'></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Worst Rank</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='t_wrank' value='$tlinfo[worstrank]' size='30' maxlength='5'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Wins</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='t_wins' value='$tlinfo[wins]' size='30' maxlength='5'></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Losses</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='t_loss' value='$tlinfo[losses]' size='30' maxlength='5'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Streak</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='t_streak' value='$tlinfo[streak]' size='30' maxlength='5'></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Best Streak</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='t_bstreak' value='$tlinfo[beststreak]' size='30' maxlength='5'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Worst Streak</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='t_wstreak' value='$tlinfo[worststreak]' size='30' maxlength='5'></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Icon</font></td>
    <td width='50%' valign='center' align='center'>
    <select name='t_icon' onChange='changeicon()'>
    <option value='blank.gif'>None</option>";
    $iconnum="$tlinfo[icon]";
    $selected[$iconnum]="SELECTED";
    $iconsdir=opendir("$dir[themes]/icons/");
    while (($file = readdir($iconsdir))!==false) {
        $splitfile=split("\.", $file);
        if(($splitfile[1]=="gif") && ($splitfile[0]!="blank")){
            $thisiconnum="$splitfile[0]";
            $out[body]=$out[body]."
            <option value='$file' $selected[$thisiconnum]>$splitfile[0]</option>";
        }

    }

    closedir($iconsdir);
    if($iconnum){
        $thisgif="$iconnum";
    }else{

        $thisgif="blank";
    }

    $out[body]=$out[body]."
    </select>
    <img src='$url[themes]/icons/$thisgif.gif' name='iconspot'>
    </td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Lock</font></td>
    <td width='50%' valign='center' align='center'>
    <img src='$url[themes]/images/lock.gif'>
    <select name='t_lock'>";
    $tday=date("Y-m-d H:i:s");
    if(($tlinfo[status]=="10") && ($tlinfo[statusdate] > $tday)){
        $out[body]=$out[body]."<option value='0'>Locked</option>";
    }else{

        $out[body]=$out[body]."<option value='0'>None</option>";
    }

    $out[body]=$out[body]."
    <option value='1'>Unlock</option>
    <option value='24'>24 hours</option>
    <option value='48'>2 days</option>
    <option value='72'>3 days</option>
    <option value='96'>4 days</option>
    <option value='120'>5 days</option>
    <option value='144'>6 days</option>
    <option value='168'>1 Week</option>
    <option value='336'>2 Weeks</option>
    <option value='50000'>Indefinite</option>
    </select></td>
    </tr>
    </table>
    </td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='action' value='editateamd'>
    <input type='hidden' name='teamid' value='$teamid'>
    <input type='hidden' name='ladderid' value='$ladderid'>
    <input type='submit' name='' value='Update Team'>
    <input type='reset' name='' value='Reset'></td>
    </form>
    </tr>
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function admin_editateamd($teamid,$ladderid,$t_alias,$t_email,$t_tag,$t_tagplace,$t_url,$t_logo,$t_contact,$t_rank,$t_lrank,$t_brank,$t_wrank,$t_wins,$t_loss,$t_streak,$t_bstreak,$t_wstreak,$t_points,$t_skill,$t_money,$t_forfeits,$t_icon,$t_lock){
    global $dir, $url, $out, $site, $admn;
    if(!$teamid){
        include("$dir[func]/error.php");
        display_error("Invalid Team ID.<br>");
    }

    if(!$ladderid){
        include("$dir[func]/error.php");
        display_error("Invalid Ladder name.<br>");
    }

    if(!$t_alias){
        include("$dir[func]/error.php");
        display_error("Invalid Team name.<br>");
    }

    if(!$t_email){
        include("$dir[func]/error.php");
        display_error("Invalid Team email.<br>");
    }

    include("$dir[func]/checkdata.php");
    $t_alias=change_charecters($t_alias);
    $t_email=change_charecters($t_email);
    $t_tag=change_charecters($t_tag);
    $t_tagplace=change_numbersonly($t_tagplace);
    $t_url=change_url($t_url);
    $t_logo=change_url($t_logo);
    $t_url=change_charecters($t_url);
    $t_logo=change_charecters($t_logo);
    $t_contact=change_charecters($t_contact);
    $t_icon=change_numbersonly($t_icon);
    $errormessage=check_validlength($errormessage, $t_alias, "3", "50", "Your team name be 3-25 Characters");
    $errormessage=check_validlength($errormessage, $t_tag, "0", "25", "Your Tag cannot Exceed 10 Characters");
    $errormessage=check_validlength($errormessage, $t_email, "5", "100", "Your must include a valid team email.<br>Team Email cannot exceed 50 characters");
    error_check($errormessage);
    $errormessage=check_emailaddress($t_email);
    error_check($errormessage);
    $errormessage=check_ban($errormessage, $t_alias, $email, $ip);
    error_check($errormessage);
    mysql_query("UPDATE teams SET
    teamname='$t_alias',
    teamemail='$t_email',
    tag='$t_tag',
    tagplace='$t_tagplace',
    website='$t_url',
    logo='$t_logo',
    contact='$t_contact'
    WHERE id='$teamid'");
    if($t_lock=="1"){
        mysql_query("UPDATE ladder_$ladderid SET status='0' WHERE id='$teamid'");
    }

    else if($t_lock >= 24){
        $lcokeduntill=date("Y-m-d H:i:s",time()+60*60*$t_lock);
        mysql_query("UPDATE ladder_$ladderid SET status='10', statusdate='$lcokeduntill' WHERE id='$teamid'");
    }

    if($t_wins > 0){
        $games=($t_wins + $t_loss);
        $percent=round($t_wins / $games * 100);
    }else{

        $games="$t_loss";
        $percent="0";
    }

    mysql_query("UPDATE ladder_$ladderid SET
    lastrank='$t_lrank',
    bestrank='$t_brank',
    worstrank='$t_wrank',
    wins='$t_wins',
    losses='$t_loss',
    games='$games',
    percent='$percent',
    streak='$t_streak',
    beststreak='$t_bstreak',
    worststreak='$t_wstreak',
    points='$t_points',
    skill='$t_skill',
    money='$t_money',
    forfeits='$t_forfeits',
    icon='$t_icon'
    WHERE id='$teamid'");
    include("$dir[func]/rankadjust.php");
    update_teamranks($ladderid,$teamid,$t_rank);
    $ranksadjusted=rank_checkadjust($ladderid);
    include("$dir[func]/admin_finishmessage.php");
    display_message("The team $t_alias was updated.<br>");
}

function manage_teammembers($ladderid,$teamid){
    global $dir, $url, $out, $site, $admn;
    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <strong>Manage Team Members</strong><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <br>
    <table width='100%' border='0'  cellspacing='0' cellpadding='0'>
    <tr>
    <td width='40%' valign='top' align='left'>
    <strong>Player Name</strong></td>
    <td width='20%' valign='top' align='left'>
    <strong>Joined Team</strong></td>
    <td width='40%' valign='top' align='center'>
    <strong>Member Rank</strong></td>
    </tr>
    <form method='post'>";
    //TABLE ATERNATING COLORS
    $altcolora="#000033";
    $altcolorb="#000020";
    $listmembers=mysql_query("SELECT playerid,status,DATE_FORMAT(joindate, '%M %d, %Y') FROM teammembers WHERE teamid='$teamid' ORDER by joindate");
    while(list($playerid,$status,$joindate)=mysql_fetch_row($listmembers)){
        $memberinfo=mysql_query("SELECT alias FROM users WHERE id='$playerid'");
        $playerinfo=mysql_fetch_array($memberinfo);
        if($altladrow=="$altcolora"){
            $altladrow="$altcolorb";
        }else{

            $altladrow="$altcolora";
        }

        $selected[$status]="SELECTED";
        $out[body]=$out[body]."
        <tr bgcolor='$altladrow'>
        <td width='40%' valign='top' align='left'>
        $playerinfo[alias]</td>
        <td width='20%' valign='top' align='left'>
        $joindate</td>
        <td width='40%' valign='top' align='center'>
        <select name='memberrank[$playerid]'>
        <option value='1' $selected[1]>Leader</option>
        <option value='2' $selected[2]>Co-Leader</option>
        <option value='3' $selected[3]>Captain</option>
        <option value='4' $selected[4]>Co-Captain</option>
        <option value='5' $selected[5]>Member</option>
        <option value='6' $selected[6]>Training</option>
        <option value='7' $selected[7]>Inactive</option>
        <option value='8' $selected[8]>Suspended</option>
        <option value='9' $selected[9]>Kick</option>
        </select></td>
        </tr>";
        $selected[$status]="";
    }

    $out[body]=$out[body]."
    </table>
    <br></td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='action' value='editateame'>
    <input type='hidden' name='teamid' value='$teamid'>
    <input type='hidden' name='ladderid' value='$ladderid'>
    <input type='submit' name='' value='Update'>
    <input type='reset' name='todo' value='Reset'>
    </td>
    </form>
    </tr>
    <tr>
    <form method='post'>
    <td width='100%' valign='center' align='center'>
    <br>
    Place member on this team<br>
    Player ID: <input type='text' name='playerid' value=''>
    <br><br>
    </td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='action' value='editateamf'>
    <input type='hidden' name='teamid' value='$teamid'>
    <input type='hidden' name='ladderid' value='$ladderid'>
    <input type='submit' name='' value='Add Member'>
    </td>
    </form>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <table width='100%' border='0'  cellspacing='0' cellpadding='0'>
    <tr>
    <td width='100%' height='100%' valign='top' align='left' colspan='3'>
    <ul>
    <li>Leader: Can do everything.<br>
    <li>Co-Leader: Invite, Kick and Manage Members, Report Losses<br>
    <li>Captain: Make and Accept Challenges, Report Losses<br>
    <li>Co-Captain: Report Losses<br>
    <li>Member: Regular Member<br>
    <li>Training: Cannot play in matches.<br>
    <li>Inactive: Cannot play in matches.<br>
    <li>Suspended: Cannot play in matches.<br>
    <li>Retired: Retired from Play. Cannot play in matches.
    </td></ul>
    </tr>
    </table>
    </td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <strong>Member Rights by Rank</strong></td>
    </tr>
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function admin_editateame($teamid,$ladderid,$memberrank){
    global $dir, $file, $url, $out, $admn;
    if(!$teamid){
        include("$dir[func]/error.php");
        display_error("Invalid Team ID.<br>");
    }

    if(!$ladderid){
        include("$dir[func]/error.php");
        display_error("Invalid Ladder ID.<br>");
    }

    if(!$memberrank){
        include("$dir[func]/error.php");
        display_error("Invalid Member ID's.<br>");
    }

    $leadcheckranks=$memberrank;
    while(list($pplayerid,$pmrank)=each($leadcheckranks)){
        if($pmrank=="1"){
            $leaderfound=1;
        }

    }

    if(!$leaderfound){
        include("$dir[func]/error.php");
        display_error("There must be a team leader.<br>");
    }

    while(list($playerid,$mrank)=each($memberrank)){
        $ememinfo=mysql_query("SELECT status FROM teammembers WHERE teamid='$teamid' AND playerid='$playerid'");
        $eminfo=mysql_fetch_array($ememinfo);
        if($mrank=="9"){
            mysql_query("DELETE FROM teammembers WHERE teamid='$teamid' AND playerid='$playerid'");
        }

        else if($eminfo[status]!=$mrank){
            mysql_query("UPDATE teammembers SET status='$mrank' WHERE teamid='$teamid' AND playerid='$playerid'");
        }

    }

    include("$dir[func]/admin_finishmessage.php");
    display_message("The team members were updated.<br>");
}

function admin_editateamf($teamid,$ladderid,$playerid){
    global $dir, $file, $url, $out, $admn;
    if(!$teamid){
        include("$dir[func]/error.php");
        display_error("Invalid Team ID.<br>");
    }

    if(!$ladderid){
        include("$dir[func]/error.php");
        display_error("Invalid Ladder ID.<br>");
    }

    if(!$playerid){
        include("$dir[func]/error.php");
        display_error("Invalid Player ID.<br>");
    }

    //CHECK IF PLAYER EXISTS
    if (mysql_num_rows(mysql_query("SELECT id FROM users WHERE id='$playerid'")) < 1){
        include("$dir[func]/error.php");
        display_error("Unknown Player ID.<br>");
    }

    $ladinfo=mysql_query("SELECT laddername,maxmembers FROM ladders WHERE id='$ladderid'");
    $linfo=mysql_fetch_array($ladinfo);
    if(!$linfo[laddername]){
        include("$dir[func]/error.php");
        display_error("Unknown Ladder.<br>");
    }

    //MAKE SURE PLAYER IS NOT ALREADY ON LADDER
    $pteams=mysql_query("SELECT teamid FROM teammembers WHERE playerid='$playerid'");
    while($row = mysql_fetch_array($pteams)){
        $check=mysql_query("SELECT id FROM ladder_$ladderid WHERE id='$row[teamid]'");
        $checkb=mysql_fetch_array($check);
        if($checkb){
            include("$dir[func]/error.php");
            display_error("This player is already on this ladder.<br>");
        }

    }

    //CHECK IF MAX MEMBERS EXISTS
    $ladders=mysql_query("SELECT id,laddername,maxmembers FROM ladders");
    while($row = mysql_fetch_array($ladders)){
        $teams=mysql_query("SELECT id FROM ladder_$row[id] WHERE id='$teamid'");
        if(mysql_fetch_array($teams)){
            if (mysql_num_rows(mysql_query("SELECT playerid FROM teammembers WHERE teamid='$teamid'")) >= $row[maxmembers]){
                include("$dir[func]/error.php");
                display_error("This team has the maximum allowed team members<br> on the $row[laddername] ladder. <br><br> No new members can be added to this team.");
            }

        }

    }

    $tday=date("Y-m-d H:i:s");
    mysql_query("INSERT INTO teammembers VALUES (
    '$teamid',
    '$playerid',
    '5',
    '$tday');");
    include("$dir[func]/admin_finishmessage.php");
    display_message("The member was added to the team.<br>");
}

function delete_team($teamid){
    global $dir;
    mysql_query("DELETE FROM teammembers WHERE teamid='$teamid'");
    mysql_query("DELETE FROM players2 WHERE pid='$teamid'");
    mysql_query("DELETE FROM brackets2 WHERE pid='$teamid'");
    mysql_query("DELETE FROM teams WHERE id='$teamid'");
    mysql_query("DELETE FROM challenges WHERE challenger='$teamid'");
    mysql_query("DELETE FROM challenges WHERE challenged='$teamid'");
    mysql_query("DELETE FROM teammembersinv WHERE teamid='$teamid'");
    $ladders = mysql_query("SELECT id FROM ladders");
    while($row = mysql_fetch_array($ladders)){
        $ladder = mysql_query("SELECT id FROM ladder_$row[id] WHERE id ='$teamid'");
        if(mysql_fetch_array($ladder)){
            mysql_query("DELETE FROM ladder_$row[id] WHERE id='$teamid'");
            $getrank=mysql_query("SELECT rank FROM ladder_$row[id] WHERE id='$teamid'");
            $getrank2=mysql_fetch_array($getrank);
            include("$dir[func]/rankadjust.php");
            update_teamranks($row[id],$teamid,$getrank2[rank]);
            $ranksadjusted=rank_checkadjust($row[id]);
        }
   } 
   include("$dir[func]/admin_finishmessage.php");
   display_message("The team has been deleted.<br>");

}

function leave_ladder($ladderid,$teamid){
    global $dir, $url, $out, $plyr, $site, $uinfo, $misc;
    if(!$ladderid){
        include("$dir[func]/error.php");
        display_error("Invalid Ladder ID.<br>");
    }

    if(!$teamid){
        include("$dir[func]/error.php");
        display_error("Invalid Team ID.<br>");
    }

    $count = 0;	
    $ladders = mysql_query("SELECT id FROM ladders");
    while($row = mysql_fetch_array($ladders)){
        $ladder = mysql_query("SELECT id FROM ladder_$row[id] WHERE id ='$teamid'");
        if(mysql_fetch_array($ladder)){
	    $count++;
        }

    }

    if($count < 2){
       include("$dir[func]/error.php");
       display_error("This team is not on any other ladders.<br>You can delete a team if you wish to remove it completely.<br>");
    }


    $getrank=mysql_query("SELECT rank FROM ladder_$ladderid WHERE id='$teamid'");
    $getrank2=mysql_fetch_array($getrank);
    mysql_query("DELETE FROM ladder_$ladderid WHERE id='$teamid'");
    include("$dir[func]/rankadjust.php");
    update_teamranks($ladderid,$teamid,$getrank2[rank]);
    $ranksadjusted=rank_checkadjust($ladderid);
    include("$dir[func]/admin_finishmessage.php");
    display_message("The team was removed from the ladder.<br>");
}

?>
