<?
function lost_password(){
    global $url, $dir, $out;
    $ip=$ip=getenv("REMOTE_ADDR");
    $tablehead=table_head("show","70%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <center>
    <br>
    $tablehead
    <center><strong><font class='catfont'>Recupera tu contrase&nacute;a</font></strong>
    <br><br>
    <form>
    Enter Player ID: <input type='text' name='playerid' value='' maxlength='5'><br><br><br>
    <inpit type='hidden' name='ip' value='$ip'>
    <input type='hidden' name='action' value='lostpassb'>
    <button type='submit' name='' value='Send Password' class='btn btn-mini btn-success'>Recuperar </button>
    $tablefoot";
    include("$dir[curtheme]");
}

function lost_passwordb($playerid,$ip){
    global $url, $dir, $out, $misc;
    if(!$playerid){
        include("$dir[func]/error.php");
        display_error("Player ID es inv&aacute;lido.<br>");
    }

    //Slightly long-winded method for checking if this ip address made a request already today.
    $day=date("z");
    $year=date("Y");
    $check = mysql_fetch_array(mysql_query("SELECT ipaddress,DATE_FORMAT(requestdate, '%j'),DATE_FORMAT(requestdate, '%Y') FROM passrequests WHERE ipaddress='$ip'"));
    if((($check[1]-1) == $day)&&($check[2] == $year)){
        include("$dir[func]/error.php");
        display_error("You cannot make a lost password request more than once a day.<br>");
    }

    else if($misc[passrequests] == 'no'){
        include("$dir[func]/error.php");
        display_error("This feature is currently disabled.<br> Please contact a staff member for further help.<br>");
    }else{

        $tday=date("Y-m-d H:i:s");
        mysql_query("INSERT INTO passrequests VALUES (
        '',
        '$tday',
        '$ip',
        '$playerid');");
        $resa=mysql_query("SELECT alias,pass,email FROM users WHERE id='$playerid'");
        $resb=mysql_fetch_array($resa);
        $tablehead=table_head("show","60%","","center");
        $tablefoot=table_foot("show");
        $bannerhead=table_head("show","488","80","center");
        $bannerfoot=table_foot("show");
        $out[body]=$out[body]."
        <center>
        $bannerhead
        $out[banner]
        $bannerfoot
        <br>
        $tablehead
        <center><strong><font class='catfont'>Password Sent </font></strong>
        <br><br>
        The password has been sent to $resb[email].<br> Any further enquiries should be submitted to a staff member.
        $tablefoot";
        include("$dir[curtheme]");
        //Email Invited Player
        $emailbody="$resb[alias],
        You recently requested your password be emailed to you!
        Your Password is: $resb[pass]
        If you did not make this request, simply ignore this email.
        ";
        include("$dir[func]/email.php");
        send_email($resb[alias], $resb[email], "Requested Password", $emailbody);
    }

}

function validate_player($vc){
    global $url, $dir, $out;
    $tablehead=table_head("show","300","","left");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    <br>
    $tablehead
    &nbsp; &nbsp;<strong><font class='catfont'>Valida tu cuenta</font></strong><br>
    <hr class='catfont' size='1'>
    <table width='300' border='0' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <form method='post'>
    <tr>
    <td width='100%' height='100%' valign='top' align='center'>
    Codigo de validacion<br>
    <input type='text' name='code' value='$vc' maxlength='50' size='30'>
    <br><br>
    <input type='hidden' name='action' value='validateb'>
    <button type='submit' class='btn btn-mini btn-info'  name='' value='Validate Me'>Validame</button>
    </td>
    </tr>
    </form>
    </table>
    $tablefoot
    </center>";
    include("$dir[curtheme]");
}

function validate_playerb($code){
    global $url, $dir, $out, $uinfo, $site;
    include("$dir[func]/checkdata.php");
    //$code=change_numbersonly($code);
    $errormessage=check_validlength($errormessage, $code, "1", "50", "Your Validation Code is Invalid");
    error_check($errormessage);
    $validatecode=mysql_query("SELECT * FROM validate WHERE vcode='$code'");
    $vc=mysql_fetch_array($validatecode);
    if(!$vc[alias]){
        include("$dir[func]/error.php");
        display_error("Unable to find that validation code.<br>");
    }

    $tday=date("Y-m-d H:i:s");
    mysql_query("INSERT INTO users VALUES (NULL,
    '$vc[alias]',
    '$vc[pass]',
    '$vc[email]',
    '$vc[icq]',
    '$vc[aim]',
    '$vc[yahoo]',
    '$vc[msn]',
    '$vc[website]',
    '$vc[logo]',
    '$vc[newsletter]',
    '$tday',
    '$vc[ipaddress]',
    '0',
    '0',
    '0',
    '0',
    '0',
    '0',
    '$vc[theme]',
    '$vc[country]');");
    $getid=mysql_query("SELECT id FROM users WHERE alias='$vc[alias]'");
    $newidnfo=mysql_fetch_array($getid);
    $playerid=$newidnfo["id"];
    mysql_query("INSERT INTO usersinfo VALUES (
    '$playerid',
    '$vc[firstname]',
    '$vc[middlename]',
    '$vc[lastname]',
    '$vc[address]',
    '$vc[city]',
    '$vc[state]',
    '$vc[zipcode]',
    '$vc[phone]',
    '$vc[birthday]',
    '$vc[occupation]',
    '$uinfo[force]');");
    clean_validations($code);
    $tablehead=table_head("show","100%","","left");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    <br>
    $tablehead
    &nbsp; &nbsp;<strong><font class='catfont'>Welcome $vc[alias]</font></strong>
    <hr class='catfont' size='1'>
    To get started on $site[shortname] you need to login using your <b>Player ID</b>.<br>
    <br>
    <strong>Your Player ID is <font color='#FF0000'>$playerid</font></strong><br>
    <br>
    <br>
    <center>
    <strong>Have Fun,<br>
    $site[longname]<br>
    $site[shortname] $site[homeurl]</strong>
    </center>
    $tablefoot";
    include("$dir[curtheme]");
}

function clean_validations($code){
    $deletedate=date("Y-m-d H:i:s",time()-60*60*24*1);
    mysql_query("DELETE FROM validate WHERE vcode='$code' OR joindate < '$deletedate'");
}

?>
