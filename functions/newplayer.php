<?
function form_newplayer(){
    global $dir, $url, $site, $uinfo, $misc, $out;
    if($misc[newplayer]!="yes"){
        include("$dir[func]/error.php");
        display_error("Signups are currently disabled.<br>");
    }

    //TABLE ATERNATING COLORS
    $altcolora="#000033";
    $altcolorb="#000020";
    $tablehead=table_head("show","100%","","left");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    <br>
    $tablehead
    &nbsp; &nbsp;<strong><font class='catfont'>$site[longname] New Player Signup</font></strong><br>
    <hr class='catfont' size='1'>
    <table width='100%' border='0' cellspacing='1' cellpadding='2'>
    <form method='post'>
    <tr class='altcolor'>
    <td width='100%' valign='center' align='left' colspan='2'>
    <strong>Player Information (Required)</strong></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Player Name/Alias</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='n_alias' value='' size='30' maxlength='25'></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Player Password</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='password' name='n_pass' value='' size='30' maxlength='10'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Player Password Again</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='password' name='n_passa' value='' size='30' maxlength='10'></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Email</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='n_email' value='' size='30' maxlength='50'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Email Again</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='n_emaila' value='' size='30' maxlength='50'></td>
    ";
    if($misc[flags]=="yes"){
        $out[body]=$out[body]."
        <tr class='altcolora'>
        <td width='50%' valign='center' align='left'>
        <font face='veradna,arial' size='2' color='#FFFFFF'>
        Country</font></td>
        <td width='50%' valign='center' align='center'>
        <select name='n_country'>
        <option value=''>Please select a country
        <option value='AF'>Afghanistan
        <option value='AL'>Albania
        <option value='DZ'>Algeria
        <option value='AD'>Andorra
        <option value='AO'>Angola
        <option value='AG'>Antigua and Barbuda
        <option value='AR'>Argentina
        <option value='AM'>Armenia
        <option value='AU'>Australia
        <option value='AT'>Austria
        <option value='AZ'>Azerbaijan
        <option value='BS'>Bahamas
        <option value='BH'>Bahrain
        <option value='BD'>Bangladesh
        <option value='BB'>Barbados
        <option value='BY'>Belarus
        <option value='BE'>Belgium
        <option value='BZ'>Belize
        <option value='BJ'>Benin
        <option value='BT'>Bhutan
        <option value='BO'>Bolivia
        <option value='BA'>Bosnia Herzegovina
        <option value='BW'>Botswana
        <option value='BR'>Brazil
        <option value='BN'>Brunei
        <option value='BG'>Bulgaria
        <option value='BF'>Burkina Faso
        <option value='BM'>Burma
        <option value='BI'>Burundi
        <option value='KH'>Cambodia
        <option value='CM'>Cameroon
        <option value='CA'>Canada
        <option value='CF'>Central African Republic
        <option value='TD'>Chad
        <option value='CL' selected>Chile
        <option value='CN'>China
        <option value='CX'>Christmas Island
        <option value='CO'>Colombia
        <option value='KM'>Comoros
        <option value='CG'>Congo
        <option value='CR'>Costa Rica
        <option value='HR'>Croatia
        <option value='CU'>Cuba
        <option value='CY'>Cyprus
        <option value='CZ'>Czech Republic
        <option value='DC'>Democratic Rep. Congo
        <option value='DK'>Denmark
        <option value='DJ'>Djibouti
        <option value='DM'>Dominica
        <option value='DO'>Dominican Republic
        <option value='EC'>Ecuador
        <option value='EG'>Egypt
        <option value='SV'>El Salvador
        <option value='GQ'>Equatorial Guinea
        <option value='ER'>Eritrea
        <option value='EE'>Estonia
        <option value='ET'>Ethiopia
        <option value='EU'>European Union
        <option value='FS'>Fed. States Micronesia
        <option value='FJ'>Fiji
        <option value='FI'>Finland
        <option value='FR'>France
        <option value='GA'>Gabon
        <option value='GM'>Gambia
        <option value='GE'>Georgia
        <option value='DE'>Germany
        <option value='GH'>Ghana
        <option value='GR'>Greece
        <option value='GD'>Grenada
        <option value='GT'>Guatemala
        <option value='GN'>Guinea
        <option value='GW'>Guinea-Bissau
        <option value='GY'>Guyana
        <option value='HT'>Haiti
        <option value='HN'>Honduras
        <option value='HK'>Hong Kong
        <option value='HU'>Hungary
        <option value='IS'>Iceland
        <option value='IN'>India
        <option value='ID'>Indonesia
        <option value='IR'>Iran
        <option value='IQ'>Iraq
        <option value='IE'>Ireland
        <option value='IL'>Israel
        <option value='IT'>Italy
        <option value='IV'>Ivory Coast
        <option value='JM'>Jamaica
        <option value='JP'>Japan
        <option value='JO'>Jordan
        <option value='KZ'>Kazakhstan
        <option value='KE'>Kenya
        <option value='KI'>Kiribati
        <option value='KW'>Kuwait
        <option value='KG'>Kyrgyzstan
        <option value='LA'>Laos
        <option value='LV'>Latvia
        <option value='LB'>Lebanon
        <option value='LR'>Liberia
        <option value='LY'>Libya
        <option value='LI'>Liechtenstein
        <option value='LT'>Lithuania
        <option value='LU'>Luxembourg
        <option value='MO'>Macau
        <option value='MK'>Macedonia
        <option value='MG'>Madagascar
        <option value='MW'>Malawi
        <option value='MY'>Malaysia
        <option value='MV'>Maldives
        <option value='ML'>Mali
        <option value='MT'>Malta
        <option value='MR'>Mauritania
        <option value='MU'>Mauritius
        <option value='MX'>Mexico
        <option value='MD'>Moldova
        <option value='MC'>Monaco
        <option value='MN'>Mongolia
        <option value='MA'>Morocco
        <option value='MZ'>Mozambique
        <option value='NA'>Namibia
        <option value='NR'>Nauru
        <option value='NP'>Nepal
        <option value='NL'>Netherlands
        <option value='AN'>Netherlands Antilles
        <option value='NZ'>New Zealand
        <option value='NI'>Nicaragua
        <option value='NE'>Niger
        <option value='NG'>Nigeria
        <option value='NK'>North Korea
        <option value='NO'>Norway
        <option value='OM'>Oman
        <option value='PK'>Pakistan
        <option value='PA'>Panama
        <option value='PG'>Papua New Guinea
        <option value='PY'>Paraguay
        <option value='PE'>Peru
        <option value='PH'>Philippines
        <option value='PL'>Poland
        <option value='PT'>Portugal
        <option value='PR'>Puerto Rico
        <option value='QA'>Qatar
        <option value='RO'>Romania
        <option value='RU'>Russia
        <option value='RW'>Rwanda
        <option value='KN'>Saint Kitts and Nevis
        <option value='LC'>Saint Lucia
        <option value='VC'>Saint Vincent/Grenadines
        <option value='ST'>Sao Tome and Principe
        <option value='SA'>Saudi Arabia
        <option value='SN'>Senegal
        <option value='SC'>Seychelles
        <option value='SL'>Sierra Leone
        <option value='SG'>Singapore
        <option value='SK'>Slovakia
        <option value='SI'>Slovenia
        <option value='SB'>Solomon Islands
        <option value='SO'>Somalia
        <option value='ZA'>South Africa
        <option value='KS'>South Korea
        <option value='ES'>Spain
        <option value='LK'>Sri Lanka
        <option value='SD'>Sudan
        <option value='SR'>Suriname
        <option value='SE'>Sweden
        <option value='CH'>Switzerland
        <option value='SY'>Syria
        <option value='TW'>Taiwan
        <option value='TJ'>Tajikistan
        <option value='TZ'>Tanzania
        <option value='TH'>Thailand
        <option value='TG'>Togo
        <option value='TO'>Tonga
        <option value='TT'>Trinidad and Tobago
        <option value='TN'>Tunisia
        <option value='TR'>Turkey
        <option value='TM'>Turkmenistan
        <option value='TV'>Tuvalu
        <option value='UG'>Uganda
        <option value='UA'>Ukraine
        <option value='AE'>United Arab Emirates
        <option value='GB'>United Kingdom
        <option value='US'>United States Of America
        <option value='UY'>Uruguay
        <option value='UZ'>Uzbekistan
        <option value='VU'>Vanuatu
        <option value='VE'>Venezuela
        <option value='VN'>Viet Nam
        <option value='EH'>Western Samoa
        <option value='YE'>Yemen
        <option value='YU'>Yugoslavia
        <option value='ZM'>Zambia
        <option value='ZW'>Zimbabwe
        </select>
        </td>
        ";
    }

    $out[body]=$out[body]."
    </tr>
    ";
    if($uinfo[allow]=="yes"){
        if($uinfo[force]=="yes"){
            $isoption="Required";
        }else{

            $isoption="Optional";
        }

        $out[body]=$out[body]."
        <tr class='altcolor'>
        <td width='100%' valign='center' align='left' colspan='2'>
        <strong>User Information ($isoption)</strong></td>
        </tr>
        <tr class='altcolorb'>
        <td width='50%' valign='center' align='left'>
        <font face='veradna,arial' size='2' color='#FFFFFF'>
        First Name</font></td>
        <td width='50%' valign='center' align='center'>
        <input type='text' name='n_fname' value='' size='30' maxlength='25'></td>
        </tr>
        <tr class='altcolora'>
        <td width='50%' valign='center' align='left'>
        <font face='veradna,arial' size='2' color='#FFFFFF'>
        Middle Name</font></td>
        <td width='50%' valign='center' align='center'>
        <input type='text' name='n_mname' value='' size='30' maxlength='25'></td>
        </tr>
        <tr class='altcolorb'>
        <td width='50%' valign='center' align='left'>
        <font face='veradna,arial' size='2' color='#FFFFFF'>
        Last Name</font></td>
        <td width='50%' valign='center' align='center'>
        <input type='text' name='n_lname' value='' size='30' maxlength='25'></td>
        </tr>
        <tr class='altcolora'>
        <td width='50%' valign='center' align='left'>
        <font face='veradna,arial' size='2' color='#FFFFFF'>
        Address</font></td>
        <td width='50%' valign='center' align='center'>
        <input type='text' name='n_addrs' value='' size='30' maxlength='50'></td>
        </tr>
        <tr class='altcolorb'>
        <td width='50%' valign='center' align='left'>
        <font face='veradna,arial' size='2' color='#FFFFFF'>
        City</font></td>
        <td width='50%' valign='center' align='center'>
        <input type='text' name='n_city' value='' size='30' maxlength='50'></td>
        </tr>
        <tr class='altcolora'>
        <td width='50%' valign='center' align='left'>
        <font face='veradna,arial' size='2' color='#FFFFFF'>
        State</font></td>
        <td width='50%' valign='center' align='center'>
        <select name='n_state'>
        <option value=''></option>
        <option value='AL'>Alabama</option>
        <option value='AK'>Alaska</option>
        <option value='AZ'>Arizona</option>
        <option value='AR'>Arkansas</option>
        <option value='CA'>California</option>
        <option value='CO'>Colorado</option>
        <option value='CT'>Connecticut</option>
        <option value='DE'>Delaware</option>
        <option value='DC'>District of Columbia</option>
        <option value='FL'>Florida</option>
        <option value='GA'>Georgia</option>
        <option value='HI'>Hawaii</option>
        <option value='ID'>Idaho</option>
        <option value='IL'>Illinois</option>
        <option value='IN'>Indiana</option>
        <option value='IA'>Iowa</option>
        <option value='KS'>Kansas</option>
        <option value='KY'>Kentucky</option>
        <option value='LA'>Louisiana</option>
        <option value='ME'>Maine</option>
        <option value='MD'>Maryland</option>
        <option value='MA'>Massachusetts</option>
        <option value='MI'>Michigan</option>
        <option value='MN'>Minnesota</option>
        <option value='MS'>Mississippi</option>
        <option value='MO'>Missouri</option>
        <option value='MT'>Montana</option>
        <option value='NE'>Nebraska</option>
        <option value='NV'>Nevada</option>
        <option value='NH'>New Hampshire</option>
        <option value='NJ'>New Jersey</option>
        <option value='NM'>New Mexico</option>
        <option value='NY'>New York</option>
        <option value='NC'>North Carolina</option>
        <option value='ND'>North Dakota</option>
        <option value='OH'>Ohio</option>
        <option value='OK'>Oklahoma</option>
        <option value='OR'>Oregon</option>
        <option value='PA'>Pennsylvania</option>
        <option value='RI'>Rhode Island</option>
        <option value='SC'>South Carolina</option>
        <option value='SD'>South Dakota</option>
        <option value='TN'>Tennessee</option>
        <option value='TX'>Texas</option>
        <option value='UT'>Utah</option>
        <option value='VT'>Vermont</option>
        <option value='VA'>Virginia</option>
        <option value='WA'>Washington</option>
        <option value='WV'>West Virginia</option>
        <option value='WI'>Wisconsin</option>
        <option value='WY'>Wyoming</option>
        <option value='IT'>International / Not in United States</option>
        </select></td>
        </tr>
        <tr class='altcolorb'>
        <td width='50%' valign='center' align='left'>
        <font face='veradna,arial' size='2' color='#FFFFFF'>
        Zip Code</font></td>
        <td width='50%' valign='center' align='center'>
        <input type='text' name='n_zip' value='' size='30' maxlength='10'></td>
        </tr>
        <tr class='altcolora'>
        <td width='50%' valign='center' align='left'>
        <font face='veradna,arial' size='2' color='#FFFFFF'>
        Phone Number</font></td>
        <td width='50%' valign='center' align='center'>
        (<input type='text' name='n_phonea' value='' size='4' maxlength='3'>)
        <input type='text' name='n_phoneb' value='' size='20' maxlength='7'></td>
        </tr>
        <tr class='altcolorb'>
        <td width='50%' valign='center' align='left'>
        <font face='veradna,arial' size='2' color='#FFFFFF'>
        Birthdate</font></td>
        <td width='50%' valign='center' align='center'>
        <font face='veradna,arial' size='2' color='#FFFFFF'>
        <select name='n_bdaym'>
        <option></option>
        <option value='01'>Ene</option>
        <option value='02'>Feb</option>
        <option value='03'>Mar</option>
        <option value='04'>Abr</option>
        <option value='05'>May</option>
        <option value='06'>Jun</option>
        <option value='07'>Jul</option>
        <option value='08'>Ago</option>
        <option value='09'>Sep</option>
        <option value='10'>Oct</option>
        <option value='11'>Nov</option>
        <option value='12'>Dic</option>
        </select>
        / <select name='n_bdayd'>
        <option></option>
        <option>01</option>
        <option>02</option>
        <option>03</option>
        <option>04</option>
        <option>05</option>
        <option>06</option>
        <option>07</option>
        <option>08</option>
        <option>09</option>
        <option>10</option>
        <option>11</option>
        <option>12</option>
        <option>13</option>
        <option>14</option>
        <option>15</option>
        <option>16</option>
        <option>17</option>
        <option>18</option>
        <option>19</option>
        <option>20</option>
        <option>21</option>
        <option>22</option>
        <option>23</option>
        <option>24</option>
        <option>25</option>
        <option>26</option>
        <option>27</option>
        <option>28</option>
        <option>29</option>
        <option>30</option>
        <option>31</option>
        </select>
        / <select name='n_bdayy'>
        <option></option>
        <option>1900</option>
        <option>1901</option>
        <option>1902</option>
        <option>1903</option>
        <option>1904</option>
        <option>1905</option>
        <option>1906</option>
        <option>1907</option>
        <option>1908</option>
        <option>1909</option>
        <option>1910</option>
        <option>1911</option>
        <option>1912</option>
        <option>1913</option>
        <option>1914</option>
        <option>1915</option>
        <option>1916</option>
        <option>1917</option>
        <option>1918</option>
        <option>1919</option>
        <option>1920</option>
        <option>1921</option>
        <option>1922</option>
        <option>1923</option>
        <option>1924</option>
        <option>1925</option>
        <option>1926</option>
        <option>1927</option>
        <option>1928</option>
        <option>1929</option>
        <option>1930</option>
        <option>1931</option>
        <option>1932</option>
        <option>1933</option>
        <option>1934</option>
        <option>1935</option>
        <option>1936</option>
        <option>1937</option>
        <option>1938</option>
        <option>1939</option>
        <option>1940</option>
        <option>1940</option>
        <option>1941</option>
        <option>1942</option>
        <option>1943</option>
        <option>1944</option>
        <option>1945</option>
        <option>1946</option>
        <option>1947</option>
        <option>1948</option>
        <option>1949</option>
        <option>1950</option>
        <option>1951</option>
        <option>1952</option>
        <option>1953</option>
        <option>1954</option>
        <option>1955</option>
        <option>1956</option>
        <option>1957</option>
        <option>1958</option>
        <option>1959</option>
        <option>1960</option>
        <option>1961</option>
        <option>1962</option>
        <option>1963</option>
        <option>1964</option>
        <option>1965</option>
        <option>1966</option>
        <option>1967</option>
        <option>1968</option>
        <option>1969</option>
        <option>1970</option>
        <option>1971</option>
        <option>1972</option>
        <option>1973</option>
        <option>1974</option>
        <option>1975</option>
        <option>1976</option>
        <option>1977</option>
        <option>1978</option>
        <option>1979</option>
        <option>1980</option>
        <option>1981</option>
        <option>1982</option>
        <option>1983</option>
        <option>1984</option>
        <option>1985</option>
        <option>1986</option>
        <option>1987</option>
        <option>1988</option>
        <option>1989</option>
        <option>1990</option>
        <option>1991</option>
        <option>1992</option>
        <option>1993</option>
        <option>1994</option>
        <option>1995</option>
        <option>1996</option>
        <option>1997</option>
        <option>1998</option>
        <option>1999</option>
        </select>
        </font></td>
        </tr>
        <tr class='altcolora'>
        <td width='50%' valign='center' align='left'>
        <font face='veradna,arial' size='2' color='#FFFFFF'>
        Occupation</font></td>
        <td width='50%' valign='center' align='center'>
        <select name='n_occup'>
        <option value=''></option>
        <option value='01'>K-12 student</option>
        <option value='02'>College/graduate student</option>
        <option value='03'>Sales/marketing</option>
        <option value='04'>Tradesman/craftsman</option>
        <option value='05'>Executive/managerial</option>
        <option value='06'>Professional (doctor, lawyer, etc.)</option>
        <option value='07'>Academic/educator</option>
        <option value='08'>Computer technical/engineering</option>
        <option value='09'>Other technical/engineering</option>
        <option value='10'>Service/customer support</option>
        <option value='11'>Clerical/administrative</option>
        <option value='12'>Homemaker</option>
        <option value='13'>Self-employed/own company</option>
        <option value='14'>Unemployed, looking for work</option>
        <option value='15'>Retired</option>
        <option value='16'>Other</option>
        </select></td>
        </tr>";
    }

    $out[body]=$out[body]."
    <tr class='altcolor'>
    <td width='100%' valign='center' align='left' colspan='2'>
    <strong>Optional Information</strong></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Icq Number</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='n_icq' value='' size='30' maxlength='10'></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Aim Name</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='n_aim' value='' size='30' maxlength='16'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    MSN Messenger</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='n_msn' value='' size='30' maxlength='35'></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Yahoo Messenger</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='n_yahoo' value='' size='30' maxlength='35'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Website Url</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='n_url' value='' size='30' maxlength='100'></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Player Logo</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='n_logo' value='' size='30' maxlength='100'></td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='center' align='left' colspan='2'>
    <strong>Miscellaneous Information</strong></td>
    </tr>";
    if($misc[joinage]){
        $out[body]=$out[body]."
        <tr class='altcolorb'>
        <td width='100%' valign='center' align='left' colspan='2'>
        <font face='veradna,arial' size='2' color='#FFFFFF'>
        <input type='checkbox' name='agree_age'> I am at least $misc[joinage] years of age</font></td>
        </tr>";
    }

    if($url[policy]){
        $out[body]=$out[body]."
        <tr class='altcolora'>
        <td width='100%' valign='center' align='left' colspan='2'>
        <font face='veradna,arial' size='2' color='#FFFFFF'>
        <input type='checkbox' name='agree_policy'> I agree to abide by the rules of this site</font></td>
        </tr>";
    }

    if($misc[newsletter]=="yes"){
        $out[body]=$out[body]."
        <tr class='altcolorb'>
        <td width='100%' valign='center' align='left' colspan='2'>
        <font face='veradna,arial' size='2' color='#FFFFFF'>
        <input type='checkbox' name='n_newsletter' value='1' CHECKED> I would like to receive the $site[shortname] newsletter</font></td>
        </tr>";
    }

    $out[body]=$out[body]."
    <tr>
    <td width='100%' valign='top' align='center' colspan='2'>
    <br>
    <input type='hidden' name='action' value='joinb'>
    <button type='submit' name='' class='btn btn-mini btn-success' value='Create My Account'>Crear mi cuenta</button></td>
    </form>
    </tr>
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function form_newplayerb($n_alias,$n_pass,$n_passa,$n_email,$n_emaila,$n_icq,$n_aim,$n_yahoo,$n_msn,$n_url,$n_logo,$n_newsletter,$n_fname,$n_mname,$n_lname,$n_addrs,$n_city,$n_state,$n_zip,$n_phonea,$n_phoneb,$n_bdaym,$n_bdayd,$n_bdayy,$n_occup,$agree_age,$agree_policy,$n_country){
    global $dir, $uinfo, $url, $file, $site, $out, $theme, $misc, $email;
    include("$dir[func]/checkdata.php");
    $errormessage=check_agrees($errormessage, $agree_age, $agree_policy);
    $errormessage=check_matching($errormessage, $n_pass, $n_passa, "You Passwords Do Not Match");
    $errormessage=check_matching($errormessage, $n_email, $n_emaila, "You Email Addresses Do Not Match");
    $errormessage=check_validlength($errormessage, $n_alias, "3", "30", "Your Player Alias Must be 3-25 Characters Long");
    $errormessage=check_validlength($errormessage, $n_pass, "2", "15", "Your Password Must be 2-10 Characters Long");
    error_check($errormessage);
    if(($n_country == "") && ($misc[flags] =="yes")){
        include("$dir[func]/error.php");
        display_error("You Must Select a Country<br>");
    }

    if(($uinfo[allow]=="yes") || ($uinfo[force]=="yes")){
        $n_fname=change_charecters($n_fname);
        $n_mname=change_charecters($n_mname);
        $n_lname=change_charecters($n_lname);
        $n_addrs=change_charecters($n_addrs);
        $n_city=change_charecters($n_city);
        $n_state=change_charecters($n_state);
        $n_zip=change_numbersonly($n_zip);
        $n_phonea=change_numbersonly($n_phonea);
        $n_phoneb=change_numbersonly($n_phoneb);
        $n_bdaym=change_numbersonly($n_bdaym);
        $n_bdayd=change_numbersonly($n_bdayd);
        $n_bdayy=change_numbersonly($n_bdayy);
        $n_occup=change_numbersonly($n_occup);
    }

    $n_phone="$n_phonea$n_phoneb";
    if($uinfo[force]=="yes"){
        $errormessage=check_requsernfo($errormessage,$n_fname,$n_mname,$n_lname,$n_addrs,$n_city,$n_state,$n_zip,$n_phone,$n_bdaym,$n_bdayd,$n_bdayy,$n_occup);
    }

    error_check($errormessage);
    $errormessage=check_validlength($errormessage, $n_alias, "3", "50", "Your Player Alias Must be 3-25 Characters Long");
    $errormessage=check_validlength($errormessage, $n_pass, "2", "25", "Your Password Must be 2-10 Characters Long");
    error_check($errormessage);
    $errormessage=check_emailaddress($n_email);
    error_check($errormessage);
    $errormessage=check_unvalusersinfoexist($errormessage, $n_alias, $n_emaila, $ip);
    $errormessage=check_usersinfoexist($errormessage, $n_alias, $n_emaila, $ip);
    error_check($errormessage);
    $n_icq=change_numbersonly($n_icq);
    $n_aim=change_charecters($n_aim);
    $n_yahoo=change_charecters($n_yahoo);
    $n_msn=change_charecters($n_msn);
    $n_url=change_url($n_url);
    $n_logo=change_url($n_logo);
    $n_logo=change_charecters($n_logo);
    $n_url=change_charecters($n_url);
    $n_email=change_charecters($n_email);
    $errormessage=check_ban($errormessage, $n_alias, $n_email, $ip);
    error_check($errormessage);
    $tday=date("Y-m-d H:i:s");
    $bday="$n_bdayy-$n_bdaym-$n_bdayd";
    $ip=getenv("REMOTE_ADDR");
    $mcode=md5(uniqid(microtime()));
    $valicode="$mcode";
    mysql_query("INSERT INTO validate VALUES (
    '$valicode',
    '$n_alias',
    '$n_pass',
    '$n_email',
    '$n_icq',
    '$n_aim',
    '$n_yahoo',
    '$n_msn',
    '$n_url',
    '$n_logo',
    '$n_newsletter',
    '$tday',
    '$ip',
    '$n_fname',
    '$n_mname',
    '$n_lname',
    '$n_addrs',
    '$n_city',
    '$n_state',
    '$n_zip',
    '$n_phone',
    '$bday',
    '$n_occup',
    '$theme[normal]',
    '$n_country');");
    $tablehead=table_head("show","500","","left");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    <br>
    $tablehead";
    if($misc[uservalidate] == 'yes'){
    	$out[body]=$out[body]." &nbsp; &nbsp;<strong><font class='catfont'>Thank you for signing up on $site[shortname]</font></strong><br>
    	<hr class='catfont' size='1'>
    	Before you can play on the ladders you must Validate your account.<br>
    	An email was sent to $n_email with a Validation Code.<br>
    	If your email supports html just click on the link in the email,<br>
    	Otherwise Click on \"Validate Account\" on the Menu to type in the Code.<br>
    	$tablefoot
    	</center>";
    	$emailbody="Thank you for signing up on $site[shortname].\n
    	Your UserName is :  $n_alias
    	Your Password is :  $n_pass \n
    	Validation Code: $valicode
    	$url[base]/$file[join]?action=validate&vc=$valicode\n\n
    	\n
    	";
    	include("$dir[func]/email.php");
    	send_email($n_alias,$n_email,"Validation Code",$emailbody);
    }else{
    	$out[body]=$out[body]." &nbsp; &nbsp;<strong><font class='catfont'>Thank you for signing up on $site[shortname]</font></strong><br>
    	<hr class='catfont' size='1'><center>
    	Before you can play on the ladders an admin must validate your account.<br>
    	An email will be sent to you,  once your account has been validated.<br>
    	<br>
    	$tablefoot
    	</center>";

	$emailbody="$n_alias has signed up on $site[shortname].\n \n
    	You can either validate or deny validation to this account by logging in to the admin cp and clicking on validation codes.\n
    	Admin login: $url[base]/$file[admin] \n\n\n";

	include("$dir[func]/email.php");
    	send_email("Admin",$email[sendaccount],"Validation Request",$emailbody);

    }
    include("$dir[curtheme]");
}

?>
