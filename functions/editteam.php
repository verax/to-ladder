<?PHP
function edit_team($teamid){
    global $dir, $url, $out, $plyr, $site, $uinfo, $misc;
    if(!$teamid){
        include("$dir[func]/error.php");
        display_error("Invalid Team ID.<br>");
    }

    //CHECK IF LEADER
    if (mysql_num_rows(mysql_query("SELECT status FROM teammembers WHERE teamid='$teamid' AND playerid='$plyr[id]' AND status='1'")) < 1){
        include("$dir[func]/error.php");
        display_error("You are not allowed to edit this team.<br>");
    }

    $teaminfo=mysql_query("SELECT * FROM teams WHERE id='$teamid'");
    $tinfo=mysql_fetch_array($teaminfo);
    if(!$tinfo[teamname]){
        include("$dir[func]/error.php");
        display_error("Unknown Team ID.<br>");
    }

    if($tinfo[tagplace]=="0"){
        $checkeda="CHECKED";
        $checkedb="";
    }else{

        $checkeda="";
        $checkedb="CHECKED";
    }

    if($misc[changeteamname]=="yes"){
        $teamnameedit="<input type='text' name='t_alias' value='$tinfo[teamname]' size='30' maxlength='25'>";
    }else{

        $teamnameedit="<input type='hidden' name='t_alias' value='$tinfo[teamname]'>$tinfo[teamname]";
    }

    //TABLE ATERNATING COLORS
    $altcolora="#000033";
    $altcolorb="#000020";
    $tablehead=table_head("show","100%","","left");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    <br>
    $tablehead
    &nbsp; &nbsp;<strong><font class='catfont'>Team Editor</font></strong><br>
    <hr class='catfont' size='1'>
    <table width='100%' border='0' cellspacing='0' cellpadding='0' valign='top' align='center'>
    <form method='post'>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>* Team Name</font></td>
    <td width='50%' valign='center' align='center'>
    $teamnameedit</td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>* Team Email</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='t_email' value='$tinfo[teamemail]' size='30' maxlength='50'></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Tag</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='t_tag' value='$tinfo[tag]' size='30' maxlength='10'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Tag Placement</font></td>
    <td width='50%' valign='center' align='center'>
    Before Name: <input type='radio' name='t_tagplace' value='0' $checkeda>
    After: <input type='radio' name='t_tagplace' value='1' $checkedb></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Team Website</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='t_url' value='$tinfo[website]' size='30' maxlength='100'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Team Logo</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='t_logo' value='$tinfo[logo]' size='30' maxlength='100'></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Contact Method (Irc Channel@Server or Icq)</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='t_contact' value='$tinfo[contact]' size='30' maxlength='50'></td>
    </tr>
    <tr>
    <td width='100%' valign='top' align='center' colspan='2'>
    <br>
    <input type='hidden' name='teamid' value='$teamid'>
    <input type='hidden' name='action' value='teamb'>
    <button type='submit' name='' value='Update Team' class='btn btn-mini btn-success'>Actualizar Clan</button>
    <button type='reset' name='' value='Reset' class='btn btn-mini btn-info'>Reset</button></td>
    </form>
    </tr>
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function edit_teamb($teamid,$t_alias,$t_email,$t_tag,$t_tagplace,$t_url,$t_logo,$t_contact){
    global $dir, $file, $url, $out, $plyr;
    if(!$teamid){
        include("$dir[func]/error.php");
        display_error("Invalid Team ID.<br>");
    }

    //CHECK IF LEADER
    if (mysql_num_rows(mysql_query("SELECT status FROM teammembers WHERE teamid='$teamid' AND playerid='$plyr[id]' AND status='1'")) < 1){
        include("$dir[func]/error.php");
        display_error("You are not allowed to edit this team.<br>");
    }

    $teaminfo=mysql_query("SELECT teamname FROM teams WHERE id='$teamid'");
    $tinfo=mysql_fetch_array($teaminfo);
    if(!$tinfo[teamname]){
        include("$dir[func]/error.php");
        display_error("Unknown Team ID.<br>");
    }

    include("$dir[func]/checkdata.php");
    $t_alias=change_charecters($t_alias);
    $t_email=change_charecters($t_email);
    $t_tag=change_charecters($t_tag);
    $errormessage=check_validlength($errormessage, $t_alias, "3", "50", "Your Name Must be 3-25 Characters Long");
    $errormessage=check_validlength($errormessage, $t_tag, "0", "25", "Your Tag Must not Exceed 10 Characters Long");
    $errormessage=check_validlength($errormessage, $t_email, "5", "100", "Your must include a valid team email.<br>Team Email cannot exceed 50 characters");
    error_check($errormessage);
    $errormessage=check_emailaddress($t_email);
    error_check($errormessage);
    if (mysql_num_rows(mysql_query("SELECT teamname FROM teams WHERE id!='$teamid' AND teamname='$t_alias'")) > 0){
        include("$dir[func]/error.php");
        display_error("That name already exist.<br>");
    }

    $t_tagplace=change_numbersonly($t_tagplace);
    $t_url=change_url($t_url);
    $t_logo=change_url($t_logo);
    $t_url=change_charecters($t_url);
    $t_logo=change_charecters($t_logo);
    $t_contact=change_charecters($t_contact);
    $errormessage=check_ban($errormessage, $t_alias, $email, $ip);
    error_check($errormessage);
    mysql_query("UPDATE teams SET
    teamname='$t_alias',
    teamemail='$t_email',
    tag='$t_tag',
    tagplace='$t_tagplace',
    website='$t_url',
    logo='$t_logo',
    contact='$t_contact'
    WHERE id='$teamid'");
    include("$dir[func]/finishmessage.php");
    display_message("Your Team Information has been Updated","phome");
}

?>
