<?
if(!IsSet($indexloaded)){
    header("Location: ./index.php");
}

//CHECK ADMIN ACCESS LEVEL
if($admn[access] < 55){
    include("$dir[func]/error.php");
    display_error("You are not allowed to perform this function.<br>");
}

function admin_maps(){
    global $dir, $url, $out, $site, $admn;
    //GET LADDERS
    $ladderslist=mysql_query("SELECT id,laddername  FROM ladders ORDER by laddername");
    while(list($id,$name)=mysql_fetch_row($ladderslist)){
        $theladders=$theladders."<option value='$id'>$name</option>";
        $foundladders=1;
    }

    if(!$foundladders){
        include("$dir[func]/error.php");
        display_error("There are no ladders.<br>");
    }

    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <form method='post'>
    <td width='100%' valign='top' align='center'>
    <strong>Manage Maps</strong><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <br>
    Select a ladder<br>
    <select name='ladderid'>$theladders</select><br>
    <br></td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='action' value='editmapsb'>
    <input type='submit' name='' value='Edit Maps'>
    </td>
    </form>
    </tr>
    </table>";
    include("$dir[curtheme]");
}

function admin_mapsb($ladderid){
    global $dir, $url, $out, $site, $admn;
    if($admn[access] < 70){
        if (mysql_num_rows(mysql_query("SELECT staffid FROM staffaccess WHERE staffid='$admn[id]' AND ladderid='$ladderid'")) < 1){
            include("$dir[func]/error.php");
            display_error("You are not allowed to manage maps on this laddder.<br>");
        }

    }

    //GET MAPS
    $mapslist=mysql_query("SELECT id,mapname FROM maps WHERE ladderid='$ladderid' ORDER by mapname");
    while(list($id,$name)=mysql_fetch_row($mapslist)){
        $themaps=$themaps."<option value='$id'>$name</option>";
    }

    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <form method='post'>
    <td width='100%' valign='top' align='center'>
    <strong>Manage Maps</strong><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <br>
    <script language='javascript'>var confirmdelete='Are you sure you want to DELETE this Map';</script>
    <select name='mapid'>
    <option value=''>Select a map or create a new one</option>
    $themaps</select><br>
    <br></td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='action' value='editmapsc'>
    <input type='hidden' name='ladderid' value='$ladderid'>
    <input type='submit' name='todo' value='Create Map'>
    <input type='submit' name='todo' value='Edit Map'>
    <input type='submit' name='todo' value='Delete Map' onClick='return confirm(confirmdelete);'>
    </td>
    </form>
    </tr>
    </table>";
    include("$dir[curtheme]");
}

function admin_mapsc($ladderid,$mapid,$todo){
    global $dir, $url, $out, $site, $admn;
    if(!$ladderid){
        include("$dir[func]/error.php");
        display_error("Invalid Ladder ID.<br>");
    }

    if((!$mapid) && ($todo!="Create Map")){
        include("$dir[func]/error.php");
        display_error("Invalid Map ID.<br>");
    }

    if($todo=="Create Map"){
        create_map($ladderid);
    }

    else if($todo=="Edit Map"){
        edit_map($mapid);
    }

    else if($todo=="Delete Map"){
        delete_map($mapid);
        include("$dir[func]/admin_finishmessage.php");
        display_message("The map was deleted.<br>");
    }

    else{
        include("$dir[func]/error.php");
        display_error("Unknown Command.<br>");
    }

}

function create_map($ladderid){
    global $dir, $url, $out, $site, $admn;
    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <form method='post'>
    <td width='100%' valign='top' align='center' colspan='2'>
    <strong>Create a Map</strong><br>
    </td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Map Name</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='m_name' value='' size='40' maxlength='25'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>File Name</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='m_fname' value='' size='40' maxlength='25'></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Info Url</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='m_url' value='' size='40' maxlength='100'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Image Url</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='m_image' value='' size='40' maxlength='100'></td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center' colspan='2'>
    <input type='hidden' name='action' value='editmapsd'>
    <input type='hidden' name='ladderid' value='$ladderid'>
    <input type='submit' name='' value='Create Map'></td>
    </form>
    </tr>
    </table>";
    include("$dir[curtheme]");
}

function admin_mapsd($ladderid,$m_name,$m_fname,$m_url,$m_image){
    global $dir, $url, $out, $site, $admn;
    include("$dir[func]/checkdata.php");
    $errormessage=check_validlength($errormessage, $m_name, "2", "25", "Your map name must be 2-25 charecters");
    error_check($errormessage);
    $m_url=change_url($m_url);
    $m_image=change_url($m_image);
    $m_name=change_charecters($m_name);
    $m_fname=change_charecters($m_fname);
    $m_url=change_charecters($m_url);
    $m_image=change_charecters($m_image);
    $tday=date("Y-m-d H:i:s");
    mysql_query("INSERT INTO maps VALUES (NULL,
    '$ladderid',
    '$m_name',
    '$m_fname',
    '$m_url',
    '$m_image',
    '$admn[id]',
    '$tday');");
    include("$dir[func]/admin_finishmessage.php");
    display_message("The map has been added.<br>");
}

function edit_map($mapid){
    global $dir, $url, $out, $site, $admn;
    $mapinfo=mysql_query("SELECT * FROM maps WHERE id='$mapid'");
    $map=mysql_fetch_array($mapinfo);
    if(!$map[id]){
        include("$dir[func]/error.php");
        display_error("Unknown Map ID.<br>");
    }

    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <form method='post'>
    <td width='100%' valign='top' align='center' colspan='2'>
    <strong>Edit a Map</strong><br>
    </td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Map Name</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='m_name' value='$map[mapname]' size='40' maxlength='25'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>File Name</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='m_fname' value='$map[mapfilename]' size='40' maxlength='25'></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Info Url</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='m_url' value='$map[mapinfolink]' size='40' maxlength='100'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Image Url</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='m_image' value='$map[imageurl]' size='40' maxlength='100'></td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center' colspan='2'>
    <input type='hidden' name='action' value='editmapse'>
    <input type='hidden' name='mapid' value='$mapid'>
    <input type='submit' name='' value='Update Map'></td>
    </form>
    </tr>
    ";
    //GET LADDERS
    $ladderslist=mysql_query("SELECT id,laddername FROM ladders WHERE id!='$map[ladderid]' ORDER by laddername");
    while(list($id,$name)=mysql_fetch_row($ladderslist)){
        $theladders=$theladders."<option value='$id'>$name</option>";
    }

    $out[body]=$out[body]."
    <tr class='altcolor'>
    <form method='post'>
    <td width='100%' valign='top' align='center' colspan='2'>
    <select name='ladderid'>$theladders</select>
    <input type='hidden' name='action' value='editmapsf'>
    <input type='hidden' name='mapid' value='$mapid'>
    <input type='submit' name='' value='Copy Map'></td>
    </form>
    </tr>
    </table>";
    include("$dir[curtheme]");
}

function admin_mapse($mapid,$m_name,$m_fname,$m_url,$m_image){
    global $dir, $url, $out, $site, $admn;
    if(!$mapid){
        include("$dir[func]/error.php");
        display_error("Invalid Map ID.<br>");
    }

    include("$dir[func]/checkdata.php");
    $errormessage=check_validlength($errormessage, $m_name, "2", "25", "Your map name must be 2-25 charecters");
    error_check($errormessage);
    $m_url=change_url($m_url);
    $m_image=change_url($m_image);
    $m_name=change_charecters($m_name);
    $m_fname=change_charecters($m_fname);
    $m_url=change_charecters($m_url);
    $m_image=change_charecters($m_image);
    $tday=date("Y-m-d H:i:s");
    mysql_query("UPDATE maps SET
    mapname='$m_name',
    mapfilename='$m_fname',
    mapinfolink='$m_url',
    imageurl='$m_image',
    editedby='$admn[id]',
    edited='$tday'
    WHERE id='$mapid'");
    include("$dir[func]/admin_finishmessage.php");
    display_message("The map has been updated.<br>");
}

function admin_mapsf($mapid,$ladderid){
    global $dir, $url, $out, $site, $admn;
    $allmapsinfo=mysql_query("SELECT * FROM maps WHERE id='$mapid'");
    $mapinfo=mysql_fetch_array($allmapsinfo);
    if(!$mapinfo[mapname]){
        include("$dir[func]/error.php");
        display_error("Unknown Map ID.<br>");
    }

    $tday=date("Y-m-d H:i:s");
    mysql_query("INSERT INTO maps VALUES (NULL,
    '$ladderid',
    '$mapinfo[mapname]',
    '$mapinfo[mapfilename]',
    '$mapinfo[mapinfolink]',
    '$mapinfo[imageurl]',
    '$admn[id]',
    '$tday');");
    include("$dir[func]/admin_finishmessage.php");
    display_message("The map has been copied.<br>");
}

function delete_map($mapid){
    mysql_query("DELETE FROM maps WHERE id='$mapid'");
}

?>
