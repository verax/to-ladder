<?
// +CHECKS
//AGE AND POLICY AGREE
function check_agrees($errormessage, $agree_age, $agree_policy){
    global $site, $url, $misc;
    if(($misc[joinage]) && (!$agree_age)){
        $errormessage=$errormessage."Debes tener al menos $misc[joinage] a&nacute;os de edad para participar en $site[shortname].<br>";
    }

    if(($url[policy]) && (!$agree_policy)){
        $errormessage=$errormessage."Debes leer y estar de acuerdo con las pol&iacute;ticas de $site[shortname] antes de participar.<br>";
    }

    return($errormessage);
}

//REQUIRED USER INFORMATION FIELDS
function check_requsernfo($errormessage, $n_fname, $n_mname, $n_lname, $n_addrs, $n_city, $n_state, $n_zip, $n_phone, $n_bdaym, $n_bdayd, $n_bdayy, $n_occup){
    if(($n_fname=="") || ($n_mname=="") || ($n_lname=="") || ($n_addrs=="") || ($n_city=="") || ($n_state=="") || ($n_zip=="") || ($n_phone=="") || ($n_bdaym=="") || ($n_bdayd=="") || ($n_bdayy=="") || ($n_occup=="")){
        $errormessage=$errormessage."All User Information Fields Are Required.<br>";
    }

    return($errormessage);
}

//CHECK FOR FIELD MATCHES
function check_matching($errormessage, $matcha, $matchb, $customerror){
    if($matcha!="$matchb"){
        $errormessage=$errormessage."$customerror<br>";
    }

    return("$errormessage");
}

//CHECK FOR VALID LENGTHS
function check_validlength($errormessage, $text, $minlength, $maxlength, $customerror){
    if ((strlen($text) < $minlength) || (strlen($text) > $maxlength)){
        $errormessage=$errormessage."$customerror<br>";
    }

    return($errormessage);
}

//CHECK IF USERS INFO ALREADY EXIST UNVALIDATED
function check_unvalusersinfoexist($errormessage, $alias, $email, $ip){
    global $misc;
    if($ip==""){
        $ip=getenv("REMOTE_ADDR");
    }

    if (mysql_num_rows(mysql_query("select alias from validate where alias='$alias'")) > 0){
        $errormessage=$errormessage."Your Player Alias is already in use on an Unvalidated Account.<br>";
    }

    if (mysql_num_rows(mysql_query("select email from validate where email='$email'")) > 0){
        $errormessage=$errormessage."Your Email Address is already in use on an Unvalidated Account.<br>";
    }

    if (mysql_num_rows(mysql_query("select ipaddress from validate where ipaddress='$ip'")) > 0){
        $errormessage=$errormessage."Your IP Address is already in use on an Unvalidated Account.<br>";
    }

    return($errormessage);
}

//CHECK IF USERS INFO ALREADY EXIST
function check_usersinfoexist($errormessage, $alias, $email, $ip){
    global $misc;
    if($ip==""){
        $ip=getenv("REMOTE_ADDR");
    }

    if (mysql_num_rows(mysql_query("select alias from users where alias='$alias'")) > 0){
        $errormessage=$errormessage."Your Player Alias is already in use.<br>";
    }

    if (mysql_num_rows(mysql_query("select email from users where email='$email'")) > 0){
        $errormessage=$errormessage."Your Email Address is already in use.<br>";
    }

    if($misc[multipleip]!="yes"){
        if($ip!="x"){
            if (mysql_num_rows(mysql_query("select ipaddress from users where ipaddress='$ip'")) > 0){
                $errormessage=$errormessage."Your IP Address is already in use.<br>";
            }

        }

    }

    return($errormessage);
}

//CHECK IF USERS INFO ALREADY EXIST ON ANOTHER ACCOUNT
function check_usersinfoexistother($errormessage, $id, $alias, $email, $ip){
    if($ip==""){
        $ip=getenv("REMOTE_ADDR");
    }

    if (mysql_num_rows(mysql_query("select alias from users where alias='$alias' AND id!='$id'")) > 0){
        $errormessage=$errormessage."Your Player Alias is already in use on another account.<br>";
    }

    if (mysql_num_rows(mysql_query("select email from users where email='$email' AND id!='$id'")) > 0){
        $errormessage=$errormessage."Your Email Address is already in use on another account.<br>";
    }

    if($ip!="x"){
        if (mysql_num_rows(mysql_query("select ipaddress from users where ipaddress='$ip' AND id!='$id'")) > 0){
            $errormessage=$errormessage."Your IP Address is already in use on another account.<br>";
        }

    }

    return($errormessage);
}

//CHECK IF TEAM NAME ALREADY EXIST
function check_teamexist($errormessage, $alias){
    if (mysql_num_rows(mysql_query("select teamname from teams where teamname='$alias'")) > 0){
        $errormessage=$errormessage."That Name is already in use.<br>";
    }

    return($errormessage);
}

//CHECK IF CLAN NAME ALREADY EXIST ON OTHER ACCOUNT
function check_clanexistother($errormessage,$alias,$clansid){
    if (mysql_num_rows(mysql_query("select clanname from clans where clanname='$alias' AND id!='$clansid'")) > 0){
        $errormessage=$errormessage."That Clan Name is already in use.<br>";
    }

    return($errormessage);
}

//BAN CHECK
function check_ban($errormessage, $alias, $email, $ip){
    global $email, $misc;
    if($misc[dobans]){
        $cuip=getenv("REMOTE_ADDR");
        if($ip==""){
            $ip="$cuip";
        }

        $splitip=split("\.", $ip);
        $emaildomain=split("@", $email);
        $baninfo=mysql_query("SELECT id,text,type,displayreason FROM bancensor ORDER by id");
        while(list($id, $text, $type, $reason)=mysql_fetch_row($baninfo)){
            if(($type=="0") && ($text=="$alias")){
                $errormessage=$errormessage."Your Alias has been Banned for the Folowing Reason<br>
                <b>$reason</b><br>For more information on this matter send an email to
                <a href='mailto:$email[banhelp]?subject=banhelp&body=bcode:$id'>$email[banhelp]</a>.
                <br>make sure to include the Following Code: bcode$id<hr>";
            }

            if(($type=="1") && ($newword=eregi("$text","$alias"))){
                $errormessage=$errormessage."Your Alias has been Banned for the Folowing Reason:<br>
                <b>$reason</b><br>For more information on this matter send an email to
                <a href='mailto:$email[banhelp]?subject=banhelp&body=bcode:$id'>$email[banhelp]</a>.
                <br>make sure to include the Following Code: bcode$id<hr>";
            }

            if(($type=="2") && ($text=="$email")){
                $errormessage=$errormessage."Your Email has been Banned for the Folowing Reason:<br>
                <b>$reason</b><br>For more information on this matter send an email to
                <a href='mailto:$email[banhelp]?subject=banhelp&body=bcode:$id'>$email[banhelp]</a>.
                <br>make sure to include the Following Code: bcode$id<hr>";
            }

            if(($type=="3") && ($text=="$emaildomain[0]")){
                $errormessage=$errormessage."Your Email Username has been Banned for the Folowing Reason:<br>
                <b>$reason</b><br>For more information on this matter send an email to
                <a href='mailto:$email[banhelp]?subject=banhelp&body=bcode:$id'>$email[banhelp]</a>.
                <br>make sure to include the Following Code: bcode$id<hr>";
            }

            if(($type=="4") && ($text=="$emaildomain[1]")){
                $errormessage=$errormessage."Your Email's Server has been Banned for the Folowing Reason:<br>
                <b>$reason</b><br>For more information on this matter send an email to
                <a href='mailto:$email[banhelp]?subject=banhelp&body=bcode:$id'>$email[banhelp]</a>.
                <br>make sure to include the Following Code: bcode$id<hr>";
            }

            if(($type=="5") && ($text=="$ip")){
                $errormessage=$errormessage."Your IP has been Banned for the Folowing Reason:<br>
                <b>$reason</b><br>For more information on this matter send an email to
                <a href='mailto:$email[banhelp]?subject=banhelp&body=bcode:$id'>$email[banhelp]</a>.
                <br>make sure to include the Following Code: bcode$id<hr>";
            }

            if($type=="6"){
                $splittext=split("\.", $text);
                if($splittext[0]=="$splitip[0]"){
                    if(($splittext[1]=="*") && ($splittext[2]=="*") && ($splittext[3]=="*")){
                        $errormessage=$errormessage."Your IP Block ($splittext[0].*.*.*) has been Banned for the Folowing Reason:<br>
                        <b>$reason</b><br>For more information on this matter send an email to
                        <a href='mailto:$email[banhelp]?subject=banhelp&body=bcode:$id'>$email[banhelp]</a>.
                        <br>make sure to include the Following Code: bcode$id<hr>";
                    }

                    if(($splittext[1]=="$splitip[1]") && ($splittext[2]=="*") && ($splittext[3]=="*")){
                        $errormessage=$errormessage."Your IP Block ($splittext[0].$splittext[1].*.*) has been Banned for the Folowing Reason:<br>
                        <b>$reason</b><br>For more information on this matter send an email to
                        <a href='mailto:$email[banhelp]?subject=banhelp&body=bcode:$id'>$email[banhelp]</a>.
                        <br>make sure to include the Following Code: bcode$id<hr>";
                    }

                    if(($splittext[1]=="$splitip[1]") && ($splittext[2]=="$splitip[2]") && ($splittext[3]=="*")){
                        $errormessage=$errormessage."Your IP Block ($splittext[0].$splittext[1].$splittext[2].*) has been Banned for the Folowing Reason:<br>
                        <b>$reason</b><br>For more information on this matter send an email to
                        <a href='mailto:$email[banhelp]?subject=banhelp&body=bcode:$id'>$bannedhelp</a>.
                        <br>make sure to include the Following Code: bcode$id<hr>";
                    }

                }

            }

        }

        return($errormessage);
    }

}

function check_staffexist($errormessage, $name){
    if (mysql_num_rows(mysql_query("select displayname from staff where displayname='$name'")) > 0){
        $errormessage=$errormessage."That Staff Display Name is already In Use.<br>";
    }

    return($errormessage);
}

function check_staffexistother($errormessage, $name, $id){
    if (mysql_num_rows(mysql_query("select id from staff where displayname='$name' AND id !='$id'")) > 0){
        $errormessage=$errormessage."That Staff Display Name is already In Use.<br>";
    }

    return($errormessage);
}

function check_givestaffrights($errormessage, $accesslevel){
    global $admn;
    if($accesslevel > $admn[access]){
        $errormessage=$errormessage."Your Access Level is only $admn[access],<br>
        You cannot give an Access Level of $accesslevel to another Staff Member.<br>";
    }

    return($errormessage);
}

function check_givestaffgames($errormessage, $gamearray){
    global $admn;
    $result=mysql_query("SELECT games FROM staff WHERE id='$admn[id]'");
    $resrow=mysql_fetch_array($result);
    $admnlgs=$resrow[games];
    $admnlgs=split("-",$admnlgs);
    $gameid=array($gamearray);
    while (list($i)=each($gameid)){
        while (list($lid,$laccess)=each($leagueid[$i])){
            if ((!in_array ("$lid", $admnlgs)) && ($laccess=="Y")){
                $errormessage=$errormessage."Cannot Add Staff to Game ID: $lid.<br>";
            }

        }

    }

    return($errormessage);
}

//CHECK DATA FORMAT
function check_datetimefields($errormessage, $datetime, $customerror){
    if (!ereg ("([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})$", $datetime)){

        $errormessage=$errormessage."$customerror<br>";
    }

    return($errormessage);
}

//CHECK IF GAME NAME OR ABREVIATION EXIST
function check_gameexist($errormessage, $name, $abrv){
    if (mysql_num_rows(mysql_query("select id from games where gamename='$name'")) > 0){
        $errormessage=$errormessage."That Game Name Already Exist.<br>";
    }

    if (mysql_num_rows(mysql_query("select id from games where abbreviation='$abrv'")) > 0){
        $errormessage=$errormessage."That Game Abreviation Already Exist.<br>";
    }

    return($errormessage);
}

//CHECK IF OTHER GAMES NAME OR ABREVIATION EXIST
function check_gameexistother($errormessage, $name, $abrv, $gameid){
    if (mysql_num_rows(mysql_query("select id from games where gamename='$name' AND id!='$gameid'")) > 0){
        $errormessage=$errormessage."That Game Name Already Exist.<br>";
    }

    if (mysql_num_rows(mysql_query("select id from games where abbreviation='$name' AND id!='$gameid'")) > 0){
        $errormessage=$errormessage."That Game Abreviation Already Exist.<br>";
    }

    return($errormessage);
}

//CHECK IF LADDER NAME OR ABREVIATION EXIST
function check_ladderexist($errormessage, $name, $abrv){
    if (mysql_num_rows(mysql_query("select id from ladders where laddername='$name'")) > 0){
        $errormessage=$errormessage."That Ladder Name Already Exist.<br>";
    }

    if (mysql_num_rows(mysql_query("select id from ladders where abbreviation='$abrv'")) > 0){
        $errormessage=$errormessage."That Ladder Abreviation Already Exist.<br>";
    }

    return($errormessage);
}

//CHECK IF OTHER LADDER NAME OR ABREVIATION EXIST
function check_ladderexistother($errormessage, $name, $abrv, $ladderid){
    if (mysql_num_rows(mysql_query("select id from ladders where laddername='$name' AND id!='$ladderid'")) > 0){
        $errormessage=$errormessage."That Ladder Name Already Exist.<br>";
    }

    if (mysql_num_rows(mysql_query("select id from ladders where abbreviation='$abrv' AND id!='$ladderid'")) > 0){
        $errormessage=$errormessage."That Ladder Abreviation Already Exist.<br>";
    }

    return($errormessage);
}

//CHECK IF OTHER LADDER NAME OR ABREVIATION EXIST
function check_emailaddress($email){
    if(!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@([0-9a-z][0-9a-z-]*[0-9a-z]\.)+[a-z]{2}[mtgvu]?$", $email)){

        $errormessage=$errormessage."That Email address is invalid.<br>";
    }

    return($errormessage);
}

// +CHANGES
//CHANGE URLS THAT FORGOT THE http://
function change_url($url){
    if(($url) && (!(eregi('(^(f|ht)tp[s]*:[/]+)(.*)', $url)))){
        $url = "http://" . $url;
    }

    return("$url");
}

// STRIP LEADING,TRAILING,DOUBLE AND WHITE SPACES
function strip_whitespace($text){
    $text=trim($text);
    $text=eregi_replace("( ){2,}", " ", $text);

    return($text);
}

// STRIP ALL SPACES
function strip_allspace($text){
    $text=trim($text);
    $text=eregi_replace("( ){2,}", "", $text);

    return($text);
}

//CHANGE CHARACTERS / STRIP HTML
function change_charecters($text){
    $text=trim($text);
    $text=eregi_replace("( ){2,}", " ", $text);

    // ⁄$text=str_replace(",", "", $text);
    $text=str_replace(";", "", $text);
    $text=htmlspecialchars("$text");
    $text=str_replace("#", "", $text);
    $text=str_replace("|", "l", $text);
    $text=str_replace("'", "&acute;", $text);
    return($text);
}

//CHANGE CHARACTERS / STRIP HTML
function unchange_charecters($text){
    $text=ereg_replace('&gt;', '>', $text);
    $text=ereg_replace('&lt;', '<', $text);
    $text=ereg_replace('&quot;', "\"", $text);
    $text=ereg_replace('&amp;', '&', $text);
    return($text);
}

//CHANGE TO BCODE CHARACTERS FROM HTML CODE
function change_tolinebreaks($text){
    $text=str_replace("<br>", "\n", $text);
    return($text);
}

//CHANGE BCODE CHARACTERS TO HTML CODE
function change_fromlinebreaks($text){
    $text=str_replace("\n", "{br}", $text);

    return($text);
}

//CHANGE BCODE CHARACTERS TO HTML CODE
function change_tocharectercode($text){
    $text=str_replace("{img=", "<img src=", $text);
        $text=str_replace("{/img", "", $text);
            $text=str_replace("{font", "<font", $text);
                $text=str_replace("{/font", "</font", $text);
                    $text=str_replace("{br", "<br", $text);
                        $text=str_replace("{b", "<b", $text);
                            $text=str_replace("{/b", "</b", $text);
                            $text=str_replace("}", ">", $text);

                            return($text);
                        }

                        //CHANGE TO BCODE CHARACTERS FROM HTML CODE
                        function change_fromcharectercode($text){
                            $text=str_replace("<img src=", "{img=", $text);
                                $text=str_replace("<font", "{font", $text);
                                    $text=str_replace("</font", "{/font", $text);
                                        $text=str_replace("<br", "{br", $text);
                                            $text=str_replace("<b", "{b", $text);
                                                $text=str_replace("</b", "{/b", $text);
                                                $text=str_replace(">", "}", $text);

                                                return($text);
                                            }

                                            //CUTS OFF AT A NON-NUMERIC CHARECTER:
                                            function change_numbersonly($value){
                                                if($value!="0"){
                                                    $value=trim($value);
                                                    $value=eregi_replace("( ){2,}", " ", $value);

                                                    $value=str_replace("-", "", $value);
                                                    $value=intval($value);
                                                    if($value=="0"){
                                                        $value="";
                                                    }

                                                }

                                                return($value);
                                            }

                                            //ENCODE
                                            function encrypt_word($word){
                                                $word=base64_encode($word);
                                                return($word);
                                            }

                                            //DECODE
                                            function decrypt_word($word){
                                                $word=base64_decode($word);
                                                return($word);
                                            }

                                            //CENSOR
                                            function change_censor($word){
                                                global $misc;
                                                $bannedwords=mysql_query("SELECT text FROM bancensor WHERE type='7' or type='8'");
                                                while(list($val)=mysql_fetch_row($bannedwords)){
                                                    $word=trim(eregi_replace($val,ban_censorchar($misc[censorschar],strlen($val))," $word "));
                                                }

                                                return($word);
                                            }

                                            //REPEAT CENSOR CHARECTER
                                            function ban_censorchar($char, $times) {
                                                $counter=0;
                                                while ($counter++<$times) {
                                                    $retstring.=$char;
                                                }

                                                return($retstring);
                                            }

                                            // CHECK AND RETURN ERROR
                                            function error_check($errormessage){
                                                global $dir;
                                                if ($errormessage){
                                                    include("$dir[func]/error.php");
                                                    display_error($errormessage);
                                                }

                                            }

                                            ?>
