<?
function invite_member($teamid){
    global $dir, $file, $url, $out, $plyr, $site;
    if(!$teamid){
        include("$dir[func]/error.php");
        display_error("Invalid Team ID.<br>");
    }

    //CHECK IF LEADER
    if (mysql_num_rows(mysql_query("SELECT status FROM teammembers WHERE teamid='$teamid' AND playerid='$plyr[id]' AND status <='2'")) < 1){
        include("$dir[func]/error.php");
        display_error("You are not allowed to add new players to this team.<br>");
    }

    $teaminfo=mysql_query("SELECT teamname FROM teams WHERE id='$teamid'");
    $tinfo=mysql_fetch_array($teaminfo);
    if(!$tinfo[teamname]){
        include("$dir[func]/error.php");
        display_error("Unknown Team.<br>");
    }

    $ladders=mysql_query("SELECT id,laddername,maxmembers FROM ladders");
    while($row = mysql_fetch_array($ladders)){
        $teams=mysql_query("SELECT id FROM ladder_$row[id] WHERE id='$teamid'");
        if(mysql_fetch_array($teams)){
            if (mysql_num_rows(mysql_query("SELECT playerid FROM teammembers WHERE teamid='$teamid'")) >= $row[maxmembers]){
                include("$dir[func]/error.php");
                display_error("Your team has the maximum allowed team members<br> on the $row[laddername] ladder. <br><br> No new members can be added to your team.");
            }

        }

    }

    $tablehead=table_head("show","300","","left");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    <br>
    $tablehead
    &nbsp; &nbsp;<strong><font class='catfont'>Add a Member to $tinfo[teamname]</font></strong><br>
    <hr class='catfont' size='1'>
    <table width='300' border='0' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <form method='post'>
    <tr>
    <td width='100%' height='100%' valign='top' align='center'>
    Member Id: <input type='text' name='inviteid' value='' size='25' maxlength='15'>
    <input type='hidden' name='teamid' value='$teamid'>
    <input type='hidden' name='action' value='addmemberb'>
    <br><br>
    <input type='submit' name='' value='Add Player'>
    </td>
    </tr>
    </form>
    </table>
    $tablefoot
    </center>";
    include("$dir[curtheme]");
}

function invite_memberb($teamid,$inviteid){
    global $dir, $file, $url, $out, $plyr, $site;
    if(!$teamid){
        include("$dir[func]/error.php");
        display_error("Invalid Team ID.<br>");
    }

    if(!$inviteid){
        include("$dir[func]/error.php");
        display_error("Invalid Player ID.<br>");
    }

    if($inviteid=="$plyr[id]"){
        include("$dir[func]/error.php");
        display_error("You cannot add yourself.<br>");
    }

    //CHECK IF LEADER
    if (mysql_num_rows(mysql_query("SELECT status FROM teammembers WHERE teamid='$teamid' AND playerid='$plyr[id]' AND status <='2'")) < 1){
        include("$dir[func]/error.php");
        display_error("You are not allowed to add members to this team.<br>");
    }

    $teaminfo=mysql_query("SELECT teamname FROM teams WHERE id='$teamid'");
    $tinfo=mysql_fetch_array($teaminfo);
    if(!$tinfo[teamname]){
        include("$dir[func]/error.php");
        display_error("Unknown Team.<br>");
    }

    //CHECK IF MAX MEMBERS EXISTS
    $ladders=mysql_query("SELECT id,laddername,maxmembers FROM ladders");
    while($row = mysql_fetch_array($ladders)){
        $teams=mysql_query("SELECT id FROM ladder_$row[id] WHERE id='$teamid'");
        if(mysql_fetch_array($teams)){
            if (mysql_num_rows(mysql_query("SELECT playerid FROM teammembers WHERE teamid='$teamid'")) >= $row[maxmembers]){
                include("$dir[func]/error.php");
                display_error("Your team has the maximum allowed team members<br> on the $row[laddername] ladder. <br><br> No new members can be added to your team.");
            }

        }

    }

    // CHECK ADD PLAYER INFO
    $invitedinfo=mysql_query("SELECT alias,email FROM users WHERE id='$inviteid'");
    $iinfo=mysql_fetch_array($invitedinfo);
    if(!$iinfo[alias]){
        include("$dir[func]/error.php");
        display_error("Unknown Player.<br>");
    }

    //CHECK IF ON TEAM
    if (mysql_num_rows(mysql_query("SELECT playerid FROM teammembers WHERE teamid='$teamid' AND playerid='$inviteid'")) > 0){
        include("$dir[func]/error.php");
        display_error("This Player is already on your team.<br>");
    }

    $ladders=mysql_query("SELECT id,laddername FROM ladders");
    while($row = mysql_fetch_array($ladders)){
        $teams=mysql_query("SELECT id FROM ladder_$row[id] WHERE id='$teamid'");
        if(mysql_fetch_array($teams)){
            $teamson = mysql_query("SELECT teamid FROM teammembers WHERE playerid='$inviteid'");
            while($teamslist = mysql_fetch_array($teamson)){
                $check=mysql_query("SELECT id FROM ladder_$row[id] WHERE id='$teamslist[teamid]'");
                if(mysql_fetch_array($check)){
                    include("$dir[func]/error.php");
                    display_error("This Player is already on a ladder (<a href='$url[base]/$file[ladder]?ladderid=$row[id]'>$row[laddername]</a>)<br> that your team is on. <br><br> Players cannot be on the same ladder more than once.<br>");
                }

            }

        }

    }

    //DO MEMBER ADD
    $tday=date("Y-m-d H:i:s");
    mysql_query("INSERT INTO teammembers VALUES (
    '$teamid',
    '$inviteid',
    '5',
    '$tday');");
    $tablehead=table_head("show","500","","left");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    <br>
    $tablehead
    <center>
    <font class='catfont'><strong>Member Added</strong></font>
    <br>
    <br>
    $iinfo[alias] has been placed on your roster.<br>
    <br>
    </center>
    $tablefoot
    </center>";
    include("$dir[curtheme]");
    //EMAIL ADDED PLAYER
    $emailbody="$iinfo[alias],
    You have just been placed on a team on $site[shortname]! \n
    Team Name: $tinfo[teamname]
    Ladder Name: $linfo[laddername]
    Team Stats: $url[base]/$file[teams]?teamid=$teamid \n
    If you do not wish to be on this team you can remove yourself in the player manager.";
    include("$dir[func]/email.php");
    send_email($iinfo[alias], $iinfo[email], "Team Invitation", $emailbody);
}

?>
