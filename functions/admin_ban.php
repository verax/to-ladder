<?
if(!IsSet($indexloaded)){
    header("Location: ./index.php");
}

//CHECK ADMIN ACCESS LEVEL
if($admn[access] < 99){
    include("$dir[func]/error.php");
    display_error("You are not allowed to perform this function.<br>");
}

function get_bannames(){
    $bantype[0]="[0] Alias Ban - Exact";
    $bantype[1]="[1] Alias Ban - Loose";
    $bantype[2]="[2] Email Ban - Exact";
    $bantype[3]="[3] Email Ban - User";
    $bantype[4]="[4] Email Ban - Host";
    $bantype[5]="[5] IP Ban - Exact";
    $bantype[6]="[6] IP Ban - Loose";
    $bantype[7]="[7] Word Censor - Exact";
    $bantype[8]="[8] Word Censor - Loose";
    return($bantype);
}

function admin_censor(){
    global $dir, $url, $out, $site, $misc;
    $bantype=get_bannames();
    //TABLE ATERNATING COLORS
    $altcolora="#000033";
    $altcolorb="#000020";
    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    <form method='post'>
    <table width='100%' border='0' bordercolor='#000000' cellspacing='0' cellpadding='0' valign='top' align='center'>
    <tr class='altcolor'>
    <td width='100%' valign='center' align='center' colspan='2'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    <strong>Create a New Ban or Censor</strong>
    </font></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Text to Ban or Censor</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='b_text' value='' size='30' maxlength='50'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Type of Ban or Censor</font></td>
    <td width='50%' valign='center' align='center'>
    <select name='b_type'>
    <option value='0'>$bantype[0]</option>
    <option value='1'>$bantype[1]</option>
    <option value='2'>$bantype[2]</option>
    <option value='3'>$bantype[3]</option>
    <option value='4'>$bantype[4]</option>
    <option value='5'>$bantype[5]</option>
    <option value='6'>$bantype[6]</option>
    <option value='7'>$bantype[7]</option>
    <option value='8'>$bantype[8]</option>
    </select></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='top' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Display Reason for Ban<br>200 Characters max<br>(User will see this)</font></td>
    <td width='50%' valign='center' align='center'>
    <textarea name='b_displayreason' rows='3' cols='25' maxlength='200'>
    </textarea></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='top' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Reason for Ban or Censor<br>200 Characters max<br>(Authorized staff will see this)</font></td>
    <td width='50%' valign='center' align='center'>
    <textarea name='b_reason' rows='3' cols='25' maxlength='200'>
    </textarea></td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='center' align='center' colspan='2'>
    <input type='hidden' name='action' value='censorb'>
    <input type='submit' name='' value='Ban It'>
    </td>
    </tr>
    </form>
    </table>
    $tablefoot
    <br>
    $tablehead
    <table width='100%' border='0' bordercolor='#000000' cellspacing='0' cellpadding='0' valign='top' align='center'>
    <tr class='altcolor'>
    <td width='100%' valign='center' align='center' colspan='5'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    <strong>Current Bans and Censors</strong>
    </font></td>
    </tr>
    <tr>
    <td width='30%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    <strong>Ban Type</strong>
    </font></td>
    <td width='35%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    <strong>Text</strong>
    </font></td>
    <td width='10%' valign='center' align='center'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    <strong>BanID</strong>
    </font></td>
    <td width='10%' valign='center' align='center'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    <strong>Edit</strong>
    </font></td>
    <td width='15%' valign='center' align='center'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    <strong>Delete</strong>
    </font></td>
    </tr>";
    $bancen=mysql_query("SELECT id,text,type FROM bancensor ORDER by type");
    while(list($id, $text, $type)=mysql_fetch_row($bancen)){
        if($altladrow=="$altcolora"){
            $altladrow="$altcolorb";
        }else{

            $altladrow="$altcolora";
        }

        $out[body]=$out[body]."
        <tr bgcolor='$altladrow'>
        <td width='30%' valign='center' align='left'>
        <font face='veradna,arial' size='2' color='#FFFFFF'>
        $bantype[$type]
        </font></td>
        <td width='35%' valign='center' align='left'>
        <font face='veradna,arial' size='2' color='#FFFFFF'>
        $text
        </font></td>
        <td width='10%' valign='center' align='center'>
        <font face='veradna,arial' size='2' color='#FFFFFF'>
        $id
        </font></td>
        <form method='post'>
        <td width='10%' valign='center' align='center'>
        <input type='hidden' name='banid' value='$id'>
        <input type='hidden' name='action' value='censorc'>
        <input type='submit' name='' value='Edit'>
        </font></td></form>
        <form method='post'>
        <td width='15%' valign='center' align='center'>
        <script language='javascript'>var confirmdelete='Are you sure you want to DELETE this Ban';</script>
        <input type='hidden' name='banid' value='$id'>
        <input type='hidden' name='action' value='censore'>
        <input type='submit' name='' value='Delete' onClick='return confirm(confirmdelete);'>
        </font></td></form>
        </tr>";
    }

    $out[body]=$out[body]."
    </table>
    $tablefoot
    <br>
    $tablehead
    <table width='100%' border='0' bordercolor='#000000' cellspacing='0' cellpadding='0' valign='top' align='center'>
    <tr class='altcolor'>
    <td width='100%' valign='center' align='center' colspan='2'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    <strong>Ban and Censor Instructions</strong>
    </font></td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='left' colspan='2'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    <strong>INSTRUCTIONS - READ BEFORE USING THIS</strong><br>
    <br>
    Ban Types:<br>
    <li>$bantype[0]<br>
    <ul>
    Prevents a name from being used on signup.
    </ul>
    <li>$bantype[1]<br>
    <ul>
    Prevents a word being used in any name on signup.<br>
    So if you ban killa you also banned armykilla84.<br>
    </ul>
    <li>$bantype[2]<br>
    <ul>
    Prevents a email address (user@host) from signing up.<br>
    </ul>
    <li>$bantype[3]<br>
    <ul>
    Prevents an email user (user@anyhost) from signing up.<br>
    </ul>
    <li>$bantype[4]<br>
    <ul>
    Prevents an email host (anybody@host) from signing up.<br>
    Good if you dont want people using yahoo accounts or something.<br>
    </ul>
    <li>$bantype[5]<br>
    <ul>
    Bans an exact IP Address from any account actions.<br>
    Must be in Exact Format: #.#.#.# .<br>
    </ul>
    <li>$bantype[6]<br>
    <ul>
    Bans a block of IP Address from any account actions.<br>
    Good if you get a trouble user on 56k but Bad because it can affect many other users.<br>
    You can ban a whole block of IP's after the first Number,<br>
    #.#.#.* OR #.#.*.* OR #.*.*.*.<br>
    The more stars you replace numbers with the greater your chances are of banning many people.<br>
    </ul>
    <li>$bantype[7]<br>
    <ul>
    Prevents EXACT words from being used where user comments or other input can be used.<br>
    If you censor \"DOG\" it will appear in a sentance as:<br>
    \"I have a ***, I like dogs and cats\".<br>
    </ul>
    <li>$bantype[8]<br>
    <ul>
    Prevents words from being used where user comments or other input can be used.<br>
    If you censor \"DOG\" it will appear in a sentance as:<br>
    \"I have a ***, I like ***s and cats\".<br>
    </ul>
    <br>
    Note:<br>
    <ul>
    All ban types are case Insensative.<br>
    So if you ban KILLA you also banned killa.<br>
    The Censor Character is: $misc[censorschar]<br>
    </ul>
    <br>
    </font></td>
    </tr>
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function admin_censorb($b_text,$b_type,$b_reason,$b_displayreason){
    global $dir, $url, $out, $admn;
    include("$dir[func]/checkdata.php");
    $b_text=change_charecters($b_text);
    $b_type=change_numbersonly($b_type);
    $b_reason=change_charecters($b_reason);
    $b_displayreason=change_charecters($b_displayreason);
    $errormessage=check_validlength($errormessage, $b_text, "2", "50", "Your Ban/Censor Word Must be 2-25 Characters Long.");
    $errormessage=check_validlength($errormessage, $b_type, "1", "2", "Your Ban Type is Invalid.");
    $errormessage=check_validlength($errormessage, $b_reason, "10", "250", "Your Ban Reason Must be 10-200 Characters.");
    $errormessage=check_validlength($errormessage, $b_displayreason, "10", "250", "Your Ban Display Reason Must be 10-200 Characters.");
    error_check($errormessage);
    $tday=date("Y-m-d H:i:s");
    mysql_query("INSERT INTO bancensor VALUES (NULL,
    '$b_text',
    '$b_type',
    '$b_reason',
    '$b_displayreason',
    '$admn[id]',
    '$tday',
    '0',
    '0');");
    include("$dir[func]/admin_finishmessage.php");
    display_message("The ban was updated.<br>");
}

function admin_censorc($banid){
    global $dir, $url, $out, $admn, $site;
    $thisban=mysql_query("SELECT * FROM bancensor WHERE id='$banid'");
    $ban=mysql_fetch_array($thisban);
    if((!$ban[id]) || (!$banid)){
        include("$dir[func]/error.php");
        display_error("Unable to edit Ban");
    }

    $bantype=get_bannames();
    $btnum="$ban[type]";
    $select[$btnum]="SELECTED";
    //TABLE ATERNATING COLORS
    $altcolora="#000033";
    $altcolorb="#000020";
    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    <form method='post'>
    <table width='100%' border='0' bordercolor='#000000' cellspacing='0' cellpadding='0' valign='top' align='center'>
    <tr class='altcolor'>
    <td width='100%' valign='center' align='center' colspan='2'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    <strong>Edit Ban and Censor</strong>
    </font></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Text to Ban or Censor</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='b_text' value='$ban[text]' size='30' maxlength='50'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Type of Ban or Censor</font></td>
    <td width='50%' valign='center' align='center'>
    <select name='b_type'>
    <option value='0' $select[0]>$bantype[0]</option>
    <option value='1' $select[1]>$bantype[1]</option>
    <option value='2' $select[2]>$bantype[2]</option>
    <option value='3' $select[3]>$bantype[3]</option>
    <option value='4' $select[4]>$bantype[4]</option>
    <option value='5' $select[5]>$bantype[5]</option>
    <option value='6' $select[6]>$bantype[6]</option>
    <option value='7' $select[7]>$bantype[7]</option>
    <option value='8' $select[8]>$bantype[8]</option>
    </select></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='top' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Display Reason for Ban<br>200 Characters max<br>(User will see this)</font></td>
    <td width='50%' valign='center' align='center'>
    <textarea name='b_displayreason' rows='3' cols='25' maxlength='200'>
    $ban[displayreason]</textarea></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='top' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Reason for Ban or Censor<br>200 Characters max<br>(Authorized staff will see this)</font></td>
    <td width='50%' valign='center' align='center'>
    <textarea name='b_reason' rows='3' cols='25' maxlength='200'>
    $ban[reason]</textarea></td>
    </tr>
    <tr class='altcolor'>
    <td width='50%' valign='center' align='center' colspan='2'>
    <input type='hidden' name='banid' value='$ban[id]'>
    <input type='hidden' name='action' value='censord'>
    <input type='submit' name='' value='Update this Ban'>
    </td>
    </tr>
    </form>
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function admin_censord($b_text,$b_type,$b_reason,$b_displayreason,$banid){
    global $dir, $url, $out, $admn;
    include("$dir[func]/checkdata.php");
    $banid=change_numbersonly($banid);
    $b_text=change_charecters($b_text);
    $b_type=change_numbersonly($b_type);
    $b_reason=change_charecters($b_reason);
    $b_displayreason=change_charecters($b_displayreason);
    $errormessage=check_validlength($errormessage, $banid, "1", "10", "Your Ban ID is Invalid.");
    $errormessage=check_validlength($errormessage, $b_text, "2", "50", "Your Ban/Censor Word Must be 2-25 Characters Long.");
    $errormessage=check_validlength($errormessage, $b_type, "1", "2", "Your Ban Type is Invalid.");
    $errormessage=check_validlength($errormessage, $b_reason, "10", "250", "Your Ban Reason Must be 10-200 Characters.");
    $errormessage=check_validlength($errormessage, $b_displayreason, "0", "250", "Your Ban Display Reason Must be 10-200 Characters.");
    error_check($errormessage);
    $tday=date("Y-m-d H:i:s");
    mysql_query("UPDATE bancensor SET
    text='$b_text',
    type='$b_type',
    reason='$b_reason',
    displayreason='$b_displayreason',
    lasteditby='$admn[id]',
    lastedit='$tday'
    WHERE id='$banid'");
    include("$dir[func]/admin_finishmessage.php");
    display_message("The ban was updated.<br>");
}

function admin_censore($banid){
    global $dir;
    mysql_query("DELETE FROM bancensor WHERE id='$banid'");
    include("$dir[func]/admin_finishmessage.php");
    display_message("The ban was deleted.<br>");
}

?>
