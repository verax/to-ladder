<?
function leave_team($teamid){
    global $dir, $file, $url, $out, $plyr, $misc;
    if(!$teamid){
        include("$dir[func]/error.php");
        display_error("Invalid Team ID.<br>");
    }

    $teaminfo=mysql_query("SELECT teamname FROM teams WHERE id='$teamid'");
    $tinfo=mysql_fetch_array($teaminfo);
    if(!$tinfo[teamname]){
        include("$dir[func]/error.php");
        display_error("Unknown Team ID.<br>");
    }

    if (mysql_num_rows(mysql_query("SELECT status FROM teammembers WHERE teamid='$teamid' AND playerid='$plyr[id]'")) < 1){
        include("$dir[func]/error.php");
        display_error("You are not on this team.<br>");
    }

    //CHECK IF OTHER MEMBERS EXIST
    if (mysql_num_rows(mysql_query("SELECT status FROM teammembers WHERE teamid='$teamid' AND playerid!='$plyr[id]'")) < 1){
        if($misc[deleteteam]=="yes"){
            //NO OTHER MEMBERS - TEAM WILL BE DELETED
            $leavemessage="There are no other members on this team.<br>
            If you leave the team it will be deleted.<br>
            Click the button below to delete this team.<br>";
            $buttonsay="<input type='submit' name='' value='Delete this Team'>";
        }else{

            //NO OTHER MEMBERS - CANNOT DELETE TEAM
            $leavemessage="You cannot leave this team.<br>
            There are no other members and deleting of teams is not permitted.<br>";
            $buttonsay="";
        }

    }else{

        //CHECK IF LEADER WILL EXIST
        if (mysql_num_rows(mysql_query("SELECT status FROM teammembers WHERE teamid='$teamid' AND playerid!='$plyr[id]' AND status='1'")) < 1){
            include("$dir[func]/error.php");
            display_error("This team must have a leader.<br>");
        }

        //JUST DELETING TEAM MEMBER
        $leavemessage="Click the button below to confirm you wish to leave this team.<br>";
        $buttonsay="<input type='submit' name='' value='Leave this Team'>";
    }

    $tablehead=table_head("show","500","","left");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    <br>
    $tablehead
    <center>
    <font class='catfont'><strong>Leave Team $tinfo[teamname]</strong></font><br>
    <br>
    $leavemessage
    <br>
    <form method='post'>
    <input type='hidden' name='teamid' value='$teamid'>
    <input type='hidden' name='action' value='leaveb'>
    $buttonsay
    </center>
    $tablefoot
    </form>
    ";
    include("$dir[curtheme]");
}

function leave_teamb($teamid){
    global $dir, $file, $url, $out, $misc, $plyr;
    if(!$teamid){
        include("$dir[func]/error.php");
        display_error("Invalid Team ID.<br>");
    }

    $teaminfo=mysql_query("SELECT teamname FROM teams WHERE id='$teamid'");
    $tinfo=mysql_fetch_array($teaminfo);
    if(!$tinfo[teamname]){
        include("$dir[func]/error.php");
        display_error("Unknown Team ID.<br>");
    }

    if (mysql_num_rows(mysql_query("SELECT status FROM teammembers WHERE teamid='$teamid' AND playerid='$plyr[id]'")) < 1){
        include("$dir[func]/error.php");
        display_error("You are not on this team.<br>");
    }

    //CHECK IF OTHER MEMBERS EXIST
    if (mysql_num_rows(mysql_query("SELECT status FROM teammembers WHERE teamid='$teamid' AND playerid!='$plyr[id]'")) < 1){
        delete_team($teamid);
    }else{

        //CHECK IF LEADER WILL EXIST
        if (mysql_num_rows(mysql_query("SELECT status FROM teammembers WHERE teamid='$teamid' AND playerid!='$plyr[id]' AND status='1'")) < 1){
            include("$dir[func]/error.php");
            display_error("This team must have a leader.<br>");
        }

        delete_member($teamid);
    }

}

function delete_member($teamid){
    global $dir, $plyr, $site;
    mysql_query("DELETE FROM teammembers WHERE teamid='$teamid' AND playerid='$plyr[id]'");
    include("$dir[func]/finishmessage.php");
    display_message("You have been removed from this team",$site[home]);
}

function delete_team($teamid){
    global $dir;
    mysql_query("DELETE FROM teammembers WHERE teamid='$teamid'");
    include("$dir[func]/finishmessage.php");
    display_message("Your team has been deleted","phome");
}

?>
