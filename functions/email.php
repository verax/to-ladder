<?PHP
function send_email($toname, $toemail, $emailsubject, $emailbody){
    global $email;
    if($email[localhost]=="yes"){
        sendmail_local($toname, $toemail, $emailsubject, $emailbody);
    }else{

        sendmail_server($toname, $toemail, $emailsubject, $emailbody);
    }

}

//SEND EMAIL FROM LOCAL HOST
function sendmail_local($toname, $toemail, $emailsubject, $emailbody){
    global $email, $site;
    $subject="$site[shortname] $emailsubject";
    $body="$emailbody\n\n$site[longname]\n$site[homeurl]\n";
    $fromname="$site[shortname]";
    $fromemail="$email[sendaccount]";
    $replyname="$site[shortname]";
    $replyemail="$email[genhelp]";
    mail($toemail,$subject,$body, "From: \"$fromname\" <$fromemail>");
}

function sendmail_server($toname, $toemail, $emailsubject, $emailbody){
    global $email, $site;
    $smtp_server = $email[smtp_server];
    $smtp_port = 25;
    $from = "$email[sendaccount]";
    $emailbody = str_replace("\n", "\r\n", $emailbody);
    $body="$emailbody\r\n$site[longname]\r\n$site[homeurl]\r\n";
    $fp = fsockopen($smtp_server, $smtp_port, &$errno, &$errstr, 50);
    socket_set_blocking($fp,-1);
    fwrite($fp, "HELO ". $smtp_server ."\r\n");
    if(($email[username])&&($email[password])){
        fwrite($fp, "AUTH LOGIN\r\n");
        fwrite($fp, base64_encode($email[username])."\r\n");
        fwrite($fp, base64_encode($email[password])."\r\n");
    }

    $reply = fgets($fp, 512);
    $reply = fgets($fp, 512);
    fwrite($fp, "MAIL FROM:<" . $from . ">\r\n");
    $reply = fgets($fp, 512);
    fwrite($fp, "RCPT TO:<". $toemail .">\r\n");
    $reply = fgets($fp, 512);
    fwrite($fp, "DATA\r\n");
    $reply = fgets($fp, 512);
    fwrite($fp, "From: $from\r\n");
    fwrite($fp, "To: $toemail\r\n");
    fwrite($fp, "Subject: $emailsubject\r\n");
    fwrite($fp, "$body\r\n.\r\n");
    fwrite($fp, "RSET\r\n");
    fwrite($fp, "NOOP\r\n");
    fwrite($fp, "QUIT");
    fclose($fp);
}

?>
