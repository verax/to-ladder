<?
if(!IsSet($indexloaded)){
    header("Location: ./index.php");
}

//CHECK ADMIN ACCESS LEVEL
if($admn[access] < 65){
    include("$dir[func]/error.php");
    display_error("You are not allowed to perform this function.<br>");
}

function get_rights($giversrights,$currentrights){
    $selected[$currentrights]="SELECTED";
    $getrights="
    <option value='0' $selected[0]>Cant Login</option>
    <option value='5' $selected[5]>Resend/Delete Validations</option>
    <option value='10' $selected[10]>Create Teams</option>
    <option value='15' $selected[15]>Edit Teams</option>
    <option value='20' $selected[20]>Delete Teams</option>
    <option value='25' $selected[25]>Report Match</option>
    <option value='30' $selected[30]>Edit/Delete Match</option>
    <option value='35' $selected[35]>Delete Challenges</option>
    <option value='40' $selected[40]>Edit Players</option>
    <option value='45' $selected[45]>Edit Players Pass</option>
    <option value='50' $selected[50]>Delete Players</option>
    <option value='55' $selected[55]>Manage Maps</option>
    <option value='60' $selected[60]>Manage Rules</option>
    <option value='63' $selected[63]>Manage Tournaments</option>
    <option value='65' $selected[65]>Manage Staff</option>";
    if($giversrights >= 70){$getrights=$getrights."<option value='70' $selected[70]>Manage all Maps</option>";}

    if($giversrights >= 75){$getrights=$getrights."<option value='75' $selected[75]>Manage all Rules</option>";}

    if($giversrights >= 80){$getrights=$getrights."<option value='80' $selected[80]>Manage all Staff</option>";}

    if($giversrights >= 85){$getrights=$getrights."<option value='85' $selected[85]>Manage Faq/Files</option>";}

    if($giversrights >= 90){$getrights=$getrights."<option value='90' $selected[90]>Manage Ladders</option>";}

    if($giversrights >= 95){$getrights=$getrights."<option value='95' $selected[95]>Manage Games</option>";}

    if($giversrights >= 99){$getrights=$getrights."<option value='99' $selected[99]>Master God Mode</option>";}

    return($getrights);
}

function admin_editstaff(){
    global $dir, $url, $out, $site, $admn;
    $thestaff=mysql_query("SELECT id,displayname,access,addedby FROM staff ORDER by displayname");
    while(list($id,$name,$access,$addedby)=mysql_fetch_row($thestaff)){
        if($access <= $admn[access]){
            if($admn[access] < 80){
                $gotname=0;
                $staffladders=mysql_query("SELECT ladderid FROM staffaccess WHERE staffid='$admn[id]'");
                while(list($lid)=mysql_fetch_row($staffladders)){
                    if(!$gotname){
                        if(mysql_num_rows(mysql_query("SELECT ladderid FROM staffaccess WHERE staffid='$id' AND ladderid='$lid'")) > 0){
                            $staffmembers=$staffmembers."<option value='$id'>$name</option>";
                            $howmanyeditable=1;
                            $gotname=1;
                        }

                    }

                }

            }else{

                $staffmembers=$staffmembers."<option value='$id'>$name</option>";
                $howmanyeditable=1;
            }

        }

    }

    if(!$howmanyeditable){
        include("$dir[func]/error.php");
        display_error("There are no staff accounts to edit.<br>");
    }

    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <form method='post'>
    <td width='100%' valign='top' align='center'>
    <strong>Edit Staff</strong><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <br>
    <select name='staffid'>$staffmembers</select><br>
    <br></td>
    </tr>
    <tr class='altcolor'>
    <script language='javascript'>var confirmdelete='Are you sure you want to DELETE this Staff Member';</script>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='action' value='editstaffb'>
    <input type='submit' name='todo' value='Edit Staff'>
    <input type='submit' name='todo' value='Delete Staff'>
    </td>
    </form>
    </tr>
    </table>
    <br><center>";
    include("$dir[curtheme]");
}

function admin_editstaffb($staffid,$todo){
    global $dir, $url, $out, $site, $admn;
    if(!$staffid){
        include("$dir[func]/error.php");
        display_error("Invalid Staff ID.<br>");
    }

    if($todo=="Edit Staff"){
        edit_staffmember($staffid);
    }

    else if($todo=="Delete Staff"){
        delete_staff($staffid);
    }

    else{
        include("$dir[func]/error.php");
        display_error("Unknown Command.<br>");
    }

}

function edit_staffmember($staffid){
    global $dir, $url, $out, $site, $admn;
    //TABLE ATERNATING COLORS
    $altcolora="#000033";
    $altcolorb="#000020";
    $result=mysql_query("SELECT * FROM staff WHERE id='$staffid' AND access <='$admn[access]'");
    $staff=mysql_fetch_array($result);
    if(!$staff[id]){
        include("$dir[func]/error.php");
        display_error("Unable to edit that Staff Member");
    }
    
    $selected="";
    $value="";
    $value=$staff[country];
    $selected[$value]="SELECTED";

    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <strong>Edit Staff - [$staffid] $staff[displayname]</strong></td>
    </tr>
    <tr>
    <td width='100%' valign='top' align='left'>
    <form method='post'>
    <table width='100%' border='0' cellspacing='0' cellpadding='0' valign='top' align='center'>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Display Name</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='s_name' value='$staff[displayname]' size='30' maxlength='25'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Password</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='password' name='s_pass' value='$staff[pass]' size='30' maxlength='25'></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Email</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='s_email' value='$staff[email]' size='30' maxlength='100'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Title</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='s_title' value='$staff[title]' size='30' maxlength='25'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Country</font></td>
    <td width='50%' valign='center' align='center'>
        <select name='p_country'>
                <option value='AF' $selected[AF]>Afghanistan
        <option value='AL' $selected[AL]>Albania
        <option value='DZ' $selected[DZ]>Algeria
        <option value='AD' $selected[AD]>Andorra
        <option value='AO' $selected[AO]>Angola
        <option value='AG' $selected[AG]>Antigua and Barbuda
        <option value='AR' $selected[AR]>Argentina
        <option value='AM' $selected[AM]>Armenia
        <option value='AU' $selected[AU]>Australia
        <option value='AT' $selected[AT]>Austria
        <option value='AZ' $selected[AZ]>Azerbaijan
        <option value='BS' $selected[BS]>Bahamas
        <option value='BH' $selected[BH]>Bahrain
        <option value='BD' $selected[BD]>Bangladesh
        <option value='BB' $selected[BB]>Barbados
        <option value='BY' $selected[BY]>Belarus
        <option value='BE' $selected[BE]>Belgium
        <option value='BZ' $selected[BZ]>Belize
        <option value='BJ' $selected[BJ]>Benin
        <option value='BT' $selected[BT]>Bhutan
        <option value='BO' $selected[BO]>Bolivia
        <option value='BA' $selected[BA]>Bosnia Herzegovina
        <option value='BW' $selected[BW]>Botswana
        <option value='BR' $selected[BR]>Brazil
        <option value='BN' $selected[BN]>Brunei
        <option value='BG' $selected[BG]>Bulgaria
        <option value='BF' $selected[BF]>Burkina Faso
        <option value='BM' $selected[BM]>Burma
        <option value='BI' $selected[BI]>Burundi
        <option value='KH' $selected[KH]>Cambodia
        <option value='CM' $selected[CM]>Cameroon
        <option value='CA' $selected[CA]>Canada
        <option value='CF' $selected[CF]>Central African Republic
        <option value='TD' $selected[TD]>Chad
        <option value='CL' $selected[CL]>Chile
        <option value='CN' $selected[CN]>China
        <option value='CX' $selected[CX]>Christmas Island
        <option value='CO' $selected[CO]>Colombia
        <option value='KM' $selected[KM]>Comoros
        <option value='CG' $selected[CG]>Congo
        <option value='CR' $selected[CR]>Costa Rica
        <option value='HR' $selected[HR]>Croatia
        <option value='CU' $selected[CU]>Cuba
        <option value='CY' $selected[CY]>Cyprus
        <option value='CZ' $selected[CZ]>Czech Republic
        <option value='DC' $selected[DC]>Democratic Rep. Congo
        <option value='DK' $selected[DK]>Denmark
        <option value='DJ' $selected[DJ]>Djibouti
        <option value='DM' $selected[DM]>Dominica
        <option value='DO' $selected[DO]>Dominican Republic
        <option value='EC' $selected[EC]>Ecuador
        <option value='EG' $selected[EG]>Egypt
        <option value='SV' $selected[SV]>El Salvador
        <option value='GQ' $selected[GQ]>Equatorial Guinea
        <option value='ER' $selected[ER]>Eritrea
        <option value='EE' $selected[EE]>Estonia
        <option value='ET' $selected[ET]>Ethiopia
        <option value='EU' $selected[EU]>European Union
        <option value='FS' $selected[FS]>Fed. States Micronesia
        <option value='FJ' $selected[FJ]>Fiji
        <option value='FI' $selected[FI]>Finland
        <option value='FR' $selected[FR]>France
        <option value='GA' $selected[GA]>Gabon
        <option value='GM' $selected[GM]>Gambia
        <option value='GE' $selected[GE]>Georgia
        <option value='DE' $selected[DE]>Germany
        <option value='GH' $selected[GH]>Ghana
        <option value='GR' $selected[GR]>Greece
        <option value='GD' $selected[GD]>Grenada
        <option value='GT' $selected[GT]>Guatemala
        <option value='GN' $selected[GN]>Guinea
        <option value='GW' $selected[GW]>Guinea-Bissau
        <option value='GY' $selected[GY]>Guyana
        <option value='HT' $selected[HT]>Haiti
        <option value='HN' $selected[HN]>Honduras
        <option value='HK' $selected[HK]>Hong Kong
        <option value='HU' $selected[HU]>Hungary
        <option value='IS' $selected[IS]>Iceland
        <option value='IN' $selected[IN]>India
        <option value='ID' $selected[ID]>Indonesia
        <option value='IR' $selected[IR]>Iran
        <option value='IQ' $selected[IQ]>Iraq
        <option value='IE' $selected[IE]>Ireland
        <option value='IL' $selected[IL]>Israel
        <option value='IT' $selected[IT]>Italy
        <option value='IV' $selected[IV]>Ivory Coast
        <option value='JM' $selected[JM]>Jamaica
        <option value='JP' $selected[JP]>Japan
        <option value='JO' $selected[JO]>Jordan
        <option value='KZ' $selected[KZ]>Kazakhstan
        <option value='KE' $selected[KE]>Kenya
        <option value='KI' $selected[KI]>Kiribati
        <option value='KW' $selected[KW]>Kuwait
        <option value='KG' $selected[KG]>Kyrgyzstan
        <option value='LA' $selected[LA]>Laos
        <option value='LV' $selected[LV]>Latvia
        <option value='LB' $selected[LB]>Lebanon
        <option value='LR' $selected[LR]>Liberia
        <option value='LY' $selected[LY]>Libya
        <option value='LI' $selected[LI]>Liechtenstein
        <option value='LT' $selected[LT]>Lithuania
        <option value='LU' $selected[LU]>Luxembourg
        <option value='MO' $selected[MO]>Macau
        <option value='MK' $selected[MK]>Macedonia
        <option value='MG' $selected[MG]>Madagascar
        <option value='MW' $selected[MW]>Malawi
        <option value='MY' $selected[MY]>Malaysia
        <option value='MV' $selected[MV]>Maldives
        <option value='ML' $selected[ML]>Mali
        <option value='MT' $selected[MT]>Malta
        <option value='MR' $selected[MR]>Mauritania
        <option value='MU' $selected[MU]>Mauritius
        <option value='MX' $selected[MX]>Mexico
        <option value='MD' $selected[MD]>Moldova
        <option value='MC' $selected[MC]>Monaco
        <option value='MN' $selected[MN]>Mongolia
        <option value='MA' $selected[MA]>Morocco
        <option value='MZ' $selected[MZ]>Mozambique
        <option value='NA' $selected[NA]>Namibia
        <option value='NR' $selected[NR]>Nauru
        <option value='NP' $selected[NP]>Nepal
        <option value='NL' $selected[NL]>Netherlands
        <option value='AN' $selected[AN]>Netherlands Antilles
        <option value='NZ' $selected[NZ]>New Zealand
        <option value='NI' $selected[NI]>Nicaragua
        <option value='NE' $selected[NE]>Niger
        <option value='NG' $selected[NG]>Nigeria
        <option value='NK' $selected[NK]>North Korea
        <option value='NO' $selected[NO]>Norway
        <option value='OM' $selected[OM]>Oman
        <option value='PK' $selected[PK]>Pakistan
        <option value='PA' $selected[PA]>Panama
        <option value='PG' $selected[PG]>Papua New Guinea
        <option value='PY' $selected[PY]>Paraguay
        <option value='PE' $selected[PE]>Peru
        <option value='PH' $selected[PH]>Philippines
        <option value='PL' $selected[PL]>Poland
        <option value='PT' $selected[PT]>Portugal
        <option value='PR' $selected[PR]>Puerto Rico
        <option value='QA' $selected[QA]>Qatar
        <option value='RO' $selected[RO]>Romania
        <option value='RU' $selected[RU]>Russia
        <option value='RW' $selected[RW]>Rwanda
        <option value='KN' $selected[KN]>Saint Kitts and Nevis
        <option value='LC' $selected[LC]>Saint Lucia
        <option value='VC' $selected[VC]>Saint Vincent/Grenadines
        <option value='ST' $selected[ST]>Sao Tome and Principe
        <option value='SA' $selected[SA]>Saudi Arabia
        <option value='SN' $selected[SN]>Senegal
        <option value='SC' $selected[SC]>Seychelles
        <option value='SL' $selected[SL]>Sierra Leone
        <option value='SG' $selected[SG]>Singapore
        <option value='SK' $selected[SK]>Slovakia
        <option value='SI' $selected[SI]>Slovenia
        <option value='SB' $selected[SB]>Solomon Islands
        <option value='SO' $selected[SO]>Somalia
        <option value='ZA' $selected[ZA]>South Africa
        <option value='KS' $selected[KS]>South Korea
        <option value='ES' $selected[ES]>Spain
        <option value='LK' $selected[LK]>Sri Lanka
        <option value='SD' $selected[SD]>Sudan
        <option value='SR' $selected[SR]>Suriname
        <option value='SE' $selected[SE]>Sweden
        <option value='CH' $selected[CH]>Switzerland
        <option value='SY' $selected[SY]>Syria
        <option value='TW' $selected[TW]>Taiwan
        <option value='TJ' $selected[TJ]>Tajikistan
        <option value='TZ' $selected[TZ]>Tanzania
        <option value='TH' $selected[TH]>Thailand
        <option value='TG' $selected[TG]>Togo
        <option value='TO' $selected[TO]>Tonga
        <option value='TT' $selected[TT]>Trinidad and Tobago
        <option value='TN' $selected[TN]>Tunisia
        <option value='TR' $selected[TR]>Turkey
        <option value='TM' $selected[TM]>Turkmenistan
        <option value='TV' $selected[TV]>Tuvalu
        <option value='UG' $selected[UG]>Uganda
        <option value='UA' $selected[UA]>Ukraine
        <option value='AE' $selected[AE]>United Arab Emirates
        <option value='GB' $selected[GB]>United Kingdom
        <option value='US' $selected[US]>United States Of America
        <option value='UY' $selected[UY]>Uruguay
        <option value='UZ' $selected[UZ]>Uzbekistan
        <option value='VU' $selected[VU]>Vanuatu
        <option value='VE' $selected[VE]>Venezuela
        <option value='VN' $selected[VN]>Viet Nam
        <option value='EH' $selected[EH]>Western Samoa
        <option value='YE' $selected[YE]>Yemen
        <option value='YU' $selected[YU]>Yugoslavia
        <option value='ZM' $selected[ZM]>Zambia
        <option value='ZW' $selected[ZW]>Zimbabwe
        </select>
    </td>
    </tr>

    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Contact Icq/Aim</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='s_contact' value='$staff[contact]' size='30' maxlength='25'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Access</font></td>
    <td width='50%' valign='center' align='center'>
    <select name='s_access'>";
    $getrights=get_rights($admn[access],$staff[access]);
    $out[body]=$out[body]."$getrights
    </select></td>
    </tr></td>
    </tr>
    </table>
    </td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <strong>Ladders</strong><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <table width='100%' border='0' cellspacing='0' cellpadding='0'>
    ";
    //GET LADDERS
    $ladderslist=mysql_query("SELECT id,laddername  FROM ladders ORDER by laddername");
    while(list($id,$name)=mysql_fetch_row($ladderslist)){
        if((mysql_num_rows(mysql_query("SELECT staffid FROM staffaccess WHERE staffid ='$admn[id]' AND ladderid='$id' OR staffid ='$staffid' AND ladderid='$id'")) > 0) || ($admn[access] >= 80)){
            if(mysql_num_rows(mysql_query("SELECT staffid FROM staffaccess WHERE staffid ='$staffid' AND ladderid='$id'")) > 0){
                $checkeda="CHECKED";
                $checkedb="";
            }else{

                $checkeda="";
                $checkedb="CHECKED";
            }

            if($altladrow=="$altcolora"){
                $altladrow="$altcolorb";
            }else{

                $altladrow="$altcolora";
            }

            $out[body]=$out[body]."
            <tr bgcolor='$altladrow'>
            <td width='50%' valign='center' align='left'>
            $name</font></td>
            <td width='50%' valign='center' align='center'>
            Yes <input type='radio' name='ladders[$id]' value='1' $checkeda>
            No  <input type='radio' name='ladders[$id]' value='0' $checkedb></td>
            </tr>";
            $howmanyeditable=1;
        }

    }

    $out[body]=$out[body]."</table>
    </td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='action' value='editstaffc'>
    <input type='hidden' name='staffid' value='$staffid'>
    <input type='submit' name='' value='Edit Staff'>
    <input type='reset' name='' value='Reset'></td>
    </form>
    </tr>
    </table>";
    include("$dir[curtheme]");
}

function admin_editstaffc($staffid,$s_name,$s_pass,$s_email,$s_title,$s_country,$s_contact,$s_access,$ladders){
    global $dir, $url, $out, $site, $admn;
    include("$dir[func]/checkdata.php");
    $s_name=change_charecters($s_name);
    $s_pass=change_charecters($s_pass);
    $s_email=change_charecters($s_email);
    $s_title=change_charecters($s_title);
    $s_contact=change_charecters($s_contact);
    $s_access=change_numbersonly($s_access);
    $errormessage=check_validlength($errormessage, $s_name, "3", "50", "Staff names must be 3-25 characters.");
    $errormessage=check_validlength($errormessage, $s_pass, "2", "25", "Staff passwords must be 2-10 characters.");
    $errormessage=check_validlength($errormessage, $s_title, "3", "25", "Staff titles must be 3-25 characters.");
    $errormessage=check_staffexistother($errormessage, $s_name, $staffid);
    $errormessage=check_givestaffrights($errormessage, $s_access);
    error_check($errormessage);
    //DONT ALLOW SELF ACCESS EDITING
    if($staffid=="$admn[id]"){
        $s_access="$admn[access]";
    }

    $tday=date("Y-m-d H:i:s");
    mysql_query("UPDATE staff SET
    displayname='$s_name',
    pass='$s_pass',
    email='$s_email',
    title='$s_title',
    access='$s_access',
    contact='$s_contact',
    lasteditby='$admn[id]',
    lastedit='$tday'
    WHERE id='$staffid'");
    if($ladders){
        while(list($ladder,$value)=each($ladders)){
            if (mysql_num_rows(mysql_query("SELECT staffid FROM staffaccess WHERE staffid='$staffid' AND ladderid='$ladder'")) > 0){
                if(!$value){
                    mysql_query("DELETE from staffaccess WHERE staffid='$staffid' AND ladderid='$ladder'");
                }

            }else{

                if($value){
                    mysql_query("INSERT INTO staffaccess VALUES (
                    '$staffid',
                    '$ladder',
                    '0',
                    '$tday');");
                }

            }

        }

    }

    include("$dir[func]/admin_finishmessage.php");
    display_message("The staff member was updated.<br>");
}

function delete_staff($staffid){
    global $dir;
    mysql_query("DELETE FROM staffaccess WHERE staffid='$staffid'");
    mysql_query("DELETE FROM staff WHERE id='$staffid'");
    include("$dir[func]/admin_finishmessage.php");
    display_message("The staff member was deleted.<br>");
}

?>
