<?PHP
if(!IsSet($indexloaded)){
    header("Location: ./index.php");
}

if($admn[access] < 85){
    include("$dir[func]/error.php");
    display_error("You are not allowed to perform this function.<br>");
}

function admin_news(){
    global $dir, $url, $out, $site, $admn;
    $newsheadlineslist=mysql_query("SELECT id,headline  FROM news ORDER by headline");
    while(list($id,$name)=mysql_fetch_row($newsheadlineslist)){
        $thenews=$thenews."<option value='$id'>$name</option>";
    }

    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <form method='post'>
    <td width='100%' valign='top' align='center'>
    <strong>News</strong><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <br>
    <script language='javascript'>var confirmdelete='Are you sure you want to DELETE this News Story';</script>
    <select name='newsid'>
    <option value=''>Select a news story or create a new one.</option>
    $thenews</select><br>
    <br></td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='action' value='newsb'>
    <input type='submit' name='todo' value='Create News'>
    <input type='submit' name='todo' value='Edit News'>
    <input type='submit' name='todo' value='Delete News' onClick='return confirm(confirmdelete);'>
    </td>
    </form>
    </tr>
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function admin_newsb($newsid,$todo){
    global $dir, $url, $out, $site, $admn;
    if((!$newsid) && ($todo!="Create News")){
        include("$dir[func]/error.php");
        display_error("Invalid Story ID.<br>");
    }

    if($todo == "Create News"){
        create_news();
    }

    else if($todo == "Edit News"){
        edit_news($newsid);
    }

    else if($todo=="Delete News"){
        delete_news($newsid);
        include("$dir[func]/admin_finishmessage.php");
        display_message("The news story was deleted.<br>");
    }

    else{
        include("$dir[func]/error.php");
        display_error("Unknown Command.<br>");
    }

}

function create_news(){
    global $dir, $url, $out, $site, $admn;
    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <form method='post'>
    <td width='100%' valign='top' align='center'>
    <strong>Create News Story</strong><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <br>
    Headline<br>
    <input type='text' name='headline' size='75' value='' maxlength='80'><br>
    <br>
    Story<br>
    <textarea name='content' rows='10' cols='75'></textarea>
    <br>
    Bcode:<br>
    {b}<b>BOLD</b>{/b}<br>

    {i}<i>Italic</i>{/i}<br>

    {font color=red}<font color='#FF0000'>red</font>{/font}

    <br><br></td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='action' value='newsc'>
    <input type='submit' name='' value='Create News'></td>
    </form>
    </tr>
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function admin_newsc($headline,$content){
    global $dir, $url, $out, $site, $admn;
    include("$dir[func]/checkdata.php");
    $errormessage=check_validlength($errormessage, $headline, "0", "80", "Your headline must not exceed 40 characters");
    $errormessage=check_validlength($errormessage, $content, "0", "5000", "Your story must not exceed 5000 characters");
    error_check($errormessage);
    $headline = change_charecters($headline);
    $content = change_fromlinebreaks($content);
    $content = change_charecters($content);
    $content = change_tocharectercode($content);
    $tday=date("Y-m-d H:i:s");
    mysql_query("INSERT INTO news VALUES (NULL, '$headline', '$content', '$admn[id]', '$tday');");
    include("$dir[func]/admin_finishmessage.php");
    display_message("The news story has been created.<br>");
}

function edit_news($newsid){
    global $dir, $url, $out, $site, $admn;
    $newsinfo=mysql_query("SELECT * FROM news WHERE id='$newsid'");
    $news=mysql_fetch_array($newsinfo);
    if(!$news[id]){
        include("$dir[func]/error.php");
        display_error("Unknown News Story ID.<br>");
    }

    include("$dir[func]/checkdata.php");
    $news[content]=change_tolinebreaks($news[content]);
    $news[content]=change_fromcharectercode($news[content]);
    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <form method='post'>
    <td width='100%' valign='top' align='center'>
    <strong>Edit a News Story</strong><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <br>
    Headline<br>
    <input type='text' name='headline' size='75' value='$news[headline]' maxlength='80'><br>
    <br>
    Story<br>
    <textarea name='content' rows='10' cols='75'>$news[content]</textarea>
    <br>
    Bcode:<br>
    {b}<b>BOLD</b>{/b}<br>

    {i}<i>Italic</i>{/i}<br>

    {font color=red}<font color='#FF0000'>red</font>{/font}

    <br><br></td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='action' value='newsd'>
    <input type='hidden' name='newsid' value='$newsid'>
    <input type='submit' name='' value='Edit News'></td>
    </form>
    </tr>
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function admin_newsd($newsid,$headline,$content){
    global $dir, $url, $out, $site, $admn;
    if(!$newsid){
        include("$dir[func]/error.php");
        display_error("Invalid News Story ID.<br>");
    }

    include("$dir[func]/checkdata.php");
    $errormessage=check_validlength($errormessage, $headline, "0", "80", "Your headline must not exceed 80 characters");
    $errormessage=check_validlength($errormessage, $content, "0", "5000", "Your message body must not exceed 5000 characters");
    error_check($errormessage);
    $headline = change_charecters($headline);
    $content = change_fromlinebreaks($content);
    $content = change_charecters($content);
    $content = change_tocharectercode($content);
    $tday=date("Y-m-d H:i:s");
    mysql_query("UPDATE news SET headline='$headline', content='$content', lasteditedby='$admn[id]', lastedit='$tday' WHERE id='$newsid'");
    include("$dir[func]/admin_finishmessage.php");
    display_message("The news story has been updated.<br>");
}

function delete_news($newsid){
    mysql_query("DELETE FROM news WHERE id='$newsid'");
}

function admin_newsletter() {
    global $dir, $url, $out, $site, $admn;
    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <form method='post'>
    <td width='100%' valign='top' align='center'>
    <strong>Create Newsletter</strong><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <br>
    Subject<br>
    <input type='text' name='subject' size='75' value='' maxlength='80'><br>
    <br>
    Message<br>
    <textarea name='message' rows='10' cols='75'></textarea>
    <br>
    <br><br></td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='action' value='newsletterb'>
    <input type='submit' name='' value='Preview Newsletter'></td>
    </form>
    </tr>
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function admin_newsletterb($subject,$message) {
    global $dir, $url, $out, $site, $admn;
    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $ladderslist=mysql_query("SELECT id,laddername FROM ladders ORDER by laddername");
    while(list($id,$name)=mysql_fetch_row($ladderslist)){
        $selectedladder="";
        $theladders.="<option value='$id'>All teams on: $name</option>";
    }

    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    <table width='70%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <form method='post'>
    <td width='100%' valign='top' align='center'>
    <strong>Send Newsletter</strong><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='top' align='center'>
    &nbsp; &nbsp;<strong><font class='catfont'>$subject</font></strong> <br></i>
    <hr class='catfont' size='1'>
    <textarea rows='10' cols='75' name='message'>$message</textarea><br>
    <hr class='catfont' size='1'>
    <br><center>
    Send To: <select name='sendto'><option value=''>Select Recipients</option><option value='0'>All Users</option>$theladders</select><br><br>
    </td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='subject' value='$subject'>
    <input type='hidden' name='message' value='$message'>
    <input type='hidden' name='status' value='the'>
    <input type='hidden' name='action' value='newsletterc'>
    <input type='submit' name='' value='Send Newsletter'></td>
    </form>
    </tr>
    </table>
    <br><center>
    $tablefoot";
    include("$dir[curtheme]");
}

function admin_newsletterc($subject,$message,$sendto) {
    global $dir, $url, $out, $site, $admn;
    if($sendto < 0){
        include("$dir[func]/error.php");
        display_error("You need to select a group of people to send to.<br>");
    }

    if(!$message){
        include("$dir[func]/error.php");
        display_error("Message is blank. You cannot send a blank message.<br>");
    }

    if(!$subject){
        include("$dir[func]/error.php");
        display_error("No Subject specified. You cannot send a message without a subject.<br>");
    }

    include("$dir[func]/email.php");
    if($sendto == '0'){
        $tolist=mysql_query("SELECT alias,email FROM users");
        while(list($alias,$email)=mysql_fetch_row($tolist)){
            send_email($alias,$email,$subject,$message);
        }

    }else{

        $teamids = mysql_query("SELECT id FROM ladder_$sendto");
        while($teams = mysql_fetch_array($teamids)){
            $teaminfo = mysql_fetch_array(mysql_query("SELECT teamname,teamemail FROM teams WHERE id ='$teams[id]'"));
            send_email($teaminfo[teamname],$teaminfo[teamemail],$subject,$message);
        }

    }

    include("$dir[func]/admin_finishmessage.php");
    display_message("The newsletter has been sent to the teams on the ladder you specified.<br><br>If you specified all users, then every user in the database will have been sent the message.<br>");
}

?>
