<?
if(!IsSet($indexloaded)){
    header("Location: ./index.php");
}

function challenge_finalize($challid){
    global $dir, $file, $url, $out, $plyr;
    if(!$challid){
        include("$dir[func]/error.php");
        display_error("Invalid Challenge ID.<br>");
    }

    //FIND AND DELETE/AWARD WINS FOR UNFINALIZED CHALLENGES
    include("$dir[func]/challengeforfeit.php");
    check_unrespondedchallenges();
    $challengeinfo=mysql_query("SELECT * FROM challenges WHERE challid='$challid'");
    $chall=mysql_fetch_array($challengeinfo);
    if(!$chall[challenger]){
        include("$dir[func]/error.php");
        display_error("Unknown Challenge ID.<br>");
    }

    if($chall[finalizedby] > 0){
        include("$dir[func]/error.php");
        display_error("This match has already been finalized.<br>");
    }

    if($chall[respondedby] < 1){
        include("$dir[func]/error.php");
        display_error("This match hasnt been responed to yet.<br>");
    }

    if(mysql_num_rows(mysql_query("SELECT teamid FROM teammembers WHERE playerid='$plyr[id]' AND teamid='$chall[challenger]' AND status <= '3'")) < 1){
        include("$dir[func]/error.php");
        display_error("You cannot finalize challenges for this team.<br>");
    }

    $challengerinfo=mysql_query("SELECT teamname FROM teams WHERE id='$chall[challenger]'");
    $chgrnfo=mysql_fetch_array($challengerinfo);
    if(!$chgrnfo[teamname]){
        include("$dir[func]/error.php");
        display_error("Unable to find your team.<br>");
    }

    $challengedinfo=mysql_query("SELECT teamname FROM teams WHERE id='$chall[challenged]' ");
    $chgdnfo=mysql_fetch_array($challengedinfo);
    if(!$chgdnfo[teamname]){
        include("$dir[func]/error.php");
        display_error("Unable to find the challenged teams information.<br>");
    }

    $ladderinfo=mysql_query("SELECT * FROM ladders WHERE id='$chall[ladderid]'");
    $linfo=mysql_fetch_array($ladderinfo);
    //GET SELECTED TIMES
    $availmatchtimes=split(",",$chall[matchtimes]);
    while(list($countimes,$matchtimes)=each($availmatchtimes)){
        if($matchtimes){
            $countimes=($countimes+1);
            $timesavailable=$timesavailable."<option value='$countimes'>$matchtimes</a>";
            $matchtimesfound=1;
        }

    }

    if($matchtimesfound){
        $timeoptions=$timeoptions."
        <tr class='altcolora'>
        <td width='50%' valign='center' align='left'>
        <font face='veradna,arial' size='2' color='#FFFFFF'>Match Time</font></td>
        <td width='50%' valign='center' align='center'>
        <select name='time'>$timesavailable</select></td>
        </tr>";
    }else{

        include("$dir[func]/error.php");
        display_error("Invalid match times.<br>Contact Staff.<br>");
    }

    //GET SELECTED MAPS
    $availmatchmaps=split(",",$chall[matchmaps]);
    while(list($countmaps,$matchmaps)=each($availmatchmaps)){
        if($matchmaps){
            $countmaps=($countmaps+1);
            $mapsinfo=mysql_query("SELECT mapname FROM maps WHERE id='$matchmaps'");
            $minfo=mysql_fetch_array($mapsinfo);
            $maplist=$maplist."
            <tr class='altcolora'>
            <td width='50%' valign='center' align='left'>
            <font face='veradna,arial' size='2' color='#FFFFFF'>#$countmaps
            <a href='$url[base]/$file[maps]?mapid=$matchmaps' target='top'>$minfo[mapname]</a></font></td>
            <td width='50%' valign='center' align='center'>
            Play <input type='checkbox' name='map[$countmaps]' value='$matchmaps'></td>
            </tr>";
            $matchmapsfound=1;
        }

    }

    $out[body]=$out[body]."
    <br><br><br>
    <table width='80%' border='1' bordercolor='#000000' cellspacing='5' cellpadding='2' align='center'>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <font class='catfont'><strong>$chgrnfo[teamname] vs $chgdnfo[teamname]</strong></font></td>
    </tr>
    <tr>
    <td width='100%' valign='top' align='left'>
    <form method='post'>
    <table width='100%' border='0' cellspacing='0' cellpadding='0' valign='top' align='center'>
    <tr>
    <td width='100%' valign='top' align='left'>
    <li><b>Your teams comments:</b> $chall[challengercomment]<br>
    <li><b>Comments from the challenged team:</b> $chall[challengedcomment]</td>
    </tr>
    </table>
    </td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <font class='catfont'><strong>Select the time you want to have the match</strong></font></td>
    </tr>
    <tr>
    <td width='100%' valign='top' align='left'>
    <table width='100%' border='0' cellspacing='0' cellpadding='0' valign='top' align='center'>
    $timeoptions
    </table>
    </td>
    </tr>";
    if($chall[matchmaps]){
        $out[body]=$out[body]."
        <tr class='altcolor'>
        <td width='100%' valign='top' align='center'>
        <font class='catfont'><strong>Select $linfo[mapscanaccept] of the maps below to play in the match</strong></font></td>
        </tr>
        <tr>
        <td width='100%' valign='top' align='left'>
        <table width='100%' border='0' cellspacing='0' cellpadding='0' valign='top' align='center'>
        $maplist
        </table>
        <br>
        </td>
        </tr>";
    }

    $out[body]=$out[body]."
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <table width='100%' border='0' cellspacing='0' cellpadding='0'>
    <tr>
    <td width='50%' valign='top' align='center'>
    <input type='hidden' name='challid' value='$challid'>
    <input type='hidden' name='action' value='finalizeb'>
    <input type='submit' name='' value='Accept Challenge'>
    </td>
    </form>
    <script language='javascript'>var forfeit='Are you sure you want to FORFEIT and recieve a LOSS to $chgdnfo[teamname]';</script>
    <form method='post'>
    <td width='50%' valign='top' align='center'>
    <input type='hidden' name='challid' value='$challid'>
    <input type='hidden' name='action' value='forfeit'>
    <input type='submit' name='' value='Forfeit Challenge' onClick='return confirm(forfeit);'>
    </td>
    </form>
    </tr>
    </table>
    </td>
    </tr>
    </table>";
    include("$dir[curtheme]");
}

function challenge_finalizeb($challid,$time,$maps){
    global $dir, $file, $url, $out, $plyr;
    if(!$challid){
        include("$dir[func]/error.php");
        display_error("Invalid Challenge ID.<br>");
    }

    if(!$time){
        include("$dir[func]/error.php");
        display_error("Invalid Match Time.<br>");
    }

    if ($chall[matchmaps]){
        if(!$maps){
            include("$dir[func]/error.php");
            display_error("You need to select the maps to play.<br>");
        }

    }

    $challengeinfo=mysql_query("SELECT * FROM challenges WHERE challid='$challid'");
    $chall=mysql_fetch_array($challengeinfo);
    if(!$chall[challenger]){
        include("$dir[func]/error.php");
        display_error("Unknown Challenge ID.<br>");
    }

    if($chall[finalizedby] > 0){
        include("$dir[func]/error.php");
        display_error("This match has already been finalized.<br>");
    }

    if($chall[respondedby] < 1){
        include("$dir[func]/error.php");
        display_error("This match hasnt been responed to yet.<br>");
    }

    if(mysql_num_rows(mysql_query("SELECT teamid FROM teammembers WHERE playerid='$plyr[id]' AND teamid='$chall[challenger]' AND status <= '3'")) < 1){
        include("$dir[func]/error.php");
        display_error("You cannot finalize challenges for this team.<br>");
    }

    $challengerinfo=mysql_query("SELECT teamname,teamemail FROM teams WHERE id='$chall[challenger]' ");
    $chgrnfo=mysql_fetch_array($challengerinfo);
    if(!$chgrnfo[teamname]){
        include("$dir[func]/error.php");
        display_error("Unable to find your team.<br>");
    }

    $challengedinfo=mysql_query("SELECT teamname,teamemail FROM teams WHERE id='$chall[challenged]'");
    $chgdnfo=mysql_fetch_array($challengedinfo);
    if(!$chgdnfo[teamname]){
        include("$dir[func]/error.php");
        display_error("Unable to find the challenged teams information.<br>");
    }

    $ladderinfo=mysql_query("SELECT * FROM ladders WHERE id='$chall[ladderid]'");
    $linfo=mysql_fetch_array($ladderinfo);
    //GET SELECTED TIME
    $availmatchtimes=split(",",$chall[matchtimes]);
    while(list($countimes,$matchtimes)=each($availmatchtimes)){
        if($matchtimes){
            $countimes=($countimes+1);
            if($countimes=="$time"){
                $finalmatchtime="$matchtimes";
                $matchtimesfound=1;
            }

        }

    }

    if(!$matchtimesfound){
        include("$dir[func]/error.php");
        display_error("Invalid match time.<br>");
    }

    if($maps){
        while(list($mapcount,$mapplay)=each($maps)){
            $countmaps=($countmaps +1);
            $finalmaps=$finalmaps."$mapplay,";
        }

    }

    if($chall[matchmaps]){
        if($countmaps!=$linfo[mapscanaccept]){
            include("$dir[func]/error.php");
            display_error("You must select $linfo[mapscanaccept] of the available maps.<br>");
        }

    }

    //FINALIZE CHALLENGE
    $tday=date("Y-m-d H:i:s");
    mysql_query("UPDATE challenges SET
    finaltime='$finalmatchtime',
    finalmaps='$finalmaps',
    finalizedby='$plyr[id]',
    finalizedtime='$tday'
    WHERE challid='$challid'");
    //Tuesday Jul 17 @ 07 pm Eastern Standard Time
    $stmatch=split(" ",$finalmatchtime);
    //UPDATE CHALLENGER
    mysql_query("UPDATE ladder_$chall[ladderid] SET
    statusdisplay='<a href=$url[base]/$file[match]?challid=$challid>Upcoming Match $stmatch[1] $stmatch[2]</a>'
    WHERE id='$chall[challenger]'");
    //UPDATE CHALLENGED
    mysql_query("UPDATE ladder_$chall[ladderid] SET
    statusdisplay='<a href=$url[base]/$file[match]?challid=$challid>Upcoming Match $stmatch[1] $stmatch[2]</a>'
    WHERE id='$chall[challenged]'");
    //SEND EMAIL TO CHALLENGED
    $emailbody="$site[shortname] Challenge Information,\n
    The challenge has been finalized and the match set!\n
    Challenging Team: $chgrnfo[teamname]
    $url[base]/$file[teams]?teamid=$chall[challenger]
    Challenged Team: $chgdnfo[teamname]
    $url[base]/$file[teams]?teamid=$chall[challenged]
    Ladder: $linfo[laddername]
    $url[base]/$file[ladder]?ladderid=$chall[ladderid]\n
    Go to the following url to view the details of this challenge.
    $url[base]/$file[match]?challid=$challid";
    include("$dir[func]/email.php");
    send_email($chgrnfo[teamname],$chgrnfo[teamemail],"Match Set","$emailbody");
    send_email($chgdnfo[teamname],$chgdnfo[teamemail],"Match Set","$emailbody");
    $out[body]=$out[body]."
    <br><br>
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <strong>The Challenge has been finalized</strong><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='left'>
    <br><ul>
    Ladder: <a href='$url[base]/$file[ladder]?ladderid=$chall[ladderid]'>$linfo[laddername]</a><br>
    Challenger: <a href='$url[base]/$file[teams]?teamid=$chall[challenger]'>$chgrnfo[teamname]</a><br>
    Challenger Rank: $chall[challengerrank]<br>
    Challenged: <a href='$url[base]/$file[teams]?teamid=$chall[challenged]'>$chgdnfo[teamname]</a><br>
    Challenged Rank: $chall[challengedrank]<br>
    </ul></td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <strong><a href='$url[base]/$file[match]?challid=$challid'>View the challenge</a></strong><br>
    </td>
    </tr>
    </table>";
    include("$dir[curtheme]");
}

?>
