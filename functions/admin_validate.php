<?
if(!IsSet($indexloaded)){
    header("Location: ./index.php");
}

//CHECK ADMIN ACCESS LEVEL
if($admn[access] < 5){
    include("$dir[func]/error.php");
    display_error("No estas autorizado para realizar esta funci&oacute;n.<br>");
}

function admin_validate(){
    global $dir, $url, $out, $site, $admn;
    $validationcodes = mysql_query("SELECT vcode,alias FROM validate ORDER by alias");
    while(list($vcode,$alias) = mysql_fetch_row($validationcodes)){
        $vcodes=$vcodes."<option value='$vcode'>$alias</option>";
        $howmanyeditable=1;
    }

    if(!$howmanyeditable){
        include("$dir[func]/error.php");
        display_error("There are no unvalidated accounts.<br>");
    }

    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <form method='post'>
    <td width='100%' valign='top' align='center'>
    <strong>Validation Codes</strong><br>
    </td> </tr> <tr>
    <td width='100%' valign='center' align='center'>
    <br>
    <script language='javascript'>var confirmdelete='Are you sure you want to DELETE this account.';</script>
    <select name='vcode'>$vcodes</select><br> <br></td> </tr> <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='action' value='validateb'>
    <input type='submit' name='todo' value='Remail'>
    <input type='submit' name='todo' value='Validate'>
    <input type='submit' name='todo' value='Delete' onClick='return confirm(confirmdelete);'> </td> </form> </tr> </table>
    $tablefoot";  include("$dir[curtheme]");
}

function admin_validateb($vcode,$todo){
    global $dir, $url, $out, $site, $admn;
    if(!$vcode){
        include("$dir[func]/error.php");
        display_error("Invalid Validation.<br>");
    }

    if($todo=="Validate"){
        validate_account($vcode);
    }

    else if($todo=="Remail"){
        remail_validation($vcode);
    }

    else if($todo=="Delete"){
        delete_validation($vcode);
        include("$dir[func]/admin_finishmessage.php");
        display_message("The unvalidated account was deleted.<br>");
    } else{

        include("$dir[func]/error.php");
        display_error("Comando desconocido.<br>");
    }

}

function validate_account($vcode){
    global $dir, $url, $out, $site, $admn;
    $validatecode=mysql_query("SELECT * FROM validate WHERE vcode='$vcode'");
    $vc=mysql_fetch_array($validatecode);
    if(!$vc[alias]){
        include("$dir[func]/error.php");
        display_error("Unable to find that validation code.<br>");
    }

    $tday=date("Y-m-d H:i:s");
    mysql_query("INSERT INTO users VALUES (NULL,  '$vc[alias]',  '$vc[pass]',  '$vc[email]',   '$vc[icq]',  '$vc[aim]',  '$vc[yahoo]', '$vc[msn]',  '$vc[website]',  '$vc[logo]',  '$vc[newsletter]',  '$tday',  '$vc[ipaddress]',  '0',  '0',  '0',  '0',  '0',  '0', '$vc[theme]','$vc[country]');");
    $getid=mysql_query("SELECT id FROM users WHERE alias='$vc[alias]'");
    $newidnfo=mysql_fetch_array($getid);
    $playerid=$newidnfo["id"];
    mysql_query("INSERT INTO usersinfo VALUES ( '$playerid',  '$vc[firstname]',  '$vc[middlename]',  '$vc[lastname]',  '$vc[address]',  '$vc[city]',  '$vc[state]',  '$vc[zipcode]',  '$vc[phone]',  '$vc[birthday]',  '$vc[occupation]', '$uinfo[force]');");
    delete_validation($vcode);
    $emailbody="
    Thank you for signing up on $site[shortname].Your account has now been validated by an administrator.\n\n
    Your Userid is :  $newidnfo[id]\n
    Your Password is $vc[pass].";
    include("$dir[func]/email.php");
    send_email($vc[alias],$vc[email],"Validation Complete","$emailbody");

    include("$dir[func]/admin_finishmessage.php");
    display_message("The account was validated.<br><br>Username: $vc[alias]<br>Player ID: $playerid<br><br> An email has been sent to the user.");

}

function remail_validation($code){
    global $dir, $file, $url, $out, $site, $admn;
    $validatecode=mysql_query("SELECT * FROM validate WHERE vcode='$code'");
    $vc=mysql_fetch_array($validatecode);
    if(!$vc[alias]){
        include("$dir[func]/error.php");
        display_error("Unable to find that validation code.<br>");
    }

    $emailbody="Thank you for signing up on $site[shortname], Before you can play on the Ladders you must validate your account.\n\nValidation Code: $vc[vcode]\n\n$url[base]/$file[join]?action=validate&vc=$vc[vcode]\n\n";
    include("$dir[func]/email.php");
    send_email($vc[alias],$vc[email],"Validation Code","$emailbody");
    include("$dir[func]/admin_finishmessage.php");
    display_message("The validation was remailed.<br>");
}

function delete_validation($code){
    mysql_query("DELETE FROM validate WHERE vcode='$code'");
}

?>
