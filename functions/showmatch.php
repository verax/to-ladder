<?
function show_match($matchid){
    global $dir, $file, $url, $out, $misc;
    $thematch=mysql_query("SELECT *,DATE_FORMAT(reportdate, '%M %d, %l:%i %p'),DATE_FORMAT(confirmdate, '%M %d, %l:%i %p') FROM matchdb WHERE matchid='$matchid'");
    $match=mysql_fetch_array($thematch);
    if(!$match[matchid]){
        include("$dir[func]/error.php");
        display_error("Unknown Match ID.<br>");
    }

    $tablehead=table_head("show","","","left");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    &nbsp; &nbsp;<strong><a href='$url[base]/$file[ladder]?ladderid=$match[ladderid]'>
    <font class='catfont'>$match[laddername] Match</font></a></strong><br>
    <hr class='catfont' size='1'>
    <table width='100%' border='0' bordercolor='#000000' cellspacing='0' cellpadding='0'>
    <tr>
    <td width='100%' valign='top' align='center'>
    <table width='100%' border='0'  cellspacing='1' cellpadding='0'>
    <tr>
    <td width='20%' valign='top' align='left'>&nbsp;</td>
    <td width='40%' valign='top' align='center' class='altcolor'>
    <strong>Winner</strong></td>
    <td width='40%' valign='top' align='center' class='altcolor'>
    <strong>Loser</strong></td>
    </tr>
    <tr>
    <td width='20%' valign='top' align='left'>Name</td>
    <td width='40%' valign='top' align='center' bgcolor='#000033'>
    <a href='$url[base]/$file[teams]?teamid=$match[winnerid]'>$match[winneralias]</a></td>
    <td width='40%' valign='top' align='center' bgcolor='#000033'>
    <a href='$url[base]/$file[teams]?teamid=$match[loserid]'>$match[loseralias]</a></td>
    </tr>";
    if($match[winrank] < 1){
        $match[winrank]="Unranked";
    }

    if($match[loserank] < 1){
        $match[loserank]="Unranked";
    }

    $out[body]=$out[body]."<tr>
    <td width='20%' valign='top' align='left'>Rank</td>
    <td width='40%' valign='top' align='center' bgcolor='#000033'>$match[winrank]</td>
    <td width='40%' valign='top' align='center' bgcolor='#000033'>$match[loserank]</td>
    </tr>";
    $matchreporter=mysql_query("SELECT alias FROM users WHERE id='$match[reporterid]'");
    $reporter=mysql_fetch_array($matchreporter);
    $matchconfirmer=mysql_query("SELECT alias FROM users WHERE id='$match[confirmerid]'");
    $confirmer=mysql_fetch_array($matchconfirmer);
    $out[body]=$out[body]."
    <tr>
    <td width='20%' valign='top' align='left'>Reported By</td>
    <td width='40%' valign='top' align='center' bgcolor='#000033'>
    <a href='$url[base]/$file[players]?playerid=$match[confirmerid]'>$confirmer[alias]</a></td>
    <td width='40%' valign='top' align='center' bgcolor='#000033'>
    <a href='$url[base]/$file[players]?playerid=$match[reporterid]'>$reporter[alias]</a></td>
    </tr>
    <tr>
    <td width='20%' valign='top' align='left'>Reported On</td>
    <td width='40%' valign='top' align='center' bgcolor='#000033'>$match[26]</td>
    <td width='40%' valign='top' align='center' bgcolor='#000033'>$match[25]</td>
    </tr>
    <tr>
    <td width='20%' valign='top' align='left'><br></td>
    <td width='40%' valign='top' align='center' bgcolor='#000033'></td>
    <td width='40%' valign='top' align='center' bgcolor='#000033'></td>
    </tr>
    <tr>
    <td width='20%' valign='top' align='left'><br></td>
    <td width='40%' valign='top' align='center' bgcolor='#000033' class='altcolor'><strong>Winning Members</strong></td>
    <td width='40%' valign='top' align='center' bgcolor='#000033' class='altcolor'><strong>Losing Members</strong></td>
    </tr>
    <tr>
    <td colspan='3' width='100%' valign='top' align='center'>
    <table width='100%' border='0'  cellspacing='1' cellpadding='0'>
    <tr>
    <td width='20%' valign='top' align='left'></td>
    <td width='40%' valign='top' align='center'>
    <table width='100%' border='0'  cellspacing='1' cellpadding='0'>";
    $winnermembers=split(",",$match[winmembers]);
    while(list($countw,$memberidw)=each($winnermembers)){
        $memberidw=split("\|",$memberidw);
        $countw=($countw+1);
        if($memberidw[0]){
            $wmemberplayed=mysql_query("SELECT alias FROM users WHERE id='$memberidw[0]'");
            $wmember=mysql_fetch_array($wmemberplayed);
            $out[body]=$out[body]."<tr>
            <td width='100%' valign='top' align='center' bgcolor='#000033'>
            <a href='$url[base]/$file[players]?playerid=$memberidw[0]'>$wmember[alias]</a></td>
            </tr>";
        }

    }

    $out[body]=$out[body]."</table>
    </td>
    <td width='40%' valign='top' align='center'>
    <table width='100%' border='0'  cellspacing='1' cellpadding='0'>";
    $losermembers=split(",",$match[losemembers]);
    while(list($countl,$memberidl)=each($losermembers)){
        $memberidl=split("\|",$memberidl);
        $countl=($countl+1);
        if($memberidl[0]){
            $lmemberplayed=mysql_query("SELECT alias FROM users WHERE id='$memberidl[0]'");
            $lmember=mysql_fetch_array($lmemberplayed);
            $out[body]=$out[body]."<tr>
            <td width='100%' valign='top' align='center' bgcolor='#000033'>
            <a href='$url[base]/$file[players]?playerid=$memberidl[0]'>$lmember[alias]</a></td>
            </tr>";
        }

    }

    $out[body]=$out[body]."</table>
    </td>
    </tr>
    </table>";
    if($match[mapsplayed]){
        $out[body]=$out[body]."
        <tr>
        <td width='20%' valign='top' align='left'><font class='catfont'><strong>Maps Played</strong></font></td>
        <td width='40%' valign='top' align='center' bgcolor='#000033'></td>
        <td width='40%' valign='top' align='center' bgcolor='#000033'></td>
        </tr>";
        $mapswon=split(",",$match[mapswon]);
        $mapsplayed=split(",",$match[mapsplayed]);
        while(list($mapnum,$mapid)=each($mapsplayed)){
            $mapnum=($mapnum+1);
            if($mapid){
                $mapplayedinfo=mysql_query("SELECT mapname FROM maps WHERE id='$mapid'");
                $mapinfo=mysql_fetch_array($mapplayedinfo);
                if(!$mapinfo[mapname]){
                    $mapinfo[mapname]="Unknown";
                }

                if(in_array("$mapnum",$mapswon)){
                    $out[body]=$out[body]."
                    <tr>
                    <td width='20%' valign='top' align='left'>#$mapnum
                    <a href='$url[base]/$file[maps]?mapid=$mapid'>$mapinfo[mapname]</a></td>
                    <td width='40%' valign='top' align='center' bgcolor='#000033'>Won</td>
                    <td width='40%' valign='top' align='center' bgcolor='#000033'>Lost</td>
                    </tr>";
                }else{

                    $out[body]=$out[body]."
                    <td width='20%' valign='top' align='left'>#$mapnum
                    <a href='$url[base]/$file[maps]?mapid=$mapid'>$mapinfo[mapname]</a></td>
                    <td width='40%' valign='top' align='center' bgcolor='#000033'>Lost</td>
                    <td width='40%' valign='top' align='center' bgcolor='#000033'>Won</td>
                    </tr>
                    ";
                }

            }

        }

        $out[body]=$out[body]."
        <tr>
        <td width='20%' valign='top' align='left'><br></td>
        <td width='40%' valign='top' align='center' bgcolor='#000033'></td>
        <td width='40%' valign='top' align='center' bgcolor='#000033'></td>
        </tr>";
    }

    $out[body]=$out[body]."
    <tr>
    <td width='100%' valign='top' align='left' colspan='3'>
    <font class='catfont'><strong>Final Results</strong></font></td>
    </tr>
    <tr>
    <td width='20%' valign='top' align='left'>Final Score</td>
    <td width='40%' valign='top' align='center' bgcolor='#000033'>$match[winscore]</td>
    <td width='40%' valign='top' align='center' bgcolor='#000033'>$match[losescore]</td>
    </tr>";
    $out[body]=$out[body]."
    <tr>
    <td width='20%' valign='top' align='left'>Match Comment</td>
    <td width='40%' valign='top' align='center' bgcolor='#000033'>$match[wincomment]</td>
    <td width='40%' valign='top' align='center' bgcolor='#000033'>$match[losecomment]</td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    $tablefoot
    ";
    include("$dir[curtheme]");
}

?>
