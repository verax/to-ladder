<?
if(!IsSet($indexloaded)){
    header("Location: ./index.php");
}

//CHECK ADMIN ACCESS LEVEL
if($admn[access] < 60){
    include("$dir[func]/error.php");
    display_error("You are not allowed to perform this function.<br>");
}

function admin_editrules(){
    global $dir, $url, $out, $site, $admn;
    //GET LADDERS
    $ladderslist=mysql_query("SELECT id,laddername  FROM ladders ORDER by laddername");
    while(list($id,$name)=mysql_fetch_row($ladderslist)){
        $theladders=$theladders."<option value='$id'>$name</option>";
        $foundladders=1;
    }

    if(!$foundladders){
        include("$dir[func]/error.php");
        display_error("There is no ladders.<br>");
    }

    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <form method='post'>
    <td width='100%' valign='top' align='center'>
    <strong>Manage Rules</strong><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <br>
    Select a ladder<br>
    <select name='ladderid'>$theladders</select><br>
    <br></td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='action' value='editrulesb'>
    <input type='submit' name='' value='Edit Rules'>
    </td>
    </form>
    </tr>
    </table>";
    include("$dir[curtheme]");
}

function admin_editrulesb($ladderid){
    global $dir, $url, $out, $site, $admn;
    if($admn[access] < 75){
        if (mysql_num_rows(mysql_query("SELECT staffid FROM staffaccess WHERE staffid='$admn[id]' AND ladderid='$ladderid'")) < 1){
            include("$dir[func]/error.php");
            display_error("You are not allowed to manage rules on this laddder.<br>");
        }

    }

    //GET RULES
    $ruleslist=mysql_query("SELECT id,ruletitle FROM rules WHERE ladderon='$ladderid' ORDER by ruletitle");
    while(list($id,$name)=mysql_fetch_row($ruleslist)){
        $therules=$therules."<option value='$id'>$name</option>";
    }

    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <form method='post'>
    <td width='100%' valign='top' align='center'>
    <strong>Manage Rules</strong><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <br>
    <script language='javascript'>var confirmdelete='Are you sure you want to DELETE this Rule';</script>
    <select name='ruleid'>
    <option value=''>Select a rule or create a new one</option>
    $therules</select><br>
    <br></td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='action' value='editrulesc'>
    <input type='hidden' name='ladderid' value='$ladderid'>
    <input type='submit' name='todo' value='Create Rule'>
    <input type='submit' name='todo' value='Edit Rule'>
    <input type='submit' name='todo' value='Delete Rule' onClick='return confirm(confirmdelete);'>
    </td>
    </form>
    </tr>
    </table>";
    include("$dir[curtheme]");
}

function admin_editrulesc($ladderid,$ruleid,$todo){
    global $dir, $url, $out, $site, $admn;
    if(!$ladderid){
        include("$dir[func]/error.php");
        display_error("Invalid Ladder ID.<br>");
    }

    if((!$ruleid) && ($todo!="Create Rule")){
        include("$dir[func]/error.php");
        display_error("Invalid Rule ID.<br>");
    }

    if($todo=="Create Rule"){
        create_rule($ladderid);
    }

    else if($todo=="Edit Rule"){
        edit_rule($ruleid);
    }

    else if($todo=="Delete Rule"){
        delete_rule($ruleid);
        include("$dir[func]/admin_finishmessage.php");
        display_message("The rule was deleted.<br>");
    }

    else{
        include("$dir[func]/error.php");
        display_error("Unknown Command.<br>");
    }

}

function create_rule($ladderid){
    global $dir, $url, $out, $site, $admn;
    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <form method='post'>
    <td width='100%' valign='top' align='center'>
    <strong>Create a Rule</strong><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <br>
    Rule Title<br>
    <input type='text' name='title' size='75' value='' maxlength='250'><br>
    <br>
    Rule Body<br>
    <textarea name='body' rows='10' cols='75'>
    </textarea>
    <br>
    Bcode:<br>
    {b}<b>BOLD</b>{/b}<br>

    {i}<i>Italic</i>{/i}<br>

    {font color=red}<font color='#FF0000'>red</font>{/font}

    <br><br></td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='action' value='editrulesd'>
    <input type='hidden' name='ladderid' value='$ladderid'>
    <input type='submit' name='' value='Create Rule'></td>
    </form>
    </tr>
    </table>";
    include("$dir[curtheme]");
}

function admin_editrulesd($ladderid,$title,$body){
    global $dir, $url, $out, $site, $admn;
    include("$dir[func]/checkdata.php");
    $errormessage=check_validlength($errormessage, $title, "0", "250", "Your Rule Title must not exceed 250 charecters");
    $errormessage=check_validlength($errormessage, $body, "0", "5000", "Your Rule Body must not exceed 5000 charecters");
    error_check($errormessage);
    $title=change_charecters($title);
    $body=change_fromlinebreaks($body);
    $body=change_charecters($body);
    $body=change_tocharectercode($body);
    $tday=date("Y-m-d H:i:s");
    mysql_query("INSERT INTO rules VALUES (NULL, '$ladderid', '$title', '$body', '$admn[id]', '$tday');");
    include("$dir[func]/admin_finishmessage.php");
    display_message("The rule has been added.<br>");
}

function edit_rule($ruleid){
    global $dir, $url, $out, $site, $admn;
    $ruleinfo=mysql_query("SELECT * FROM rules WHERE id='$ruleid'");
    $rule=mysql_fetch_array($ruleinfo);
    if(!$rule[id]){
        include("$dir[func]/error.php");
        display_error("Unknown Rule ID.<br>");
    }

    include("$dir[func]/checkdata.php");
    $rule[rulebody]=change_tolinebreaks($rule[rulebody]);
    $rule[rulebody]=change_fromcharectercode($rule[rulebody]);
    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <form method='post'>
    <td width='100%' valign='top' align='center'>
    <strong>Edit a Rule</strong><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <br>
    Rule Title<br>
    <input type='text' name='title' size='75' value='$rule[ruletitle]' maxlength='250'><br>
    <br>
    Rule Body<br>
    <textarea name='body' rows='10' cols='75'>
    $rule[rulebody]</textarea>
    <br>
    Bcode:<br>
    {b}<b>BOLD</b>{/b}<br>

    {i}<i>Italic</i>{/i}<br>

    {font color=red}<font color='#FF0000'>red</font>{/font}

    <br><br></td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='action' value='editrulese'>
    <input type='hidden' name='ruleid' value='$ruleid'>
    <input type='submit' name='' value='Edit Rule'></td>
    </form>
    </tr>";
    //GET LADDERS
    $ladderslist=mysql_query("SELECT id,laddername FROM ladders WHERE id!='$map[ladderid]' ORDER by laddername");
    while(list($id,$name)=mysql_fetch_row($ladderslist)){
        $theladders=$theladders."<option value='$id'>$name</option>";
    }

    $out[body]=$out[body]."
    <tr class='altcolor'>
    <form method='post'>
    <td width='100%' valign='top' align='center' colspan='2'>
    <select name='ladderid'>$theladders</select>
    <input type='hidden' name='action' value='editrulesf'>
    <input type='hidden' name='ruleid' value='$ruleid'>
    <input type='submit' name='' value='Copy Rules'></td>
    </form>
    </tr>
    </table>";
    include("$dir[curtheme]");
}

function admin_editrulese($ruleid,$title,$body){
    global $dir, $url, $out, $site, $admn;
    if(!$ruleid){
        include("$dir[func]/error.php");
        display_error("Invalid Rule ID.<br>");
    }

    include("$dir[func]/checkdata.php");
    $errormessage=check_validlength($errormessage, $title, "0", "250", "Your Rule Title must not exceed 250 charecters");
    $errormessage=check_validlength($errormessage, $body, "0", "5000", "Your Rule Body must not exceed 5000 charecters");
    error_check($errormessage);
    $title=change_charecters($title);
    $body=change_fromlinebreaks($body);
    $body=change_charecters($body);
    $body=change_tocharectercode($body);
    $tday=date("Y-m-d H:i:s");
    mysql_query("UPDATE rules SET
    ruletitle='$title',
    rulebody='$body',
    lasteditby='$admn[id]',
    lastedit='$tday'
    WHERE id='$ruleid'");
    include("$dir[func]/admin_finishmessage.php");
    display_message("The rule has been updated.<br>");
}

function delete_rule($ruleid){
    mysql_query("DELETE FROM rules WHERE id='$ruleid'");
}

function admin_editrulesf($ruleid,$ladderid){
    global $dir, $url, $out, $site, $admn;
    $allrulesinfo=mysql_query("SELECT * FROM rules WHERE id='$ruleid'");
    $ruleinfo=mysql_fetch_array($allrulesinfo);
    if(!$ruleinfo[ruletitle]){
        include("$dir[func]/error.php");
        display_error("Unknown Rule ID.<br>");
    }

    $tday=date("Y-m-d H:i:s");
    mysql_query("INSERT INTO rules VALUES (NULL,
    '$ladderid',
    '$ruleinfo[ruletitle]',
    '$ruleinfo[rulebody]',
    '$admn[id]',
    '$tday');");
    include("$dir[func]/admin_finishmessage.php");
    display_message("The rule has been copied.<br>");
}

?>
