<?
if(!IsSet($indexloaded)){
    header("Location: ./index.php");
}

//CHECK ADMIN ACCESS LEVEL
if($admn[access] < 95){
    include("$dir[func]/error.php");
    display_error("You are not allowed to perform this function.<br>");
}

function admin_editgames(){
    global $dir, $url, $out, $site, $admn;
    //GET GAMES
    $gameslist=mysql_query("SELECT id,gamename FROM games ORDER by gamename");
    while(list($id,$name)=mysql_fetch_row($gameslist)){
        $thegames=$thegames."<option value='$id'>$name</option>";
    }

    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <form method='post'>
    <td width='100%' valign='top' align='center'>
    <strong>Manage Games</strong><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <br>
    Select a Game<br>
    <select name='gameid'>
    <option value=''>Select a game or create a new one</option>
    $thegames</select><br>
    <br></td>
    </tr>
    <tr class='altcolor'>
    <script language='javascript'>var confirmdelete='Are you 100% sure you want to DELETE this Game. All ladders must be deleted from it or it wont work.';</script>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='action' value='editgamesb'>
    <input type='submit' name='todo' value='Create Game'>
    <input type='submit' name='todo' value='Edit Game'>
    <input type='submit' name='todo' value='Delete Game' onClick='return confirm(confirmdelete);'>
    </td>
    </form>
    </tr>
    </table>";
    include("$dir[curtheme]");
}

function admin_editgamesb($gameid,$todo){
    global $dir, $url, $out, $site, $admn;
    if((!$gameid) && ($todo!="Create Game")){
        include("$dir[func]/error.php");
        display_error("Invalid Game ID.<br>");
    }

    if($todo=="Create Game"){
        create_game();
    }

    else if($todo=="Edit Game"){
        edit_game($gameid);
    }

    else if($todo=="Delete Game"){
        delete_game($gameid);
        include("$dir[func]/admin_finishmessage.php");
        display_message("The game was deleted.<br>");
    }

    else{
        include("$dir[func]/error.php");
        display_error("Unknown Command.<br>");
    }

}

function create_game(){
    global $dir, $url, $out, $site, $admn;
    //TABLE ATERNATING COLORS
    $altcolora="#000033";
    $altcolorb="#000020";
    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <strong>Create Game</strong></td>
    </tr>
    <tr>
    <td width='100%' valign='top' align='left'>
    <form method='post'>
    <table width='100%' border='0' cellspacing='0' cellpadding='0' valign='top' align='center'>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    Game Full Name (e.g Rainbow Six 3)</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='g_name' value='' size='30' maxlength='25'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    Game Abbreviation (e.g RS3)</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='g_abrv' value='' size='30' maxlength='10'></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='top' align='left'>
    Game Description<br>200 Characters max</font></td>
    <td width='50%' valign='center' align='center'>
    <textarea name='g_desc' rows='3' cols='25' maxlength='200'></textarea></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='top' align='left'>
    Active Game (no will disable all ladders)</font></td>
    <td width='50%' valign='center' align='center'>
    <select name='g_active'>
    <option value='1'>Yes</option>
    <option value='0'>No</option>
    </select>
    </td>
    </tr>
    </table>
    </td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='action' value='editgamesc'>
    <input type='submit' name='' value='Create the Game'></td>
    </form>
    </tr>
    </table>";
    include("$dir[curtheme]");
}

function admin_editgamesc($g_name,$g_abrv,$g_desc,$g_active,$g_fid){
    global $dir, $url, $out, $site, $admn;
    include("$dir[func]/checkdata.php");
    $g_name=change_charecters($g_name);
    $g_abrv=change_charecters($g_abrv);
    $g_desc=change_fromlinebreaks($g_desc);
    $g_desc=change_charecters($g_desc);
    $g_desc=change_tocharectercode($g_desc);
    $g_active=change_numbersonly($g_active);
    $g_fid=change_numbersonly($g_fid);
    $errormessage=check_validlength($errormessage, $g_name, "2", "50", "Your games name must be 2-25 characters.");
    $errormessage=check_validlength($errormessage, $g_abrv, "2", "25", "Your games abbreviation must be 2-10 characters.");
    $errormessage=check_validlength($errormessage, $g_desc, "0", "250", "Your games description must not exceed 200 characters.");
    error_check($errormessage);
    $errormessage=check_gameexist($errormessage, $g_name, $g_abrv);
    error_check($errormessage);
    $tday=date("Y-m-d H:i:s");
    mysql_query("INSERT INTO games VALUES (NULL,
    '$g_name',
    '$g_abrv',
    '$g_desc',
    '$g_active',
    '$g_fid',
    '$admn[id]',
    '$tday');");
    include("$dir[func]/admin_finishmessage.php");
    display_message("The game was created. <br>You can now create ladders for this game using the 'Manage Ladders' option.<br>");
}

function edit_game($gameid){
    global $dir, $url, $out, $site, $admn;
    $thisgame=mysql_query("SELECT * FROM games WHERE id='$gameid'");
    $game=mysql_fetch_array($thisgame);
    if(!$game[gamename]){
        include("$dir[func]/error.php");
        display_error("Unknown Game ID.<br>");
    }

    $gamestatus="$game[status]";
    $selected[$gamestatus]="SELECTED";
    include("$dir[func]/checkdata.php");
    $game[description]=change_tolinebreaks($game[description]);
    $game[description]=change_fromcharectercode($game[description]);
    //TABLE ATERNATING COLORS
    $altcolora="#000033";
    $altcolorb="#000020";
    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <strong>Edit Game</strong></td>
    </tr>
    <tr>
    <td width='100%' valign='top' align='left'>
    <form method='post'>
    <table width='100%' border='0' cellspacing='0' cellpadding='0' valign='top' align='center'>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    Games Full Name (e.g Half Life)</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='g_name' value='$game[gamename]' size='30' maxlength='25'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    Games Abbreviation (e.g HL)</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='g_abrv' value='$game[abbreviation]' size='30' maxlength='10'></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='top' align='left'>
    Game Description<br>200 Characters max<br></font></td>
    <td width='50%' valign='center' align='center'>
    <textarea name='g_desc' rows='3' cols='25' maxlength='200'>
    $game[description]</textarea></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='top' align='left'>
    Active Game (no will disable all ladders)</font></td>
    <td width='50%' valign='center' align='center'>
    <select name='g_active'>
    <option value='1' $selected[1]>Yes</option>
    <option value='0' $selected[0]>No</option>
    </select>
    </td>
    </tr>
    </table>
    </td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='action' value='editgamesd'>
    <input type='hidden' name='gameid' value='$gameid'>
    <input type='submit' name='' value='Update Game'>
    <input type='reset' name='' value='Reset'></td>
    </form>
    </tr>
    </table>";
    include("$dir[curtheme]");
}

function admin_editgamesd($gameid,$g_name,$g_abrv,$g_desc,$g_active,$g_fid){
    global $dir, $url, $out, $site, $admn;
    include("$dir[func]/checkdata.php");
    $g_name=change_charecters($g_name);
    $g_abrv=change_charecters($g_abrv);
    $g_desc=change_fromlinebreaks($g_desc);
    $g_desc=change_charecters($g_desc);
    $g_desc=change_tocharectercode($g_desc);
    $g_active=change_numbersonly($g_active);
    $g_fid=change_numbersonly($g_fid);
    $errormessage=check_validlength($errormessage, $g_name, "2", "50", "Your Games Name Must be 2-25 Characters Long.");
    $errormessage=check_validlength($errormessage, $g_abrv, "2", "25", "Your Games Abbreviation Must be 2-10 Characters Long.");
    $errormessage=check_validlength($errormessage, $g_desc, "0", "250", "Your Games Description Must not Exceed 200 Characters.");
    error_check($errormessage);
    $errormessage=check_gameexistother($errormessage, $g_name, $g_abrv, $gameid);
    error_check($errormessage);
    $tday=date("Y-m-d H:i:s");
    mysql_query("UPDATE games SET
    gamename='$g_name',
    abbreviation='$g_abrv',
    description='$g_desc',
    status='$g_active',
    forumid='$g_fid',
    lasteditby='$admn[id]',
    lastedit='$tday'
    WHERE id='$gameid'");
    include("$dir[func]/admin_finishmessage.php");
    display_message("The game was updated.<br>");
}

function delete_game($gameid){
    global $dir, $url, $out, $site, $admn;
    //CHECK IF LADDERS ON GAME
    if (mysql_num_rows(mysql_query("SELECT id FROM ladders WHERE gameon='$gameid'")) > 0){
        include("$dir[func]/error.php");
        display_error("All ladders must be deleted from this game before deleting it.<br>");
    }

    mysql_query("DELETE FROM games WHERE id='$gameid'");
}

?>
