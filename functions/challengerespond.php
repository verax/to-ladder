<?
function challenge_respond($challid){
    global $dir, $file, $url, $out, $plyr;
    if(!$challid){
        include("$dir[func]/error.php");
        display_error("Invalid Challenge ID.<br>");
    }

    $challengeinfo=mysql_query("SELECT * FROM challenges WHERE challid='$challid'");
    $chall=mysql_fetch_array($challengeinfo);
    if(!$chall[challenger]){
        include("$dir[func]/error.php");
        display_error("Unknown Challenge ID.<br>");
    }

    if($chall[finalizedby] > 0){
        include("$dir[func]/error.php");
        display_error("This match has already been finalized.<br>");
    }

    if($chall[respondedby] > 0){
        include("$dir[func]/error.php");
        display_error("This match has already been responed to.<br>");
    }

    if(mysql_num_rows(mysql_query("SELECT teamid FROM teammembers WHERE playerid='$plyr[id]' AND teamid='$chall[challenged]' AND status <= '3'")) < 1){
        include("$dir[func]/error.php");
        display_error("You cannot respond to challenges for this team.<br>");
    }

    $challengerinfo=mysql_query("SELECT teamname FROM teams WHERE id='$chall[challenger]'");
    $chgrnfo=mysql_fetch_array($challengerinfo);
    if(!$chgrnfo[teamname]){
        include("$dir[func]/error.php");
        display_error("Unable to find challengers team.<br>");
    }

    $challengedinfo=mysql_query("SELECT teamname FROM teams WHERE id='$chall[challenged]'");
    $chgdnfo=mysql_fetch_array($challengedinfo);
    if(!$chgdnfo[teamname]){
        include("$dir[func]/error.php");
        display_error("Unable to find your teams information.<br>");
    }

    $ladderinfo=mysql_query("SELECT * FROM ladders WHERE id='$chall[ladderid]'");
    $linfo=mysql_fetch_array($ladderinfo);
    /*
    INFO NEEDED
    -> from ladders
    playdays
    timescangive
    mapscangive
    starthour
    endhour
    timezone
    finalizehours
    -> to challenges
    matchtimes
    matchmaps
    challengedcomment
    respondedby
    respondedtime
    */
    //GET DAYS MATCH CAN BE ON
    $finalizehours=$linfo[finalizehours];
    $playdays=$linfo[playdays];
    $spreadhours=24;
    $starthours=($finalizehours + $spreadhours);
    while($countdays < $playdays){
        $thisday=date("l M d", mktime(date("H")+$starthours,date("i"),date("s"),date("m"),date("d")+$countdays,date("Y")));
        //$challengedays=$challengedays."<option>$thisday</option>";
        //GET CHALLENGABLE HOURS
        $hour="$linfo[starthour]";
        $stophour=($linfo[endhour] + 1);
        while(($hour < $stophour) && ($hour < 25)){
            $thishour=date("g a", mktime($hour,1,1,1,1,1));
            $challengetimes=$challengetimes."<option>$thisday @ $thishour $linfo[timezone]</option>";
            $hour++;
        }

        $countdays++;
    }

    //HOW MANY TIME CHALLENGER CAN GIVE
    $counttimes=1;
    $timescount=($linfo[timescangive] + 1);
    while($counttimes < $timescount){
        $timeoptions=$timeoptions."
        <tr class='altcolora'>
        <td width='50%' valign='center' align='left'>
        <font face='veradna,arial' size='2' color='#FFFFFF'>Match Time Option #$counttimes</font></td>
        <td width='50%' valign='center' align='center'>
        <select name='time[$counttimes]'>$challengetimes</select></td>
        </tr>";
        $counttimes++;
    }

    //MAPS AVAILABLE
    $mapsplayed=mysql_query("SELECT id,mapname FROM maps WHERE ladderid='$chall[ladderid]' ORDER by mapname");
    while(list($mapid,$mapname)=mysql_fetch_row($mapsplayed)){
        $maps=$maps."<option value='$mapid'>$mapname</option>";
    }

    $mapnum=1;
    while($mapnum <= $linfo[mapscangive]){
        $maplist=$maplist."
        <tr class='altcolora'>
        <td width='50%' valign='center' align='left'>
        <font face='veradna,arial' size='2' color='#FFFFFF'>Map Option #$mapnum</font></td>
        <td width='50%' valign='center' align='center'>
        <select name='map[$mapnum]'>$maps</select></td>
        </tr>";
        $mapnum++;
    }

    $out[body]=$out[body]."
    <br><br>
    <table width='80%' border='1' bordercolor='#000000' cellspacing=5' cellpadding='2' align='center'>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <font class='catfont'><strong>$chgrnfo[teamname] vs $chgdnfo[teamname]</strong></font></td>
    </tr>
    <tr>
    <td width='100%' valign='top' align='left'>
    <form method='post'>
    <table width='100%' border='0' cellspacing='0' cellpadding='0' valign='top' align='center'>
    <tr>
    <td width='100%' valign='top' align='left'>
    <li><b>Comments from the challenger:</b> $chall[challengercomment]</td>
    </tr>
    </table>
    </td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <font class='catfont'><strong>Select match times for the challenger to pick from</strong></font></td>
    </tr>
    <tr>
    <td width='100%' valign='top' align='left'>
    <table width='100%' border='0' cellspacing='0' cellpadding='0' valign='top' align='center'>
    $timeoptions
    </table>
    </td>
    </tr>";
    if(($linfo[minmaps] > 0)&&($linfo[maxmaps] > 0)){
        $out[body]=$out[body]."
        <tr class='altcolor'>
        <td width='100%' valign='top' align='center'>
        <font class='catfont'><strong>Select Maps for the challenger to pick from</strong></font></td>
        </tr>
        <tr>
        <td width='100%' valign='top' align='left'>
        <table width='100%' border='0' cellspacing='0' cellpadding='0' valign='top' align='center'>
        $maplist
        </table>
        </td>
        </tr>
        ";
    }

    $out[body]=$out[body]."
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <font class='catfont'><strong>Server/Host and Misc. Information</strong></font></td>
    </tr>
    <tr>
    <td width='100%' valign='top' align='center'>
    <textarea name='comment' rows='5' cols='50' maxlength='200'>
    </textarea><br>
    200 Characters Max
    </td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <table width='100%' border='0' cellspacing='0' cellpadding='0'>
    <tr>
    <td width='50%' valign='top' align='center'>
    <input type='hidden' name='challid' value='$challid'>
    <input type='hidden' name='action' value='respondb'>
    <input type='submit' name='' value='Accept Challenge'>
    </td>
    </form>
    <script language='javascript'>var forfeit='Are you sure you want to FORFEIT and recieve a LOSS to $chgrnfo[teamname]';</script>
    <form method='post'>
    <td width='50%' valign='top' align='center'>
    <input type='hidden' name='challid' value='$challid'>
    <input type='hidden' name='action' value='forfeit'>
    <input type='submit' name='' value='Forfeit Challenge' onClick='return confirm(forfeit);'>
    </td>
    </form>
    </tr>
    </table>
    </td>
    </tr>
    </table>";
    include("$dir[curtheme]");
}

function challenge_respondb($challid,$time,$map,$comment){
    global $dir, $file, $url, $out, $plyr;
    if(!$challid){
        include("$dir[func]/error.php");
        display_error("Invalid Challenge ID.<br>");
    }

    $challengeinfo=mysql_query("SELECT * FROM challenges WHERE challid='$challid'");
    $chall=mysql_fetch_array($challengeinfo);
    if(!$chall[challenger]){
        include("$dir[func]/error.php");
        display_error("Unknown Challenge ID.<br>");
    }

    if($chall[finalizedby] > 0){
        include("$dir[func]/error.php");
        display_error("This match has already been finalized.<br>");
    }

    if($chall[respondedby] > 0){
        include("$dir[func]/error.php");
        display_error("This match has already been responed to.<br>");
    }

    if(mysql_num_rows(mysql_query("SELECT teamid FROM teammembers WHERE playerid='$plyr[id]' AND teamid='$chall[challenged]' AND status <= '3'")) < 1){
        include("$dir[func]/error.php");
        display_error("You cannot respond to challenges for this team.<br>");
    }

    $challengerinfo=mysql_query("SELECT teamname,teamemail FROM teams WHERE id='$chall[challenger]'");
    $chgrnfo=mysql_fetch_array($challengerinfo);
    if(!$chgrnfo[teamname]){
        include("$dir[func]/error.php");
        display_error("Unable to find challengers team.<br>");
    }

    $challengedinfo=mysql_query("SELECT teamname FROM teams WHERE id='$chall[challenged]'");
    $chgdnfo=mysql_fetch_array($challengedinfo);
    if(!$chgdnfo[teamname]){
        include("$dir[func]/error.php");
        display_error("Unable to find your teams information.<br>");
    }

    $ladderinfo=mysql_query("SELECT * FROM ladders WHERE id='$chall[ladderid]'");
    $linfo=mysql_fetch_array($ladderinfo);
    if(!$linfo[laddername]){
        include("$dir[func]/error.php");
        display_error("Invalid Ladder.<br>");
    }

    include("$dir[func]/checkdata.php");
    //GET ARRAY OF TIMES AND CHECK FOR DUPLICATES
    while(list($timenum,$timeday)=each($time)){
        $timeday=change_charecters($timeday);
        //STRIP ANY COMMAS
        $timeday=str_replace(",", "", $timeday);
        $secondtime="$firsttime";
        $thirdtime="$secondtimeb";
        if(($firsttime=="$timeday") || ($thirdtime=="$timeday")){
            include("$dir[func]/error.php");
            display_error("You must select different times and dates.<br>");
        }else{

            $matchtimes=$matchtimes."$timeday,";
        }

        $firsttime="$timeday";
        $secondtimeb="$secondtime";
    }

    //GET ARRAY OF MAPS AND CHECK FOR DUPLICATES
    if($map){
        while(list($mapnum,$mapid)=each($map)){
            $mapid=change_numbersonly($mapid);
            $secondmap="$firstmap";
            $thirdmap="$secondmapb";
            if(($firstmap=="$mapid") || ($thirdmap=="$mapid")){
                include("$dir[func]/error.php");
                display_error("You must select different maps.<br>");
            }else{

                $matchmaps=$matchmaps."$mapid,";
            }

            $firstmap="$mapid";
            $secondmapb="$secondmap";
        }

    }

    if(($linfo[minmaps]>0)&&($linfo[maxmaps]>0)){
        if(!$matchmaps){
            include("$dir[func]/error.php");
            display_error("You must select what maps to play.<br>");
        }

    }

    if($comment){
        $comment=wordwrap($comment,20," ",1);
        $comment=change_censor($comment);
        $comment=change_charecters($comment);
    }

    //Update Challenge
    $tday=date("Y-m-d H:i:s");
    mysql_query("UPDATE challenges SET
    matchtimes='$matchtimes',
    matchmaps='$matchmaps',
    challengedcomment='$comment',
    respondedby='$plyr[id]',
    respondedtime='$tday'
    WHERE challid='$challid'");
    //Update Challenger
    mysql_query("UPDATE ladder_$chall[ladderid] SET
    statusdisplay='<a href=$url[base]/$file[match]?challid=$challid>Challenge accepted</a>'
    WHERE id='$chall[challenger]'");
    //Update Challenged
    mysql_query("UPDATE ladder_$chall[ladderid] SET
    statusdisplay='<a href=$url[base]/$file[match]?challid=$challid>Accepted challenge</a>'
    WHERE id='$chall[challenged]'");
    //SEND EMAIL TO CHALLENGER
    $emailbody="$site[shortname] Challenge Information,\n
    Your challenge has been accepted!\n
    Your Team: $chgrnfo[teamname]
    $url[base]/$file[teams]?teamid=$chall[challenger]
    Challenged Team: $chgdnfo[teamname]
    $url[base]/$file[teams]?teamid=$chall[challenged]
    Ladder: $linfo[laddername]
    $url[base]/$file[ladder]?ladderid=$chall[ladderid]\n
    You must finalize this challenge within $linfo[finalizehours] hours.
    If you do not respond you will recieve and automatic forfeit loss.\n
    Go to the following url to finalize this challenge
    or click on finalize challenge at the top of the ladder
    $url[base]/$file[match]?action=finalize&challid=$challid";
    include("$dir[func]/email.php");
    send_email($chgrnfo[teamname],$chgrnfo[teamemail],"Challenge Response","$emailbody");
    $out[body]=$out[body]."
    <br><br>
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <strong>The Challenge has been updated</strong><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='left'>
    <br><ul>
    Ladder: <a href='$url[base]/$file[ladder]?ladderid=$chall[ladderid]'>$linfo[laddername]</a><br>
    Challenger: <a href='$url[base]/$file[teams]?teamid=$chall[challenger]'>$chgrnfo[teamname]</a><br>
    Challenger Rank: $chall[challengerrank]<br>
    Challenged: <a href='$url[base]/$file[teams]?teamid=$chall[challenged]'>$chgdnfo[teamname]</a><br>
    Challenged Rank: $chall[challengedrank]<br>
    </ul></td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <strong><a href='$url[base]/$file[match]?challid=$challid'>View the challenge</a></strong><br>
    </td>
    </tr>
    </table>";
    include("$dir[curtheme]");
}

?>
