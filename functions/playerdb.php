<?
function player_database($page,$orderby){
    global $dir, $file, $url, $out, $misc, $site, $admn;
    $result=mysql_query("select COUNT(*) from users");
    $result=mysql_fetch_array($result);
    $totalusers="$result[0]";
    $maxresults=$misc[playerdb];
    $stop="$maxresults";
    if(!$page){
        $start="1";
    }else{

        $start="$page";
    }

    if($start > $totalusers){
        $start=0;
    }

    $page="$start";
    $start=($start-1);
    $altcolora="#000033";
    $altcolorb="#000020";
    $altcolora="' class='altcolora";
    $altcolorb="' class='altcolorb";
    if($orderby == "id_up"){
        $order = "id";
    }else if ($orderby == "id_down"){

        $order = "id desc";
    }else if ($orderby == "joindate_down"){

        $order = "joindate desc";
    }else if ($orderby == "joindate_up"){

        $order = "joindate";
    }else if ($orderby == "alias_down"){

        $order = "alias desc";
    }else if ($orderby == "alias_up"){

        $order = "alias";
    }else if ($orderby == "email_down"){

        $order = "email desc";
    }else if ($orderby == "email_up"){

        $order = "email";
    }else if ($orderby == "icq_down"){

        $order = "icq desc";
    }else if ($orderby == "icq_up"){

        $order = "icq";
    }else if ($orderby == "aim_down"){

        $order = "aim desc";
    }else if ($orderby == "aim_up"){

        $order = "aim";
    }else if ($orderby == "msn_down"){

        $order = "msn desc";
    }else if ($orderby == "msn_up"){

        $order = "msn";
    }else if ($orderby == "country_down"){

        $order = "country desc";
    }else if ($orderby == "country_up"){

        $order = "country";
    }else if ($orderby == "website_down"){

        $order = "website desc";
    }else if ($orderby == "website_up"){

        $order = "website";
    }else{

        $order = "alias";
    }
    $result=mysql_query("SELECT id,alias,email,icq,aim,msn,website,country,DATE_FORMAT(joindate, '%M %d'),ipaddress from users ORDER BY $order LIMIT $start,$stop");
      

    while(list($pid,$alias,$email,$icq,$aim,$msn,$website,$country,$joindate,$ip)=mysql_fetch_row($result)) {
        if($altladrow=="$altcolora"){
            $altladrow="$altcolorb";
        }else{

            $altladrow="$altcolora";
        }

        if($icq > 0){
            $icq="
            <a href='http://wwp.icq.com/scripts/search.dll?to=$icq'><img src='$url[themes]/images/icq_addme.gif' border='0'></a>";
        }else{

            $icq="";
        }

        if($aim){
            $aim="<a href='aim:addbuddy?screenname=$aim'><img src='$url[themes]/images/aim.gif' border='0'></a>";
        }else{

            $aim="";
        }

        if($msn){
            $msn="<a href='mailto:$msn'><img src='$url[themes]/images/msn.gif' border='0'></a>";
        }else{

            $msn="";
        }

        if($website){
            $website="<a href='$website' target='top'><img src='$url[themes]/images/home.gif' border='0'></a>";
        }else{

            $website="";
        }

        if($country){
            $country="<img width='20' height='14' src='$url[themes]/images/flags/$country[0]$country[1]_small.gif'>";
        }else{

            $country="";
        }

        if($ip){
            $ip="<img src='$url[themes]/images/ip.gif' border='0' alt='$ip' style='cursor:hand;'>";
        }else{

            $ip="?";
        }

        $playerlist=$playerlist."
        <tr bgcolor='$altladrow'>
        <td width='' valign='center' align='left'><a href='$url[base]/$file[players]?playerid=$pid'>$alias</a></td>
        <td width='' valign='center' align='center'>$pid</td>
        <td width='' valign='center' align='center'>$joindate</td>
        <td width='' valign='center' align='center'><a href='mailto:$email'><img src='$url[themes]/images/email.gif' border='0'></a></td>
        <td width='' valign='center' align='center'>$icq</td>
        <td width='' valign='center' align='center'>$aim</td>
        <td width='' valign='center' align='center'>$msn</td>
        <td width='' valign='center' align='center'>$website</td>
        ";
        if($misc[flags] == "yes"){
            $playerlist=$playerlist."
            <td width='' valign='center' align='center'>$country</td>
            ";
        }

        if($admn[id]){
            $playerlist=$playerlist."<td width='' valign='center' align='center'>$ip</td>";
        }

        $playerlist=$playerlist."</tr>";
        $counter++;
    }

    if($order == "alias desc"){
        $alias = "alias_up";
    }else{

        $alias = "alias_down";
    }

    if($order == "id desc"){
        $id = "id_up";
    }else{

        $id = "id_down";
    }

    if($order == "joindate desc"){
        $joindate = "joindate_up";
    }else{

        $joindate = "joindate_down";
    }

    if($order == "email desc"){
        $email = "email_up";
    }else{

        $email = "email_down";
    }

    if($order == "icq desc"){
        $icq = "icq_up";
    }else{

        $icq = "icq_down";
    }

    if($order == "aim desc"){
        $aim = "aim_up";
    }else{

        $aim = "aim_down";
    }

    if($order == "msn desc"){
        $msn = "msn_up";
    }else{

        $msn = "msn_down";
    }

    if($order == "website desc"){
        $website = "website_up";
    }else{

        $website = "website_down";
    }

    if($order == "country desc"){
        $country = "country_up";
    }else{

        $country = "country_down";
    }

    $tablehead=table_head("show","100%","","left");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    <br>
    <center>
    $tablehead
    &nbsp; &nbsp;<strong><font class='catfont'>$site[shortname] Player Database</font></strong><br>
    <hr class='catfont' size='1'>
    <table width='100%' border='0' bordercolor='#000000' cellspacing='1' cellpadding='1'>
    <tr class='altcolor'>
    <td width='25%' valign='center' align='left'><strong><a href='$url[base]/$file[stats]?action=playerdb&orderby=$alias'>Player Name</a></strong></td>
    <td width='5%' valign='center' align='center'><strong><a href='$url[base]/$file[stats]?action=playerdb&orderby=$id'>ID</a></strong></td>
    <td width='15%' valign='center' align='center'><strong><a href='$url[base]/$file[stats]?action=playerdb&orderby=$joindate'>Joindate</a></strong></td>
    <td width='10%' valign='center' align='center'><strong><a href='$url[base]/$file[stats]?action=playerdb&orderby=$email'>Email</a></strong></td>
    <td width='10%' valign='center' align='center'><strong><a href='$url[base]/$file[stats]?action=playerdb&orderby=$icq'>Icq</a></strong></td>
    <td width='10%' valign='center' align='center'><strong><a href='$url[base]/$file[stats]?action=playerdb&orderby=$aim'>Aim</a></strong></td>
    <td width='10%' valign='center' align='center'><strong><a href='$url[base]/$file[stats]?action=playerdb&orderby=$msn'>MSN</a></strong></td>
    <td width='9%' valign='center' align='center'><strong><a href='$url[base]/$file[stats]?action=playerdb&orderby=$website'>Website</a></strong></td>
    ";
    if($misc[flags] == "yes"){
        $out[body]=$out[body]."
        <td width='8%' valign='center' align='center'><strong><a href='$url[base]/$file[stats]?action=playerdb&orderby=$country'>Flag</a></strong></td>
        ";
    }

    if($admn[id]){
        $out[body]=$out[body]."<td width='5%' valign='center' align='center'><strong>Ip</strong></td>";
    }

    $out[body]=$out[body]."
    </tr>
    $playerlist
    ";
    $pagenumb=1;
    if ($totalusers > $maxresults) {
        $totalpages=($totalusers / $maxresults);
        while($pagenum < $totalpages){
            $pagenum++;
            if($pagenumb=="$page"){
                $pages=$pages."[<a href='$url[base]/$file[stats]?action=playerdb&orderby=$orderby&page=$pagenumb'>$pagenum</a>] ";
            }else{

                $pages=$pages."<a href='$url[base]/$file[stats]?action=playerdb&orderby=$orderby&page=$pagenumb'>$pagenum</a> ";
            }

            $pagenumb=($pagenumb + $maxresults);
        }

    }

    if($pages){
        $out[body]=$out[body]."
        <tr class='altcolor'>
        <td width='100%' valign='center' align='center' colspan='10'><strong>$pages</strong></td>
        </tr>";
    }

    $out[body]=$out[body]."
    </table>
    $tablefoot";
    $tablehead=table_head("show","400","","left");
    $tablefoot=table_foot("show");
    $out[body]=$out[body]."
    <form method='post' action='$url[base]/$file[search]'>
    $tablehead
    &nbsp; &nbsp;<strong><font class='catfont'>Search $site[longname]</font></strong><br>
    <hr class='catfont' size='1'>
    <center>
    <input type='text' name='find' value='' size='30' maxlength='50'>
    <select name='type'>
    <option value='2'>Players</option>
    <option value='1'>Teams</option>
    </select>
    <input type='hidden' name='action' value='search'>
    <input type='submit' name='' value='Search'>
    </center>
    $tablefoot
    </form>
    </center>";
    include("$dir[curtheme]");
}

?>
