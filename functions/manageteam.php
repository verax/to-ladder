<?PHP
function manage_members($teamid){
    global $dir, $file, $url, $out, $plyr;
    if(!$teamid){
        include("$dir[func]/error.php");
        display_error("Invalid Team ID.<br>");
    }

    //CHECK IF LEADER OR CO-LEADER
    if (mysql_num_rows(mysql_query("SELECT status FROM teammembers WHERE teamid='$teamid' AND playerid='$plyr[id]' AND status <='2'")) < 1){
        include("$dir[func]/error.php");
        display_error("You are not allowed to manage members on this team.<br>");
    }

    $teaminfo=mysql_query("SELECT teamname FROM teams WHERE id='$teamid'");
    $tinfo=mysql_fetch_array($teaminfo);
    if(!$tinfo[teamname]){
        include("$dir[func]/error.php");
        display_error("Unknown Team.<br>");
    }

    $altcolora="#000033";
    $altcolorb="#000020";
    $listmembers=mysql_query("SELECT playerid,status,DATE_FORMAT(joindate, '%M %d, %Y') FROM teammembers WHERE teamid='$teamid' ORDER by joindate");
    while(list($playerid,$status,$joindate)=mysql_fetch_row($listmembers)){
        $memberinfo=mysql_query("SELECT alias FROM users WHERE id='$playerid'");
        $playerinfo=mysql_fetch_array($memberinfo);
        $selected[$status]="SELECTED";
        if($altladrow=="$altcolora"){
            $altladrow="$altcolorb";
        }else{

            $altladrow="$altcolora";
        }

        $playerdrop=$playerdrop."
        <tr bgcolor='$altladrow'>
        <td width='35%' valign='top' align='left'>
        <strong>$playerinfo[alias]</strong></td>
        <td width='35%' valign='top' align='center'>
        <strong>$joindate</strong></td>
        <td width='30%' valign='top' align='center'>
        <select name='memberrank[$playerid]'>
        <option value='1' $selected[1]>Lider</option>
        <option value='2' $selected[2]>Co-Lider</option>
        <option value='3' $selected[3]>Capitan</option>
        <option value='4' $selected[4]>Co-Capitan</option>
        <option value='5' $selected[5]>Miembro</option>
        <option value='6' $selected[6]>Mascota</option>
        <option value='7' $selected[7]>Inactivo</option>
        <option value='8' $selected[8]>Suspendido</option>
        <option value='9' $selected[9]>Exiliado</option>
        </select></td>
        </tr>";
        $selected[$status]="";
        $memberexist=1;
    }

    $tablehead=table_head("show","100%","","left");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    <br>
    $tablehead
    &nbsp; &nbsp;<strong><font class='catfont'>Manage Team Members</font></strong><br>
    <hr class='catfont' size='1'>
    <table width='100%' border='0' cellspacing='1' cellpadding='1'>
    <tr class='altcolor'>
    <td width='35%' valign='top' align='left'>
    <strong>Player Name</strong></td>
    <td width='35%' valign='top' align='center'>
    <strong>Joined Team</strong></td>
    <td width='30%' valign='top' align='center'>
    <strong>Member Rank</strong></td>
    </tr>
    <form method='post'>
    $playerdrop
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center' colspan='3'>
    <input type='hidden' name='teamid' value='$teamid'>
    <input type='hidden' name='action' value='membersb'>
    <input type='submit' name='' value='Update'>
    <input type='reset' name='' value='Reset'></td>
    </tr>
    </form>
    </table>
    $tablefoot
    <br>
    $tablehead
    &nbsp; &nbsp;<strong><font class='catfont'>Member Rights Legend</font></strong><br>
    <hr class='catfont' size='1'>
    <table width='100%' border='0'  cellspacing='1' cellpadding='0'>
    <tr>
    <ul>
    <td width='100%' height='100%' valign='top' align='left' colspan='3'>
    <li>Leader: Can do everything.<br>
    <li>Co-Leader: Invite, Kick and Manage Members, Report Losses<br>
    <li>Captain: Make and Accept Challenges, Report Losses<br>
    <li>Co-Captain: Report Losses<br>
    <li>Member: Regular Member<br>
    <li>Training: Cannot play in matches.<br>
    <li>Inactive: Cannot play in matches.<br>
    <li>Suspended: Cannot play in matches.<br>
    <li>Kick: Removes player from your team.<br>
    </td>
    </ul>
    </tr>
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function manage_membersb($teamid,$memberrank){
    global $dir, $file, $url, $out, $plyr;
    if(!$teamid){
        include("$dir[func]/error.php");
        display_error("Invalid Team ID.<br>");
    }

    if(!$memberrank){
        include("$dir[func]/error.php");
        display_error("Invalid Member ID's.<br>");
    }

    //CHECK IF LEADER OR CO-LEADER
    if (mysql_num_rows(mysql_query("SELECT status FROM teammembers WHERE teamid='$teamid' AND playerid='$plyr[id]' AND status <='2'")) < 1){
        include("$dir[func]/error.php");
        display_error("You are not allowed to manage members on this team.<br>");
    }

    $teammemberinfo=mysql_query("SELECT status FROM teammembers WHERE teamid='$teamid' AND playerid='$plyr[id]'");
    $tmeminfo=mysql_fetch_array($teammemberinfo);
    if(!$tmeminfo[status] > 2){
        include("$dir[func]/error.php");
        display_error("You are not allowed to manage this teams members.<br>");
    }

    $teaminfo=mysql_query("SELECT teamname FROM teams WHERE id='$teamid'");
    $tinfo=mysql_fetch_array($teaminfo);
    if(!$tinfo[teamname]){
        include("$dir[func]/error.php");
        display_error("Unknown Team.<br>");
    }

    $leadcheckranks=$memberrank;
    while(list($pplayerid,$pmrank)=each($leadcheckranks)){
        if($pmrank=="1"){
            $leaderfound=1;
        }

    }

    if(!$leaderfound){
        include("$dir[func]/error.php");
        display_error("You must have a team leader.<br>");
    }

    while(list($playerid,$mrank)=each($memberrank)){
        $ememinfo=mysql_query("SELECT status FROM teammembers WHERE teamid='$teamid' AND playerid='$playerid'");
        $eminfo=mysql_fetch_array($ememinfo);
        if(($tmeminfo[status] <= $eminfo[status]) && ($tmeminfo[status] <= $mrank)){
            if($mrank=="9"){
                mysql_query("DELETE FROM teammembers WHERE teamid='$teamid' AND playerid='$playerid'");
            }

            else if($eminfo[status]!=$mrank){
                mysql_query("UPDATE teammembers SET status='$mrank' WHERE teamid='$teamid' AND playerid='$playerid'");
            }

        }

    }

    include("$dir[func]/finishmessage.php");
    display_message("You Member Information has been Updated","phome");
}

?>
