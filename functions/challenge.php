<?PHP
if(!IsSet($indexloaded)){
    header("Location: ./index.php");
}

function team_challenge($ladderid,$cteamid,$teamid){
    global $dir, $file, $url, $out, $plyr, $site,$misc;
    $now=date("Y-m-d H:i:s");
    if(!$ladderid){
        include("$dir[func]/error.php");
        display_error("Invalid Ladder ID.<br>");
    }

    $teams=mysql_query("SELECT teamid FROM teammembers WHERE playerid='$plyr[id]'");
    while($row = mysql_fetch_array($teams)){
        $team = mysql_query("SELECT id FROM ladder_$ladderid WHERE id = '$row[teamid]'");
        $tid = mysql_fetch_array($team);
        if($tid[id]){
            $tmember[teamid] = $tid[id];
        }

    }

    if(!$tmember[teamid]){
        include("$dir[func]/error.php");
        display_error("You are not allowed to make challenges for this team.<br>");
    }

    if(($cteamid) && ($cteamid!="$tmember[teamid]")){
        include("$dir[func]/error.php");
        display_error("Invalid Team ID.<br>");
    }

    $teaminfo=mysql_query("SELECT teamname FROM teams WHERE id='$tmember[teamid]'");
    $tinfo=mysql_fetch_array($teaminfo);
    if(!$tinfo[teamname]){
        include("$dir[func]/error.php");
        display_error("Unknown Team.<br>");
    }

    $ladderinfo=mysql_query("SELECT * FROM ladders WHERE id='$ladderid'");
    $linfo=mysql_fetch_array($ladderinfo);
    if(!$linfo[laddername]){
        include("$dir[func]/error.php");
        display_error("Unknown Ladder.<br>");
    }

    if(!$linfo[challenge]){
        include("$dir[func]/error.php");
        display_error("Challenging is not allowed on this ladder.<br>");
    }

    //CHECK FOR GAME STATUS
    if (mysql_num_rows(mysql_query("SELECT id FROM games WHERE status < '1' AND id='$linfo[gameon]'")) > 0){
        include("$dir[func]/error.php");
        display_error("This Ladder is temporarily closed.<br>");
    }

    //CHECK IF CHALLENGER IS IN EXISTING CHALLENGE
    if(mysql_num_rows(mysql_query("SELECT challenger FROM challenges WHERE (challenger='$cteamid' OR challenged='$cteamid') AND ladderid = '$linfo[id]'")) > 0){
        include("$dir[func]/error.php");
        display_error("You are already involved in a challenge.<br>");
    }

    $challengerstats=mysql_query("SELECT status,statusdate,rank FROM ladder_$ladderid WHERE id='$tmember[teamid]'");
    $cstats=mysql_fetch_array($challengerstats);
    if(($cstats[status]=="10") && ($cstats[statusdate] > $now)){
        include("$dir[func]/error.php");
        display_error("Your team is locked and cannot challenge untill $cstats[statusdate].<br>");
    }

    if(($cstats[status]=="21") && ($cstats[statusdate] > $now)){
        include("$dir[func]/error.php");
        display_error("Your team cannot challenge untill $cstats[statusdate].<br>");
    }

    //if($cstats[rank]=="1"){
    //include("$dir[func]/error.php");
    //display_error("You are already ranked 1, There is nobody to challenge.<br>");
    //}

    if($cstats[rank]=="0"){
        $totalranked=mysql_query("SELECT COUNT(*) FROM ladder_$ladderid WHERE rank > '0'");
        $totalranks=mysql_fetch_array($totalranked);
        $totalranks="$totalranks[0]";
        if($totalranks < 1){
            include("$dir[func]/error.php");
            display_error("There are no ranked teams to challenge.<br>");
        }

        $challengerank=($totalranks - $linfo[challranks]);
    }

    else{
        $challengerank=($cstats[rank] - $linfo[challranks]);
        if($misc[challengedown] == "yes"){
            $challengerankdown=($cstats[rank] + $linfo[challranks]);
        }else if($misc[challengedown] == "no"){

            $challengerankdown=$cstats[rank];
        }

    }

    if($challengerank < 1){
        $challengerank="1";
    }

    $totalranked=mysql_query("SELECT COUNT(*) FROM ladder_$ladderid WHERE rank > '0'");
    $totalranked=mysql_fetch_array($totalranked);
    $totalranked="$totalranked[0]";
    if($challengerankdown > $totalranked){
        $challengerankdown=$totalranked;
    }

    if($teamid){
        $challengeranks=mysql_query("SELECT id,status,statusdate FROM ladder_$ladderid WHERE rank >= '$challengerank' AND rank <= '$challengerankdown' AND rank != '$cstats[rank]' AND id='$teamid'");
        $challenged=mysql_fetch_array($challengeranks);
        if(!$challenged[id]){
            include("$dir[func]/error.php");
            display_error("Invalid Challenge.<br>");
        }

        $challengableteam=mysql_query("SELECT teamname FROM teams WHERE id='$teamid'");
        $cteam=mysql_fetch_array($challengableteam);
        if(!$cteam[teamname]){
            include("$dir[func]/error.php");
            display_error("Unknown Team.<br>");
        }

        if(($challenged[status]=="10") && ($challenged[statusdate] > $now)){
            include("$dir[func]/error.php");
            display_error("This team is locked and cannot be challenged untill $challenged[statusdate].<br>");
        }

        if(($challenged[status]=="20") && ($challenged[statusdate] > $now)){
            include("$dir[func]/error.php");
            display_error("This team cannot be challenged untill $challenged[statusdate].<br>");
        }

        $out[body]=$out[body]."
        <table width='100%' align='center' border='0' cellspacing='0' cellpadding='5'>
        <tr>
        <td width='100%' valign='top' align='left'>
        <br><br><br>
        <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
        <tr class='altcolor'>
        <td width='100%' valign='top' align='center'>
        <strong>Confirm this challenge</strong><br>
        </td>
        </tr>
        <tr>
        <form method='post'>
        <td width='100%' valign='center' align='center'>
        <table width='100%' align='center' border='0' cellspacing='2' cellpadding='0'>
        <tr>
        <td width='50%' valign='center' align='left'>
        <br><ul>
        Ladder: <a href='$url[base]/$file[ladder]?ladder=$ladderid'>$linfo[laddername]</a><br>
        Challenger: <a href='$url[base]/$file[teams]?teamid=$tmember[teamid]'>$tinfo[teamname]</a><br>
        Challenged: <a href='$url[base]/$file[teams]?teamid=$teamid'>$cteam[teamname]</a>
        </ul></td>
        <td width='50%' valign='center' align='center'>
        Server/Host and Misc. Information<br>
        <textarea name='comment' rows='5' cols='50' maxlength='200'>
        </textarea><br>
        200 Characters Max</td>
        </tr>
        </table>
        </td>
        </tr>
        <tr class='altcolor'>
        <td width='100%' valign='top' align='center'>
        <input type='submit' name='' value='Challenge'>
        <input type='hidden' name='action' value='challengeb'>
        <input type='hidden' name='ladderid' value='$ladderid'>
        <input type='hidden' name='cteamid' value='$tmember[teamid]'>
        <input type='hidden' name='teamid' value='$teamid'>
        </td>
        </tr>
        </table>
        <br>
        </td>
        </form>
        </tr>
        </table>";
    }else{

        $challengeranks=mysql_query("SELECT id,status,statusdate,rank,wins,losses,percent,streak,skill,forfeits FROM ladder_$ladderid WHERE rank >= '$challengerank' AND rank <= '$challengerankdown' AND rank != '$cstats[rank]' ORDER by rank");
        while(list($id,$status,$statusdate,$rank,$wins,$losses,$percent,$streak,$skill,$forfeits)=mysql_fetch_row($challengeranks)){
            if(($status=="10") && ($statusdate > $now)){
                $id="";
            }

            else if(($status=="20") && ($statusdate > $now)){
                $id="";
            }

            else if(mysql_num_rows(mysql_query("SELECT challenger FROM challenges WHERE challenger='$id' OR challenged='$id'")) > 0){
                $id="";
            }

            else{
                if($id){
                    $challengableteam=mysql_query("SELECT teamname FROM teams WHERE id='$id'");
                    $cteam=mysql_fetch_array($challengableteam);
                }

                if($cteam[teamname]){
                    $canchall=$canchall."
                    <tr bgcolor='#000033'>
                    <td width='' valign='center' align='left'>
                    <a href='$url[base]/$file[match]?action=challenge&ladderid=$ladderid&cteamid=$tmember[teamid]&teamid=$id'>$cteam[teamname]</a></td>
                    <td width='' valign='center' align='center'><font color='#0099FF'>$rank</font></td>
                    <td width='' valign='center' align='center'><font color='#0099FF'>$wins</font></td>
                    <td width='' valign='center' align='center'><font color='#0099FF'>$losses</font></td>
                    <td width='' valign='center' align='center'><font color='#0099FF'>$percent%</font></td>
                    <td width='' valign='center' align='center'><font color='#0099FF'>$streak</font></td>
                    </tr>";
                }

            }

        }

        if($canchall){
            $tailmessage="Your Rank: $cstats[rank], Highest Challengable Rank: $challengerank, Challenge Limit: $linfo[challranks]";
        }else{

            $tailmessage="There are currently no available challenges for you";
        }

        $out[body]=$out[body]."
        <table width='100%' align='center' border='0' cellspacing='0' cellpadding='5'>
        <tr>
        <td width='100%' valign='top' align='left'>
        <br><br><br><br>
        <table width='100%' border='0' cellspacing='2' cellpadding='1'>
        <tr class='altcolor'>
        <td width='100%' valign='top' align='left' colspan='8'>
        <font size='1' class='catfont'><strong>
        Click on the team you wish to challenge</strong></font></td>
        </tr>
        <tr class='altcolor'>
        <td width='' valign='center' align='left'><font size='1' class='catfont'><strong>Name</strong></font></td>
        <td width='' valign='center' align='center'><font size='1' class='catfont'><strong>Rank</strong></font></td>
        <td width='' valign='center' align='center'><font size='1' class='catfont'><strong>Wins</strong></font></td>
        <td width='' valign='center' align='center'><font size='1' class='catfont'><strong>Losses</strong></font></td>
        <td width='' valign='center' align='center'><font size='1' class='catfont'><strong>Win%</strong></font></td>
        <td width='' valign='center' align='center'><font size='1' class='catfont'><strong>Streak</strong></font></td>
        </tr>
        $canchall
        <tr class='altcolor'>
        <td width='' valign='center' align='center' colspan='8'>
        <font size='1'>$tailmessage</font></td>
        </tr>
        </table>
        <br>
        </td>
        </tr>
        </table>";
    }//AXA

    include("$dir[curtheme]");
}

function team_challengeb($ladderid,$cteamid,$teamid,$comment){
    global $dir, $file, $url, $out, $plyr, $site, $misc;
    $now=date("Y-m-d H:i:s");
    if(!$ladderid){
        include("$dir[func]/error.php");
        display_error("Invalid Ladder ID.<br>");
    }

    if(!$cteamid){
        include("$dir[func]/error.php");
        display_error("Invalid Challenger ID.<br>");
    }

    if(!$teamid){
        include("$dir[func]/error.php");
        display_error("Invalid Challenged ID.<br>");
    }

    if($cteamid=="$teamid"){
        include("$dir[func]/error.php");
        display_error("You cannot challenge yourself.<br>");
    }

    if (mysql_num_rows(mysql_query("SELECT teamid FROM teammembers WHERE playerid='$plyr[id]' AND teamid='$cteamid' AND status <= '3'")) < 1){
        include("$dir[func]/error.php");
        display_error("You are not allowed to make challenges for this team.<br>");
    }

    //CHECK CHALLENGER
    $cteaminfo=mysql_query("SELECT teamname FROM teams WHERE id='$cteamid'");
    $ctinfo=mysql_fetch_array($cteaminfo);
    $cladderinfo=mysql_query("SELECT id,status,statusdate,rank FROM ladder_$ladderid WHERE id='$cteamid'");
    $cladinfo=mysql_fetch_array($cladderinfo);
    if((!$ctinfo[teamname]) || (!$cladinfo[id])){
        include("$dir[func]/error.php");
        display_error("Unable to find your team information.<br>");
    }

    //CHECK CHALLENGED
    $teaminfo=mysql_query("SELECT teamname,teamemail FROM teams WHERE id='$teamid'");
    $tinfo=mysql_fetch_array($teaminfo);
    $ladderinfo=mysql_query("SELECT id,status,statusdate,rank FROM ladder_$ladderid WHERE id='$teamid'");
    $ladinfo=mysql_fetch_array($ladderinfo);
    if((!$tinfo[teamname]) || (!$ladinfo[id])){
        include("$dir[func]/error.php");
        display_error("Unable to find the challenged teams information.<br>");
    }

    //CHECK LADDER INFO
    $ladderinfo=mysql_query("SELECT * FROM ladders WHERE id='$ladderid'");
    $linfo=mysql_fetch_array($ladderinfo);
    if(!$linfo[laddername]){
        include("$dir[func]/error.php");
        display_error("Unknown Ladder.<br>");
    }

    //CHECK IF CHALLENGER IS IN EXISTING CHALLENGE
    if(mysql_num_rows(mysql_query("SELECT challenger FROM challenges WHERE (challenger='$cteamid' OR challenged='$cteamid') AND ladderid = '$linfo[id]'")) > 0){
        include("$dir[func]/error.php");
        display_error("You are already involved in a challenge.<br>");
    }

    //CHECK IF CHALLENGED IS IN EXISTING CHALLENGE
    if(mysql_num_rows(mysql_query("SELECT challenger FROM challenges WHERE challenger='$teamid' OR challenged='$teamid'")) > 0){
        include("$dir[func]/error.php");
        display_error("This team is already involved in a challenge.<br>");
    }

    //CHECK CHALLENGERs STATUS
    $challengerstats=mysql_query("SELECT status,statusdate,rank FROM ladder_$ladderid WHERE id='$cteamid'");
    $cstats=mysql_fetch_array($challengerstats);
    if(($cstats[status]=="10") && ($cstats[statusdate] > $now)){
        include("$dir[func]/error.php");
        display_error("Your team is locked and cannot challenge untill $cstats[statusdate].<br>");
    }

    if(($cstats[status]=="21") && ($cstats[statusdate] > $now)){
        include("$dir[func]/error.php");
        display_error("Your team cannot challenge untill $cstats[statusdate].<br>");
    }

    //CHECK CHALLENGEDs STATUS
    $challengedstats=mysql_query("SELECT status,statusdate,rank FROM ladder_$ladderid WHERE id='$teamid'");
    $stats=mysql_fetch_array($challengedstats);
    if(($stats[status]=="10") && ($stats[statusdate] > $now)){
        include("$dir[func]/error.php");
        display_error("This team is locked and cannot be challenged untill $stats[statusdate].<br>");
    }

    if(($stats[status]=="21") && ($stats[statusdate] > $now)){
        include("$dir[func]/error.php");
        display_error("This team cannot be challenged untill $stats[statusdate].<br>");
    }

    if($cstats[rank]=="0"){
        $totalranked=mysql_query("SELECT COUNT(*) FROM ladder_$ladderid WHERE rank > '0'");
        $totalranks=mysql_fetch_array($totalranked);
        $totalranks="$totalranks[0]";
        if($totalranks < 1){
            include("$dir[func]/error.php");
            display_error("There are no ranked teams to challenge.<br>");
        }

        $challengerank=($totalranks - $linfo[challranks]);
    }else{

        $challengerank=($cstats[rank] - $linfo[challranks]);
        if($misc[challengedown] == "yes"){
            $challengerankdown=($cstats[rank] + $linfo[challranks]);
        }else if($misc[challengedown] == "no"){

            $challengerankdown=$cstats[rank];
        }

    }

    if($challengerank < 1){
        $challengerank="1";
    }

    $totalranked=mysql_query("SELECT COUNT(*) FROM ladder_$ladderid WHERE rank > '0'");
    $totalranked=mysql_fetch_array($totalranked);
    $totalranked="$totalranked[0]";
    if($challengerankdown > $totalranked){
        $challengerankdown=$totalranked;
    }

    //CHECK IF VALID RANKED CHALLENGE
    if(mysql_num_rows(mysql_query("SELECT rank FROM ladder_$ladderid WHERE id='$teamid' AND (rank >= '$challengerank' AND  rank <= '$challengerankdown') AND rank != '$cstats[rank]'")) < 1){
        include("$dir[func]/error.php");
        display_error("Invalid Challenge.<br>");
    }

    if($comment){
        $comment=wordwrap($comment,20," ",1);
        include("$dir[func]/checkdata.php");
        $comment=change_censor($comment);
        $comment=change_charecters($comment);
    }

    // CREATE CHALLENGE
    $tday=date("Y-m-d H:i:s");
    $mcode=md5(uniqid(microtime()));
    $mcode="$mcode";
    mysql_query("INSERT INTO challenges VALUES (
    '$mcode',
    '$cteamid',
    '$teamid',
    '$ctinfo[teamname]',
    '$tinfo[teamname]',
    '$ladderid',
    '$cstats[rank]',
    '$stats[rank]',
    '$comment',
    '$plyr[id]',
    '$tday',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '');");
    $getchallenge=mysql_query("SELECT challid FROM challenges WHERE challenger='$cteamid' AND challenged='$teamid'");
    $chall=mysql_fetch_array($getchallenge);
    if($chall[challid]){
        //UPDATE CHALLENGER
        mysql_query("UPDATE ladder_$ladderid SET
        statusdisplay='<a href=$url[base]/$file[match]?challid=$chall[challid]>Has challenged</a>'
        WHERE id='$cteamid'");
        //UPDATE CHALLENGED
        mysql_query("UPDATE ladder_$ladderid SET
        statusdisplay='<a href=$url[base]/$file[match]?challid=$chall[challid]>Has been challenged</a>'
        WHERE id='$teamid'");
        //SEND EMAIL TO CHALLENGED TEAM
        $emailbody="You have been challenged on $site[shortname]:
        Your Team: $tinfo[teamname]
        Challenging Team: $ctinfo[teamname]
        $url[base]/$file[teams]?teamid=$cteamid
        Ladder: $linfo[laddername]\n
        You must respond to this challenge within $linfo[respondhours] hours. If you fail to respond you will recieve an automatic forfeit loss.\n
        Go to the following url to respond to this challenge or click on respond to challenge on the ladder page:
        $url[base]/$file[match]?action=respond&challid=$chall[challid]";
        include("$dir[func]/email.php");
        send_email($tinfo[teamname],$tinfo[teamemail],"Challenge Notification",$emailbody);
        $out[body]=$out[body]."
        <br><br><br>
        <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
        <tr class='altcolor'>
        <td width='100%' valign='top' align='center'>
        <strong>Your Challenge has been recorded</strong><br>
        </td>
        </tr>
        <tr>
        <td width='100%' valign='center' align='left'>
        <br><ul>
        Ladder: <a href='$url[base]/$file[ladder]?ladderid=$ladderid'>$linfo[laddername]</a><br>
        Challenger: <a href='$url[base]/$file[teams]?teamid=$cteamid'>$ctinfo[teamname]</a><br>
        Your Rank: $cstats[rank]<br>
        Challenged: <a href='$url[base]/$file[teams]?teamid=$teamid'>$tinfo[teamname]</a><br>
        Challenged Rank: $stats[rank]<br>
        </ul></td>
        </tr>
        <tr class='altcolor'>
        <td width='100%' valign='top' align='center'>
        <strong><a href='$url[base]/$file[match]?challid=$chall[challid]'>View your challenge</a></strong><br>
        </td>
        </tr>
        </table>";
    }else{

        $out[body]=$out[body]."
        <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
        <tr class='altcolor'>
        <td width='100%' valign='top' align='center'>
        <strong>Challenge Error</strong><br>
        </td>
        </tr>
        <tr>
        <td width='100%' valign='center' align='left'>
        <br><ul>
        An Unknown Error has occurred<br>
        Contact Staff for help
        </ul></td>
        </tr>
        <tr class='altcolor'>
        <td width='100%' valign='top' align='center'>
        <strong>Challenge Error</strong><br>
        </td>
        </tr>
        </table>";
    }

    include("$dir[curtheme]");
}

/*
1 = challenger-matchnotaccepted <- cant challenge
2 = challenged-matchnotaccepted <- cant challenge
3 = challenger-matchscheduled <- cant challenge
4 = challenged-matchscheduled <- cant challenge
5 = wonchallenge <- 24 hour window cant be challenged
6 = lostchallenge <- cant challenge for 24 hours
*/
?>
