<?
if(!IsSet($indexloaded)){
    header("Location: ./index.php");
}

//CHECK ADMIN ACCESS LEVEL
if($admn[access] < 85){
    include("$dir[func]/error.php");
    display_error("You are not allowed to perform this function.<br>");
}

function admin_links(){
    global $dir, $url, $out, $site, $admn;
    //GET FILES
    $faqslist=mysql_query("SELECT id,name FROM links ORDER by name");
    while(list($id,$name)=mysql_fetch_row($faqslist)){
        $thelinks=$thelinks."<option value='$id'>$name</option>";
    }

    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <form method='post'>
    <td width='100%' valign='top' align='center'>
    <strong>Link Manager</strong><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <br>
    <script language='javascript'>var confirmdelete='Are you sure you want to DELETE this Link';</script>
    <select name='linkid'>
    <option value=''>Select a link or create a new one</option>
    $thelinks</select><br>
    <br></td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='action' value='linkmanb'>
    <input type='submit' name='todo' value='Create Link'>
    <input type='submit' name='todo' value='Edit Link'>
    <input type='submit' name='todo' value='Delete Link' onClick='return confirm(confirmdelete);'>
    </td>
    </form>
    </tr>
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function admin_linksb($linkid,$todo){
    global $dir, $url, $out, $site, $admn;
    if((!$linkid) && ($todo!="Create Link")){
        include("$dir[func]/error.php");
        display_error("Invalid Link ID.<br>");
    }

    if($todo=="Create Link"){
        create_link();
    }

    else if($todo=="Edit Link"){
        edit_link($linkid);
    }

    else if($todo=="Delete Link"){
        delete_link($linkid);
        include("$dir[func]/admin_finishmessage.php");
        display_message("The link was deleted.<br>");
    }

    else{
        include("$dir[func]/error.php");
        display_error("Unknown Command.<br>");
    }

}

function create_link(){
    global $dir, $url, $out, $site, $admn;
    //TABLE ATERNATING COLORS
    $altcolora="#000033";
    $altcolorb="#000020";
    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    <table width='100%' border='0' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <form method='post'>
    <td width='100%' valign='top' align='center' colspan='2'>
    <strong>Create a Link</strong><br>
    </td>
    </tr>
    <tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Link Name (alt if image)</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='l_name' value='' size='50' maxlength='250'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Link Url</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='l_url' value='' size='50' maxlength='250'></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Image Url (not required)</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='l_img' value='' size='50' maxlength='250'></td>
    </tr>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center' colspan='2'>
    <input type='hidden' name='action' value='linkmanc'>
    <input type='submit' name='' value='Create Link'></td>
    </form>
    </tr>
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function admin_linksc($l_name,$l_url,$l_img){
    global $dir, $url, $out, $site, $admn;
    include("$dir[func]/checkdata.php");
    $errormessage=check_validlength($errormessage, $l_name, "3", "250", "Your link name must be 3-250 charecters");
    $errormessage=check_validlength($errormessage, $l_url, "7", "250", "Your link url must be 7-250 charecters");
    error_check($errormessage);
    $l_url=change_url($l_url);
    $l_img=change_url($l_img);
    $l_name=change_charecters($l_name);
    $l_url=change_charecters($l_url);
    $l_img=change_charecters($l_img);
    $tday=date("Y-m-d H:i:s");
    mysql_query("INSERT INTO links VALUES (
    NULL,
    '$l_name',
    '$l_url',
    '$l_img',
    '0',
    '$admn[id]',
    '$tday');");
    include("$dir[func]/admin_finishmessage.php");
    display_message("The link has been added.<br>");
}

function edit_link($linkid){
    global $dir, $url, $out, $site, $admn;
    $linkinfo=mysql_query("SELECT * FROM links WHERE id='$linkid'");
    $link=mysql_fetch_array($linkinfo);
    if(!$link[id]){
        include("$dir[func]/error.php");
        display_error("Unknown Link ID.<br>");
    }

    //TABLE ATERNATING COLORS
    $altcolora="#000033";
    $altcolorb="#000020";
    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    <table width='100%' border='0' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <form method='post'>
    <td width='100%' valign='top' align='center' colspan='2'>
    <strong>Edit a Link</strong><br>
    </td>
    </tr>
    <tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Link Name (alt is image)</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='l_name' value='$link[name]' size='50' maxlength='250'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Link Url</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='l_url' value='$link[linkurl]' size='50' maxlength='250'></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Image Url (not required)</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='l_img' value='$link[imageurl]' size='50' maxlength='250'></td>
    </tr>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center' colspan='2'>
    <input type='hidden' name='action' value='linkmand'>
    <input type='hidden' name='linkid' value='$linkid'>
    <input type='submit' name='' value='Update Link'></td>
    </form>
    </tr>
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function admin_linksd($linkid,$l_name,$l_url,$l_img){
    global $dir, $url, $out, $site, $admn;
    include("$dir[func]/checkdata.php");
    $errormessage=check_validlength($errormessage, $l_name, "3", "250", "Your link name must be 3-250 charecters");
    $errormessage=check_validlength($errormessage, $l_url, "7", "250", "Your link link must be 7-250 charecters");
    error_check($errormessage);
    $l_url=change_url($l_url);
    $l_img=change_url($l_img);
    $l_name=change_charecters($l_name);
    $l_url=change_charecters($l_url);
    $l_img=change_charecters($l_img);
    $tday=date("Y-m-d H:i:s");
    mysql_query("UPDATE links SET
    name='$l_name',
    linkurl='$l_url',
    imageurl='$l_img',
    lasteditby='$admn[id]',
    lastedit='$tday'
    WHERE id='$linkid'");
    include("$dir[func]/admin_finishmessage.php");
    display_message("The link has been updated.<br>");
}

function delete_link($linkid){
    mysql_query("DELETE FROM links WHERE id='$linkid'");
}

?>
