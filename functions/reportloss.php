<?PHP
function report_loss_init($ladderid){
    global $dir, $file, $url, $out, $plyr, $site;
    if($ladderid){
        $teamslist=mysql_query("SELECT id FROM ladder_$ladderid");
        while($row = mysql_fetch_array($teamslist)){
            $tlist = mysql_query("SELECT id,teamname FROM teams WHERE id = '$row[id]'");
            if(list($teamid,$name)=mysql_fetch_row($tlist)){
                $selectedteam="";
                if($id=="$teamid"){
                    $selectedteam="SELECTED";
                }

                $theteams.="<option value='$teamid' $selectedteam>$name</option>";
            }

        }

        $forminfo="
        Select Winner</small><br>
        <select name='winnerid'>$theteams</select>
        <br><small></small><br><br>
        <input type='hidden' name='ladderid' value='$ladderid'>
        <input type='hidden' name='action' value='report'>
        <button type='submit' name='' value='Report Loss' class='btn btn-mini btn-success'>Reportar derrota</button><br><br><br>";
    }else{

        $ladderslist=mysql_query("SELECT id,laddername FROM ladders ORDER by laddername");
        while(list($id,$name)=mysql_fetch_row($ladderslist)){
            $selectedladder="";
            if($ladderid=="$id"){
                $selectedladder="SELECTED";
            }

            $theladders.="<option value='$id' $selectedladder>$name</option>";
        }

        $forminfo="
        Seleccionar ladder</small><br>
        <select name='ladderid'>$theladders</select>
        <br><small></small><br><br>
        <input type='hidden' name='action' value='report_init'>
        <button type='submit' name='' value='Continue' class='btn btn-mini btn-info'>Continuar</button><br><br><br>";
    }

    $tablehead=table_head("show","300","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    <br>
    <center>
    $tablehead
    <table width='300' border='0' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr>
    <td width='100%' height='100%' valign='top' align='center'>
    <strong>Reportar una derrota</strong>
    <form method='post'>
    $loserforminfo
    $forminfo
    </form>
    </td>
    </tr>
    </table>
    $tablefoot
    </center>";
    include("$dir[curtheme]");
}

function report_loss($teamid,$winnerid,$challid,$ladderid){
    global $dir, $file, $url, $out, $plyr, $site;
    if($teamid){
        if($teamid=="$winnerid"){
            include("$dir[func]/error.php");
            display_error("You cannot report losses to yourself.<br>");
        }

    }

    if($teamid == '0'){
        include("$dir[func]/error.php");
        display_error("You are not on this ladder.<br>");
    }

    if($challid){
        $thechallenge=mysql_query("SELECT challenger,challenged,ladderid FROM challenges WHERE challid='$challid'");
        $chall=mysql_fetch_array($thechallenge);
        if(!$chall[ladderid]){
            include("$dir[func]/error.php");
            display_error("Unknown Challenge ID.<br>");
        }

        $ladderid = $chall[ladderid];
        $teams=mysql_query("SELECT teamid FROM teammembers WHERE playerid='$plyr[id]'");
        while($row = mysql_fetch_array($teams)){
            $team = mysql_query("SELECT id FROM ladder_$chall[ladderid] WHERE id ='$row[teamid]'");
            $tid = mysql_fetch_array($team);
            if($tid[id]){
                $rptinfo[teamid] = $tid[id];
            }

        }

        $teamid="$rptinfo[teamid]";
        if($rptinfo[teamid]=="$chall[challenger]"){
            $winnerid="$chall[challenged]";
        }else{

            $winnerid="$chall[challenger]";
        }

    }

    if($teamid){
        $losingteaminfo=mysql_query("SELECT teamname FROM teams WHERE id='$teamid'");
        $ltinfo=mysql_fetch_array($losingteaminfo);
        if(!$ltinfo[teamname]){
            include("$dir[func]/error.php");
            display_error("Unknown Losing Team.<br>");
        }

        $loserforminfo="<b>Losing Team:</b><br> $ltinfo[teamname]<br><input type='hidden' name='teamid' value='$teamid'>";
        $teamladder="$ladderid";
    }else{

        $teamson=mysql_query("SELECT teamid FROM teammembers WHERE playerid='$plyr[id]' AND status <='4' ORDER by joindate");
        while(list($team)=mysql_fetch_row($teamson)){
            $teaminfo=mysql_query("SELECT teamname FROM teams WHERE id='$team'");
            $tinfo=mysql_fetch_array($teaminfo);
            $currentmatch=mysql_query("SELECT challid,challenger,challenged FROM challenges WHERE challenger='$team' AND finalizedby > '0' OR challenged='$team' AND finalizedby > '0'");
            $cminfo=mysql_fetch_array($currentmatch);
            if($cminfo[challid]){
                if($team=="$cminfo[challenger]"){
                    $vsid="$cminfo[challenged]";
                }else{

                    $vsid="$cminfo[challenger]";
                }

                $challengingteam=mysql_query("SELECT teamname FROM teams WHERE id='$vsid'");
                $cmtinfo=mysql_fetch_array($challengingteam);
                $scheduledmatches=$scheduledmatches."";
            }

            $onteam=1;
        }

        if($onteam){
            $loserforminfo="";
        }else{

            include("$dir[func]/error.php");
            display_error("You are not on any teams. <br>You need to join a ladder before you can report losses.<br>");
        }

    }

    if($winnerid){
        if($challid){
            $wteam[teamid]="$winnerid";
        }else{

            if (mysql_num_rows(mysql_query("SELECT id FROM teams WHERE id='$winnerid'")) < 1){
                include("$dir[func]/error.php");
                display_error("Unknown Team ID.<br>");
            }

            $winnersteam=mysql_query("SELECT id FROM teams WHERE id='$winnerid'");
            $wteam=mysql_fetch_array($winnersteam);
        }

        $winnersteaminfo=mysql_query("SELECT teamname FROM teams WHERE id='$winnerid'");
        $wteaminfo=mysql_fetch_array($winnersteaminfo);
        if(!$wteaminfo[teamname]){
            include("$dir[func]/error.php");
            display_error("Unknown Winning Team.<br>");
        }

        $ldinfo=mysql_query("SELECT laddername FROM ladders WHERE id='$ladderid'");
        $lddinfo=mysql_fetch_array($ldinfo);
        $forminfo="
        <br>
        <b>Winning Team:</b><br>
        $wteaminfo[teamname]<br><br>
        <b>Ladder:</b><br>
        $lddinfo[laddername]
        <input type='hidden' name='winnerid' value='$winnerid'>
        <input type='hidden' name='ladderid' value='$ladderid'>
        <br><br><br>
        <input type='hidden' name='action' value='reportb'>
        <button type='submit' name='' value='Report Loss' class='btn-mini btn-success''>Reportar derrota</button>";
    }else{

        include("$dir[func]/error.php");
        display_error("No se especific&oacute; el equipo ganador.<br>");
    }

    $tablehead=table_head("show","300","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    <br>
    <center>
    $tablehead
    <table width='300' border='0' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr>
    <td width='100%' height='100%' valign='top' align='center'>
    <strong>Reportar una derrota</strong>
    <form method='post'>
    $loserforminfo
    $forminfo
    </form>
    </td>
    </tr>
    </table>
    $tablefoot
    </center>";
    include("$dir[curtheme]");
}

function report_lossb($teamid,$winnerid,$ladderid){
    global $dir, $file, $url, $out, $plyr;
    if (mysql_num_rows(mysql_query("SELECT status FROM teammembers WHERE teamid='$teamid' AND playerid='$plyr[id]' AND status <='4'")) < 1){
        include("$dir[func]/error.php");
        display_error("You are not allowed to report losses for this team.<br>");
    }

    $loserinfo=mysql_query("SELECT teamname FROM teams WHERE id='$teamid'");
    $ltinfo=mysql_fetch_array($loserinfo);
    if(!$ltinfo[teamname]){
        include("$dir[func]/error.php");
        display_error("Unable to find your team.<br>");
    }

    $winnerinfo=mysql_query("SELECT teamname FROM teams WHERE id='$winnerid'");
    $wtinfo=mysql_fetch_array($winnerinfo);
    if(!$wtinfo[teamname]){
        include("$dir[func]/error.php");
        display_error("Unable to find winning team. $winnerid<br>");
    }

    //LADDER INFORMATION
    $ladderinfo=mysql_query("SELECT type,minmembers,openplay FROM ladders WHERE id='$ladderid'");
    $linfo=mysql_fetch_array($ladderinfo);
    if(!$linfo[type]){
        include("$dir[func]/error.php");
        display_error("Unknown ladder.<br>");
    }

    if (mysql_num_rows(mysql_query("SELECT teamid FROM teammembers WHERE teamid='$teamid'")) < $linfo[minmembers]){
        include("$dir[func]/error.php");
        display_error("Your team must have at least $linfo[minmembers] members before you can play matches on this ladder.<br>");
    }

    if (mysql_num_rows(mysql_query("SELECT teamid FROM teammembers WHERE teamid='$winnerid'")) < $linfo[minmembers]){
        include("$dir[func]/error.php");
        display_error("The winning team must have at least $linfo[minmembers] members before you can play matches on this ladder.<br>");
    }

    if (mysql_num_rows(mysql_query("SELECT id FROM ladder_$ladderid WHERE id='$teamid'")) < 1){
        include("$dir[func]/error.php");
        display_error("Your team is missing from the ladder.<br>");
    }

    if (mysql_num_rows(mysql_query("SELECT id FROM ladder_$ladderid WHERE id='$winnerid'")) < 1){
        include("$dir[func]/error.php");
        display_error("Winning team is missing from the ladder.<br>");
    }

    $tday=date("Y-m-d H:i:s");
    //CHECK IF LOSING TEAM IS LOCKED
    if (mysql_num_rows(mysql_query("SELECT status FROM ladder_$ladderid WHERE id='$teamid' AND status='10' AND statusdate > '$tday'")) > 0){
        include("$dir[func]/error.php");
        display_error("Your team is locked and cannot report losses.<br>");
    }

    //CHECK IF WINNING TEAM IS LOCKED
    if (mysql_num_rows(mysql_query("SELECT status FROM ladder_$ladderid WHERE id='$winnerid' AND status='10' AND statusdate > '$tday'")) > 0){
        include("$dir[func]/error.php");
        display_error("The winning team is locked and cannot recieve wins.<br>");
    }

    get_matchinfo($teamid,$winnerid,$ladderid);
}

function get_matchinfo($loserid,$winnerid,$ladderid){
    global $dir, $file, $url, $out, $plyr, $site, $misc;
    $ladinfo=mysql_query("SELECT gameon,laddername,minmaps,maxmaps FROM ladders WHERE id='$ladderid'");
    $linfo=mysql_fetch_array($ladinfo);
    if(!$linfo[laddername]){
        include("$dir[func]/error.php");
        display_error("Unknown Ladder ID.<br>");
    }

    //CHECK FOR GAME STATUS
    if (mysql_num_rows(mysql_query("SELECT id FROM games WHERE status < '1' AND id='$linfo[gameon]'")) > 0){
        include("$dir[func]/error.php");
        display_error("This Ladder is temporarily closed.<br>");
    }

    $color='gray';
    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    <br>
    $tablehead
    <table width='60%' border='0' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolorc'>
    <td width='100%' height='100%' valign='top' align='center' colspan='2'>
    <strong>Reporting Loss on $linfo[laddername]</strong><br>
    </td>
    <form method='post'>
    </tr>";
    $altcolora="#000033";
    $altcolorb="#000020";
    $losersmembers=mysql_query("SELECT playerid,status FROM teammembers WHERE teamid='$loserid' ORDER by status");
    while(list($playerid,$status)=mysql_fetch_row($losersmembers)){
        $memberinfo=mysql_query("SELECT alias FROM users WHERE id='$playerid'");
        $minfo=mysql_fetch_array($memberinfo);
        if($altladrow=="$altcolora"){
            $altladrow="$altcolorb";
        }else{

            $altladrow="$altcolora";
        }

        $out[body]=$out[body]."
        <tr bgcolor='$altladrow'>
        <td width='50%' valign='center' align='left'>
        <a href='$url[base]/$file[players]?playerid=$playerid' target='new'>
        $minfo[alias]</a></td>
        <td width='50%' valign='center' align='right'>";
        if($status < 6){
            $out[body]=$out[body]."Played in Match:
            <input type='checkbox' name='member[$playerid]' value='1'>
            <input type='hidden' name='kills[$playerid]' value='0' maxlength='2' size='5'>";
        }else{

            $status=member_status($status);
            $out[body]=$out[body]."$status";
        }

        $out[body]=$out[body]."</td>
        </tr>";
    }

    if(($linfo[minmaps] > '0')&&($linfo[maxmaps] > '0')){
        $out[body]=$out[body]."
        <tr class='altcolorc'>
        <td width='100%' height='100%' valign='top' align='center' colspan='2'>
        <strong>Maps Played in match</strong><br>
        </td>
        </tr>";
        $mapsplayed=mysql_query("SELECT id,mapname FROM maps WHERE ladderid='$ladderid' ORDER by mapname");
        while(list($mapid,$mapname)=mysql_fetch_row($mapsplayed)){
            $maplist=$maplist."<option value='$mapid'>$mapname</option>";
        }

        $mapnum=1;
        while($mapnum <= $linfo[maxmaps]){
            $out[body]=$out[body]."
            <tr>
            <td width='100%' valign='center' align='left'>
            #$mapnum Map Played</td>
            <td width='100%' valign='center' align='right'>
            <select name='map[$mapnum]'>";
            if($mapnum > $linfo[minmaps]){
                $out[body]=$out[body]."<option value='0'>Not Played</option>";
            }

            $out[body]=$out[body]."
            $maplist
            </select>
            </td>
            </tr>";
            $mapnum++;
        }

    }

    if($misc[losercomment]=="yes"){
        $out[body]=$out[body]."
        <tr class='altcolorc'>
        <td width='100%' height='100%' valign='top' align='center' colspan='2'>
        <strong>Comment on this match</strong><br>
        </td>
        </tr>
        <tr>
        <td width='100%' height='100%' valign='center' align='center' colspan='2'>
        <input type='text' name='lcomment' maxlength='50' size='50'><br>
        <small>50 Characters Max</small>
        </td>
        </tr>";
    }

    $out[body]=$out[body]."
    <tr>
    <td width='100%' height='100%' valign='top' align='center' colspan='5'>
    <br>
    <input type='hidden' name='ladderid' value='$ladderid'>
    <input type='hidden' name='winnerid' value='$winnerid'>
    <input type='hidden' name='loserid' value='$loserid'>
    <input type='hidden' name='action' value='reportc'>
    <button type='submit' name='' value='Report Loss' class='btn-mini btn-success''>Reportar derrota</button>
    <button type='reset' name='' value='Reset' class='btn btn-mini'>Reset</button></td>
    </td>
    </form>
    </tr>
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function report_lossc($ladderid,$winnerid,$loserid,$member,$map,$lcomment,$kills){
    global $dir, $file, $url, $out, $plyr, $site;
    if($winnerid=="$loserid"){
        include("$dir[func]/error.php");
        display_error("You cannot report losses to yourself.<br>");
    }

    //CHECK IF CAN REPORT
    if (mysql_num_rows(mysql_query("SELECT status FROM teammembers WHERE teamid='$loserid' AND playerid='$plyr[id]' AND status <='4'")) < 1){
        include("$dir[func]/error.php");
        display_error("You are not allowed to report losses for this team.<br>");
    }

    $teaminfo=mysql_query("SELECT teamname FROM teams WHERE id='$loserid'");
    $tinfo=mysql_fetch_array($teaminfo);
    if(!$tinfo[teamname]){
        include("$dir[func]/error.php");
        display_error("Your team is not on this ladder.<br>");
    }

    $wteaminfo=mysql_query("SELECT teamname FROM teams WHERE id='$winnerid'");
    $wintinfo=mysql_fetch_array($wteaminfo);
    if(!$wintinfo[teamname]){
        include("$dir[func]/error.php");
        display_error("The winning team is not on this ladder.<br>");
    }

    if (mysql_num_rows(mysql_query("SELECT id FROM ladder_$ladderid WHERE id='$loserid'")) < 1){
        include("$dir[func]/error.php");
        display_error("Your team is missing from the ladder.<br>");
    }

    if (mysql_num_rows(mysql_query("SELECT id FROM ladder_$ladderid WHERE id='$winnerid'")) < 1){
        include("$dir[func]/error.php");
        display_error("The winning team is missing from the ladder.<br>");
    }

    if(!$member){
        include("$dir[func]/error.php");
        display_error("You must select the members that played.<br>");
    }

    $ladinfo=mysql_query("SELECT gameon,laddername,minmaps,maxmaps FROM ladders WHERE id='$ladderid'");
    $linfo=mysql_fetch_array($ladinfo);
    if(($linfo[minmaps] > '0')&&($linfo[maxmaps] > '0')){
        if(!$map){
            include("$dir[func]/error.php");
            display_error("You must select the maps you played.<br>");
        }

    }

    //CHECK FOR UNCONFIRMED MATCH
    if (mysql_num_rows(mysql_query("SELECT matchid FROM matchdbval WHERE winnerid='$winnerid' AND loserid='$loserid'")) > 0){
        include("$dir[func]/error.php");
        display_error("This match has already been reported but not confirmed by the winner.<br>");
    }

    //CECK FOR CHALLENGE OR IF OPEN PLAY IS ALLOWED
    $thechallenge=mysql_query("SELECT challid FROM challenges WHERE challenger='$winnerid' AND challenged='$loserid' AND finalizedby > '0' OR challenger='$loserid' AND challenged='$winnerid' AND finalizedby > '0'");
    $chall=mysql_fetch_array($thechallenge);
    if(!$chall[challid]){
        if(mysql_num_rows(mysql_query("SELECT openplay FROM ladders WHERE id='$ladderid' AND openplay > '0'")) < 1){
            include("$dir[func]/error.php");
            display_error("Open play is not allowed on this ladder.<br>All matches must be scheduled challenges.<br>");
        }

    }

    include("$dir[func]/checkdata.php");
    if($lcomment){
        $lcomment=wordwrap($lcomment,20," ",1);
        $lcomment=change_censor($lcomment);
        $lcomment=change_charecters($lcomment);
    }

    while(list($memberid,$played)=each($member)){
        if($played){
            $memberid=change_numbersonly($memberid);
            $kills[$memberid]=change_numbersonly($kills[$memberid]);
            $membersplayed=$membersplayed."$memberid|$kills[$memberid],";
            $totalkills=($totalkills + $kills[$memberid]);
        }

    }

    if(($linfo[minmaps] > '0')&&($linfo[maxmaps] > '0')){
        while(list($mapselid,$mapid)=each($map)){
            if($mapid){
                $mapid=change_numbersonly($mapid);
                $mapsplayed=$mapsplayed."$mapid,";
            }

        }

    }

    if(!$membersplayed){
        include("$dir[func]/error.php");
        display_error("You must select the members that played.<br>");
    }

    if(($linfo[minmaps] > '0')&&($linfo[maxmaps] > '0')){
        if(!$mapsplayed){
            include("$dir[func]/error.php");
            display_error("You must select the maps you played.<br>");
        }

    }

    //GET WINNER STATS
    //IF A STATUS DOESNT ALLOW A REPORT DO IT HERE
    $winteaminfo=mysql_query("SELECT status,rank,games,percent FROM ladder_$ladderid WHERE id='$winnerid'");
    $wtinfo=mysql_fetch_array($winteaminfo);
    //GET LOSER STATS
    //IF A STATUS DOESNT ALLOW A REPORT DO IT HERE
    $losteaminfo=mysql_query("SELECT status,rank,games,percent FROM ladder_$ladderid WHERE id='$loserid'");
    $ltinfo=mysql_fetch_array($losteaminfo);
    // GET SKILL INFO
    if($wtinfo[rank] > $ltinfo[rank]){
        $rankdifference=($wtinfo[rank] - $ltinfo[rank]);
    }else{

        $rankdifference="1";
    }

    if($ltinfo[percent]){
        $percentoratio=($wtinfo[percent] / $ltinfo[percent]);
    }else{

        $percentoratio="1";
    }

    /* CHANGED SKILL TO KILLS
    $skilladjust=($rankdifference * $percentoratio / 10);
    $skillfactor=($skilladjust + $wtinfo[games] / 100);
    if($skillfactor > 20){
        $gainedskill="20";
    }else{

        $gainedskill=round($skillfactor);
    }

    */
    $gainedskill="$totalkills";
    //IF BOTH RANKED AND LOSER WAS RANKED BETTER THAN WINNER, WINNER GETS EXTRA POINTS
    if(($wtinfo[rank] > 0) && ($ltinfo[rank] > 0) && ($wtinfo[rank] > $ltinfo[rank])){
        $trankedplayers=mysql_query("SELECT COUNT(*) FROM ladder_$ladderid WHERE rank > '0'");
        $trplayers=mysql_fetch_array($trankedplayers);
        $rankedplayers="$rplayers[0]";
        $gainedpoints=($rankedplayers - $ltinfo[rank]);
        if($gainedpoints < 10){
            $gainedpoints=($gainedpoints + 10);
        }

    }else{

        //GET FLAT RATE OF 10 POINTS
        $gainedpoints="10";
    }

    //CLEAN UNCONFIRMED MATCHES
    clean_unconfirmedmatches();
    //ADD TO UNVALIDATED MATCHES
    $tday=date("Y-m-d H:i:s");
    $mcode=md5(uniqid(microtime()));
    $mcode="$mcode";
    $ip=getenv("REMOTE_ADDR");
    mysql_query("INSERT INTO matchdbval VALUES (
    '$mcode',
    '$winnerid',
    '$loserid',
    '$tinfo[teamname]',
    '$ladderid',
    '$membersplayed',
    '$mapsplayed',
    '$lcomment',
    '$wtinfo[rank]',
    '$ltinfo[rank]',
    '$gainedpoints',
    '$gainedskill',
    '$plyr[id]',
    '$tday',
    '$ip');");
    $getcode=mysql_query("SELECT matchid FROM matchdbval WHERE winnerid='$winnerid' AND loserid='$loserid'");
    $match=mysql_fetch_array($getcode);
    $matchcode="$match[matchid]";
    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    <br>
    $tablehead
    <table width='100%' border='0' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <strong>Your loss has been reported</strong><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='left'>
    <br><ul>
    Winner: <a href='$url[base]/$file[teams]?teamid=$winnerid'>$wintinfo[teamname]</a><br>
    Loser: <a href='$url[base]/$file[teams]?teamid=$loserid'>$tinfo[teamname]</a>
    <br>
    The winner will not recieve the win untill they confirm this result.<br>
    </ul></td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <strong><a href='$url[base]/$file[ladder]?ladderid=$ladderid&cid=$loserid'>View the Ladder</a></strong><br>
    </td>
    </tr>
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function clean_unconfirmedmatches(){
    $deletedate=date("Y-m-d H:i:s",time()-60*60*24*1);
    mysql_query("DELETE FROM matchdbval WHERE reporteddate < '$deletedate'");
}

function member_status($status){
    if($status=="1"){ $status="Leader"; }

    if($status=="2"){ $status="Co-Leader"; }

    if($status=="3"){ $status="Captain"; }

    if($status=="4"){ $status="Co-Captain"; }

    if($status=="5"){ $status="Member"; }

    if($status=="6"){ $status="Training"; }

    if($status=="7"){ $status="Inactive"; }

    if($status=="8"){ $status="Suspended"; }

    return($status);
}

?>
