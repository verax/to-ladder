<?
if(!IsSet($indexloaded)){
    header("Location: ./index.php");
}

//CHECK ADMIN ACCESS LEVEL
if($admn[access] < 15){
    include("$dir[func]/error.php");
    display_error("You are not allowed to perform this function.<br>");
}

function admin_comments(){
    global $dir, $url, $out, $site, $admn;
    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    <table width='100%' border='0' bordercolor='#000000' cellspacing='1' cellpadding='1'>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <strong>Admin Comments</strong>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <br>
    <table width='100%' border='0'cellspacing='0' cellpadding='0'>
    <tr>
    <td width='50%' valign='center' align='center'>
    <form method='post'>
    <input type='hidden' name='action' value='admincommentsb'>
    <input type='submit' name='' value='Player Comments'>
    </td></form>
    <td width='50%' valign='center' align='center'>
    <form method='post'>
    <input type='hidden' name='action' value='admincommentsf'>
    <input type='submit' name='' value='Team Comments'>
    </td></form>
    </tr>
    </table>
    <br></td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <strong>Admin Comments</strong></td>
    </tr>
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function admin_commentsb(){
    global $dir, $url, $out, $site, $admn;
    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    <table width='100%' border='0' bordercolor='#000000' cellspacing='1' cellpadding='1'>
    <tr class='altcolor'>
    <form method='post'>
    <td width='100%' valign='top' align='center'>
    <strong>Admin Comments</strong><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <br>
    Player ID<br>
    <input type='text' name='playerid' value='' size='25' maxlength='25'><br>
    <br></td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='action' value='admincommentsc'>
    <input type='submit' name='' value='Manage Comments'>
    </td>
    </form>
    </tr>
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function admin_commentsc($playerid){
    global $dir, $url, $out, $site, $admn;
    if(!$playerid){
        include("$dir[func]/error.php");
        display_error("Invalid Player ID.<br>");
    }

    $playerinfo=mysql_query("SELECT alias FROM users WHERE id='$playerid'");
    $info=mysql_fetch_array($playerinfo);
    if(!$info[alias]){
        include("$dir[func]/error.php");
        display_error("Unknown Player ID.<br>");
    }

    //TABLE ATERNATING COLORS
    $altcolora="#000033";
    $altcolorb="#000020";
    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    <table width='100%' border='0' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <form method='post'>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <strong>Add Comments to Player: $info[alias]</strong>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <table width='100%' border='0'cellspacing='0' cellpadding='0'>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>Comment</td>
    <td width='50%' valign='center' align='center'>
    <textarea name='c_comment' rows='3' cols='25' maxlength='200'>
    </textarea></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>Display</td>
    <td width='50%' valign='center' align='center'>
    <select name='c_display'>
    <option value='1'>Public can view</option>
    <option value='0'>Admin view only</option>
    </select></td>
    </tr>
    </table>
    </td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='playerid' value='$playerid'>
    <input type='hidden' name='action' value='admincommentsd'>
    <input type='submit' name='' value='Add Comments'>
    </td>
    </tr>
    </form>
    </table>
    $tablefoot
    <br>
    $tablehead
    <table width='100%' border='0' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <form method='post'>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <strong>Current Comments for Player: $info[alias]</strong>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <table width='100%' border='0'cellspacing='1' cellpadding='1'>
    <tr bgcolor='3a4466'>
    <td width='' valign='center' align='left'><strong>Comment</strong></td>
    <td width='' valign='center' align='center'><strong>Public</strong></td>
    <td width='' valign='center' align='center'><strong>Date Added</strong></td>
    <td width='' valign='center' align='center'><strong>Added by</strong></td>
    <td width='' valign='center' align='center'><strong>Delete</strong></td>
    </tr>";
    //GET COMMENTS
    $getcomments=mysql_query("SELECT commentid,comment,display,addedby,DATE_FORMAT(added, '%M %d, %l:%i %p') FROM staffcomments WHERE id='$playerid' AND isateam='0' ORDER by added");
    while(list($commentid,$comment,$display,$addedby,$added)=mysql_fetch_row($getcomments)){
        if($display==1){
            $display="Yes";
        }else{

            $display="No";
        }

        $staffmember=mysql_query("SELECT displayname FROM staff WHERE id='$addedby'");
        $staff=mysql_fetch_array($staffmember);
        if($altladrow=="$altcolora"){
            $altladrow="$altcolorb";
        }else{

            $altladrow="$altcolora";
        }

        $out[body]=$out[body]."
        <form method='post'>
        <tr bgcolor='$altladrow'>
        <td width='' valign='center' align='left'>$comment</td>
        <td width='' valign='center' align='center'>$display</td>
        <td width='' valign='center' align='center'>$added</td>
        <td width='' valign='center' align='center'>$staff[displayname]</td>
        <td width='' valign='center' align='center'>
        <input type='hidden' name='commentid' value='$commentid'>
        <input type='hidden' name='action' value='admincommentse'>
        <input type='submit' name='' value='Delete'>
        </td>
        </tr>
        </form>";
    }

    $out[body]=$out[body]."
    </table>
    </td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'><strong>Player Comments</strong></td>
    </tr>
    </form>
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function admin_commentsd($playerid,$c_comment,$c_display){
    global $dir, $url, $out, $site, $admn;
    include("$dir[func]/checkdata.php");
    $errormessage=check_validlength($errormessage, $c_comment, "10", "250", "Your comment must be 10-250 charecters");
    error_check($errormessage);
    $c_comment=change_charecters($c_comment);
    $tday=date("Y-m-d H:i:s");
    mysql_query("INSERT INTO staffcomments VALUES (
    NULL,
    '$playerid',
    '0',
    '$c_comment',
    '$c_display',
    '$admn[id]',
    '$tday');");
    include("$dir[func]/admin_finishmessage.php");
    display_message("The comment has been added.<br>");
}

function admin_commentse($commentid){
    global $dir;
    mysql_query("DELETE FROM staffcomments WHERE commentid='$commentid'");
    include("$dir[func]/admin_finishmessage.php");
    display_message("The comment has been deleted.<br>");
}

function admin_commentsf(){
    global $dir, $url, $out, $site, $admn;
    //GET LADDERS
    $ladderslist=mysql_query("SELECT id,laddername  FROM ladders ORDER by laddername");
    while(list($id,$name)=mysql_fetch_row($ladderslist)){
        $theladders=$theladders."<option value='$id'>$name</option>";
        $foundladders=1;
    }

    if(!$foundladders){
        include("$dir[func]/error.php");
        display_error("There are no ladders.<br>");
    }

    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    <table width='100%' border='0' bordercolor='#000000' cellspacing='1' cellpadding='1'>
    <tr class='altcolor'>
    <form method='post'>
    <td width='100%' valign='top' align='center'>
    <strong>Team Comments</strong><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <br>
    Select a ladder<br>
    <select name='ladderid'>$theladders</select><br>
    <br></td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='action' value='admincommentsg'>
    <input type='submit' name='' value='Find Team'>
    </td>
    </form>
    </tr>
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function admin_commentsg($ladderid){
    global $dir, $url, $out, $site, $admn;
    if(!$ladderid){
        include("$dir[func]/error.php");
        display_error("Invalid Ladder ID.<br>");
    }

    if($admn[access] < 99){
        if (mysql_num_rows(mysql_query("SELECT staffid FROM staffaccess WHERE staffid='$admn[id]' AND ladderid='$ladderid'")) < 1){
            include("$dir[func]/error.php");
            display_error("You are not allowed to manage comments on this laddder.<br>");
        }

    }

    //GET TEAMS
    $teamslist=mysql_query("SELECT id FROM ladder_$ladderid");
    while(list($id)=mysql_fetch_row($teamslist)){
        $name=mysql_fetch_array(mysql_query("SELECT teamname FROM teams WHERE id='$id'"));
        $theteams=$theteams."<option value='$id'>$name[teamname]</option>";
        $foundladders=1;
    }

    if(!$foundladders){
        include("$dir[func]/error.php");
        display_error("There are no teams on this ladder.<br>");
    }

    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <form method='post'>
    <td width='100%' valign='top' align='center'>
    <strong>Edit a Team</strong><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <br>
    <select name='teamid'>$theteams</select><br>
    <br></td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='action' value='admincommentsh'>
    <input type='submit' name='' value='Manage Comments'>
    </td>
    </form>
    </tr>
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

////////// //////////
function admin_commentsh($teamid){
    global $dir, $url, $out, $site, $admn;
    if(!$teamid){
        include("$dir[func]/error.php");
        display_error("Invalid Team ID.<br>");
    }

    $teaminfo=mysql_query("SELECT teamname FROM teams WHERE id='$teamid'");
    $info=mysql_fetch_array($teaminfo);
    if(!$info[teamname]){
        include("$dir[func]/error.php");
        display_error("Unknown Team ID.<br>");
    }

    //TABLE ATERNATING COLORS
    $altcolora="#000033";
    $altcolorb="#000020";
    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    <table width='100%' border='0' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <form method='post'>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <strong>Add Comments to Team: $info[teamname]</strong>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <table width='100%' border='0'cellspacing='0' cellpadding='0'>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>Comment</td>
    <td width='50%' valign='center' align='center'>
    <textarea name='c_comment' rows='3' cols='25' maxlength='200'>
    </textarea></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>Display</td>
    <td width='50%' valign='center' align='center'>
    <select name='c_display'>
    <option value='1'>Public can view</option>
    <option value='0'>Admin view only</option>
    </select></td>
    </tr>
    </table>
    </td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='teamid' value='$teamid'>
    <input type='hidden' name='action' value='admincommentsi'>
    <input type='submit' name='' value='Player Comments'>
    </td>
    </tr>
    </form>
    </table>
    $tablefoot
    <br>
    $tablehead
    <table width='100%' border='0' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <form method='post'>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <strong>Current Comments for Team: $info[teamname]</strong>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <table width='100%' border='0'cellspacing='1' cellpadding='1'>
    <tr bgcolor='3a4466'>
    <td width='' valign='center' align='left'><strong>Comment</strong></td>
    <td width='' valign='center' align='center'><strong>Public</strong></td>
    <td width='' valign='center' align='center'><strong>Date Added</strong></td>
    <td width='' valign='center' align='center'><strong>Added by</strong></td>
    <td width='' valign='center' align='center'><strong>Delete</strong></td>
    </tr>";
    //GET COMMENTS
    $getcomments=mysql_query("SELECT commentid,comment,display,addedby,DATE_FORMAT(added, '%M %d, %l:%i %p') FROM staffcomments WHERE id='$teamid' AND isateam='1' ORDER by added");
    while(list($commentid,$comment,$display,$addedby,$added)=mysql_fetch_row($getcomments)){
        if($display==1){
            $display="Yes";
        }else{

            $display="No";
        }

        $staffmember=mysql_query("SELECT displayname FROM staff WHERE id='$addedby'");
        $staff=mysql_fetch_array($staffmember);
        if($altladrow=="$altcolora"){
            $altladrow="$altcolorb";
        }else{

            $altladrow="$altcolora";
        }

        $out[body]=$out[body]."
        <form method='post'>
        <tr bgcolor='$altladrow'>
        <td width='' valign='center' align='left'>$comment</td>
        <td width='' valign='center' align='center'>$display</td>
        <td width='' valign='center' align='center'>$added</td>
        <td width='' valign='center' align='center'>$staff[displayname]</td>
        <td width='' valign='center' align='center'>
        <input type='hidden' name='commentid' value='$commentid'>
        <input type='hidden' name='action' value='admincommentse'>
        <input type='submit' name='' value='Delete'>
        </td>
        </tr>
        </form>";
    }

    $out[body]=$out[body]."
    </table>
    </td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'><strong>Player Comments</strong></td>
    </tr>
    </form>
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function admin_commentsi($teamid,$c_comment,$c_display){
    global $dir, $url, $out, $site, $admn;
    include("$dir[func]/checkdata.php");
    $errormessage=check_validlength($errormessage, $c_comment, "10", "250", "Your comment must be 10-250 charecters");
    error_check($errormessage);
    $c_comment=change_charecters($c_comment);
    $tday=date("Y-m-d H:i:s");
    mysql_query("INSERT INTO staffcomments VALUES (
    NULL,
    '$teamid',
    '1',
    '$c_comment',
    '$c_display',
    '$admn[id]',
    '$tday');");
    include("$dir[func]/admin_finishmessage.php");
    display_message("The comment has been added.<br>");
}

?>
