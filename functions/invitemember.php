<?
function invite_member($teamid){
    global $dir, $file, $url, $out, $plyr, $site;
    if(!$teamid){
        include("$dir[func]/error.php");
        display_error("Invalid Team ID.<br>");
    }

    //CHECK IF LEADER
    if (mysql_num_rows(mysql_query("SELECT status FROM teammembers WHERE teamid='$teamid' AND playerid='$plyr[id]' AND status <='2'")) < 1){
        include("$dir[func]/error.php");
        display_error("You are not allowed to invite members to this team.<br>");
    }

    $teaminfo=mysql_query("SELECT teamname FROM teams WHERE id='$teamid'");
    $tinfo=mysql_fetch_array($teaminfo);
    if(!$tinfo[teamname]){
        include("$dir[func]/error.php");
        display_error("Unknown Team.<br>");
    }

    $ladders=mysql_query("SELECT id,laddername,maxmembers FROM ladders");
    while($row = mysql_fetch_array($ladders)){
        $teams=mysql_query("SELECT id FROM ladder_$row[id] WHERE id='$teamid'");
        if(mysql_fetch_array($teams)){
            if (mysql_num_rows(mysql_query("SELECT playerid FROM teammembers WHERE teamid='$teamid'")) >= $row[maxmembers]){
                include("$dir[func]/error.php");
                display_error("Your team has the maximum allowed team members<br> on the $row[laddername] ladder. <br><br> No new members can be added to your team.");
            }

        }

    }

    $tablehead=table_head("show","300","","left");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    <br>
    $tablehead
    &nbsp; &nbsp;<strong><font class='catfont'>Invite a Member to $tinfo[teamname]</font></strong><br>
    <hr class='catfont' size='1'>
    <table width='300' border='0' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <form method='post'>
    <tr>
    <td width='100%' height='100%' valign='top' align='center'>
    Member Id: <input type='text' name='inviteid' value='' size='25' maxlength='15'>
    <input type='hidden' name='teamid' value='$teamid'>
    <input type='hidden' name='action' value='inviteb'>
    <br><br>
    <input type='submit' name='' value='Invite Player'>
    </td>
    </tr>
    </form>
    </table>
    $tablefoot
    </center>";
    include("$dir[curtheme]");
}

function invite_memberb($teamid,$inviteid){
    global $dir, $file, $url, $out, $plyr, $site;
    if(!$teamid){
        include("$dir[func]/error.php");
        display_error("Invalid Team ID.<br>");
    }

    if(!$inviteid){
        include("$dir[func]/error.php");
        display_error("Invalid Invited Player ID.<br>");
    }

    if($inviteid=="$plyr[id]"){
        include("$dir[func]/error.php");
        display_error("You cannot invite yourself.<br>");
    }

    //CHECK IF LEADER
    if (mysql_num_rows(mysql_query("SELECT status FROM teammembers WHERE teamid='$teamid' AND playerid='$plyr[id]' AND status <='2'")) < 1){
        include("$dir[func]/error.php");
        display_error("You are not allowed to invite players to this team.<br>");
    }

    $teaminfo=mysql_query("SELECT teamname FROM teams WHERE id='$teamid'");
    $tinfo=mysql_fetch_array($teaminfo);
    if(!$tinfo[teamname]){
        include("$dir[func]/error.php");
        display_error("Unknown Team.<br>");
    }

    //CHECK IF MAX MEMBERS EXISTS
    $ladders=mysql_query("SELECT id,laddername,maxmembers FROM ladders");
    while($row = mysql_fetch_array($ladders)){
        $teams=mysql_query("SELECT id FROM ladder_$row[id] WHERE id='$teamid'");
        if(mysql_fetch_array($teams)){
            if (mysql_num_rows(mysql_query("SELECT playerid FROM teammembers WHERE teamid='$teamid'")) >= $row[maxmembers]){
                include("$dir[func]/error.php");
                display_error("Your team has the maximum allowed team members<br> on the $row[laddername] ladder. <br><br> No new members can be added to your team.");
            }

        }

    }

    // CHECK INVITED PLAYER INFO
    $invitedinfo=mysql_query("SELECT alias,email FROM users WHERE id='$inviteid'");
    $iinfo=mysql_fetch_array($invitedinfo);
    if(!$iinfo[alias]){
        include("$dir[func]/error.php");
        display_error("Unknown Player.<br>");
    }

    //CHECK IF ON TEAM
    if (mysql_num_rows(mysql_query("SELECT playerid FROM teammembers WHERE teamid='$teamid' AND playerid='$inviteid'")) > 0){
        include("$dir[func]/error.php");
        display_error("This Player is already on your team.<br>");
    }

    $ladders=mysql_query("SELECT id,laddername FROM ladders");
    while($row = mysql_fetch_array($ladders)){
        $teams=mysql_query("SELECT id FROM ladder_$row[id] WHERE id='$teamid'");
        if(mysql_fetch_array($teams)){
            $teamson = mysql_query("SELECT teamid FROM teammembers WHERE playerid='$inviteid'");
            while($teamslist = mysql_fetch_array($teamson)){
                $check=mysql_query("SELECT id FROM ladder_$row[id] WHERE id='$teamslist[teamid]'");
                if(mysql_fetch_array($check)){
                    include("$dir[func]/error.php");
                    display_error("This Player is already on a ladder (<a href='$url[base]/$file[ladder]?ladderid=$row[id]'>$row[laddername]</a>)<br> that your team is on. <br><br> Players cannot be on the same ladder more than once.<br>");
                }

            }

        }

    }

    //CHECK IF ALREADY INVITED
    if (mysql_num_rows(mysql_query("SELECT playerid FROM teammembersinv WHERE teamid='$teamid' AND playerid='$inviteid'")) > 0){
        include("$dir[func]/error.php");
        display_error("This Player ($iinfo[alias]) is already invited to your team.<br>They have 7 days to accept before the invitation expires.<br>");
    }

   //DO MEMBER INVITE 

	$tday=date("Y-m-d H:i:s");
	mysql_query("INSERT INTO teammembersinv VALUES (

	'$teamid',
	'$inviteid',
	'$plyr[id]',
	'$tday');");

	$tablehead=table_head("show","500","","left");
	$tablefoot=table_foot("show");

	$bannerhead=table_head("show","488","80","center");
	$bannerfoot=table_foot("show");

	$out[body]=$out[body]."
	<center>
	$bannerhead
	$out[banner]
	$bannerfoot



	<br>
	$tablehead
	<center>
	<font class='catfont'><strong>Member Invited</strong></font>
	<br>

	<br>
	$iinfo[alias] has been sent an email inviting them to your team. <br>They have 7 days to respond before the invite expires.<br>
	<br>
	</center>
	$tablefoot
	</center>";
	include("$dir[curtheme]");


	//Email Invited Player

	$emailbody="$iinfo[alias],
	You have been invited to join a team on $site[shortname]! \n
	Team Name: $tinfo[teamname]
	Ladder Name: $linfo[laddername]
	Team Stats: $url[base]/$file[teams]?teamid=$teamid \n
	If you wish to join this team, please click here: $url[base]/$file[teams]?teamid=$teamid?inviteid=$plyr[id]\n
	You may also join at any time within the next 7 days via your Player Manager.
	";


	include("$dir[func]/email.php");
	send_email($iinfo[alias], $iinfo[email], "Team Addition", $emailbody);
}

    function accept_invite($teamid,$inviteid){
        global $dir, $file, $url, $out, $plyr, $site;
        $tday=date("Y-m-d H:i:s");
        $datech = mysql_query("SELECT invitedate FROM teammembersinv WHERE teamid='$teamid' AND playerid='$inviteid'");
        $checkd=mysql_fetch_array($datech);

        /*
        if($tday > ($checkd[invitedate]-$expiry)){
            mysql_query("DELETE FROM teammembersinv WHERE teamid='$teamid' AND playerid='$inviteid'");
        }
        */

        $accinfo=mysql_query("SELECT teamname FROM teams WHERE id='$teamid'");
        $ainfo=mysql_fetch_array($accinfo);
        if (mysql_num_rows(mysql_query("SELECT playerid FROM teammembersinv WHERE teamid='$teamid' AND playerid='$inviteid'")) > 0){
            mysql_query("INSERT INTO teammembers VALUES (
            '$teamid',
            '$inviteid',
            '5',
            '$tday');");
            mysql_query("DELETE FROM teammembersinv WHERE teamid='$teamid' AND playerid='$inviteid'");
            $tablehead=table_head("show","500","","left");
            $tablefoot=table_foot("show");
            $bannerhead=table_head("show","488","80","center");
            $bannerfoot=table_foot("show");
            $out[body]=$out[body]."
            <center>
            $bannerhead
            $out[banner]
            $bannerfoot
            <br>
            $tablehead
            <center>
            <font class='catfont'><strong>Member Added</strong></font>
            <br>
            <br>
            You have been successfully added to the team, $ainfo[teamname].
            <br>
            </center>
            $tablefoot
            </center>";
            include("$dir[curtheme]");
        }

        else{
            include("$dir[func]/error.php");
            display_error("This Invite does not exist or has expired.<br>You will need to have your team invite you again.<br>");
        }

    }
?>