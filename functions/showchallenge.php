<?
function show_challenge($challid){
    global $dir, $file, $url, $out, $misc;
    $thechallenge=mysql_query("SELECT *,DATE_FORMAT(challengedtime, '%M %d, %l:%i %p'),DATE_FORMAT(respondedtime, '%M %d, %l:%i %p'),DATE_FORMAT(finalizedtime, '%M %d, %l:%i %p') FROM challenges WHERE challid='$challid'");
    $chall=mysql_fetch_array($thechallenge);
    if(!$chall[challid]){
        include("$dir[func]/error.php");
        display_error("Unknown Challenge ID.<br>");
    }

    $ladderinfo=mysql_query("SELECT laddername FROM ladders WHERE id='$chall[ladderid]'");
    $linfo=mysql_fetch_array($ladderinfo);
    $challengedinfo=mysql_query("SELECT teamname FROM teams WHERE id='$chall[challenged]'");
    $cgdinfo=mysql_fetch_array($challengedinfo);
    $challengerplayer=mysql_query("SELECT alias FROM users WHERE id='$chall[challengedby]'");
    $cgrpinfo=mysql_fetch_array($challengerplayer);
    if(!$cgrpinfo[alias]){
        $cgrpinfo[alias]="Unknown";
    }

    if($chall[respondedby] > 0){
        $respondplayer=mysql_query("SELECT alias FROM users WHERE id='$chall[respondedby]'");
        $rsppinfo=mysql_fetch_array($respondplayer);
        if(!$rsppinfo[alias]){
            $rsppinfo[alias]="Unknown";
        }

    }

    if($chall[finalizedby] > 0){
        $finalizeplayer=mysql_query("SELECT alias FROM users WHERE id='$chall[finalizedby]'");
        $fnlpinfo=mysql_fetch_array($finalizeplayer);
        if(!$fnlpinfo[alias]){
            $fnlpinfo[alias]="Unknown";
        }

    }

    $tablehead=table_head("show","","","left");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='0'>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'><strong>
    <a href='$url[base]/$file[ladder]?ladderid=$chall[ladderid]'>
    $linfo[laddername] Challenge Match</a></strong><br></td>
    </tr>
    <tr>
    <td width='100%' valign='top' align='center'>
    <table width='100%' border='0'  cellspacing='1' cellpadding='0'>
    <tr>
    <td width='30%' valign='top' align='left'></td>
    <td width='70%' valign='top' align='left'><br></td>
    </tr>
    <tr>
    <td width='30%' valign='top' align='left'>&nbsp;</td>
    <td width='70%' valign='top' align='left'>
    <font class='catfont'><strong>Challenge Information</strong></font></td>
    </tr>
    <tr>
    <td width='30%' valign='top' align='left'><strong>Challenge</strong></td>
    <td width='70%' valign='top' align='left'>
    <a href='$url[base]/$file[teams]?teamid=$chall[challenger]'>$chall[challengeralias]</a> (rank $chall[challengerrank]) Challenged <a href='$url[base]/$file[teams]?teamid=$chall[challenged]'>$cgdinfo[teamname]</a> for rank $chall[challengedrank]</td>
    </tr>
    <tr>
    <td width='30%' valign='top' align='left'><strong>Challenger</strong></td>
    <td width='70%' valign='top' align='left'>
    Challenge made by <a href='$url[base]/$file[players]?playerid=$chall[challengedby]'>$cgrpinfo[alias]</a> at $chall[20]</td>
    </tr>
    <tr>
    <td width='30%' valign='top' align='left'><strong>Comments</strong></td>
    <td width='70%' valign='top' align='left'>
    $chall[challengercomment]</td>
    </tr>
    <tr>
    <td width='30%' valign='top' align='left'></td>
    <td width='70%' valign='top' align='left'><br></td>
    </tr>
    <tr>
    <td width='30%' valign='top' align='left'>&nbsp;</td>
    <td width='70%' valign='top' align='left'>
    <font class='catfont'><strong>Response Information</strong></font></td>
    </tr>";
    if($rsppinfo[alias]){
        $out[body]=$out[body]."
        <tr>
        <td width='30%' valign='top' align='left'><strong>Responder</strong></td>
        <td width='70%' valign='top' align='left'>
        Responded by <a href='$url[base]/$file[players]?playerid=$chall[respondedby]'>$rsppinfo[alias]</a> at $chall[21]</td>
        </tr>";
        $timeoptions=split(",",$chall[matchtimes]);
        while(list($counttimes,$matchtimes)=each($timeoptions)){
            if($matchtimes){
                $countt=($countt+1);
                $out[body]=$out[body]."
                <tr>
                <td width='30%' valign='top' align='left'><strong>Time Option #$countt</strong></td>
                <td width='70%' valign='top' align='left'>$matchtimes</td>
                </tr>";
            }

        }

        $mapoptions=split(",",$chall[matchmaps]);
        while(list($countmaps,$matchmaps)=each($mapoptions)){
            if($matchmaps){
                $laddermaps=mysql_query("SELECT mapname FROM maps WHERE id='$matchmaps'");
                $maps=mysql_fetch_array($laddermaps);
                if($maps[mapname]){
                    $countm=($countm+1);
                    $out[body]=$out[body]."
                    <tr>
                    <td width='30%' valign='top' align='left'><strong>Map Option #$countm</strong></td>
                    <td width='70%' valign='top' align='left'>
                    <a href='$url[base]/$file[maps]?mapid=$matchmaps'>$maps[mapname]</a></td>
                    </tr>";
                }

            }

        }

        $out[body]=$out[body]."
        <tr>
        <td width='30%' valign='top' align='left'><strong>Comments</strong></td>
        <td width='70%' valign='top' align='left'>
        $chall[challengedcomment]</td>
        </tr>";
    }else{

        $out[body]=$out[body]."
        <tr>
        <td width='30%' valign='top' align='left'><strong>&nbsp;</strong></td>
        <td width='70%' valign='top' align='left'>
        Awaiting Response [<a href='$url[base]/$file[match]?action=respond&challid=$challid'>Respond</a>]</td>
        </tr>";
    }

    $out[body]=$out[body]."
    <tr>
    <td width='30%' valign='top' align='left'></td>
    <td width='70%' valign='top' align='left'><br></td>
    </tr>
    <tr>
    <td width='30%' valign='top' align='left'>&nbsp;</td>
    <td width='70%' valign='top' align='left'>
    <font class='catfont'><strong>Final Match Information</strong></font></td>
    </tr>";
    if($fnlpinfo[alias]){
        $out[body]=$out[body]."
        <tr>
        <td width='30%' valign='top' align='left'><strong>Match Time</strong></td>
        <td width='70%' valign='top' align='left'>
        $chall[finaltime]</td>
        </tr>
        ";
        if($chall[finalmaps]){
            $finalmaps=split(",",$chall[finalmaps]);
            while(list($countmap,$matchmap)=each($finalmaps)){
                if($matchmap){
                    $laddermap=mysql_query("SELECT mapname FROM maps WHERE id='$matchmap'");
                    $map=mysql_fetch_array($laddermap);
                    if($map[mapname]){
                        $countmb=($countmb+1);
                        $out[body]=$out[body]."
                        <tr>
                        <td width='30%' valign='top' align='left'><strong>Match Maps</strong></td>
                        <td width='70%' valign='top' align='left'>
                        $countmb. <a href='$url[base]/$file[maps]?mapid=$matchmap'>$map[mapname]</a>
                        </td>
                        </tr> ";
                    }

                }

            }

        }

        $out[body]=$out[body]."
        <tr>
        <td width='30%' valign='top' align='left'><strong>Finalizer</strong></td>
        <td width='70%' valign='top' align='left'>
        Finalized by <a href='$url[base]/$file[players]?playerid=$chall[finalizedby]'>$fnlpinfo[alias]</a> at $chall[22] <br><br> </td>
        </tr>
        </table>
        </td>
        </tr>
        <tr class='altcolor'>
      
        </tr>
        </table>
        $tablefoot";
    }else{

        $out[body]=$out[body]."
        <tr>
        <td width='30%' valign='top' align='left'><strong>&nbsp;</strong></td>
        <td width='70%' valign='top' align='left'>
        Awaiting finalization [<a href='$url[base]/$file[match]?action=finalize&challid=$challid'>Finalize</a>]</td>
        </tr>
        </table>
        </td>
        </tr>
        <tr class='altcolor'>
        <td width='100%' valign='top' align='center'><strong>
        &nbsp;</strong><br></td>
        </tr>
        </table>
        $tablefoot";
    }

    include("$dir[curtheme]");
}

?>
