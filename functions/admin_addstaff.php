<?
if(!IsSet($indexloaded)){
    header("Location: ./index.php");
}

//CHECK ADMIN ACCESS LEVEL
if($admn[access] < 65){
    include("$dir[func]/error.php");
    display_error("You are not allowed to perform this function.<br>");
}

function get_rights($giversrights){
    $getrights="
    <option value='0'>Cant Login</option>
    <option value='5'>Resend/Delete Validations</option>
    <option value='10'>Create Teams</option>
    <option value='15'>Edit Teams</option>
    <option value='20'>Delete Teams</option>
    <option value='25'>Report Match</option>
    <option value='30'>Edit/Delete Match</option>
    <option value='35'>Delete Challenges</option>
    <option value='40'>Edit Players</option>
    <option value='45'>Edit Players Pass</option>
    <option value='50'>Delete Players</option>
    <option value='55'>Manage Maps</option>
    <option value='60'>Manage Rules</option>
    <option value='63'>Manage Tournaments</option>
    <option value='65'>Manage Staff</option>";
    if($giversrights >= 70){$getrights=$getrights."<option value='70'>Manage all Maps</option>";}

    if($giversrights >= 75){$getrights=$getrights."<option value='75'>Manage all Rules</option>";}

    if($giversrights >= 80){$getrights=$getrights."<option value='80'>Manage all Staff</option>";}

    if($giversrights >= 85){$getrights=$getrights."<option value='85'>Manage Faq/Files</option>";}

    if($giversrights >= 90){$getrights=$getrights."<option value='90'>Manage Games</option>";}

    if($giversrights >= 95){$getrights=$getrights."<option value='95'>Manage Ladders</option>";}

    if($giversrights >= 99){$getrights=$getrights."<option value='99'>Master God Mode</option>";}

    return($getrights);
}

function admin_addstaff(){
    global $dir, $url, $out, $site, $admn;
    //TABLE ATERNATING COLORS
    $altcolora="#000033";
    $altcolorb="#000020";
    $tablehead=table_head("show","300","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <strong>Create new Staff</strong></td>
    </tr>
    <tr>
    <td width='100%' valign='top' align='left'>
    <form method='post'>
    <table width='100%' border='0' cellspacing='0' cellpadding='0' valign='top' align='center'>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Display Name</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='s_name' value='' size='30' maxlength='25'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Password</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='s_pass' value='' size='30' maxlength='25'></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Email</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='s_email' value='' size='30' maxlength='100'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Title</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='s_title' value='' size='30' maxlength='25'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Country</font></td>
    <td width='50%' valign='center' align='center'>
    <select name='s_country'>
        <option value=''>Please select a country
        <option value='AF'>Afghanistan
        <option value='AL'>Albania
        <option value='DZ'>Algeria
        <option value='AD'>Andorra
        <option value='AO'>Angola
        <option value='AG'>Antigua and Barbuda
        <option value='AR'>Argentina
        <option value='AM'>Armenia
        <option value='AU'>Australia
        <option value='AT'>Austria
        <option value='AZ'>Azerbaijan
        <option value='BS'>Bahamas
        <option value='BH'>Bahrain
        <option value='BD'>Bangladesh
        <option value='BB'>Barbados
        <option value='BY'>Belarus
        <option value='BE'>Belgium
        <option value='BZ'>Belize
        <option value='BJ'>Benin
        <option value='BT'>Bhutan
        <option value='BO'>Bolivia
        <option value='BA'>Bosnia Herzegovina
        <option value='BW'>Botswana
        <option value='BR'>Brazil
        <option value='BN'>Brunei
        <option value='BG'>Bulgaria
        <option value='BF'>Burkina Faso
        <option value='BM'>Burma
        <option value='BI'>Burundi
        <option value='KH'>Cambodia
        <option value='CM'>Cameroon
        <option value='CA'>Canada
        <option value='CF'>Central African Republic
        <option value='TD'>Chad
        <option value='CL' selected>Chile
        <option value='CN'>China
        <option value='CX'>Christmas Island
        <option value='CO'>Colombia
        <option value='KM'>Comoros
        <option value='CG'>Congo
        <option value='CR'>Costa Rica
        <option value='HR'>Croatia
        <option value='CU'>Cuba
        <option value='CY'>Cyprus
        <option value='CZ'>Czech Republic
        <option value='DC'>Democratic Rep. Congo
        <option value='DK'>Denmark
        <option value='DJ'>Djibouti
        <option value='DM'>Dominica
        <option value='DO'>Dominican Republic
        <option value='EC'>Ecuador
        <option value='EG'>Egypt
        <option value='SV'>El Salvador
        <option value='GQ'>Equatorial Guinea
        <option value='ER'>Eritrea
        <option value='EE'>Estonia
        <option value='ET'>Ethiopia
        <option value='EU'>European Union
        <option value='FS'>Fed. States Micronesia
        <option value='FJ'>Fiji
        <option value='FI'>Finland
        <option value='FR'>France
        <option value='GA'>Gabon
        <option value='GM'>Gambia
        <option value='GE'>Georgia
        <option value='DE'>Germany
        <option value='GH'>Ghana
        <option value='GR'>Greece
        <option value='GD'>Grenada
        <option value='GT'>Guatemala
        <option value='GN'>Guinea
        <option value='GW'>Guinea-Bissau
        <option value='GY'>Guyana
        <option value='HT'>Haiti
        <option value='HN'>Honduras
        <option value='HK'>Hong Kong
        <option value='HU'>Hungary
        <option value='IS'>Iceland
        <option value='IN'>India
        <option value='ID'>Indonesia
        <option value='IR'>Iran
        <option value='IQ'>Iraq
        <option value='IE'>Ireland
        <option value='IL'>Israel
        <option value='IT'>Italy
        <option value='IV'>Ivory Coast
        <option value='JM'>Jamaica
        <option value='JP'>Japan
        <option value='JO'>Jordan
        <option value='KZ'>Kazakhstan
        <option value='KE'>Kenya
        <option value='KI'>Kiribati
        <option value='KW'>Kuwait
        <option value='KG'>Kyrgyzstan
        <option value='LA'>Laos
        <option value='LV'>Latvia
        <option value='LB'>Lebanon
        <option value='LR'>Liberia
        <option value='LY'>Libya
        <option value='LI'>Liechtenstein
        <option value='LT'>Lithuania
        <option value='LU'>Luxembourg
        <option value='MO'>Macau
        <option value='MK'>Macedonia
        <option value='MG'>Madagascar
        <option value='MW'>Malawi
        <option value='MY'>Malaysia
        <option value='MV'>Maldives
        <option value='ML'>Mali
        <option value='MT'>Malta
        <option value='MR'>Mauritania
        <option value='MU'>Mauritius
        <option value='MX'>Mexico
        <option value='MD'>Moldova
        <option value='MC'>Monaco
        <option value='MN'>Mongolia
        <option value='MA'>Morocco
        <option value='MZ'>Mozambique
        <option value='NA'>Namibia
        <option value='NR'>Nauru
        <option value='NP'>Nepal
        <option value='NL'>Netherlands
        <option value='AN'>Netherlands Antilles
        <option value='NZ'>New Zealand
        <option value='NI'>Nicaragua
        <option value='NE'>Niger
        <option value='NG'>Nigeria
        <option value='NK'>North Korea
        <option value='NO'>Norway
        <option value='OM'>Oman
        <option value='PK'>Pakistan
        <option value='PA'>Panama
        <option value='PG'>Papua New Guinea
        <option value='PY'>Paraguay
        <option value='PE'>Peru
        <option value='PH'>Philippines
        <option value='PL'>Poland
        <option value='PT'>Portugal
        <option value='PR'>Puerto Rico
        <option value='QA'>Qatar
        <option value='RO'>Romania
        <option value='RU'>Russia
        <option value='RW'>Rwanda
        <option value='KN'>Saint Kitts and Nevis
        <option value='LC'>Saint Lucia
        <option value='VC'>Saint Vincent/Grenadines
        <option value='ST'>Sao Tome and Principe
        <option value='SA'>Saudi Arabia
        <option value='SN'>Senegal
        <option value='SC'>Seychelles
        <option value='SL'>Sierra Leone
        <option value='SG'>Singapore
        <option value='SK'>Slovakia
        <option value='SI'>Slovenia
        <option value='SB'>Solomon Islands
        <option value='SO'>Somalia
        <option value='ZA'>South Africa
        <option value='KS'>South Korea
        <option value='ES'>Spain
        <option value='LK'>Sri Lanka
        <option value='SD'>Sudan
        <option value='SR'>Suriname
        <option value='SE'>Sweden
        <option value='CH'>Switzerland
        <option value='SY'>Syria
        <option value='TW'>Taiwan
        <option value='TJ'>Tajikistan
        <option value='TZ'>Tanzania
        <option value='TH'>Thailand
        <option value='TG'>Togo
        <option value='TO'>Tonga
        <option value='TT'>Trinidad and Tobago
        <option value='TN'>Tunisia
        <option value='TR'>Turkey
        <option value='TM'>Turkmenistan
        <option value='TV'>Tuvalu
        <option value='UG'>Uganda
        <option value='UA'>Ukraine
        <option value='AE'>United Arab Emirates
        <option value='GB'>United Kingdom
        <option value='US'>United States Of America
        <option value='UY'>Uruguay
        <option value='UZ'>Uzbekistan
        <option value='VU'>Vanuatu
        <option value='VE'>Venezuela
        <option value='VN'>Viet Nam
        <option value='EH'>Western Samoa
        <option value='YE'>Yemen
        <option value='YU'>Yugoslavia
        <option value='ZM'>Zambia
        <option value='ZW'>Zimbabwe
        </select>
    </td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Contact Icq/Aim</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='s_contact' value='' size='30' maxlength='25'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Access</font></td>
    <td width='50%' valign='center' align='center'>
    <select name='s_access'>";
    $getrights=get_rights($admn[access]);
    $out[body]=$out[body]."$getrights
    </select></td>
    </tr></td>
    </tr>
    </table>
    </td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <strong>Ladders</strong><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <table width='100%' border='0' cellspacing='0' cellpadding='0'>
    ";
    //GET LADDERS
    $ladderslist=mysql_query("SELECT id,laddername  FROM ladders ORDER by laddername");
    while(list($id,$name)=mysql_fetch_row($ladderslist)){
        if((mysql_num_rows(mysql_query("SELECT staffid FROM staffaccess WHERE staffid ='$admn[id]' AND ladderid='$id'")) > 0) || ($admn[access] >= 80)){
            if($altladrow=="$altcolora"){
                $altladrow="$altcolorb";
            }else{

                $altladrow="$altcolora";
            }

            $out[body]=$out[body]."
            <tr bgcolor='$altladrow'>
            <td width='50%' valign='center' align='left'>
            $name</font></td>
            <td width='50%' valign='center' align='center'>
            Yes <input type='checkbox' name='ladders[$id]' value='1'> </td>
            </tr>";
            $howmanyeditable=1;
        }

    }

    $out[body]=$out[body]."</table>
    </td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='action' value='addstaffb'>
    <input type='submit' name='' value='Create Staff'></td>
    </form>
    </tr>
    </table>";
    include("$dir[curtheme]");
}

function admin_addstaffb($s_name,$s_pass,$s_email,$s_title,$s_country,$s_contact,$s_access,$ladders){
    global $dir, $url, $out, $site, $admn;
    include("$dir[func]/checkdata.php");
    $s_name=change_charecters($s_name);
    $s_pass=change_charecters($s_pass);
    $s_email=change_charecters($s_email);
    $s_title=change_charecters($s_title);
    $s_contact=change_charecters($s_contact);
    $s_access=change_numbersonly($s_access);
    $errormessage=check_validlength($errormessage, $s_name, "3", "50", "Staff names must be 3-25 characters.");
    $errormessage=check_validlength($errormessage, $s_pass, "2", "25", "Staff passwords must be 2-10 characters.");
    $errormessage=check_validlength($errormessage, $s_title, "3", "25", "Staff titles must be 3-25 characters.");
    $errormessage=check_staffexist($errormessage, $s_name);
    $errormessage=check_givestaffrights($errormessage, $s_access);
    error_check($errormessage);
    //ADD STAFF
    $tday=date("Y-m-d H:i:s");
    mysql_query("INSERT INTO staff VALUES (NULL,
    '$s_name',
    '$s_pass',
    '$s_email',
    '$s_title',
    '$s_country',
    '$s_access',
    '$s_contact',
    '$tday',
    '$admn[id]',
    '$tday',
    '0',
    '0');");
    $result=mysql_query("SELECT id FROM staff WHERE displayname='$s_name' AND pass='$s_pass'");
    $staffinfo=mysql_fetch_array($result);
    $staffid="$staffinfo[id]";
    if($ladders){
        while(list($ladder,$value)=each($ladders)){
            if($value){
                mysql_query("INSERT INTO staffaccess VALUES (
                '$staffid',
                '$ladder',
                '0',
                '$tday');");
            }

        }

    }

    include("$dir[func]/admin_finishmessage.php");
    display_message("The staff member was added.<br><br>Login ID: $staffid<br>");
}

?>
