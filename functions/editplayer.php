<?
function edit_player(){
    global $dir, $url, $out, $plyr, $site, $uinfo, $misc, $theme;
    $memberinfo=mysql_query("SELECT * FROM users WHERE id='$plyr[id]' AND pass='$plyr[pass]'");
    $info=mysql_fetch_array($memberinfo);
    if(!$info[alias]){
        include("$dir[func]/loginforms.php");
        form_login();
        exit;
    }

    include("$dir[func]/checkdata.php");
    $info[pass]=encrypt_word($info[pass]);
    //TABLE ATERNATING COLORS
    $altcolora="#000033";
    $altcolorb="#000020";
    $tablehead=table_head("show","100%","","left");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    <br>
    $tablehead
    &nbsp; &nbsp;<strong><font class='catfont'>Player Editor</font></strong><br>
    <hr class='catfont' size='1'>
    <table width='100%' border='0' bordercolor='#000000' cellspacing='0' cellpadding='0' valign='top' align='center'>
    <form method='post'>
    <tr class='altcolor'>
    <td width='100%' valign='center' align='left' colspan='2'>
    <strong>Player Information (Required)</strong></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Player Name/Alias</font></td>
    <td width='50%' valign='center' align='center'>";
    if($misc[changeplyrname]=="yes"){
        $out[body]=$out[body]."<input type='text' name='n_alias' value='$info[alias]' size='30' maxlength='25'>";
    }else{

        $out[body]=$out[body]."$info[alias]<input type='hidden' name='n_alias' value='$info[alias]' size='30' maxlength='25'>";
    }

    $out[body]=$out[body]."</td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Player Password</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='password' name='n_pass' value='$info[pass]' size='30' maxlength='10'></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Player Password Again</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='password' name='n_passa' value='$info[pass]' size='30' maxlength='10'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Email</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='n_email' value='$info[email]' size='30' maxlength='50'></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Email Again</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='n_emaila' value='$info[email]' size='30' maxlength='50'></td>
    ";
    if($misc[flags]=="yes"){
	  $selected="";
    	  $value="";
    	  $value=$info[country];
        $selected[$value]="SELECTED";

        $out[body]=$out[body]."
        <tr class='altcolorb'>
        <td width='50%' valign='center' align='left'>
        <font face='veradna,arial' size='2' color='#FFFFFF'>
        Country</font></td>
        <td width='50%' valign='center' align='center'>
        <select name='n_country'>
        <option value='AF' $selected[AF]>Afghanistan
        <option value='AL' $selected[AL]>Albania
        <option value='DZ' $selected[DZ]>Algeria
        <option value='AD' $selected[AD]>Andorra
        <option value='AO' $selected[AO]>Angola
        <option value='AG' $selected[AG]>Antigua and Barbuda
        <option value='AR' $selected[AR]>Argentina
        <option value='AM' $selected[AM]>Armenia
        <option value='AU' $selected[AU]>Australia
        <option value='AT' $selected[AT]>Austria
        <option value='AZ' $selected[AZ]>Azerbaijan
        <option value='BS' $selected[BS]>Bahamas
        <option value='BH' $selected[BH]>Bahrain
        <option value='BD' $selected[BD]>Bangladesh
        <option value='BB' $selected[BB]>Barbados
        <option value='BY' $selected[BY]>Belarus
        <option value='BE' $selected[BE]>Belgium
        <option value='BZ' $selected[BZ]>Belize
        <option value='BJ' $selected[BJ]>Benin
        <option value='BT' $selected[BT]>Bhutan
        <option value='BO' $selected[BO]>Bolivia
        <option value='BA' $selected[BA]>Bosnia Herzegovina
        <option value='BW' $selected[BW]>Botswana
        <option value='BR' $selected[BR]>Brazil
        <option value='BN' $selected[BN]>Brunei
        <option value='BG' $selected[BG]>Bulgaria
        <option value='BF' $selected[BF]>Burkina Faso
        <option value='BM' $selected[BM]>Burma
        <option value='BI' $selected[BI]>Burundi
        <option value='KH' $selected[KH]>Cambodia
        <option value='CM' $selected[CM]>Cameroon
        <option value='CA' $selected[CA]>Canada
        <option value='CF' $selected[CF]>Central African Republic
        <option value='TD' $selected[TD]>Chad
        <option value='CL' $selected[CL]>Chile
        <option value='CN' $selected[CN]>China
        <option value='CX' $selected[CX]>Christmas Island
        <option value='CO' $selected[CO]>Colombia
        <option value='KM' $selected[KM]>Comoros
        <option value='CG' $selected[CG]>Congo
        <option value='CR' $selected[CR]>Costa Rica
        <option value='HR' $selected[HR]>Croatia
        <option value='CU' $selected[CU]>Cuba
        <option value='CY' $selected[CY]>Cyprus
        <option value='CZ' $selected[CZ]>Czech Republic
        <option value='DC' $selected[DC]>Democratic Rep. Congo
        <option value='DK' $selected[DK]>Denmark
        <option value='DJ' $selected[DJ]>Djibouti
        <option value='DM' $selected[DM]>Dominica
        <option value='DO' $selected[DO]>Dominican Republic
        <option value='EC' $selected[EC]>Ecuador
        <option value='EG' $selected[EG]>Egypt
        <option value='SV' $selected[SV]>El Salvador
        <option value='GQ' $selected[GQ]>Equatorial Guinea
        <option value='ER' $selected[ER]>Eritrea
        <option value='EE' $selected[EE]>Estonia
        <option value='ET' $selected[ET]>Ethiopia
        <option value='EU' $selected[EU]>European Union
        <option value='FS' $selected[FS]>Fed. States Micronesia
        <option value='FJ' $selected[FJ]>Fiji
        <option value='FI' $selected[FI]>Finland
        <option value='FR' $selected[FR]>France
        <option value='GA' $selected[GA]>Gabon
        <option value='GM' $selected[GM]>Gambia
        <option value='GE' $selected[GE]>Georgia
        <option value='DE' $selected[DE]>Germany
        <option value='GH' $selected[GH]>Ghana
        <option value='GR' $selected[GR]>Greece
        <option value='GD' $selected[GD]>Grenada
        <option value='GT' $selected[GT]>Guatemala
        <option value='GN' $selected[GN]>Guinea
        <option value='GW' $selected[GW]>Guinea-Bissau
        <option value='GY' $selected[GY]>Guyana
        <option value='HT' $selected[HT]>Haiti
        <option value='HN' $selected[HN]>Honduras
        <option value='HK' $selected[HK]>Hong Kong
        <option value='HU' $selected[HU]>Hungary
        <option value='IS' $selected[IS]>Iceland
        <option value='IN' $selected[IN]>India
        <option value='ID' $selected[ID]>Indonesia
        <option value='IR' $selected[IR]>Iran
        <option value='IQ' $selected[IQ]>Iraq
        <option value='IE' $selected[IE]>Ireland
        <option value='IL' $selected[IL]>Israel
        <option value='IT' $selected[IT]>Italy
        <option value='IV' $selected[IV]>Ivory Coast
        <option value='JM' $selected[JM]>Jamaica
        <option value='JP' $selected[JP]>Japan
        <option value='JO' $selected[JO]>Jordan
        <option value='KZ' $selected[KZ]>Kazakhstan
        <option value='KE' $selected[KE]>Kenya
        <option value='KI' $selected[KI]>Kiribati
        <option value='KW' $selected[KW]>Kuwait
        <option value='KG' $selected[KG]>Kyrgyzstan
        <option value='LA' $selected[LA]>Laos
        <option value='LV' $selected[LV]>Latvia
        <option value='LB' $selected[LB]>Lebanon
        <option value='LR' $selected[LR]>Liberia
        <option value='LY' $selected[LY]>Libya
        <option value='LI' $selected[LI]>Liechtenstein
        <option value='LT' $selected[LT]>Lithuania
        <option value='LU' $selected[LU]>Luxembourg
        <option value='MO' $selected[MO]>Macau
        <option value='MK' $selected[MK]>Macedonia
        <option value='MG' $selected[MG]>Madagascar
        <option value='MW' $selected[MW]>Malawi
        <option value='MY' $selected[MY]>Malaysia
        <option value='MV' $selected[MV]>Maldives
        <option value='ML' $selected[ML]>Mali
        <option value='MT' $selected[MT]>Malta
        <option value='MR' $selected[MR]>Mauritania
        <option value='MU' $selected[MU]>Mauritius
        <option value='MX' $selected[MX]>Mexico
        <option value='MD' $selected[MD]>Moldova
        <option value='MC' $selected[MC]>Monaco
        <option value='MN' $selected[MN]>Mongolia
        <option value='MA' $selected[MA]>Morocco
        <option value='MZ' $selected[MZ]>Mozambique
        <option value='NA' $selected[NA]>Namibia
        <option value='NR' $selected[NR]>Nauru
        <option value='NP' $selected[NP]>Nepal
        <option value='NL' $selected[NL]>Netherlands
        <option value='AN' $selected[AN]>Netherlands Antilles
        <option value='NZ' $selected[NZ]>New Zealand
        <option value='NI' $selected[NI]>Nicaragua
        <option value='NE' $selected[NE]>Niger
        <option value='NG' $selected[NG]>Nigeria
        <option value='NK' $selected[NK]>North Korea
        <option value='NO' $selected[NO]>Norway
        <option value='OM' $selected[OM]>Oman
        <option value='PK' $selected[PK]>Pakistan
        <option value='PA' $selected[PA]>Panama
        <option value='PG' $selected[PG]>Papua New Guinea
        <option value='PY' $selected[PY]>Paraguay
        <option value='PE' $selected[PE]>Peru
        <option value='PH' $selected[PH]>Philippines
        <option value='PL' $selected[PL]>Poland
        <option value='PT' $selected[PT]>Portugal
        <option value='PR' $selected[PR]>Puerto Rico
        <option value='QA' $selected[QA]>Qatar
        <option value='RO' $selected[RO]>Romania
        <option value='RU' $selected[RU]>Russia
        <option value='RW' $selected[RW]>Rwanda
        <option value='KN' $selected[KN]>Saint Kitts and Nevis
        <option value='LC' $selected[LC]>Saint Lucia
        <option value='VC' $selected[VC]>Saint Vincent/Grenadines
        <option value='ST' $selected[ST]>Sao Tome and Principe
        <option value='SA' $selected[SA]>Saudi Arabia
        <option value='SN' $selected[SN]>Senegal
        <option value='SC' $selected[SC]>Seychelles
        <option value='SL' $selected[SL]>Sierra Leone
        <option value='SG' $selected[SG]>Singapore
        <option value='SK' $selected[SK]>Slovakia
        <option value='SI' $selected[SI]>Slovenia
        <option value='SB' $selected[SB]>Solomon Islands
        <option value='SO' $selected[SO]>Somalia
        <option value='ZA' $selected[ZA]>South Africa
        <option value='KS' $selected[KS]>South Korea
        <option value='ES' $selected[ES]>Spain
        <option value='LK' $selected[LK]>Sri Lanka
        <option value='SD' $selected[SD]>Sudan
        <option value='SR' $selected[SR]>Suriname
        <option value='SE' $selected[SE]>Sweden
        <option value='CH' $selected[CH]>Switzerland
        <option value='SY' $selected[SY]>Syria
        <option value='TW' $selected[TW]>Taiwan
        <option value='TJ' $selected[TJ]>Tajikistan
        <option value='TZ' $selected[TZ]>Tanzania
        <option value='TH' $selected[TH]>Thailand
        <option value='TG' $selected[TG]>Togo
        <option value='TO' $selected[TO]>Tonga
        <option value='TT' $selected[TT]>Trinidad and Tobago
        <option value='TN' $selected[TN]>Tunisia
        <option value='TR' $selected[TR]>Turkey
        <option value='TM' $selected[TM]>Turkmenistan
        <option value='TV' $selected[TV]>Tuvalu
        <option value='UG' $selected[UG]>Uganda
        <option value='UA' $selected[UA]>Ukraine
        <option value='AE' $selected[AE]>United Arab Emirates
        <option value='GB' $selected[GB]>United Kingdom
        <option value='US' $selected[US]>United States Of America
        <option value='UY' $selected[UY]>Uruguay
        <option value='UZ' $selected[UZ]>Uzbekistan
        <option value='VU' $selected[VU]>Vanuatu
        <option value='VE' $selected[VE]>Venezuela
        <option value='VN' $selected[VN]>Viet Nam
        <option value='EH' $selected[EH]>Western Samoa
        <option value='YE' $selected[YE]>Yemen
        <option value='YU' $selected[YU]>Yugoslavia
        <option value='ZM' $selected[ZM]>Zambia
        <option value='ZW' $selected[ZW]>Zimbabwe
        </select>
        </td>
        ";
    }

    $out[body]=$out[body]."
    </tr>
    ";
    if($uinfo[edit]=="yes"){
	  $detailedinfo=mysql_query("SELECT * FROM usersinfo WHERE id='$plyr[id]'");
        $dinfo=mysql_fetch_array($detailedinfo);
        $out[body]=$out[body]."
        <tr class='altcolor'>
        <td width='100%' valign='center' align='left' colspan='2'>
        <strong>User Information</strong></td>
        </tr>
        <tr class='altcolora'>
        <td width='50%' valign='center' align='left'>
        <font face='veradna,arial' size='2' color='#FFFFFF'>
        First Name</font></td>
        <td width='50%' valign='center' align='center'>
        <input type='text' name='n_fname' value='$dinfo[firstname]' size='30' maxlength='25'></td>
        </tr>
        <tr class='altcolorb'>
        <td width='50%' valign='center' align='left'>
        <font face='veradna,arial' size='2' color='#FFFFFF'>
        Middle Name</font></td>
        <td width='50%' valign='center' align='center'>
        <input type='text' name='n_mname' value='$dinfo[middlename]' size='30' maxlength='25'></td>
        </tr>
        <tr class='altcolora'>
        <td width='50%' valign='center' align='left'>
        <font face='veradna,arial' size='2' color='#FFFFFF'>
        Last Name</font></td>
        <td width='50%' valign='center' align='center'>
        <input type='text' name='n_lname' value='$dinfo[lastname]' size='30' maxlength='25'></td>
        </tr>
        <tr class='altcolorb'>
        <td width='50%' valign='center' align='left'>
        <font face='veradna,arial' size='2' color='#FFFFFF'>
        Address</font></td>
        <td width='50%' valign='center' align='center'>
        <input type='text' name='n_addrs' value='$dinfo[address]' size='30' maxlength='50'></td>
        </tr>
        <tr class='altcolora'>
        <td width='50%' valign='center' align='left'>
        <font face='veradna,arial' size='2' color='#FFFFFF'>
        City</font></td>
        <td width='50%' valign='center' align='center'>
        <input type='text' name='n_city' value='$dinfo[city]' size='30' maxlength='50'></td>
        </tr>
        <tr class='altcolorb'>
        <td width='50%' valign='center' align='left'>
        <font face='veradna,arial' size='2' color='#FFFFFF'>
        State</font></td>
        <td width='50%' valign='center' align='center'>
	  ";
 	  $selected="";
    	  $value="";
    	  $value=$dinfo[state];
        $selected[$value]="SELECTED";

        $out[body]=$out[body]."	
        <select name='n_state'>
        <option value=''></option>
        <option value='AL' $selected[AL]>Alabama</option>
        <option value='AK' $selected[AK]>Alaska</option>
        <option value='AZ' $selected[AZ]>Arizona</option>
        <option value='AR' $selected[AR]>Arkansas</option>
        <option value='CA' $selected[CA]>California</option>
        <option value='CO' $selected[CO]>Colorado</option>
        <option value='CT' $selected[CT]>Connecticut</option>
        <option value='DE' $selected[DE]>Delaware</option>
        <option value='DC' $selected[DC]>District of Columbia</option>
        <option value='FL' $selected[FL]>Florida</option>
        <option value='GA' $selected[GA]>Georgia</option>
        <option value='HI' $selected[HI]>Hawaii</option>
        <option value='ID' $selected[ID]>Idaho</option>
        <option value='IL' $selected[IL]>Illinois</option>
        <option value='IN' $selected[IN]>Indiana</option>
        <option value='IA' $selected[IA]>Iowa</option>
        <option value='KS' $selected[KS]>Kansas</option>
        <option value='KY' $selected[KY]>Kentucky</option>
        <option value='LA' $selected[LA]>Louisiana</option>
        <option value='ME' $selected[ME]>Maine</option>
        <option value='MD' $selected[MD]>Maryland</option>
        <option value='MA' $selected[MA]>Massachusetts</option>
        <option value='MI' $selected[MI]>Michigan</option>
        <option value='MN' $selected[MN]>Minnesota</option>
        <option value='MS' $selected[MS]>Mississippi</option>
        <option value='MO' $selected[MO]>Missouri</option>
        <option value='MT' $selected[MT]>Montana</option>
        <option value='NE' $selected[NE]>Nebraska</option>
        <option value='NV' $selected[NV]>Nevada</option>
        <option value='NH' $selected[NH]>New Hampshire</option>
        <option value='NJ' $selected[NJ]>New Jersey</option>
        <option value='NM' $selected[NM]>New Mexico</option>
        <option value='NY' $selected[NY]>New York</option>
        <option value='NC' $selected[NC]>North Carolina</option>
        <option value='ND' $selected[ND]>North Dakota</option>
        <option value='OH' $selected[OH]>Ohio</option>
        <option value='OK' $selected[OK]>Oklahoma</option>
        <option value='OR' $selected[OR]>Oregon</option>
        <option value='PA' $selected[PA]>Pennsylvania</option>
        <option value='RI' $selected[RI]>Rhode Island</option>
        <option value='SC' $selected[SC]>South Carolina</option>
        <option value='SD' $selected[SD]>South Dakota</option>
        <option value='TN' $selected[TN]>Tennessee</option>
        <option value='TX' $selected[TX]>Texas</option>
        <option value='UT' $selected[UT]>Utah</option>
        <option value='VT' $selected[VT]>Vermont</option>
        <option value='VA' $selected[VA]>Virginia</option>
        <option value='WA' $selected[WA]>Washington</option>
        <option value='WV' $selected[WV]>West Virginia</option>
        <option value='WI' $selected[WI]>Wisconsin</option>
        <option value='WY' $selected[WY]>Wyoming</option>
        <option value='IT' $selected[IT]>International / Not in United States</option>
        </select></td>
        </tr>
        <tr class='altcolora'>
        <td width='50%' valign='center' align='left'>
        <font face='veradna,arial' size='2' color='#FFFFFF'>
        Zip Code</font></td>
        <td width='50%' valign='center' align='center'>
        <input type='text' name='n_zip' value='$dinfo[zipcode]' size='30' maxlength='10'></td>
        </tr>
        <tr class='altcolorb'>
        <td width='50%' valign='center' align='left'>
        <font face='veradna,arial' size='2' color='#FFFFFF'>
        Phone Number</font></td>
        <td width='50%' valign='center' align='center'>
        (<input type='text' name='n_phonea' value='' size='4' maxlength='3'>)
        <input type='text' name='n_phoneb' value='' size='20' maxlength='7'></td>
        </tr>
        <tr class='altcolora'>
        <td width='50%' valign='center' align='left'>
        <font face='veradna,arial' size='2' color='#FFFFFF'>
        Birthdate</font></td>
        <td width='50%' valign='center' align='center'>
        <font face='veradna,arial' size='2' color='#FFFFFF'>
        <select name='n_bdaym'>
        <option></option>
        <option value='01'>Jan</option>
        <option value='02'>Feb</option>
        <option value='03'>Mar</option>
        <option value='04'>Apr</option>
        <option value='05'>May</option>
        <option value='06'>Jun</option>
        <option value='07'>Jul</option>
        <option value='08'>Aug</option>
        <option value='09'>Sep</option>
        <option value='10'>Oct</option>
        <option value='11'>Nov</option>
        <option value='12'>Dec</option>
        </select>
        / <select name='n_bdayd'>
        <option></option>
        <option>01</option>
        <option>02</option>
        <option>03</option>
        <option>04</option>
        <option>05</option>
        <option>06</option>
        <option>07</option>
        <option>08</option>
        <option>09</option>
        <option>10</option>
        <option>11</option>
        <option>12</option>
        <option>13</option>
        <option>14</option>
        <option>15</option>
        <option>16</option>
        <option>17</option>
        <option>18</option>
        <option>19</option>
        <option>20</option>
        <option>21</option>
        <option>22</option>
        <option>23</option>
        <option>24</option>
        <option>25</option>
        <option>26</option>
        <option>27</option>
        <option>28</option>
        <option>29</option>
        <option>30</option>
        <option>31</option>
        </select>
        / <select name='n_bdayy'>
        <option></option>
        <option>1900</option>
        <option>1901</option>
        <option>1902</option>
        <option>1903</option>
        <option>1904</option>
        <option>1905</option>
        <option>1906</option>
        <option>1907</option>
        <option>1908</option>
        <option>1909</option>
        <option>1910</option>
        <option>1911</option>
        <option>1912</option>
        <option>1913</option>
        <option>1914</option>
        <option>1915</option>
        <option>1916</option>
        <option>1917</option>
        <option>1918</option>
        <option>1919</option>
        <option>1920</option>
        <option>1921</option>
        <option>1922</option>
        <option>1923</option>
        <option>1924</option>
        <option>1925</option>
        <option>1926</option>
        <option>1927</option>
        <option>1928</option>
        <option>1929</option>
        <option>1930</option>
        <option>1931</option>
        <option>1932</option>
        <option>1933</option>
        <option>1934</option>
        <option>1935</option>
        <option>1936</option>
        <option>1937</option>
        <option>1938</option>
        <option>1939</option>
        <option>1940</option>
        <option>1940</option>
        <option>1941</option>
        <option>1942</option>
        <option>1943</option>
        <option>1944</option>
        <option>1945</option>
        <option>1946</option>
        <option>1947</option>
        <option>1948</option>
        <option>1949</option>
        <option>1950</option>
        <option>1951</option>
        <option>1952</option>
        <option>1953</option>
        <option>1954</option>
        <option>1955</option>
        <option>1956</option>
        <option>1957</option>
        <option>1958</option>
        <option>1959</option>
        <option>1960</option>
        <option>1961</option>
        <option>1962</option>
        <option>1963</option>
        <option>1964</option>
        <option>1965</option>
        <option>1966</option>
        <option>1967</option>
        <option>1968</option>
        <option>1969</option>
        <option>1970</option>
        <option>1971</option>
        <option>1972</option>
        <option>1973</option>
        <option>1974</option>
        <option>1975</option>
        <option>1976</option>
        <option>1977</option>
        <option>1978</option>
        <option>1979</option>
        <option>1980</option>
        <option>1981</option>
        <option>1982</option>
        <option>1983</option>
        <option>1984</option>
        <option>1985</option>
        <option>1986</option>
        <option>1987</option>
        <option>1988</option>
        <option>1989</option>
        <option>1990</option>
        <option>1991</option>
        <option>1992</option>
        <option>1993</option>
        <option>1994</option>
        <option>1995</option>
        <option>1996</option>
        <option>1997</option>
        <option>1998</option>
        <option>1999</option>
        </select>
        </font></td>
        </tr>
        <tr class='altcolorb'>
        <td width='50%' valign='center' align='left'>
        <font face='veradna,arial' size='2' color='#FFFFFF'>
        Occupation</font></td>
        <td width='50%' valign='center' align='center'>
	  ";

 	  $selected="";
    	  $value="";
    	  $value=$dinfo[occupation];
        $selected[$value]="SELECTED";
	        

    	  $out[body]=$out[body]."
        <select name='n_occup'>
        <option value=''></option>
        <option value='01' $selected[01]>K-12 student</option>
        <option value='02' $selected[02]>College/graduate student</option>
        <option value='03' $selected[03]>Sales/marketing</option>
        <option value='04' $selected[04]>Tradesman/craftsman</option>
        <option value='05' $selected[05]>Executive/managerial</option>
        <option value='06' $selected[06]>Professional (doctor, lawyer, etc.)</option>
        <option value='07' $selected[07]>Academic/educator</option>
        <option value='08' $selected[08]>Computer technical/engineering</option>
        <option value='09' $selected[09]>Other technical/engineering</option>
        <option value='10' $selected[10]>Service/customer support</option>
        <option value='11' $selected[11]>Clerical/administrative</option>
        <option value='12' $selected[12]>Homemaker</option>
        <option value='13' $selected[13]>Self-employed/own company</option>
        <option value='14' $selected[14]>Unemployed, looking for work</option>
        <option value='15' $selected[15]>Retired</option>
        <option value='16' $selected[16]>Other</option>
        </select></td>
        </tr>";
    }

    $out[body]=$out[body]."
    <tr class='altcolor'>
    <td width='100%' valign='center' align='left' colspan='2'>
    <strong>Optional Information</strong></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Icq Number</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='n_icq' value='$info[icq]' size='30' maxlength='10'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Aim Name</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='n_aim' value='$info[aim]' size='30' maxlength='16'></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    MSN Messenger</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='n_msn' value='$info[msn]' size='30' maxlength='35'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Yahoo</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='n_yahoo' value='$info[yahoo]' size='30' maxlength='35'></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Website Url</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='n_url' value='$info[website]' size='30' maxlength='100'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Player Logo</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='n_logo' value='$info[logo]' size='30' maxlength='100'></td>
    </tr>
    ";
    $themes.="<option value='$info[theme]'>$info[theme]</option>";
    if($theme[option1]){
        $themes.="<option value='$theme[option1]'>$theme[option1]</option>";
    }

    if($theme[option2]){
        $themes.="<option value='$theme[option2]'>$theme[option2]</option>";
    }

    if($theme[option3]){
        $themes.="<option value='$theme[option3]'>$theme[option3]</option>";
    }

    if($theme[option4]){
        $themes.="<option value='$theme[option4]'>$theme[option4]</option>";
    }

    if($theme[option5]){
        $themes.="<option value='$theme[option5]'>$theme[option5]</option>";
    }

    $out[body]=$out[body]."
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    Theme</font></td>
    <td width='50%' valign='center' align='center'>
    <select name='n_theme'>$themes</select></td>
    </tr>
    ";
    if($misc[newsletter]=="yes"){
        if($info[newsletter]=="1"){
            $chkd="CHECKED";
        }

    }

    $out[body]=$out[body]."
    <tr>
    <td width='100%' valign='center' align='center' colspan='2'>
    <br>
    <input type='hidden' name='action' value='editplayerb'>
    <input type='submit' name='' value='Update My Account'>
    <input type='reset' name='' value='Reset Changes'>
    </td>
    </form>
    </tr>
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function edit_playerb($n_alias,$n_pass,$n_passa,$n_email,$n_emaila,$n_fname,$n_mname,$n_lname,$n_addrs,$n_city,$n_state,$n_zip,$n_phonea,$n_phoneb,$n_bdaym,$n_bdayd,$n_bdayy,$n_occup,$n_icq,$n_aim,$n_yahoo,$n_msn,$n_url,$n_logo,$n_newsletter,$n_theme,$n_country){
    global $dir, $url, $out, $plyr, $uinfo;
    include("$dir[func]/checkdata.php");
    if(($n_country == "") && ($misc[flags] =="yes")){
        include("$dir[func]/error.php");
        display_error("You Must Select a Country<br>");
    }

    $plyrpass=encrypt_word($plyr[pass]);
    if($plyrpass=="$n_pass"){
        $n_pass=decrypt_word($n_pass);
        $n_passa=decrypt_word($n_passa);
    }

    if($uinfo[allow]=="yes"){
        $n_fname=change_charecters($n_fname);
        $n_mname=change_charecters($n_mname);
        $n_lname=change_charecters($n_lname);
        $n_addrs=change_charecters($n_addrs);
        $n_city=change_charecters($n_city);
        $n_state=change_charecters($n_state);
        $n_zip=change_numbersonly($n_zip);
        $n_phonea=change_numbersonly($n_phonea);
        $n_phoneb=change_numbersonly($n_phoneb);
        $n_bdaym=change_numbersonly($n_bdaym);
        $n_bdayd=change_numbersonly($n_bdayd);
        $n_bdayy=change_numbersonly($n_bdayy);
        $n_occup=change_numbersonly($n_occup);
    }

    $n_phone="$n_phonea$n_phoneb";
    $errormessage=check_matching($errormessage, $n_pass, $n_passa, "You Passwords Do Not Match");
    $errormessage=check_matching($errormessage, $n_email, $n_emaila, "You Email Addresses Do Not Match");
    $errormessage=check_validlength($errormessage, $n_alias, "3", "30", "Your Player Alias Must be 3-25 Characters Long");
    $errormessage=check_validlength($errormessage, $n_pass, "2", "15", "Your Password Must be 2-10 Characters Long");
    error_check($errormessage);
    $n_alias=change_charecters($n_alias);
    $n_pass=change_charecters($n_pass);
    $errormessage=check_validlength($errormessage, $n_alias, "3", "50", "Your Player Alias Must be 3-25 Characters Long");
    $errormessage=check_validlength($errormessage, $n_pass, "2", "25", "Your Password Must be 2-10 Characters Long");
    error_check($errormessage);
    $errormessage=check_emailaddress($n_email);
    error_check($errormessage);
    $errormessage=check_unvalusersinfoexist($errormessage, $n_alias, $n_emaila, "x");
    $errormessage=check_usersinfoexistother($errormessage, $plyr[id], $n_alias, $n_emaila, "x");
    error_check($errormessage);
    $n_icq=change_numbersonly($n_icq);
    $n_aim=change_charecters($n_aim);
    $n_yahoo=change_charecters($n_yahoo);
    $n_url=change_url($n_url);
    $n_logo=change_url($n_logo);
    $n_logo=change_charecters($n_logo);
    $n_url=change_charecters($n_url);
    $n_email=change_charecters($n_email);
    $errormessage=check_ban($errormessage, $n_alias, $n_email, $ip);
    error_check($errormessage);
    if (mysql_num_rows(mysql_query("select id from users where id='$plyr[id]' AND pass='$plyr[pass]'")) > 0){
        mysql_query("UPDATE users SET
        alias='$n_alias',
        pass='$n_pass',
        email='$n_email',
        icq='$n_icq',
        aim='$n_aim',
        yahoo='$n_yahoo',
        msn='$n_msn',
        website='$n_url',
        logo='$n_logo',
        newsletter='$n_newsletter',
        theme='$n_theme',
        country='$n_country'
        WHERE id='$plyr[id]' AND pass='$plyr[pass]'");
        if($uinfo[edit]=="yes"){
            if($n_fname){
                mysql_query("UPDATE usersinfo SET firstname='$n_fname' WHERE id='$plyr[id]'");
            }

            if($n_mname){
                mysql_query("UPDATE usersinfo SET middlename='$n_mname' WHERE id='$plyr[id]'");
            }

            if($n_lname){
                mysql_query("UPDATE usersinfo SET lastname='$n_lname' WHERE id='$plyr[id]'");
            }

            if($n_addrs){
                mysql_query("UPDATE usersinfo SET address='$n_addrs' WHERE id='$plyr[id]'");
            }

            if($n_city){
                mysql_query("UPDATE usersinfo SET city='$n_city' WHERE id='$plyr[id]'");
            }

            if($n_state){
                mysql_query("UPDATE usersinfo SET state='$n_state' WHERE id='$plyr[id]'");
            }

            if($n_zip){
                mysql_query("UPDATE usersinfo SET zipcode='$n_zip' WHERE id='$plyr[id]'");
            }

            if($n_phone){
                mysql_query("UPDATE usersinfo SET phone='$n_phone' WHERE id='$plyr[id]'");
            }

            if(($n_bdaym) && ($n_bdayd) && ($n_bdayy)){
                $bday="$n_bdayy-$n_bdaym-$n_bdayd";
                mysql_query("UPDATE usersinfo SET birthday='$bday' WHERE id='$plyr[id]'");
            }

            if($n_occup){
                mysql_query("UPDATE usersinfo SET occupation='$n_occup' WHERE id='$plyr[id]'");
            }

        }

    }else{

        include("$dir[func]/loginforms.php");
        form_login();
        exit;
    }

    include("$dir[func]/finishmessage.php");
    display_message("You Player Information has been Updated","phome");
}

?>
