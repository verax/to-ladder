<?
if(!IsSet($indexloaded)){
    header("Location: ./index.php");
}

//CHECK ADMIN ACCESS LEVEL
if($admn[access] < 35){
    include("$dir[func]/error.php");
    display_error("You are not allowed to perform this function.<br>");
}

function admin_challenge(){
    global $dir, $url, $out, $site, $admn;
    //GET LADDERS
    $ladderslist=mysql_query("SELECT id,laddername  FROM ladders ORDER by laddername");
    while(list($id,$name)=mysql_fetch_row($ladderslist)){
        $theladders=$theladders."<option value='$id'>$name</option>";
        $foundladders=1;
    }

    if(!$foundladders){
        include("$dir[func]/error.php");
        display_error("There is no ladders.<br>");
    }

    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <form method='post'>
    <td width='100%' valign='top' align='center'>
    <strong>Edit a Challenge</strong><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <br>
    Select a ladder<br>
    <select name='ladderid'>$theladders</select><br>
    <br></td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='action' value='editchallengeb'>
    <input type='submit' name='' value='Find Challenge'>
    </td>
    </form>
    </tr>
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function admin_challengeb($ladderid){
    global $dir, $url, $out, $site, $admn;
    if(!$ladderid){
        include("$dir[func]/error.php");
        display_error("Invalid Ladder ID.<br>");
    }

    //CHECK STAFF CAN MANAGE THIS LADDER
    if($admn[access] < 99){
        if (mysql_num_rows(mysql_query("SELECT staffid FROM staffaccess WHERE staffid='$admn[id]' AND ladderid='$ladderid'")) < 1){
            include("$dir[func]/error.php");
            display_error("You are not allowed to edit challenges on this laddder.<br>");
        }

    }

    //GET CHALLENGES
    $challengelist=mysql_query("SELECT challid,challengeralias,challengedalias,DATE_FORMAT(challengedtime, '%M %d, %l:%i %p') FROM challenges WHERE ladderid='$ladderid' ORDER by challengedtime");
    while(list($challid,$challengeralias,$challengedalias,$challengedtime)=mysql_fetch_row($challengelist)){
        $thechallenges=$thechallenges."<option value='$challid'>$challengeralias vs $challengedalias - $challengedtime</option>";
        $foundladders=1;
    }

    if(!$foundladders){
        include("$dir[func]/error.php");
        display_error("There is no challenges on this ladder to edit.<br>");
    }

    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <form method='post'>
    <td width='100%' valign='top' align='center'>
    <strong>Edit a Challenge</strong><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <br>
    <select name='challid'>$thechallenges</select><br>
    <br></td>
    </tr>
    <script language='javascript'>var confirmdelete='Are you sure you want to DELETE this challenge';</script>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='action' value='editchallengec'>
    <input type='submit' name='todo' value='Delete Challenge' onClick='return confirm(confirmdelete);'>
    </td>
    </form>
    </tr>
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function admin_challengec($challid,$todo){
    global $dir, $url, $out, $site, $admn;
    if(!$challid){
        include("$dir[func]/error.php");
        display_error("Invalid Challenge ID.<br>");
    }

    if($todo=="Delete Challenge"){
        delete_challenge($challid);
        include("$dir[func]/admin_finishmessage.php");
        display_message("The challenge was deleted.<br>");
    }

    else{
        include("$dir[func]/error.php");
        display_error("Unknown Command.<br>");
    }

}

function delete_challenge($challid){
    $wl=mysql_query("SELECT challenger,challenged,ladderid FROM challenges WHERE challid='$challid'");
    $wlid=mysql_fetch_array($wl);
    mysql_query("UPDATE ladder_$wlid[ladderid] SET statusdisplay='' WHERE id='$wlid[challenger]'");
    mysql_query("UPDATE ladder_$wlid[ladderid] SET statusdisplay='' WHERE id='$wlid[challenged]'");
    mysql_query("DELETE FROM challenges WHERE challid='$challid'");
}

?>
