<?
if(!IsSet($indexloaded)){
    header("Location: ./index.php");
}

//CHECK ADMIN ACCESS LEVEL
if($admn[access] < 40){
    include("$dir[func]/error.php");
    display_error("You are not allowed to perform this function.<br>");
}

function admin_editplayer(){
    global $dir, $url, $out, $site, $admn;
    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <form method='post'>
    <td width='100%' valign='top' align='center'>
    <strong>Edit a Player</strong><br>
    </td>
    </tr>
    <tr>
    <script language='javascript'>var confirmdelete='Are you sure you want to DELETE this Player';</script>
    <td width='100%' valign='center' align='center'>
    <br>
    Player ID<br>
    <input type='text' name='playerid' value=''><br>
    <br></td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='action' value='editplayerb'>
    <input type='submit' name='todo' value='Edit Player'> ";
    if($admn[access] >= 45){
        $out[body]=$out[body]."<input type='submit' name='todo' value='Delete Player' onClick='return confirm(confirmdelete);'>";
    }

    $out[body]=$out[body]."
    </td>
    </form>
    </tr>
    </table>";
    include("$dir[curtheme]");
}

function admin_editplayerb($playerid,$todo){
    global $dir, $url, $out, $site, $admn;
    if(!$playerid){
        include("$dir[func]/error.php");
        display_error("Invalid Player ID.<br>");
    }

    if($todo=="Edit Player"){
        edit_account($playerid);
    }

    else if($todo=="Delete Player"){
        delete_account($playerid);
    }

    else{
        include("$dir[func]/error.php");
        display_error("Unknwon Command.<br>");
    }

}

function delete_account($playerid){
    global $dir, $url, $out, $site, $admn;
    //CHECK IF PLAYER EXISTS
    if (mysql_num_rows(mysql_query("SELECT id FROM users WHERE id='$playerid'")) < 1){
        include("$dir[func]/error.php");
        display_error("Unknown Player ID.<br>");
    }

    //CHECK IF ON A TEAM
    if (mysql_num_rows(mysql_query("SELECT playerid FROM teammembers WHERE playerid='$playerid'")) > 0){
        include("$dir[func]/error.php");
        display_error("You must remove this player from all teams before you can delete them.<br>");
    }

    mysql_query("DELETE FROM users WHERE id='$playerid'");
    mysql_query("DELETE FROM usersinfo WHERE id='$playerid'");
    include("$dir[func]/admin_finishmessage.php");
    display_message("The player account was deleted.<br>");
}

function edit_account($playerid){
    global $dir, $url, $out, $site, $admn, $misc, $theme;
    $teaminfo=mysql_query("SELECT * FROM users WHERE id='$playerid'");
    $pinfo=mysql_fetch_array($teaminfo);
    if(!$pinfo[alias]){
        include("$dir[func]/error.php");
        display_error("Unknown Player.<br>");
    }

    //TABLE ATERNATING COLORS
    $altcolora="#000033";
    $altcolorb="#000020";
    if($tinfo[tagplace]=="0"){
        $checkeda="CHECKED";
        $checkedb="";
    }else{

        $checkeda="";
        $checkedb="CHECKED";
    }

    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <strong>Edit a Player</strong></td>
    </tr>
    <tr>
    <td width='100%' valign='top' align='left'>
    <form method='post'>
    <table width='100%' border='0' cellspacing='0' cellpadding='0' valign='top' align='center'>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>* Player Name</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='p_alias' value='$pinfo[alias]' size='30' maxlength='25'></td>
    </tr>";
    if($admn[access] >= 45){
        $out[body]=$out[body]."
        <tr class='altcolorb'>
        <td width='50%' valign='center' align='left'>
        <font face='veradna,arial' size='2' color='#FFFFFF'>* Player Pass</font></td>
        <td width='50%' valign='center' align='center'>
        <input type='password' name='p_pass' value='$pinfo[pass]' size='30' maxlength='10'></td>
        </tr>";
        $showip=1;
    }else{

        $showip=0;
        $out[body]=$out[body]."
        <input type='hidden' name='p_pass' value='$pinfo[pass]' size='30' maxlength='10'>";
    }

    $out[body]=$out[body]."
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>* Player Email</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='p_email' value='$pinfo[email]' size='30' maxlength='50'></td>
    </tr>
    ";
    if($misc[flags]=="yes"){

	  // $selected="";
    	  // $value="";
    	  // $value=$pinfo[country];
        // $selected[$value]="SELECTED";

        $out[body]=$out[body]."
        <tr class='altcolorb'>
        <td width='50%' valign='center' align='left'>
        <font face='veradna,arial' size='2' color='#FFFFFF'>
        Country</font></td>
        <td width='50%' valign='center' align='center'>
        <select name='p_country'>
        <option value='AF' >Afghanistan</option>
        <option value='AL' >Albania</option>
        <option value='DZ' >Algeria</option>
        <option value='AD' >Andorra</option>
        <option value='AO' >Angola</option>
        <option value='AG' >Antigua and Barbuda</option>
        <option value='AR' >Argentina</option>
        <option value='AM' >Armenia</option>
        <option value='AU' >Australia</option>
        <option value='AT' >Austria</option>
        <option value='AZ' >Azerbaijan</option>
        <option value='BS' >Bahamas</option>
        <option value='BH' >Bahrain</option>
        <option value='BD' >Bangladesh</option>
        <option value='BB' >Barbados</option>
        <option value='BY' >Belarus</option>
        <option value='BE' >Belgium</option>
        <option value='BZ' >Belize</option>
        <option value='BJ' >Benin</option>
        <option value='BT' >Bhutan</option>
        <option value='BO' >Bolivia</option>
        <option value='BA' >Bosnia Herzegovia</option>na
        <option value='BW' >Botswana</option>
        <option value='BR' >Brazil</option>
        <option value='BN' >Brunei</option>
        <option value='BG' >Bulgaria</option>
        <option value='BF' >Burkina Faso</option>
        <option value='BM' >Burma</option>
        <option value='BI' >Burundi</option>
        <option value='KH' >Cambodia</option>
        <option value='CM' >Cameroon</option>
        <option value='CA' >Canada</option>
        <option value='CF' >Central African Republic</option>
        <option value='TD' >Chad</option>
        <option value='CL' selected>Chile</option>
        <option value='CN' >China</option>
        <option value='CX' >Christmas Island</option>
        <option value='CO' >Colombia</option>
        <option value='KM' >Comoros</option>
        <option value='CG' >Congo</option>
        <option value='CR' >Costa Rica</option>
        <option value='HR' >Croatia</option>
        <option value='CU' >Cuba</option>
        <option value='CY' >Cyprus</option>
        <option value='CZ' >Czech Republic</option>
        <option value='DC' >Democratic Rep. Congo</option>
        <option value='DK' >Denmark</option>
        <option value='DJ' >Djibouti</option>
        <option value='DM' >Dominica</option>
        <option value='DO' >Dominican Republic</option>
        <option value='EC' >Ecuador</option>
        <option value='EG' >Egypt</option>
        <option value='SV' >El Salvador</option>
        <option value='GQ' >Equatorial Guinea</option>
        <option value='ER' >Eritrea</option>
        <option value='EE' >Estonia</option>
        <option value='ET' >Ethiopia</option>
        <option value='EU' >European Union</option>
        <option value='FS' >Fed. States Micronesia</option>
        <option value='FJ' >Fiji</option>
        <option value='FI' >Finland</option>
        <option value='FR' >France</option>
        <option value='GA' >Gabon</option>
        <option value='GM' >Gambia</option>
        <option value='GE' >Georgia</option>
        <option value='DE' >Germany</option>
        <option value='GH' >Ghana</option>
        <option value='GR' >Greece</option>
        <option value='GD' >Grenada</option>
        <option value='GT' >Guatemala</option>
        <option value='GN' >Guinea</option>
        <option value='GW' >Guinea-Bissau</option>
        <option value='GY' >Guyana</option>
        <option value='HT' >Haiti</option>
        <option value='HN' >Honduras</option>
        <option value='HK' >Hong Kong</option>
        <option value='HU' >Hungary</option>
        <option value='IS' >Iceland</option>
        <option value='IN' >India</option>
        <option value='ID' >Indonesia</option>
        <option value='IR' >Iran</option>
        <option value='IQ' >Iraq</option>
        <option value='IE' >Ireland</option>
        <option value='IL' >Israel</option>
        <option value='IT' >Italy</option>
        <option value='IV' >Ivory Coast</option>
        <option value='JM' >Jamaica</option>
        <option value='JP' >Japan</option>
        <option value='JO' >Jordan</option>
        <option value='KZ' >Kazakhstan</option>
        <option value='KE' >Kenya</option>
        <option value='KI' >Kiribati</option>
        <option value='KW' >Kuwait</option>
        <option value='KG' >Kyrgyzstan</option>
        <option value='LA' >Laos</option>
        <option value='LV' >Latvia</option>
        <option value='LB' >Lebanon</option>
        <option value='LR' >Liberia</option>
        <option value='LY' >Libya</option>
        <option value='LI' >Liechtenstein</option>
        <option value='LT' >Lithuania</option>
        <option value='LU' >Luxembourg</option>
        <option value='MO' >Macau</option>
        <option value='MK' >Macedonia</option>
        <option value='MG' >Madagascar</option>
        <option value='MW' >Malawi</option>
        <option value='MY' >Malaysia</option>
        <option value='MV' >Maldives</option>
        <option value='ML' >Mali</option>
        <option value='MT' >Malta</option>
        <option value='MR' >Mauritania</option>
        <option value='MU' >Mauritius</option>
        <option value='MX' >Mexico</option>
        <option value='MD' >Moldova</option>
        <option value='MC' >Monaco</option>
        <option value='MN' >Mongolia</option>
        <option value='MA' >Morocco</option>
        <option value='MZ' >Mozambique</option>
        <option value='NA' >Namibia</option>
        <option value='NR' >Nauru</option>
        <option value='NP' >Nepal</option>
        <option value='NL' >Netherlands</option>
        <option value='AN' >Netherlands Antilles</option>
        <option value='NZ' >New Zealand</option>
        <option value='NI' >Nicaragua</option>
        <option value='NE' >Niger</option>
        <option value='NG' >Nigeria</option>
        <option value='NK' >North Korea</option>
        <option value='NO' >Norway</option>
        <option value='OM' >Oman</option>
        <option value='PK' >Pakistan</option>
        <option value='PA' >Panama</option>
        <option value='PG' >Papua New Guinea</option>
        <option value='PY' >Paraguay</option>
        <option value='PE' >Peru</option>
        <option value='PH' >Philippines</option>
        <option value='PL' >Poland</option>
        <option value='PT' >Portugal</option>
        <option value='PR' >Puerto Rico</option>
        <option value='QA' >Qatar</option>
        <option value='RO' >Romania</option>
        <option value='RU' >Russia</option>
        <option value='RW' >Rwanda</option>
        <option value='KN' >Saint Kitts and Nevis</option>
        <option value='LC' >Saint Lucia</option>
        <option value='VC' >Saint Vincent/Grenadines</option>
        <option value='ST' >Sao Tome and Principe</option>
        <option value='SA' >Saudi Arabia</option>
        <option value='SN' >Senegal</option>
        <option value='SC' >Seychelles</option>
        <option value='SL' >Sierra Leone</option>
        <option value='SG' >Singapore</option>
        <option value='SK' >Slovakia</option>
        <option value='SI' >Slovenia</option>
        <option value='SB' >Solomon Islands</option>
        <option value='SO' >Somalia</option>
        <option value='ZA' >South Africa</option>
        <option value='KS' >South Korea</option>
        <option value='ES' >Spain</option>
        <option value='LK' >Sri Lanka</option>
        <option value='SD' >Sudan</option>
        <option value='SR' >Suriname</option>
        <option value='SE' >Sweden</option>
        <option value='CH' >Switzerland</option>
        <option value='SY' >Syria</option>
        <option value='TW' >Taiwan</option>
        <option value='TJ' >Tajikistan</option>
        <option value='TZ' >Tanzania</option>
        <option value='TH' >Thailand</option>
        <option value='TG' >Togo</option>
        <option value='TO' >Tonga</option>
        <option value='TT' >Trinidad and Tobago</option>
        <option value='TN' >Tunisia</option>
        <option value='TR' >Turkey</option>
        <option value='TM' >Turkmenistan</option>
        <option value='TV' >Tuvalu</option>
        <option value='UG' >Uganda</option>
        <option value='UA' >Ukraine</option>
        <option value='AE' >United Arab Emirates</option>
        <option value='GB' >United Kingdom</option>
        <option value='US' >United States Of America</option>
        <option value='UY' >Uruguay</option>
        <option value='UZ' >Uzbekistan</option>
        <option value='VU' >Vanuatu</option>
        <option value='VE' >Venezuela</option>
        <option value='VN' >Viet Nam</option>
        <option value='EH' >Western Samoa</option>
        <option value='YE' >Yemen</option>
        <option value='YU' >Yugoslavia</option>
        <option value='ZM' >Zambia</option>
        <option value='ZW' >Zimbabwe</option>
        </select>
        </td>
        ";
    }

    $out[body]=$out[body]."
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Icq Number</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='p_icq' value='$pinfo[icq]' size='30' maxlength='10'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Aim Name</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='p_aim' value='$pinfo[aim]' size='30' maxlength='16'></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>MSN</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='p_msn' value='$pinfo[msn]' size='30' maxlength='35'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Yahoo</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='p_yahoo' value='$pinfo[yahoo]' size='30' maxlength='35'></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Website Url</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='p_url' value='$pinfo[website]' size='30' maxlength='100'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Logo</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='p_logo' value='$pinfo[logo]' size='30' maxlength='100'></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Matches Won</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='p_won' value='$pinfo[matcheswon]' size='30' maxlength='10'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Matches Lost</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='p_loss' value='$pinfo[matcheslost]' size='30' maxlength='10'></td>
    </tr>
    ";
    $themes.="<option value='$pinfo[theme]'>$pinfo[theme]</option>";
    if($theme[option1]){
        $themes.="<option value='$theme[option1]'>$theme[option1]</option>";
    }

    if($theme[option2]){
        $themes.="<option value='$theme[option2]'>$theme[option2]</option>";
    }

    if($theme[option3]){
        $themes.="<option value='$theme[option3]'>$theme[option3]</option>";
    }

    if($theme[option4]){
        $themes.="<option value='$theme[option4]'>$theme[option4]</option>";
    }

    if($theme[option5]){
        $themes.="<option value='$theme[option5]'>$theme[option5]</option>";
    }

    $out[body]=$out[body]."
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Ladder Theme</font></td>
    <td width='50%' valign='center' align='center'>
    <select name='p_theme' value='$pinfo[theme]'>$themes</select></td>
    </tr>
    ";
    if($showip){
        $out[body]=$out[body]."
        <tr class='altcolorb'>
        <td width='50%' valign='center' align='left'>
        <font face='veradna,arial' size='2' color='#FFFFFF'>IP Address</font></td>
        <td width='50%' valign='center' align='center'>
        <font face='veradna,arial' size='2' color='#FFFFFF'>$pinfo[ipaddress]</font></td>
        </tr>";
    }

    if($misc[newsletter]=="yes"){
    if($pinfo[newsletter]=="1"){
            $chkd="CHECKED";
    }

        $out[body]=$out[body]."
        <tr class='altcolora'>
        <td width='100%' valign='center' align='left' colspan='2'>
        <font face='veradna,arial' size='2' color='#FFFFFF'>
        <input type='checkbox' name='p_newsl' value='1' $chkd> Receive the $site[shortname] newsletter</font></td>
        </tr>";
    }

    $detailedinfo=mysql_query("SELECT * FROM usersinfo WHERE id='$playerid'");
    $dinfo=mysql_fetch_array($detailedinfo);

    if($dinfo[firstname]){

		 $out[body]=$out[body]."

    		 <tr class='altcolor' cellpadding='2' cellspacing='2'>
    		 <td width='100%' valign='top' align='center' colspan='2'>
    		 <strong>Other Information</strong></td>
    		 </tr>

		 <tr class='altcolora'>
        	 <td width='50%' valign='center' align='left'>
        	 <font face='veradna,arial' size='2' color='#FFFFFF'>First Name</font></td>
        	 <td width='50%' valign='center' align='left'>
             <font face='veradna,arial' size='2' color='#FFFFFF'>$dinfo[firstname]</font></td>
             </tr>

		 <tr class='altcolorb'>
        	 <td width='50%' valign='center' align='left'>
        	 <font face='veradna,arial' size='2' color='#FFFFFF'>Middle Name</font></td>
        	 <td width='50%' valign='center' align='left'>
             <font face='veradna,arial' size='2' color='#FFFFFF'>$dinfo[middlename]</font></td>
             </tr>


	       <tr class='altcolora'>
        	 <td width='50%' valign='center' align='left'>
        	 <font face='veradna,arial' size='2' color='#FFFFFF'>Last Name</font></td>
        	 <td width='50%' valign='center' align='left'>
             <font face='veradna,arial' size='2' color='#FFFFFF'>$dinfo[lastname]</font></td>
             </tr>


	 	 <tr class='altcolorb'>
        	 <td width='50%' valign='center' align='left'>
        	 <font face='veradna,arial' size='2' color='#FFFFFF'>Address</font></td>
        	 <td width='50%' valign='center' align='left'>
             <font face='veradna,arial' size='2' color='#FFFFFF'>$dinfo[address]</font></td>
             </tr>


	 	 <tr class='altcolora'>
        	 <td width='50%' valign='center' align='left'>
        	 <font face='veradna,arial' size='2' color='#FFFFFF'>City</font></td>
        	 <td width='50%' valign='center' align='left'>
             <font face='veradna,arial' size='2' color='#FFFFFF'>$dinfo[city]</font></td>
             </tr>


	 	 <tr class='altcolorb'>
        	 <td width='50%' valign='center' align='left'>
        	 <font face='veradna,arial' size='2' color='#FFFFFF'>State</font></td>
        	 <td width='50%' valign='center' align='left'>
             <font face='veradna,arial' size='2' color='#FFFFFF'>$dinfo[state]</font></td>
             </tr>


	 	 <tr class='altcolora'>
        	 <td width='50%' valign='center' align='left'>
        	 <font face='veradna,arial' size='2' color='#FFFFFF'>Zipcode</font></td>
        	 <td width='50%' valign='center' align='left'>
             <font face='veradna,arial' size='2' color='#FFFFFF'>$dinfo[zipcode]</font></td>
             </tr>


	 	 <tr class='altcolorb'>
        	 <td width='50%' valign='center' align='left'>
        	 <font face='veradna,arial' size='2' color='#FFFFFF'>Phone</font></td>
        	 <td width='50%' valign='center' align='left'>
             <font face='veradna,arial' size='2' color='#FFFFFF'>$dinfo[phone]</font></td>
             </tr>


	 	 <tr class='altcolora'>
        	 <td width='50%' valign='center' align='left'>
        	 <font face='veradna,arial' size='2' color='#FFFFFF'>Birthday</font></td>
        	 <td width='50%' valign='center' align='left'>
             <font face='veradna,arial' size='2' color='#FFFFFF'>$dinfo[birthday]</font></td>
             </tr>

		 ";
		 switch($dinfo[occupation]){

		 case 01:
		 $occ = "K-12 student";
		 break;

		 case 02:
		 $occ = "Collge/Graduate Student";
		 break;

		 case 03:
		 $occ ="Sales/marketing";
		 break;

		 case 04:
		 $occ="Tradesman/craftsman";
		 break;

		 case 05:
		 $occ="Executive/managerial";
		 break;

		 case 06:
		 $occ="Professional (doctor, lawyer, etc.)";
		 break;

		 case 07:
		 $occ="Academic/educator";
		 break;

		 case 08:
		 $occ="Computer technical/engineering";
		 break;

		 case 09:
		 $occ="Other technical/engineering";
		 break;

		 case 10:
		 $occ="Service/customer support";
		 break;

		 case 11:
		 $occ="Clerical/administrative";
		 break;

		 case 12:
		 $occ="Homemaker";
		 break;

		 case 13:
		 $occ="Self-employed/own company";
		 break;

		 case 14:
		 $occ="Unemployed, looking for work";
		 break;

		 case 15:
		 $occ="Retired";
		 break;

		 default:
		 $occ="Other";
		 break;

 	}


		 $out[body]=$out[body]."
	 	 <tr class='altcolorb'>
        	 <td width='50%' valign='center' align='left'>
        	 <font face='veradna,arial' size='2' color='#FFFFFF'>Occupation</font></td>
        	 <td width='50%' valign='center' align='left'>
             <font face='veradna,arial' size='2' color='#FFFFFF'>$occ</font></td>
             </tr>



		 ";

    }


    $out[body]=$out[body]."
    </table>
    </td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='action' value='editplayerc'>
    <input type='hidden' name='playerid' value='$playerid'>
    <input type='submit' name='' value='Update Player'>
    <input type='reset' name='' value='Reset'></td>
    </form>
    </tr>
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function admin_editplayerc($playerid,$p_alias,$p_pass,$p_email,$p_icq,$p_aim,$p_msn,$p_yahoo,$p_url,$p_logo,$p_won,$p_loss,$p_points,$p_skill,$p_money,$p_newsl,$p_theme,$p_country){
    global $dir, $url, $out, $site, $admn;
    include("$dir[func]/checkdata.php");
    $p_alias=change_charecters($p_alias);
    $p_email=change_charecters($p_email);
    $p_pass=change_charecters($p_pass);
    $errormessage=check_validlength($errormessage, $p_alias, "3", "50", "Player names must be 3-25 characters");
    $errormessage=check_validlength($errormessage, $p_pass, "2", "15", "Player passwords must be 2-15 characters");
    error_check($errormessage);
    $errormessage=check_emailaddress($p_email);
    error_check($errormessage);
    $errormessage=check_unvalusersinfoexist($errormessage, $p_alias, $p_email, "x");
    $errormessage=check_usersinfoexistother($errormessage, $playerid, $p_alias, $p_email, "x");
    error_check($errormessage);
    $p_icq=change_numbersonly($p_icq);
    $p_aim=change_charecters($p_aim);
    $p_yahoo=change_charecters($p_yahoo);
    $p_url=change_url($p_url);
    $p_logo=change_url($p_logo);
    $p_url=change_charecters($p_url);
    $p_logo=change_charecters($p_logo);
    $p_email=change_charecters($p_email);
    $errormessage=check_ban($errormessage, $p_alias, $p_email, $ip);
    error_check($errormessage);
    mysql_query("UPDATE users SET
    alias='$p_alias',
    pass='$p_pass',
    email='$p_email',
    icq='$p_icq',
    aim='$p_aim',
    msn='$p_msn',
    yahoo='$p_yahoo',
    website='$p_url',
    logo='$p_logo',
    newsletter='$p_newsl',
    matcheswon='$p_won',
    matcheslost='$p_loss',
    points='$p_points',
    skill='$p_skill',
    money='$p_money',
    theme='$p_theme',
    country='$p_country'
    WHERE id='$playerid'");
    include("$dir[func]/admin_finishmessage.php");
    display_message("The player was updated.<br>");
}

?>
