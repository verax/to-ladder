<?
if(!IsSet($indexloaded)){
    header("Location: ./index.php");
}

//CHECK ADMIN ACCESS LEVEL
if($admn[access] < 25){
    include("$dir[func]/error.php");
    display_error("You are not allowed to perform this function.<br>");
}

function admin_reportmatch(){
    global $dir, $url, $out, $site, $admn;
    //GET LADDERS
    $ladderslist=mysql_query("SELECT id,laddername  FROM ladders ORDER by laddername");
    while(list($id,$name)=mysql_fetch_row($ladderslist)){
        $theladders=$theladders."<option value='$id'>$name</option>";
        $foundladders=1;
    }

    if(!$foundladders){
        include("$dir[func]/error.php");
        display_error("There is no ladders.<br>");
    }

    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <form method='post'>
    <td width='100%' valign='top' align='center'>
    <strong>Report Match</strong><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <br>
    Select a ladder<br>
    <select name='ladderid'>$theladders</select><br>
    <br></td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='action' value='reportmatchb'>
    <input type='submit' name='' value='Select Ladder'>
    </td>
    </form>
    </tr>
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function admin_reportmatchb($ladderid){
    global $dir, $url, $out, $site, $admn;
    if(!$ladderid){
        include("$dir[func]/error.php");
        display_error("Invalid Ladder ID.<br>");
    }

    if($admn[access] < 99){
        if (mysql_num_rows(mysql_query("SELECT staffid FROM staffaccess WHERE staffid='$admn[id]' AND ladderid='$ladderid'")) < 1){
            include("$dir[func]/error.php");
            display_error("You are not allowed to report matches on this laddder.<br>");
        }

    }

    //GET TEAMS
    $teamslist=mysql_query("SELECT id FROM ladder_$ladderid");
    while(list($id)=mysql_fetch_row($teamslist)){
        $name=mysql_fetch_array(mysql_query("SELECT teamname FROM teams WHERE id='$id'"));
        $theteams=$theteams."<option value='$id'>$name[teamname]</option>";
        $foundladders=1;
    }

    if(!$foundladders){
        include("$dir[func]/error.php");
        display_error("There is no teams on this ladder.<br>");
    }

    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <form method='post'>
    <td width='100%' valign='top' align='center'>
    <strong>Report Match</strong><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <br>
    Winning Team<br>
    <select name='wteamid'>$theteams</select><br>
    Losing Team<br>
    <select name='lteamid'>$theteams</select><br>
    <br></td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='action' value='reportmatchc'>
    <input type='hidden' name='ladderid' value='$ladderid'>
    <input type='submit' name='' value='Report Match'>
    </td>
    </form>
    </tr>
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function admin_reportmatchc($ladderid,$wteamid,$lteamid){
    global $dir, $url, $out, $site, $admn, $misc, $file;
    if(!$ladderid){
        include("$dir[func]/error.php");
        display_error("Invalid Ladder ID.<br>");
    }

    if((!$wteamid) || (!$lteamid)){
        include("$dir[func]/error.php");
        display_error("Invalid Team ID's.<br>");
    }

    if($wteamid=="$lteamid"){
        include("$dir[func]/error.php");
        display_error("The winner and loser are the same.<br>");
    }
    $ladderinfo=mysql_query("SELECT laddername,openplay FROM ladders WHERE id='$ladderid'");
    $ladder=mysql_fetch_array($ladderinfo);
    //GET WINNER STATS
    $winnerteam=mysql_query("SELECT teamname FROM teams WHERE id='$wteamid'");
    $winner=mysql_fetch_array($winnerteam);
    $winteaminfo=mysql_query("SELECT status,rank,games,percent FROM ladder_$ladderid WHERE id='$wteamid'");
    $wtinfo=mysql_fetch_array($winteaminfo);
    //GET LOSER STATS
    $loserteam=mysql_query("SELECT teamname FROM teams WHERE id='$lteamid'");
    $loser=mysql_fetch_array($loserteam);
    $losteaminfo=mysql_query("SELECT status,rank,games,percent FROM ladder_$ladderid WHERE id='$lteamid'");
    $ltinfo=mysql_fetch_array($losteaminfo);
    // GET SKILL INFO
    if($wtinfo[rank] > $ltinfo[rank]){
        $rankdifference=($wtinfo[rank] - $ltinfo[rank]);
    }else{

        $rankdifference="1";
    }

    if($ltinfo[percent]){
        $percentoratio=($wtinfo[percent] / $ltinfo[percent]);
    }else{

        $percentoratio="1";
    }

    //CHECK FOR CHALLENGE OR IF OPEN PLAY IS ALLOWED
    $thechallenge=mysql_query("SELECT * FROM challenges WHERE challenger='$wteamid' AND challenged='$lteamid' AND finalizedby > '0' OR challenger='$lteamid' AND challenged='$wteamid' AND finalizedby > '0'");
    $chall=mysql_fetch_array($thechallenge);
    if($chall[challid]){
        if($misc[challranking] < 2){

        	if($match[winnerid]=="$chall[challenger]"){
            		$wtempnewrank="$chall[challengedrank]";
        	}else{
            		$wtempnewrank=0;
        	}
	}
	else if($misc[challranking] > 1){

		if($match[winnerid]=="$chall[challenger]"){
            		$wtempnewrank=($chall[challengedrank] / 2 - 0.5);
        	}else{

            		$wtempnewrank=0;
        	}
	}
	else{
		$wtempnewrank=0;
	}
    }else{

       		$wtempnewrank=0;

    }

    if($chall[challid]){
        //DELETE CHALLENGE
        mysql_query("DELETE FROM challenges WHERE challid='$chall[challid]'");
    }

    //ADD TO UNVALIDATED MATCHES
    $tday=date("Y-m-d H:i:s");
    $mcode=md5(uniqid(microtime()));
    $mcode="$mcode";
    $ip=getenv("REMOTE_ADDR");
    mysql_query("INSERT INTO matchdb VALUES (
    '$mcode',
    '$wteamid',
    '$lteamid',
    '$winner[teamname]',
    '$loser[teamname]',
    '$ladderid',
    '$ladder[laddername]',
    '$wtinfo[rank]',
    '$ltinfo[rank]',
    '0',
    '0',
    '0',
    '0',
    '0',
    '0',
    '0',
    '0',
    '0',
    '0',
    '0',
    '$tday',
    '$ip',
    '0',
    '$tday',
    '$ip');");
    $getcode=mysql_query("SELECT matchid FROM matchdb WHERE winnerid='$wteamid' AND loserid='$lteamid'");
    $match=mysql_fetch_array($getcode);
    $matchcode="$match[matchid]";
    //GET WINNERS LADDER STATS
    $winnerladder=mysql_query("SELECT * FROM ladder_$ladderid WHERE id='$wteamid'");
    $wlinfo=mysql_fetch_array($winnerladder);
    //GET LOSERS LADDER STATS
    $loserladder=mysql_query("SELECT * FROM ladder_$ladderid WHERE id='$lteamid'");
    $llinfo=mysql_fetch_array($loserladder);
    //GET TOTAL RANKERS
    $totalranked=mysql_query("SELECT COUNT(*) FROM ladder_$ladderid WHERE rank > '0'");
    $totalranks=mysql_fetch_array($totalranked);
    $totalranks="$totalranks[0]";
    if(!$chall[challid]){
        //Winner moves half way up to losers position
        if($misc[ranking] < 2){
        	//UPDATE LADDER RANKS
        	//IF BOTH TEAMS ARE ALREADY RANKED
        	if(($wtinfo[rank] > 0) && ($ltinfo[rank] > 0)){
            		$wtempnewrank=($wtinfo[rank] + $ltinfo[rank]);
            		$wtempnewrank=($wtempnewrank / 2 - 0.5);
            		if($wtempnewrank > $totalranks){
                		$wtempnewrank="$totalranks";
            		}

        	}

        	else if($wtinfo[rank] < 1){
            		$wtempnewrank=($totalranks + 1);
        	}else{
            		$wtempnewrank="";
        	}

        //Winner takes losers position
        }else if($misc[ranking] > 1){

 		//UPDATE LADDER RANKS
        	//IF BOTH TEAMS ARE ALREADY RANKED
        	if(($wtinfo[rank] > 0) && ($ltinfo[rank] > 0)){
            		$wtempnewrank=$ltinfo[rank];         
        	}

        	else if($wtinfo[rank] < 1){
            		$wtempnewrank=($totalranks + 1);
        	}else{
            		$wtempnewrank="";
        	}

	}else{
		$wtempnewrank="";
	}

    }

    $wtempnewrank=round($wtempnewrank);
    include("$dir[func]/rankadjust.php");
    //IF RANK IS BETTER THAN CURRENT RANK UPDATE LADDER
    if(($wtempnewrank) && ($wtempnewrank > 0)){
        if(($wtempnewrank < $wlinfo[rank]) || ($wlinfo[rank] < 1)){
            update_teamranks($ladderid,$wteamid,$wtempnewrank);
            $updatedrank="$wtempnewrank";
        }

    }

    //UPDATE WINNERS LADDER STATS
    if($updatedrank){
        $newlastrank="$wlinfo[rank]";
        if(($updatedrank < $wlinfo[bestrank]) || ($wlinfo[bestrank] < 1)){
            $newbestrank="$updatedrank";
        }else{

            $newbestrank="$wlinfo[bestrank]";
        }

    }else{

        $newlastrank="$wlinfo[lastrank]";
        $newbestrank="$wlinfo[bestrank]";
    }

    $newwins=($wlinfo[wins] + 1);
    $newgames=($newwins + $wlinfo[losses]);
    $newpercent=round($newwins / $newgames * 100);
    $newstreak=($wlinfo[streak] + 1);
    if($newstreak < 1){
        $newstreak="1";
    }

    if($newstreak > $wlinfo[beststreak]){
        $newbeststreak="$newstreak";
    }else{

        $newbeststreak="$wlinfo[beststreak]";
    }

    if($llinfo[rank] > 0){
        $newpoints=($totalranks - $llinfo[rank]);
        $newpoints=round($newpoints / 10);
    }

    if($newpoints < 10){
        $newpoints="10";
    }

    $newskill=($wlinfo[skill] + $totalkills);
    $newpoints=($newpoints + $wlinfo[points]);
    mysql_query("UPDATE ladder_$ladderid SET
    lastmatch='$tday',
    lastplayed='$lteamid',
    statusdisplay='',
    lastrank='$newlastrank',
    bestrank='$newbestrank',
    wins='$newwins',
    games='$newgames',
    percent='$newpercent',
    streak='$newstreak',
    beststreak='$newbeststreak',
    points='$newpoints',
    skill='$newskill'
    WHERE id='$wteamid'");
    //UPDATE LOSERS LADDER STATS
    $newlastrank="$llinfo[rank]";
    if(($llinfo[rank] < $llinfo[bestrank]) || ($llinfo[bestrank]=="0")){
        $newbestrank="$llinfo[rank]";
    }else{

        $newbestrank="$llinfo[bestrank]";
    }

    if($llinfo[rank] > $llinfo[worstrank]){
        $newworstrank="$llinfo[rank]";
    }else{

        $newworstrank="$llinfo[worstrank]";
    }

    $newlosses=($llinfo[losses] + 1);
    $newgames=($newlosses + $llinfo[wins]);
    $newpercent=round($llinfo[wins] / $newgames * 100);
    $newstreak=($llinfo[streak] - 1);
    if($newstreak > 0){
        $newstreak="-1";
    }

    if($newstreak < $llinfo[worststreak]){
        $newworststreak="$newstreak";
    }else{

        $newworststreak="$llinfo[worststreak]";
    }

    $newskill=($llinfo[skill] + $match[skill]);
    mysql_query("UPDATE ladder_$ladderid SET
    lastmatch='$tday',
    lastplayed='$wteamid',
    statusdisplay='',
    lastrank='$newlastrank',
    bestrank='$newbestrank',
    worstrank='$newworstrank',
    losses='$newlosses',
    games='$newgames',
    percent='$newpercent',
    streak='$newstreak',
    worststreak='$newworststreak',
    skill='$newskill'
    WHERE id='$lteamid'");
    $ranksadjusted=rank_checkadjust($ladderid);
    $matchcode="$match[matchid]";
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2' valign='center'>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <strong>The loss has been reported</strong><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='left'>
    <br><ul><center>
    The winner has recieved the win and the ranks have been updated.<br>
    </ul></td></center>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <strong><a href='$url[base]/$file[ladder]?ladderid=$ladderid'>View the Ladder</a></strong><br>
    </td>
    </tr>
    $tablefoot
    </table>";
    include("$dir[curtheme]");
}

?>
