<?PHP
if(!IsSet($indexloaded)){
    header("Location: ./index.php");
}

if($admn[access] < 90){
    include("$dir[func]/error.php");
    display_error("You are not allowed to perform this function.<br>");
}

function admin_editladders(){
    global $dir, $url, $out, $site, $admn;
    $ladderslist=mysql_query("SELECT id,laddername  FROM ladders ORDER by laddername");
    while(list($id,$name)=mysql_fetch_row($ladderslist)){
        $theladders=$theladders."<option value='$id'>$name</option>";
    }

    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <form method='post'>
    <td width='100%' valign='top' align='center'>
    <strong>Manage Ladders</strong><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <br>
    Select a ladder<br>
    <select name='ladderid'>
    <option value=''>Select a ladder or create a new one</option>
    $theladders</select><br>
    <br></td>
    </tr>
    <tr class='altcolor'>
    <script language='javascript'>var confirmdelete='Are you 100% sure you want to DELETE this LADDER - This will delete all teams on this ladder and remove all members from those teams. All tournaments on this ladder will be deleted also.';</script>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='action' value='editladdersb'>
    <input type='submit' name='todo' value='Create Ladder'>
    <input type='submit' name='todo' value='Edit Ladder'>
    <input type='submit' name='todo' value='Delete Ladder' onClick='return confirm(confirmdelete);'>
    </td>
    </form>
    </tr>
    </table>";
    include("$dir[curtheme]");
}

function admin_editladdersb($ladderid,$todo){
    global $dir, $url, $out, $site, $admn;
    if((!$ladderid) && ($todo!="Create Ladder")){
        include("$dir[func]/error.php");
        display_error("Invalid Ladder ID.<br>");
    }

    if($todo=="Create Ladder"){
        create_ladder();
    }

    else if($todo=="Edit Ladder"){
        edit_ladder($ladderid);
    }

    else if($todo=="Delete Ladder"){
        delete_ladder($ladderid);
        include("$dir[func]/admin_finishmessage.php");
        display_message("The ladder was deleted.<br>");
    }

    else{
        include("$dir[func]/error.php");
        display_error("Unknown Command.<br>");
    }

}

function create_ladder(){
    global $dir, $url, $out, $site, $admn;
    $curgames=mysql_query("SELECT id,gamename FROM games ORDER by gamename");
    while(list($id, $name)=mysql_fetch_row($curgames)){
        $thegames=$thegames."<option value='$id'>$name</option>";
        $howmanyeditable=1;
    }

    if(!$howmanyeditable){
        include("$dir[func]/error.php");
        display_error("You must create games before ladders.<br>");
    }

    //TABLE ATERNATING COLORS
    $altcolora="#000033";
    $altcolorb="#000020";
    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    <table width='100%' border='1' bordercolor='#000000' cellspacing='1' cellpadding='4'>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <strong>Create Ladder</strong></td>
    </tr>
    <tr>
    <td width='100%' valign='top' align='left'>
    <form method='post'>
    <table width='100%' border='0' cellspacing='1' cellpadding='1' valign='top' align='center'>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    *Ladders Game Category</td>
    <td width='50%' valign='center' align='left'>
    <select name='ld_gameid'>
    $thegames
    </select></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    *Ladders Full Name [e.g. Rainbow Six 3 Teams]</td>
    <td width='50%' valign='center' align='left'>
    <input type='text' name='ld_name' value='$ladderinfo[laddername]' size='30' maxlength='30'></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    *Ladder Abbreviation [RST]</td>
    <td width='50%' valign='center' align='left'>
    <input type='text' name='ld_abrv' value='$ladderinfo[abbreviation]' size='30' maxlength='10'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='top' align='left'>
    Ladder Description<br>200 Characters max</td>
    <td width='50%' valign='center' align='left'><textarea name='ld_desc' rows='3' cols='25' maxlength='200'>$ladderinfo[description]</textarea></td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$ladderinfo[type];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    Ladder Type</td>
    <td width='50%' valign='center' align='left'>
    <select name='ld_type'>
    <option value='1' $selected[1]>Singles 1v1</option>
    <option value='3' $selected[3]>Teams/Clans</option>
    </select></td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$ladderinfo[openplay];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    Ladder Play</td>
    <td width='50%' valign='center' align='left'>
    <select name='ld_openreport'>
    <option value='1' $selected[1]>1: Challenges and Open Play - Teams/Players Unranked On Signup</option>
    <option value='0' $selected[0]>2: Challenges Only - Teams/Players Ranked On Signup</option>
    <option value='2' $selected[2]>3: Open Play Only - Teams/Players Unranked On Signup</option>
    </select></td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$ladderinfo[minmembers];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    Min/Max Team Members [Set both to One for 1v1 ladders]</td>
    <td width='50%' valign='center' align='left'>
    <select name='ld_plyrmin'>
    <option value='1' $selected[1]>Min 1</option>
    <option value='2' $selected[2]>2</option>
    <option value='3' $selected[3]>3</option>
    <option value='4' $selected[4]>4</option>
    <option value='5' $selected[5]>5</option>
    <option value='6' $selected[6]>6</option>
    <option value='7' $selected[7]>7</option>
    <option value='8' $selected[8]>8</option>
    <option value='9' $selected[9]>9</option>
    <option value='10' $selected[10]>10</option>
    <option value='11' $selected[11]>11</option>
    <option value='12' $selected[12]>12</option>
    <option value='13' $selected[13]>13</option>
    <option value='14' $selected[14]>14</option>
    <option value='15' $selected[15]>15</option>
    <option value='16' $selected[16]>16</option>
    <option value='17' $selected[17]>17</option>
    <option value='18' $selected[18]>18</option>
    <option value='19' $selected[19]>19</option>
    <option value='20' $selected[20]>20</option>
    <option value='21' $selected[21]>21</option>
    <option value='22' $selected[22]>22</option>
    <option value='23' $selected[23]>23</option>
    <option value='24' $selected[24]>24</option>
    <option value='25' $selected[25]>25</option>
    <option value='26' $selected[26]>26</option>
    <option value='27' $selected[27]>27</option>
    <option value='28' $selected[28]>28</option>
    <option value='29' $selected[29]>29</option>
    <option value='30' $selected[30]>30</option>
    <option value='31' $selected[31]>31</option>
    <option value='32' $selected[32]>32</option>
    <option value='33' $selected[33]>33</option>
    <option value='34' $selected[34]>34</option>
    <option value='35' $selected[35]>35</option>
    <option value='36' $selected[36]>36</option>
    <option value='37' $selected[37]>37</option>
    <option value='38' $selected[38]>38</option>
    <option value='39' $selected[39]>39</option>
    <option value='40' $selected[40]>40</option>
    <option value='41' $selected[41]>41</option>
    <option value='42' $selected[42]>42</option>
    <option value='43' $selected[43]>43</option>
    <option value='44' $selected[44]>44</option>
    <option value='45' $selected[45]>45</option>
    <option value='46' $selected[46]>46</option>
    <option value='47' $selected[47]>47</option>
    <option value='48' $selected[48]>48</option>
    <option value='49' $selected[49]>49</option>
    <option value='50' $selected[50]>50</option>
    <option value='60' $selected[60]>60</option>
    <option value='70' $selected[70]>70</option>
    <option value='80' $selected[80]>80</option>
    <option value='90' $selected[90]>90</option>
    <option value='100' $selected[100]>100</option>
    </select> /
    ";
    $selected="";
    $value="";
    $value=$ladderinfo[maxmembers];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <select name='ld_plyrmax'>
    <option value='1' $selected[1]>Max 1</option>
    <option value='2' $selected[2]>2</option>
    <option value='3' $selected[3]>3</option>
    <option value='4' $selected[4]>4</option>
    <option value='5' $selected[5]>5</option>
    <option value='6' $selected[6]>6</option>
    <option value='7' $selected[7]>7</option>
    <option value='8' $selected[8]>8</option>
    <option value='9' $selected[9]>9</option>
    <option value='10' $selected[10]>10</option>
    <option value='11' $selected[11]>11</option>
    <option value='12' $selected[12]>12</option>
    <option value='13' $selected[13]>13</option>
    <option value='14' $selected[14]>14</option>
    <option value='15' $selected[15]>15</option>
    <option value='16' $selected[16]>16</option>
    <option value='17' $selected[17]>17</option>
    <option value='18' $selected[18]>18</option>
    <option value='19' $selected[19]>19</option>
    <option value='20' $selected[20]>20</option>
    <option value='21' $selected[21]>21</option>
    <option value='22' $selected[22]>22</option>
    <option value='23' $selected[23]>23</option>
    <option value='24' $selected[24]>24</option>
    <option value='25' $selected[25]>25</option>
    <option value='26' $selected[26]>26</option>
    <option value='27' $selected[27]>27</option>
    <option value='28' $selected[28]>28</option>
    <option value='29' $selected[29]>29</option>
    <option value='30' $selected[30]>30</option>
    <option value='31' $selected[31]>31</option>
    <option value='32' $selected[32]>32</option>
    <option value='33' $selected[33]>33</option>
    <option value='34' $selected[34]>34</option>
    <option value='35' $selected[35]>35</option>
    <option value='36' $selected[36]>36</option>
    <option value='37' $selected[37]>37</option>
    <option value='38' $selected[38]>38</option>
    <option value='39' $selected[39]>39</option>
    <option value='40' $selected[40]>40</option>
    <option value='41' $selected[41]>41</option>
    <option value='42' $selected[42]>42</option>
    <option value='43' $selected[43]>43</option>
    <option value='44' $selected[44]>44</option>
    <option value='45' $selected[45]>45</option>
    <option value='46' $selected[46]>46</option>
    <option value='47' $selected[47]>47</option>
    <option value='48' $selected[48]>48</option>
    <option value='49' $selected[49]>49</option>
    <option value='50' $selected[50]>50</option>
    <option value='60' $selected[60]>60</option>
    <option value='70' $selected[70]>70</option>
    <option value='80' $selected[80]>80</option>
    <option value='90' $selected[90]>90</option>
    <option value='100' $selected[100]>100</option>
    </select></td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$ladderinfo[minmaps];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    Minimum Maps Played each match</td>
    <td width='50%' valign='center' align='left'>
    <select name='ld_minmaps'>
    <option value='0' $selected[1]>No Maps</option>
    <option value='1' $selected[1]>1</option>
    <option value='2' $selected[2]>2</option>
    <option value='3' $selected[3]>3</option>
    <option value='4' $selected[4]>4</option>
    <option value='5' $selected[5]>5</option>
    <option value='6' $selected[6]>6</option>
    <option value='7' $selected[7]>7</option>
    <option value='8' $selected[8]>8</option>
    <option value='9' $selected[9]>9</option>
    <option value='10' $selected[10]>10</option>
    </select></td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$ladderinfo[maxmaps];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    Maximum Maps Played each match</td>
    <td width='50%' valign='center' align='left'>
    <select name='ld_maxmaps'>
    <option value='0' $selected[0]>No Maps</option>
    <option value='1' $selected[1]>1</option>
    <option value='2' $selected[2]>2</option>
    <option value='3' $selected[3]>3</option>
    <option value='4' $selected[4]>4</option>
    <option value='5' $selected[5]>5</option>
    <option value='6' $selected[6]>6</option>
    <option value='7' $selected[7]>7</option>
    <option value='8' $selected[8]>8</option>
    <option value='9' $selected[9]>9</option>
    <option value='10' $selected[10]>10</option>
    </select></td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$ladderinfo[inactivedaysa];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    Inactive Days until drop [Repeats]</td>
    <td width='50%' valign='center' align='left'>
    <select name='ld_inactvdaysa'>
    <option value='500000' $selected[500000]>Disabled</option>
    <option value='1' $selected[1]>1 day</option>
    <option value='2' $selected[2]>2 days</option>
    <option value='3' $selected[3]>3</option>
    <option value='4' $selected[4]>4</option>
    <option value='5' $selected[5]>5</option>
    <option value='6' $selected[6]>6</option>
    <option value='7' $selected[7]>7 - 1 week</option>
    <option value='8' $selected[8]>8</option>
    <option value='9' $selected[9]>9</option>
    <option value='10' $selected[10]>10</option>
    <option value='11' $selected[11]>11</option>
    <option value='12' $selected[12]>12</option>
    <option value='13' $selected[13]>13</option>
    <option value='14' $selected[14]>14 - 2 weeks</option>
    <option value='15' $selected[15]>15</option>
    <option value='16' $selected[16]>16</option>
    <option value='17' $selected[17]>17</option>
    <option value='18' $selected[18]>18</option>
    <option value='19' $selected[19]>19</option>
    <option value='20' $selected[20]>20</option>
    <option value='21' $selected[21]>21 - 3 weeks</option>
    <option value='22' $selected[22]>22</option>
    <option value='23' $selected[23]>23</option>
    <option value='24' $selected[24]>24</option>
    <option value='25' $selected[25]>25</option>
    <option value='26' $selected[26]>26</option>
    <option value='27' $selected[27]>27</option>
    <option value='28' $selected[28]>28 - 4 weeks</option>
    <option value='29' $selected[29]>29</option>
    <option value='30' $selected[30]>30 days</option>
    </select></td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$ladderinfo[inactivedrop];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    Inactive drop rank amount [If drops enabled above]</td>
    <td width='50%' valign='center' align='left'>
    <select name='ld_inactvdrop'>
    <option value='1' $selected[1]>1 rank</option>
    <option value='2' $selected[2]>2 ranks</option>
    <option value='3' $selected[3]>3</option>
    <option value='4' $selected[4]>4</option>
    <option value='5' $selected[5]>5</option>
    <option value='6' $selected[6]>6</option>
    <option value='7' $selected[7]>7</option>
    <option value='8' $selected[8]>8</option>
    <option value='9' $selected[9]>9</option>
    <option value='10' $selected[10]>10</option>
    <option value='11' $selected[11]>11</option>
    <option value='12' $selected[12]>12</option>
    <option value='13' $selected[13]>13</option>
    <option value='14' $selected[14]>14</option>
    <option value='15' $selected[15]>15</option>
    <option value='16' $selected[16]>16</option>
    <option value='17' $selected[17]>17</option>
    <option value='18' $selected[18]>18</option>
    <option value='19' $selected[19]>19</option>
    <option value='20' $selected[20]>20</option>
    <option value='21' $selected[21]>21</option>
    <option value='22' $selected[22]>22</option>
    <option value='23' $selected[23]>23</option>
    <option value='24' $selected[24]>24</option>
    <option value='25' $selected[25]>25 ranks</option>
    </select></td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$ladderinfo[inactivedaysb];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    Inactive Days until teams deleted</td>
    <td width='50%' valign='center' align='left'>
    <select name='ld_inactvdaysb'>
    <option value='500000' $selected[500000]>Disabled</option>
    <option value='1' $selected[1]>1 day</option>
    <option value='2' $selected[2]>2 days</option>
    <option value='3' $selected[3]>3</option>
    <option value='4' $selected[4]>4</option>
    <option value='5' $selected[5]>5</option>
    <option value='6' $selected[6]>6</option>
    <option value='7' $selected[7]>7 - 1 week</option>
    <option value='8' $selected[8]>8</option>
    <option value='9' $selected[9]>9</option>
    <option value='10' $selected[10]>10</option>
    <option value='11' $selected[11]>11</option>
    <option value='12' $selected[12]>12</option>
    <option value='13' $selected[13]>13</option>
    <option value='14' $selected[14]>14 - 2 weeks</option>
    <option value='15' $selected[15]>15</option>
    <option value='16' $selected[16]>16</option>
    <option value='17' $selected[17]>17</option>
    <option value='18' $selected[18]>18</option>
    <option value='19' $selected[19]>19</option>
    <option value='20' $selected[20]>20</option>
    <option value='21' $selected[21]>21 - 3 weeks</option>
    <option value='22' $selected[22]>22</option>
    <option value='23' $selected[23]>23</option>
    <option value='24' $selected[24]>24</option>
    <option value='25' $selected[25]>25</option>
    <option value='26' $selected[26]>26</option>
    <option value='27' $selected[27]>27</option>
    <option value='28' $selected[28]>28 - 4 weeks</option>
    <option value='29' $selected[29]>29</option>
    <option value='30' $selected[30]>30 days</option>
    <option value='35' $selected[35]>35 - 5 weeks</option>
    <option value='42' $selected[42]>42 - 6 weeks</option>
    <option value='49' $selected[49]>49 - 7 weeks</option>
    <option value='56' $selected[56]>56 - 8 weeks</option>
    <option value='63' $selected[63]>63 - 9 weeks</option>
    </select></td>
    </tr>
    </table>
    </td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <strong>Challenging Information</strong></td>
    </tr>
    <tr>
    <td width='100%' valign='top' align='left'>
    <table width='100%' border='0' cellspacing='1' cellpadding='1' valign='top' align='center'>
    ";
    $selected="";
    $value="";
    $value=$ladderinfo[challenge];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    Allow Challenging</td>
    <td width='50%' valign='center' align='left'>
    <select name='ld_chal'>
    <option value='0' $selected[0]>No</option>
    <option value='1' $selected[1]>Yes</option>
    </select></td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$ladderinfo[challranks];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    Maximum Ranks Teams Can Challenge Above Their Rank</td>
    <td width='50%' valign='center' align='left'>
    <select name='ld_chalranks'>
    <option value='1000' $selected[1000]>Anyone</option>
    <option value='1' $selected[1]>1</option>
    <option value='2' $selected[2]>2</option>
    <option value='3' $selected[3]>3</option>
    <option value='4' $selected[4]>4</option>
    <option value='5' $selected[5]>5</option>
    <option value='6' $selected[6]>6</option>
    <option value='7' $selected[7]>7</option>
    <option value='8' $selected[8]>8</option>
    <option value='9' $selected[9]>9</option>
    <option value='10' $selected[10]>10</option>
    <option value='11' $selected[11]>11</option>
    <option value='12' $selected[12]>12</option>
    <option value='13' $selected[13]>13</option>
    <option value='14' $selected[14]>14</option>
    <option value='15' $selected[15]>15</option>
    <option value='16' $selected[16]>16</option>
    <option value='17' $selected[17]>17</option>
    <option value='18' $selected[18]>18</option>
    <option value='19' $selected[19]>19</option>
    <option value='20' $selected[20]>20</option>
    <option value='21' $selected[21]>21</option>
    <option value='22' $selected[22]>22</option>
    <option value='23' $selected[23]>23</option>
    <option value='24' $selected[24]>24</option>
    <option value='25' $selected[25]>25</option>
    <option value='26' $selected[26]>26</option>
    <option value='27' $selected[27]>27</option>
    <option value='28' $selected[28]>28</option>
    <option value='29' $selected[29]>29</option>
    <option value='30' $selected[30]>30</option>
    <option value='31' $selected[31]>31</option>
    <option value='32' $selected[32]>32</option>
    <option value='33' $selected[33]>33</option>
    <option value='34' $selected[34]>34</option>
    <option value='35' $selected[35]>35</option>
    <option value='36' $selected[36]>36</option>
    <option value='37' $selected[37]>37</option>
    <option value='38' $selected[38]>38</option>
    <option value='39' $selected[39]>39</option>
    <option value='40' $selected[40]>40</option>
    <option value='41' $selected[41]>41</option>
    <option value='42' $selected[42]>42</option>
    <option value='43' $selected[43]>43</option>
    <option value='44' $selected[44]>44</option>
    <option value='45' $selected[45]>45</option>
    <option value='46' $selected[46]>46</option>
    <option value='47' $selected[47]>47</option>
    <option value='48' $selected[48]>48</option>
    <option value='49' $selected[49]>49</option>
    <option value='50' $selected[50]>50</option>
    <option value='60' $selected[60]>60</option>
    <option value='70' $selected[70]>70</option>
    <option value='80' $selected[80]>80</option>
    <option value='90' $selected[90]>90</option>
    <option value='100' $selected[100]>100</option>
    </select></td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$ladderinfo[respondhours];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    Hours to respond to challenges [Before forfeit awarded]</td>
    <td width='50%' valign='center' align='left '>
    <select name='ld_chalrespnd'>
    <option value='1' $selected[1]>1 Hour</option>
    <option value='2' $selected[2]>2</option>
    <option value='3' $selected[3]>3</option>
    <option value='4' $selected[4]>4</option>
    <option value='5' $selected[5]>5</option>
    <option value='6' $selected[6]>6</option>
    <option value='7' $selected[7]>7</option>
    <option value='8' $selected[8]>8</option>
    <option value='9' $selected[9]>9</option>
    <option value='10' $selected[10]>10</option>
    <option value='11' $selected[11]>11</option>
    <option value='12' $selected[12]>12 - 1/2 day</option>
    <option value='13' $selected[13]>13</option>
    <option value='14' $selected[14]>14</option>
    <option value='15' $selected[15]>15</option>
    <option value='16' $selected[16]>16</option>
    <option value='17' $selected[17]>17</option>
    <option value='18' $selected[18]>18</option>
    <option value='19' $selected[19]>19</option>
    <option value='20' $selected[20]>20</option>
    <option value='21' $selected[21]>21</option>
    <option value='22' $selected[22]>22</option>
    <option value='23' $selected[23]>23</option>
    <option value='24' $selected[24]>24 - 1 day</option>
    <option value='25' $selected[25]>25</option>
    <option value='26' $selected[26]>26</option>
    <option value='27' $selected[27]>27</option>
    <option value='28' $selected[28]>28</option>
    <option value='29' $selected[29]>29</option>
    <option value='30' $selected[30]>30</option>
    <option value='31' $selected[31]>31</option>
    <option value='32' $selected[32]>32</option>
    <option value='33' $selected[33]>33</option>
    <option value='34' $selected[34]>34</option>
    <option value='35' $selected[35]>35</option>
    <option value='36' $selected[36]>36 - 1 1/2 day</option>
    <option value='37' $selected[37]>37</option>
    <option value='38' $selected[38]>38</option>
    <option value='39' $selected[39]>39</option>
    <option value='40' $selected[40]>40</option>
    <option value='41' $selected[41]>41</option>
    <option value='42' $selected[42]>42</option>
    <option value='43' $selected[43]>43</option>
    <option value='44' $selected[44]>44</option>
    <option value='45' $selected[45]>45</option>
    <option value='46' $selected[46]>46</option>
    <option value='47' $selected[47]>47</option>
    <option value='48' $selected[48]>48 - 2 days</option>
    <option value='60' $selected[60]>60 - 2 1/2 days</option>
    <option value='72' $selected[72]>72 - 3 days</option>
    <option value='84' $selected[84]>84 - 3 1/2 days</option>
    <option value='96' $selected[96]>96 - 4 days</option>
    <option value='108' $selected[108]>108 - 4 1/2 days</option>
    <option value='120' $selected[120]>120 - 5 days</option>
    <option value='144' $selected[144]>144 - 6 days</option>
    <option value='168' $selected[168]>168 - 7 days</option>
    <option value='500000' $selected[500000]>Anytime- Not Recommended</option>
    </select></td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$ladderinfo[playdays];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    Maximum Days to play match</td>
    <td width='50%' valign='center' align='left'>
    <select name='ld_chalplydays'>
    <option value='1' $selected[1]>1 day</option>
    <option value='2' $selected[2]>2 days</option>
    <option value='3' $selected[3]>3 days</option>
    <option value='4' $selected[4]>4 days</option>
    <option value='5' $selected[5]>5 days</option>
    <option value='6' $selected[6]>6 days</option>
    <option value='7' $selected[7]>7 days</option>
    <option value='8' $selected[8]>8 days</option>
    <option value='9' $selected[9]>9 days</option>
    <option value='10' $selected[10]>10 days</option>
    <option value='11' $selected[11]>11 days</option>
    <option value='12' $selected[12]>12 days</option>
    <option value='13' $selected[13]>13 days</option>
    <option value='14' $selected[14]>14 days</option>
    <option value='15' $selected[15]>15 days</option>
    <option value='16' $selected[16]>16 days</option>
    <option value='17' $selected[17]>17 days</option>
    <option value='18' $selected[18]>18 days</option>
    <option value='19' $selected[19]>19 days</option>
    <option value='20' $selected[20]>20 days</option>
    </select></td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$ladderinfo[timescangive];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    Amount of date/times challenged suggests</td>
    <td width='50%' valign='center' align='left'>
    <select name='ld_chaltimea'>
    <option value='1' $selected[1]>1</option>
    <option value='2' $selected[2]>2</option>
    <option value='3' $selected[3]>3</option>
    <option value='4' $selected[4]>4</option>
    <option value='5' $selected[5]>5</option>
    <option value='6' $selected[6]>6</option>
    <option value='7' $selected[7]>7</option>
    <option value='8' $selected[8]>8</option>
    <option value='9' $selected[9]>9</option>
    <option value='10' $selected[10]>10</option>
    </select></td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$ladderinfo[starthour];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    Earliest Match Start Time</td>
    <td width='50%' valign='center' align='left'>
    <select name='ld_chalmstart'>
    <option value='1' $selected[1]>1am</option>
    <option value='2' $selected[2]>2am</option>
    <option value='3' $selected[3]>3am</option>
    <option value='4' $selected[4]>4am</option>
    <option value='5' $selected[5]>5am</option>
    <option value='6' $selected[6]>6am</option>
    <option value='7' $selected[7]>7am</option>
    <option value='8' $selected[8]>8am</option>
    <option value='9' $selected[9]>9am</option>
    <option value='10' $selected[10]>10am</option>
    <option value='11' $selected[11]>11am</option>
    <option value='12' $selected[12]>Midday</option>
    <option value='13' $selected[13]>1pm</option>
    <option value='14' $selected[14]>2pm</option>
    <option value='15' $selected[15]>3pm</option>
    <option value='16' $selected[16]>4pm</option>
    <option value='17' $selected[17]>5pm</option>
    <option value='18' $selected[18]>6pm</option>
    <option value='19' $selected[19]>7pm</option>
    <option value='20' $selected[20]>8pm</option>
    <option value='21' $selected[21]>9pm</option>
    <option value='22' $selected[22]>10pm</option>
    <option value='23' $selected[23]>11pm</option>
    <option value='24' $selected[24]>Midnight</option>
    </select></td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$ladderinfo[endhour];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    Latest Match Start Time</td>
    <td width='50%' valign='center' align='left'>
    <select name='ld_chalmend'>
    <option value='1' $selected[1]>1am</option>
    <option value='2' $selected[2]>2am</option>
    <option value='3' $selected[3]>3am</option>
    <option value='4' $selected[4]>4am</option>
    <option value='5' $selected[5]>5am</option>
    <option value='6' $selected[6]>6am</option>
    <option value='7' $selected[7]>7am</option>
    <option value='8' $selected[8]>8am</option>
    <option value='9' $selected[9]>9am</option>
    <option value='10' $selected[10]>10am</option>
    <option value='11' $selected[11]>11am</option>
    <option value='12' $selected[12]>Midday</option>
    <option value='13' $selected[13]>1pm</option>
    <option value='14' $selected[14]>2pm</option>
    <option value='15' $selected[15]>3pm</option>
    <option value='16' $selected[16]>4pm</option>
    <option value='17' $selected[17]>5pm</option>
    <option value='18' $selected[18]>6pm</option>
    <option value='19' $selected[19]>7pm</option>
    <option value='20' $selected[20]>8pm</option>
    <option value='21' $selected[21]>9pm</option>
    <option value='22' $selected[22]>10pm</option>
    <option value='23' $selected[23]>11pm</option>
    <option value='24' $selected[24]>Midnight</option>
    </select></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    Displayed Timezone</td>
    <td width='50%' valign='center' align='left'>
    <input type='text' name='ld_chalmtzone' value='$ladderinfo[timezone]' size='30' maxlength='25'></td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$ladderinfo[mapscangive];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    Amount of maps challenged suggests [If maps enabled above]</td>
    <td width='50%' valign='center' align='left'>
    <select name='ld_chalmapsa'>
    <option value='1' $selected[1]>1</option>
    <option value='2' $selected[2]>2</option>
    <option value='3' $selected[3]>3</option>
    <option value='4' $selected[4]>4</option>
    <option value='5' $selected[5]>5</option>
    <option value='6' $selected[6]>6</option>
    <option value='7' $selected[7]>7</option>
    <option value='8' $selected[8]>8</option>
    <option value='9' $selected[9]>9</option>
    <option value='10' $selected[10]>10</option>
    </select></td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$ladderinfo[finalizehours];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    Hours to finalize before forfeit awarded<br></td>
    <td width='50%' valign='center' align='left'>
    <select name='ld_chalfinal'>
    <option value='1' $selected[1]>1 Hour</option>
    <option value='2' $selected[2]>2</option>
    <option value='3' $selected[3]>3</option>
    <option value='4' $selected[4]>4</option>
    <option value='5' $selected[5]>5</option>
    <option value='6' $selected[6]>6</option>
    <option value='7' $selected[7]>7</option>
    <option value='8' $selected[8]>8</option>
    <option value='9' $selected[9]>9</option>
    <option value='10' $selected[10]>10</option>
    <option value='11' $selected[11]>11</option>
    <option value='12' $selected[12]>12 - 1/2 day</option>
    <option value='13' $selected[13]>13</option>
    <option value='14' $selected[14]>14</option>
    <option value='15' $selected[15]>15</option>
    <option value='16' $selected[16]>16</option>
    <option value='17' $selected[17]>17</option>
    <option value='18' $selected[18]>18</option>
    <option value='19' $selected[19]>19</option>
    <option value='20' $selected[20]>20</option>
    <option value='21' $selected[21]>21</option>
    <option value='22' $selected[22]>22</option>
    <option value='23' $selected[23]>23</option>
    <option value='24' $selected[24]>24 - 1 day</option>
    <option value='25' $selected[25]>25</option>
    <option value='26' $selected[26]>26</option>
    <option value='27' $selected[27]>27</option>
    <option value='28' $selected[28]>28</option>
    <option value='29' $selected[29]>29</option>
    <option value='30' $selected[30]>30</option>
    <option value='31' $selected[31]>31</option>
    <option value='32' $selected[32]>32</option>
    <option value='33' $selected[33]>33</option>
    <option value='34' $selected[34]>34</option>
    <option value='35' $selected[35]>35</option>
    <option value='36' $selected[36]>36 - 1 1/2 day</option>
    <option value='37' $selected[37]>37</option>
    <option value='38' $selected[38]>38</option>
    <option value='39' $selected[39]>39</option>
    <option value='40' $selected[40]>40</option>
    <option value='41' $selected[41]>41</option>
    <option value='42' $selected[42]>42</option>
    <option value='43' $selected[43]>43</option>
    <option value='44' $selected[44]>44</option>
    <option value='45' $selected[45]>45</option>
    <option value='46' $selected[46]>46</option>
    <option value='47' $selected[47]>47</option>
    <option value='48' $selected[48]>48 - 2 days</option>
    <option value='60' $selected[60]>60 - 2 1/2 days</option>
    <option value='72' $selected[72]>72 - 3 days</option>
    <option value='84' $selected[84]>84 - 3 1/2 days</option>
    <option value='96' $selected[96]>96 - 4 days</option>
    <option value='108' $selected[108]>108 - 4 1/2 days</option>
    <option value='120' $selected[120]>120 - 5 days</option>
    <option value='144' $selected[144]>144 - 6 days</option>
    <option value='168' $selected[168]>168 - 7 days</option>
    <option value='500000' $selected[500000]>Anytime- Not Recommended</option>
    </select></td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$ladderinfo[mapscanaccept];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    Amount of maps challenger chooses [If maps enabled above]</td>
    <td width='50%' valign='center' align='left'>
    <select name='ld_chalmapsb'>
    <option value='1' $selected[1]>1</option>
    <option value='2' $selected[2]>2</option>
    <option value='3' $selected[3]>3</option>
    <option value='4' $selected[4]>4</option>
    <option value='5' $selected[5]>5</option>
    <option value='6' $selected[6]>6</option>
    <option value='7' $selected[7]>7</option>
    <option value='8' $selected[8]>8</option>
    <option value='9' $selected[9]>9</option>
    <option value='10' $selected[10]>10</option>
    </select></td>
    </tr>
    </table>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='top' align='left'>
    <table width='100%' border='0' cellspacing='0' cellpadding='0' valign='top' align='center'>
    </table>
    </td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='action' value='editladdersc'>
    <input type='submit' name='' value='Create the Ladder'></td>
    </form>
    </tr>
    </table>";
    include("$dir[curtheme]");
}

function admin_editladdersc($ld_gameid,$ld_name,$ld_abrv,$ld_desc,$ld_type,$ld_openreport,$ld_plyrmin,$ld_plyrmax,$ld_minmaps,$ld_maxmaps,$ld_inactvdaysa,$ld_inactvdrop,$ld_inactvdaysb,$ld_chal,$ld_chalranks,$ld_chalrespnd,$ld_chalplydays,$ld_chaltimea,$ld_chalmstart,$ld_chalmend,$ld_chalmtzone,$ld_chalmapsa,$ld_chalfinal,$ld_chalmapsb,$ld_skill){
    global $dir, $url, $out, $site, $admn;
    include("$dir[func]/checkdata.php");
    $ld_gameid=change_numbersonly($ld_gameid);
    if (mysql_num_rows(mysql_query("select gamename from games where id='$ld_gameid'")) < 1){
        $errormessage=$errormessage."That game is invalid.<br>";
        error_check($errormessage);
    }

    if ((($ld_plyrmin > 1)||($ld_plyrmax > 1))&&($ld_type < 2)){
        $errormessage=$errormessage."Minimum and Maximum players must be set to one for 1v1 ladders.<br>";
        error_check($errormessage);
    }

    if ($ld_plyrmin > $ld_plyrmax){
        $errormessage=$errormessage."Maximum players must be equal to or greater than minimum players.<br>";
        error_check($errormessage);
    }

    if (($ld_minmaps < 1)&&($ld_maxmaps > 0)){
        $errormessage=$errormessage."Maps must be set to 'No Maps' for both fields.<br>";
        error_check($errormessage);
    }

    if ($ld_minmaps > $ld_maxmaps){
        $errormessage=$errormessage."Maximum maps must be equal to or greater than minimum maps.<br>";
        error_check($errormessage);
    }

    if (($ld_openreport < 1)&&($ld_chal < 1)){
        $errormessage=$errormessage."Challenges must be set to YES on 'Challenge Only' ladders..<br>";
        error_check($errormessage);
    }

    if (($ld_openreport > 0)&&($ld_openreport < 2)){
        if($ld_chal < 1) {
            $errormessage=$errormessage."Challenges must be set to yes on 'Challenges and Open Play' ladders.<br>";
            error_check($errormessage);
        }

    }

    if ($ld_openreport >1){
        if($ld_chal > 0) {
            $errormessage=$errormessage."Challenges must be set to NO on 'Open Play Only' ladders.<br>";
            error_check($errormessage);
        }else{

            $ld_openreport = 1;
        }

    }

    if (($ld_chalmend <= $ld_chalmstart)&&($ld_chal)){
        $errormessage=$errormessage."The latest match start time must come after <br>the earliest match start time in the list.<br><br>For 24 hour challenging, set start time to 1am and end time to Midnight.<br>";
        error_check($errormessage);
    }

    if (($ld_chalmapsa < $ld_chalmapsb)&&($ld_chal)&&($ld_maxmaps >0)){
        $errormessage=$errormessage."Number of maps suggested must exceed or equal number of maps to be chosen by challenger when finalizing.<br>";
        error_check($errormessage);
    }

    $ld_name=change_charecters($ld_name);
    $ld_abrv=change_charecters($ld_abrv);
    $ld_desc=change_charecters($ld_desc);
    $ld_type=change_numbersonly($ld_type);
    $ld_openreport=change_numbersonly($ld_openreport);
    $ld_plyrmin=change_numbersonly($ld_plyrmin);
    $ld_plyrmax=change_numbersonly($ld_plyrmax);
    $ld_minmaps=change_numbersonly($ld_minmaps);
    $ld_maxmaps=change_numbersonly($ld_maxmaps);
    $ld_inactvdaysa=change_numbersonly($ld_inactvdaysa);
    $ld_inactvdrop=change_numbersonly($ld_inactvdrop);
    $ld_inactvdaysb=change_numbersonly($ld_inactvdaysb);
    $ld_chal=change_numbersonly($ld_chal);
    $ld_chalranks=change_numbersonly($ld_chalranks);
    $ld_chalrespnd=change_numbersonly($ld_chalrespnd);
    $ld_chalplydays=change_numbersonly($ld_chalplydays);
    $ld_chaltimea=change_numbersonly($ld_chaltimea);
    $ld_chalmstart=change_numbersonly($ld_chalmstart);
    $ld_chalmend=change_numbersonly($ld_chalmend);
    $ld_chalmtzone=change_charecters($ld_chalmtzone);
    $ld_chalmapsa=change_numbersonly($ld_chalmapsa);
    $ld_chalfinal=change_numbersonly($ld_chalfinal);
    $ld_chalmapsb=change_numbersonly($ld_chalmapsb);
    $ld_skill=change_numbersonly($ld_skill);
    $errormessage=check_validlength($errormessage, $ld_name, "2", "50", "Your Ladders Name Must be 2-25 Characters Long.");
    $errormessage=check_validlength($errormessage, $ld_abrv, "2", "25", "Your Ladders Abbreviation Must be 2-10 Characters Long.");
    $errormessage=check_validlength($errormessage, $ld_desc, "0", "250", "Your Ladders Description Must not Exceed 200 Characters.");
    $errormessage=check_validlength($errormessage, $ld_type, "1", "2", "Your Ladder Type is Invalid.");
    error_check($errormessage);
    //CHECK IF LADDER/ABBREVIATION EXIST HERE
    $errormessage=check_ladderexist($errormessage, $ld_name, $ld_abrv);
    error_check($errormessage);
    $tday=date("Y-m-d H:i:s");
    mysql_query("INSERT INTO ladders VALUES (NULL,
    '$ld_gameid',
    '$ld_name',
    '$ld_abrv',
    '$ld_type',
    '$ld_desc',
    '$ld_openreport',
    '$ld_plyrmin',
    '$ld_plyrmax',
    '$ld_minmaps',
    '$ld_maxmaps',
    '$ld_inactvdaysa',
    '$ld_inactvdrop',
    '$ld_inactvdaysb',
    '$ld_chal',
    '$ld_chalranks',
    '$ld_chalrespnd',
    '$ld_chalplydays',
    '$ld_chaltimea',
    '$ld_chalmstart',
    '$ld_chalmend',
    '$ld_chalmtzone',
    '$ld_chalmapsa',
    '$ld_chalfinal',
    '$ld_chalmapsb',
    '$ld_skill',
    '$admn[id]',
    '$tday',
    '0',
    '0');");
    $getid=mysql_query("SELECT id FROM ladders WHERE laddername='$ld_name'");
    $ldid=mysql_fetch_array($getid);
    if($ldid[id]){
        //DROP LADDER IF EXISTS
        mysql_query("DROP TABLE IF EXISTS ladder_$ldid[id]");
        //CREATE LADDER TABLE
        mysql_query("CREATE TABLE ladder_$ldid[id] (
        id int(10) DEFAULT '0' NOT NULL,
        joindate datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
        lastmatch datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
        lastplayed int(10) DEFAULT '0' NOT NULL,
        status int(10) DEFAULT '0' NOT NULL,
        statusdate datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
        statusdisplay varchar(250) NOT NULL,
        rank int(10) DEFAULT '0' NOT NULL,
        lastrank int(10) DEFAULT '0' NOT NULL,
        bestrank int(10) DEFAULT '0' NOT NULL,
        worstrank int(10) DEFAULT '0' NOT NULL,
        wins int(10) DEFAULT '0' NOT NULL,
        losses int(10) DEFAULT '0' NOT NULL,
        games int(10) DEFAULT '0' NOT NULL,
        percent int(10) DEFAULT '0' NOT NULL,
        streak int(10) DEFAULT '0' NOT NULL,
        beststreak int(10) DEFAULT '0' NOT NULL,
        worststreak int(10) DEFAULT '0' NOT NULL,
        points int(10) DEFAULT '0' NOT NULL,
        skill int(10) DEFAULT '0' NOT NULL,
        money int(10) DEFAULT '0' NOT NULL,
        forfeits int(10) DEFAULT '0' NOT NULL,
        icon tinyint(2) DEFAULT '0' NOT NULL,
        PRIMARY KEY (id)
        )");
    }

    include("$dir[func]/admin_finishmessage.php");
    display_message("The ladder has been added.<br>");
}

function edit_ladder($ladderid){
    global $dir, $url, $out, $admn, $site;
    $thisladder=mysql_query("SELECT * FROM ladders WHERE id='$ladderid'");
    $ladderinfo=mysql_fetch_array($thisladder);
    if((!$ladderid) || (!$ladderinfo[id])){
        include("$dir[func]/error.php");
        display_error("Invalid Ladder ID");
    }

    $curgames=mysql_query("SELECT id,gamename FROM games ORDER by gamename");
    while(list($id, $name)=mysql_fetch_row($curgames)){
        if($ladderinfo[gameon]=="$id"){
            $thegames=$thegames."<option value='$id' SELECTED>$name</option>";
        }else{

            $thegames=$thegames."<option value='$id'>$name</option>";
        }

    }

    //TABLE ATERNATING COLORS
    $altcolora="#000033";
    $altcolorb="#000020";
    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    <table width='100%' border='1' bordercolor='#000000' cellspacing='4' cellpadding='2'>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <strong>Edit Ladder</strong></td>
    </tr>
    <tr>
    <td width='100%' valign='top' align='left'>
    <form method='post'>
    <table width='100%' border='0' cellspacing='1' cellpadding='1' valign='top' align='center'>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    Ladders Game Category</td>
    <td width='50%' valign='center' align='left'>
    <select name='ld_gameid'>
    $thegames
    </select></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    Ladders Full Name [e.g. Rainbow Six 3 Teams]</td>
    <td width='50%' valign='center' align='left'>
    <input type='text' name='ld_name' value='$ladderinfo[laddername]' size='30' maxlength='30'></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    Ladder Abbreviation [e.g RST]</td>
    <td width='50%' valign='center' align='left'>
    <input type='text' name='ld_abrv' value='$ladderinfo[abbreviation]' size='30' maxlength='10'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='top' align='left'>
    Ladder Description<br>200 Characters max</td>
    <td width='50%' valign='center' align='left'><textarea name='ld_desc' rows='3' cols='25' maxlength='200'>
    $ladderinfo[description]</textarea></td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$ladderinfo[openplay];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    Ladder Play</td>
    <td width='50%' valign='center' align='left'>
    <select name='ld_openreport'>
    <option value='1' $selected[1]>1: Challenges and Open Play - Teams/Players Unranked On Signup</option>
    <option value='0' $selected[0]>2: Challenges Only - Teams/Players Ranked On Signup</option>
    <option value='2' $selected[2]>3: Open Play Only - Teams/Players Unranked On Signup</option>
    </select></td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$ladderinfo[minmembers];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    Min/Max Team Members [Set both to One for 1v1 ladders]</td>
    <td width='50%' valign='center' align='left'>
    <select name='ld_plyrmin'>
    <option value='1' $selected[1]>Min 1</option>
    <option value='2' $selected[2]>2</option>
    <option value='3' $selected[3]>3</option>
    <option value='4' $selected[4]>4</option>
    <option value='5' $selected[5]>5</option>
    <option value='6' $selected[6]>6</option>
    <option value='7' $selected[7]>7</option>
    <option value='8' $selected[8]>8</option>
    <option value='9' $selected[9]>9</option>
    <option value='10' $selected[10]>10</option>
    <option value='11' $selected[11]>11</option>
    <option value='12' $selected[12]>12</option>
    <option value='13' $selected[13]>13</option>
    <option value='14' $selected[14]>14</option>
    <option value='15' $selected[15]>15</option>
    <option value='16' $selected[16]>16</option>
    <option value='17' $selected[17]>17</option>
    <option value='18' $selected[18]>18</option>
    <option value='19' $selected[19]>19</option>
    <option value='20' $selected[20]>20</option>
    <option value='21' $selected[21]>21</option>
    <option value='22' $selected[22]>22</option>
    <option value='23' $selected[23]>23</option>
    <option value='24' $selected[24]>24</option>
    <option value='25' $selected[25]>25</option>
    <option value='26' $selected[26]>26</option>
    <option value='27' $selected[27]>27</option>
    <option value='28' $selected[28]>28</option>
    <option value='29' $selected[29]>29</option>
    <option value='30' $selected[30]>30</option>
    <option value='31' $selected[31]>31</option>
    <option value='32' $selected[32]>32</option>
    <option value='33' $selected[33]>33</option>
    <option value='34' $selected[34]>34</option>
    <option value='35' $selected[35]>35</option>
    <option value='36' $selected[36]>36</option>
    <option value='37' $selected[37]>37</option>
    <option value='38' $selected[38]>38</option>
    <option value='39' $selected[39]>39</option>
    <option value='40' $selected[40]>40</option>
    <option value='41' $selected[41]>41</option>
    <option value='42' $selected[42]>42</option>
    <option value='43' $selected[43]>43</option>
    <option value='44' $selected[44]>44</option>
    <option value='45' $selected[45]>45</option>
    <option value='46' $selected[46]>46</option>
    <option value='47' $selected[47]>47</option>
    <option value='48' $selected[48]>48</option>
    <option value='49' $selected[49]>49</option>
    <option value='50' $selected[50]>50</option>
    <option value='60' $selected[60]>60</option>
    <option value='70' $selected[70]>70</option>
    <option value='80' $selected[80]>80</option>
    <option value='90' $selected[90]>90</option>
    <option value='100' $selected[100]>100</option>
    </select> /
    ";
    $selected="";
    $value="";
    $value=$ladderinfo[maxmembers];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <select name='ld_plyrmax'>
    <option value='1' $selected[1]>Max 1</option>
    <option value='2' $selected[2]>2</option>
    <option value='3' $selected[3]>3</option>
    <option value='4' $selected[4]>4</option>
    <option value='5' $selected[5]>5</option>
    <option value='6' $selected[6]>6</option>
    <option value='7' $selected[7]>7</option>
    <option value='8' $selected[8]>8</option>
    <option value='9' $selected[9]>9</option>
    <option value='10' $selected[10]>10</option>
    <option value='11' $selected[11]>11</option>
    <option value='12' $selected[12]>12</option>
    <option value='13' $selected[13]>13</option>
    <option value='14' $selected[14]>14</option>
    <option value='15' $selected[15]>15</option>
    <option value='16' $selected[16]>16</option>
    <option value='17' $selected[17]>17</option>
    <option value='18' $selected[18]>18</option>
    <option value='19' $selected[19]>19</option>
    <option value='20' $selected[20]>20</option>
    <option value='21' $selected[21]>21</option>
    <option value='22' $selected[22]>22</option>
    <option value='23' $selected[23]>23</option>
    <option value='24' $selected[24]>24</option>
    <option value='25' $selected[25]>25</option>
    <option value='26' $selected[26]>26</option>
    <option value='27' $selected[27]>27</option>
    <option value='28' $selected[28]>28</option>
    <option value='29' $selected[29]>29</option>
    <option value='30' $selected[30]>30</option>
    <option value='31' $selected[31]>31</option>
    <option value='32' $selected[32]>32</option>
    <option value='33' $selected[33]>33</option>
    <option value='34' $selected[34]>34</option>
    <option value='35' $selected[35]>35</option>
    <option value='36' $selected[36]>36</option>
    <option value='37' $selected[37]>37</option>
    <option value='38' $selected[38]>38</option>
    <option value='39' $selected[39]>39</option>
    <option value='40' $selected[40]>40</option>
    <option value='41' $selected[41]>41</option>
    <option value='42' $selected[42]>42</option>
    <option value='43' $selected[43]>43</option>
    <option value='44' $selected[44]>44</option>
    <option value='45' $selected[45]>45</option>
    <option value='46' $selected[46]>46</option>
    <option value='47' $selected[47]>47</option>
    <option value='48' $selected[48]>48</option>
    <option value='49' $selected[49]>49</option>
    <option value='50' $selected[50]>50</option>
    <option value='60' $selected[60]>60</option>
    <option value='70' $selected[70]>70</option>
    <option value='80' $selected[80]>80</option>
    <option value='90' $selected[90]>90</option>
    <option value='100' $selected[100]>100</option>
    </select></td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$ladderinfo[minmaps];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    Minimum Maps Played each match</td>
    <td width='50%' valign='center' align='left'>
    <select name='ld_minmaps'>
    <option value='0' $selected[1]>No Maps</option>
    <option value='1' $selected[1]>1</option>
    <option value='2' $selected[2]>2</option>
    <option value='3' $selected[3]>3</option>
    <option value='4' $selected[4]>4</option>
    <option value='5' $selected[5]>5</option>
    <option value='6' $selected[6]>6</option>
    <option value='7' $selected[7]>7</option>
    <option value='8' $selected[8]>8</option>
    <option value='9' $selected[9]>9</option>
    <option value='10' $selected[10]>10</option>
    </select></td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$ladderinfo[maxmaps];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    Maximum Maps Played each match</td>
    <td width='50%' valign='center' align='left'>
    <select name='ld_maxmaps'>
    <option value='0' $selected[0]>No Maps</option>
    <option value='1' $selected[1]>1</option>
    <option value='2' $selected[2]>2</option>
    <option value='3' $selected[3]>3</option>
    <option value='4' $selected[4]>4</option>
    <option value='5' $selected[5]>5</option>
    <option value='6' $selected[6]>6</option>
    <option value='7' $selected[7]>7</option>
    <option value='8' $selected[8]>8</option>
    <option value='9' $selected[9]>9</option>
    <option value='10' $selected[10]>10</option>
    </select></td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$ladderinfo[inactivedaysa];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    Inactive Days until drop [Repeats]</td>
    <td width='50%' valign='center' align='left'>
    <select name='ld_inactvdaysa'>
    <option value='500000' $selected[500000]>Disabled</option>
    <option value='1' $selected[1]>1 day</option>
    <option value='2' $selected[2]>2 days</option>
    <option value='3' $selected[3]>3</option>
    <option value='4' $selected[4]>4</option>
    <option value='5' $selected[5]>5</option>
    <option value='6' $selected[6]>6</option>
    <option value='7' $selected[7]>7 - 1 week</option>
    <option value='8' $selected[8]>8</option>
    <option value='9' $selected[9]>9</option>
    <option value='10' $selected[10]>10</option>
    <option value='11' $selected[11]>11</option>
    <option value='12' $selected[12]>12</option>
    <option value='13' $selected[13]>13</option>
    <option value='14' $selected[14]>14 - 2 weeks</option>
    <option value='15' $selected[15]>15</option>
    <option value='16' $selected[16]>16</option>
    <option value='17' $selected[17]>17</option>
    <option value='18' $selected[18]>18</option>
    <option value='19' $selected[19]>19</option>
    <option value='20' $selected[20]>20</option>
    <option value='21' $selected[21]>21 - 3 weeks</option>
    <option value='22' $selected[22]>22</option>
    <option value='23' $selected[23]>23</option>
    <option value='24' $selected[24]>24</option>
    <option value='25' $selected[25]>25</option>
    <option value='26' $selected[26]>26</option>
    <option value='27' $selected[27]>27</option>
    <option value='28' $selected[28]>28 - 4 weeks</option>
    <option value='29' $selected[29]>29</option>
    <option value='30' $selected[30]>30 days</option>
    </select></td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$ladderinfo[inactivedrop];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    Inactive drop rank amount [If drops enabled above]</td>
    <td width='50%' valign='center' align='left'>
    <select name='ld_inactvdrop'>
    <option value='1' $selected[1]>1 rank</option>
    <option value='2' $selected[2]>2 ranks</option>
    <option value='3' $selected[3]>3</option>
    <option value='4' $selected[4]>4</option>
    <option value='5' $selected[5]>5</option>
    <option value='6' $selected[6]>6</option>
    <option value='7' $selected[7]>7</option>
    <option value='8' $selected[8]>8</option>
    <option value='9' $selected[9]>9</option>
    <option value='10' $selected[10]>10</option>
    <option value='11' $selected[11]>11</option>
    <option value='12' $selected[12]>12</option>
    <option value='13' $selected[13]>13</option>
    <option value='14' $selected[14]>14</option>
    <option value='15' $selected[15]>15</option>
    <option value='16' $selected[16]>16</option>
    <option value='17' $selected[17]>17</option>
    <option value='18' $selected[18]>18</option>
    <option value='19' $selected[19]>19</option>
    <option value='20' $selected[20]>20</option>
    <option value='21' $selected[21]>21</option>
    <option value='22' $selected[22]>22</option>
    <option value='23' $selected[23]>23</option>
    <option value='24' $selected[24]>24</option>
    <option value='25' $selected[25]>25 ranks</option>
    </select></td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$ladderinfo[inactivedaysb];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    Inactive Days until teams deleted</td>
    <td width='50%' valign='center' align='left'>
    <select name='ld_inactvdaysb'>
    <option value='500000' $selected[500000]>Disabled</option>
    <option value='1' $selected[1]>1 day</option>
    <option value='2' $selected[2]>2 days</option>
    <option value='3' $selected[3]>3</option>
    <option value='4' $selected[4]>4</option>
    <option value='5' $selected[5]>5</option>
    <option value='6' $selected[6]>6</option>
    <option value='7' $selected[7]>7 - 1 week</option>
    <option value='8' $selected[8]>8</option>
    <option value='9' $selected[9]>9</option>
    <option value='10' $selected[10]>10</option>
    <option value='11' $selected[11]>11</option>
    <option value='12' $selected[12]>12</option>
    <option value='13' $selected[13]>13</option>
    <option value='14' $selected[14]>14 - 2 weeks</option>
    <option value='15' $selected[15]>15</option>
    <option value='16' $selected[16]>16</option>
    <option value='17' $selected[17]>17</option>
    <option value='18' $selected[18]>18</option>
    <option value='19' $selected[19]>19</option>
    <option value='20' $selected[20]>20</option>
    <option value='21' $selected[21]>21 - 3 weeks</option>
    <option value='22' $selected[22]>22</option>
    <option value='23' $selected[23]>23</option>
    <option value='24' $selected[24]>24</option>
    <option value='25' $selected[25]>25</option>
    <option value='26' $selected[26]>26</option>
    <option value='27' $selected[27]>27</option>
    <option value='28' $selected[28]>28 - 4 weeks</option>
    <option value='29' $selected[29]>29</option>
    <option value='30' $selected[30]>30 days</option>
    <option value='35' $selected[35]>35 - 5 weeks</option>
    <option value='42' $selected[42]>42 - 6 weeks</option>
    <option value='49' $selected[49]>49 - 7 weeks</option>
    <option value='56' $selected[56]>56 - 8 weeks</option>
    <option value='63' $selected[63]>63 - 9 weeks</option>
    </select></td>
    </tr>
    </table>
    </td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <strong>Challenging Information</strong></td>
    </tr>
    <tr>
    <td width='100%' valign='top' align='left'>
    <table width='100%' border='0' cellspacing='1' cellpadding='1' valign='top' align='center'>
    ";
    $selected="";
    $value="";
    $value=$ladderinfo[challenge];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    Allow Challenging</td>
    <td width='50%' valign='center' align='left'>
    <select name='ld_chal'>
    <option value='1' $selected[1]>Yes</option>
    <option value='0' $selected[0]>No</option>
    </select></td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$ladderinfo[challranks];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    Maximum Ranks Teams Can Challenge Above Their Rank</td>
    <td width='50%' valign='center' align='left'>
    <select name='ld_chalranks'>
    <option value='1000' $selected[1000]>Anyone</option>
    <option value='1' $selected[1]>1</option>
    <option value='2' $selected[2]>2</option>
    <option value='3' $selected[3]>3</option>
    <option value='4' $selected[4]>4</option>
    <option value='5' $selected[5]>5</option>
    <option value='6' $selected[6]>6</option>
    <option value='7' $selected[7]>7</option>
    <option value='8' $selected[8]>8</option>
    <option value='9' $selected[9]>9</option>
    <option value='10' $selected[10]>10</option>
    <option value='11' $selected[11]>11</option>
    <option value='12' $selected[12]>12</option>
    <option value='13' $selected[13]>13</option>
    <option value='14' $selected[14]>14</option>
    <option value='15' $selected[15]>15</option>
    <option value='16' $selected[16]>16</option>
    <option value='17' $selected[17]>17</option>
    <option value='18' $selected[18]>18</option>
    <option value='19' $selected[19]>19</option>
    <option value='20' $selected[20]>20</option>
    <option value='21' $selected[21]>21</option>
    <option value='22' $selected[22]>22</option>
    <option value='23' $selected[23]>23</option>
    <option value='24' $selected[24]>24</option>
    <option value='25' $selected[25]>25</option>
    <option value='26' $selected[26]>26</option>
    <option value='27' $selected[27]>27</option>
    <option value='28' $selected[28]>28</option>
    <option value='29' $selected[29]>29</option>
    <option value='30' $selected[30]>30</option>
    <option value='31' $selected[31]>31</option>
    <option value='32' $selected[32]>32</option>
    <option value='33' $selected[33]>33</option>
    <option value='34' $selected[34]>34</option>
    <option value='35' $selected[35]>35</option>
    <option value='36' $selected[36]>36</option>
    <option value='37' $selected[37]>37</option>
    <option value='38' $selected[38]>38</option>
    <option value='39' $selected[39]>39</option>
    <option value='40' $selected[40]>40</option>
    <option value='41' $selected[41]>41</option>
    <option value='42' $selected[42]>42</option>
    <option value='43' $selected[43]>43</option>
    <option value='44' $selected[44]>44</option>
    <option value='45' $selected[45]>45</option>
    <option value='46' $selected[46]>46</option>
    <option value='47' $selected[47]>47</option>
    <option value='48' $selected[48]>48</option>
    <option value='49' $selected[49]>49</option>
    <option value='50' $selected[50]>50</option>
    <option value='60' $selected[60]>60</option>
    <option value='70' $selected[70]>70</option>
    <option value='80' $selected[80]>80</option>
    <option value='90' $selected[90]>90</option>
    <option value='100' $selected[100]>100</option>
    </select></td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$ladderinfo[respondhours];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    Hours to respond to challenges [Before forfeit awarded]</td>
    <td width='50%' valign='center' align='left '>
    <select name='ld_chalrespnd'>
    <option value='1' $selected[1]>1 Hour</option>
    <option value='2' $selected[2]>2</option>
    <option value='3' $selected[3]>3</option>
    <option value='4' $selected[4]>4</option>
    <option value='5' $selected[5]>5</option>
    <option value='6' $selected[6]>6</option>
    <option value='7' $selected[7]>7</option>
    <option value='8' $selected[8]>8</option>
    <option value='9' $selected[9]>9</option>
    <option value='10' $selected[10]>10</option>
    <option value='11' $selected[11]>11</option>
    <option value='12' $selected[12]>12 - 1/2 day</option>
    <option value='13' $selected[13]>13</option>
    <option value='14' $selected[14]>14</option>
    <option value='15' $selected[15]>15</option>
    <option value='16' $selected[16]>16</option>
    <option value='17' $selected[17]>17</option>
    <option value='18' $selected[18]>18</option>
    <option value='19' $selected[19]>19</option>
    <option value='20' $selected[20]>20</option>
    <option value='21' $selected[21]>21</option>
    <option value='22' $selected[22]>22</option>
    <option value='23' $selected[23]>23</option>
    <option value='24' $selected[24]>24 - 1 day</option>
    <option value='25' $selected[25]>25</option>
    <option value='26' $selected[26]>26</option>
    <option value='27' $selected[27]>27</option>
    <option value='28' $selected[28]>28</option>
    <option value='29' $selected[29]>29</option>
    <option value='30' $selected[30]>30</option>
    <option value='31' $selected[31]>31</option>
    <option value='32' $selected[32]>32</option>
    <option value='33' $selected[33]>33</option>
    <option value='34' $selected[34]>34</option>
    <option value='35' $selected[35]>35</option>
    <option value='36' $selected[36]>36 - 1 1/2 day</option>
    <option value='37' $selected[37]>37</option>
    <option value='38' $selected[38]>38</option>
    <option value='39' $selected[39]>39</option>
    <option value='40' $selected[40]>40</option>
    <option value='41' $selected[41]>41</option>
    <option value='42' $selected[42]>42</option>
    <option value='43' $selected[43]>43</option>
    <option value='44' $selected[44]>44</option>
    <option value='45' $selected[45]>45</option>
    <option value='46' $selected[46]>46</option>
    <option value='47' $selected[47]>47</option>
    <option value='48' $selected[48]>48 - 2 days</option>
    <option value='60' $selected[60]>60 - 2 1/2 days</option>
    <option value='72' $selected[72]>72 - 3 days</option>
    <option value='84' $selected[84]>84 - 3 1/2 days</option>
    <option value='96' $selected[96]>96 - 4 days</option>
    <option value='108' $selected[108]>108 - 4 1/2 days</option>
    <option value='120' $selected[120]>120 - 5 days</option>
    <option value='144' $selected[144]>144 - 6 days</option>
    <option value='168' $selected[168]>168 - 7 days</option>
    <option value='500000' $selected[500000]>Anytime- Not Recommended</option>
    </select></td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$ladderinfo[playdays];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    Maximum Days to play match</td>
    <td width='50%' valign='center' align='left'>
    <select name='ld_chalplydays'>
    <option value='1' $selected[1]>1 day</option>
    <option value='2' $selected[2]>2 days</option>
    <option value='3' $selected[3]>3 days</option>
    <option value='4' $selected[4]>4 days</option>
    <option value='5' $selected[5]>5 days</option>
    <option value='6' $selected[6]>6 days</option>
    <option value='7' $selected[7]>7 days</option>
    <option value='8' $selected[8]>8 days</option>
    <option value='9' $selected[9]>9 days</option>
    <option value='10' $selected[10]>10 days</option>
    <option value='11' $selected[11]>11 days</option>
    <option value='12' $selected[12]>12 days</option>
    <option value='13' $selected[13]>13 days</option>
    <option value='14' $selected[14]>14 days</option>
    <option value='15' $selected[15]>15 days</option>
    <option value='16' $selected[16]>16 days</option>
    <option value='17' $selected[17]>17 days</option>
    <option value='18' $selected[18]>18 days</option>
    <option value='19' $selected[19]>19 days</option>
    <option value='20' $selected[20]>20 days</option>
    </select></td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$ladderinfo[timescangive];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    Amount of date/times challenged suggests</td>
    <td width='50%' valign='center' align='left'>
    <select name='ld_chaltimea'>
    <option value='1' $selected[1]>1</option>
    <option value='2' $selected[2]>2</option>
    <option value='3' $selected[3]>3</option>
    <option value='4' $selected[4]>4</option>
    <option value='5' $selected[5]>5</option>
    <option value='6' $selected[6]>6</option>
    <option value='7' $selected[7]>7</option>
    <option value='8' $selected[8]>8</option>
    <option value='9' $selected[9]>9</option>
    <option value='10' $selected[10]>10</option>
    </select></td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$ladderinfo[starthour];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    Earliest Match Start Time</td>
    <td width='50%' valign='center' align='left'>
    <select name='ld_chalmstart'>
    <option value='1' $selected[1]>1am</option>
    <option value='2' $selected[2]>2am</option>
    <option value='3' $selected[3]>3am</option>
    <option value='4' $selected[4]>4am</option>
    <option value='5' $selected[5]>5am</option>
    <option value='6' $selected[6]>6am</option>
    <option value='7' $selected[7]>7am</option>
    <option value='8' $selected[8]>8am</option>
    <option value='9' $selected[9]>9am</option>
    <option value='10' $selected[10]>10am</option>
    <option value='11' $selected[11]>11am</option>
    <option value='12' $selected[12]>Midday</option>
    <option value='13' $selected[13]>1pm</option>
    <option value='14' $selected[14]>2pm</option>
    <option value='15' $selected[15]>3pm</option>
    <option value='16' $selected[16]>4pm</option>
    <option value='17' $selected[17]>5pm</option>
    <option value='18' $selected[18]>6pm</option>
    <option value='19' $selected[19]>7pm</option>
    <option value='20' $selected[20]>8pm</option>
    <option value='21' $selected[21]>9pm</option>
    <option value='22' $selected[22]>10pm</option>
    <option value='23' $selected[23]>11pm</option>
    <option value='24' $selected[24]>Midnight</option>
    </select></td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$ladderinfo[endhour];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    Latest Match Start Time</td>
    <td width='50%' valign='center' align='left'>
    <select name='ld_chalmend'>
    <option value='1' $selected[1]>1am</option>
    <option value='2' $selected[2]>2am</option>
    <option value='3' $selected[3]>3am</option>
    <option value='4' $selected[4]>4am</option>
    <option value='5' $selected[5]>5am</option>
    <option value='6' $selected[6]>6am</option>
    <option value='7' $selected[7]>7am</option>
    <option value='8' $selected[8]>8am</option>
    <option value='9' $selected[9]>9am</option>
    <option value='10' $selected[10]>10am</option>
    <option value='11' $selected[11]>11am</option>
    <option value='12' $selected[12]>Midday</option>
    <option value='13' $selected[13]>1pm</option>
    <option value='14' $selected[14]>2pm</option>
    <option value='15' $selected[15]>3pm</option>
    <option value='16' $selected[16]>4pm</option>
    <option value='17' $selected[17]>5pm</option>
    <option value='18' $selected[18]>6pm</option>
    <option value='19' $selected[19]>7pm</option>
    <option value='20' $selected[20]>8pm</option>
    <option value='21' $selected[21]>9pm</option>
    <option value='22' $selected[22]>10pm</option>
    <option value='23' $selected[23]>11pm</option>
    <option value='24' $selected[24]>Midnight</option>
    </select></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    Displayed Timezone</td>
    <td width='50%' valign='center' align='left'>
    <input type='text' name='ld_chalmtzone' value='$ladderinfo[timezone]' size='30' maxlength='25'></td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$ladderinfo[mapscangive];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    Amount of maps challenged suggests [If maps enabled above]</td>
    <td width='50%' valign='center' align='left'>
    <select name='ld_chalmapsa'>
    <option value='1' $selected[1]>1</option>
    <option value='2' $selected[2]>2</option>
    <option value='3' $selected[3]>3</option>
    <option value='4' $selected[4]>4</option>
    <option value='5' $selected[5]>5</option>
    <option value='6' $selected[6]>6</option>
    <option value='7' $selected[7]>7</option>
    <option value='8' $selected[8]>8</option>
    <option value='9' $selected[9]>9</option>
    <option value='10' $selected[10]>10</option>
    </select></td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$ladderinfo[finalizehours];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    Hours to finalize before forfeit awarded<br></td>
    <td width='50%' valign='center' align='left'>
    <select name='ld_chalfinal'>
    <option value='1' $selected[1]>1 Hour</option>
    <option value='2' $selected[2]>2</option>
    <option value='3' $selected[3]>3</option>
    <option value='4' $selected[4]>4</option>
    <option value='5' $selected[5]>5</option>
    <option value='6' $selected[6]>6</option>
    <option value='7' $selected[7]>7</option>
    <option value='8' $selected[8]>8</option>
    <option value='9' $selected[9]>9</option>
    <option value='10' $selected[10]>10</option>
    <option value='11' $selected[11]>11</option>
    <option value='12' $selected[12]>12 - 1/2 day</option>
    <option value='13' $selected[13]>13</option>
    <option value='14' $selected[14]>14</option>
    <option value='15' $selected[15]>15</option>
    <option value='16' $selected[16]>16</option>
    <option value='17' $selected[17]>17</option>
    <option value='18' $selected[18]>18</option>
    <option value='19' $selected[19]>19</option>
    <option value='20' $selected[20]>20</option>
    <option value='21' $selected[21]>21</option>
    <option value='22' $selected[22]>22</option>
    <option value='23' $selected[23]>23</option>
    <option value='24' $selected[24]>24 - 1 day</option>
    <option value='25' $selected[25]>25</option>
    <option value='26' $selected[26]>26</option>
    <option value='27' $selected[27]>27</option>
    <option value='28' $selected[28]>28</option>
    <option value='29' $selected[29]>29</option>
    <option value='30' $selected[30]>30</option>
    <option value='31' $selected[31]>31</option>
    <option value='32' $selected[32]>32</option>
    <option value='33' $selected[33]>33</option>
    <option value='34' $selected[34]>34</option>
    <option value='35' $selected[35]>35</option>
    <option value='36' $selected[36]>36 - 1 1/2 day</option>
    <option value='37' $selected[37]>37</option>
    <option value='38' $selected[38]>38</option>
    <option value='39' $selected[39]>39</option>
    <option value='40' $selected[40]>40</option>
    <option value='41' $selected[41]>41</option>
    <option value='42' $selected[42]>42</option>
    <option value='43' $selected[43]>43</option>
    <option value='44' $selected[44]>44</option>
    <option value='45' $selected[45]>45</option>
    <option value='46' $selected[46]>46</option>
    <option value='47' $selected[47]>47</option>
    <option value='48' $selected[48]>48 - 2 days</option>
    <option value='60' $selected[60]>60 - 2 1/2 days</option>
    <option value='72' $selected[72]>72 - 3 days</option>
    <option value='84' $selected[84]>84 - 3 1/2 days</option>
    <option value='96' $selected[96]>96 - 4 days</option>
    <option value='108' $selected[108]>108 - 4 1/2 days</option>
    <option value='120' $selected[120]>120 - 5 days</option>
    <option value='144' $selected[144]>144 - 6 days</option>
    <option value='168' $selected[168]>168 - 7 days</option>
    <option value='500000' $selected[500000]>Anytime- Not Recommended</option>
    </select></td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$ladderinfo[mapscanaccept];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    Amount of maps challenger chooses [If maps enabled above]</td>
    <td width='50%' valign='center' align='left'>
    <select name='ld_chalmapsb'>
    <option value='1' $selected[1]>1</option>
    <option value='2' $selected[2]>2</option>
    <option value='3' $selected[3]>3</option>
    <option value='4' $selected[4]>4</option>
    <option value='5' $selected[5]>5</option>
    <option value='6' $selected[6]>6</option>
    <option value='7' $selected[7]>7</option>
    <option value='8' $selected[8]>8</option>
    <option value='9' $selected[9]>9</option>
    <option value='10' $selected[10]>10</option>
    </select></td>
    </tr>
    </table>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='top' align='left'>
    <table width='100%' border='0' cellspacing='0' cellpadding='0' valign='top' align='center'>
    ";
    $selected="";
    $value="";
    $value=$ladderinfo[skillneeded];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    </table>
    </td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='ladderid' value='$ladderid'>
    <input type='hidden' name='ld_type' value='$ladderinfo[type]'>
    <input type='hidden' name='action' value='editladdersd'>
    <input type='submit' name='' value='Update Ladder'>
    <input type='reset' name='' value='Reset'> </td>
    </form>
    </tr>
    </table>";
    include("$dir[curtheme]");
}

function admin_editladdersd($ladderid,$ld_gameid,$ld_name,$ld_abrv,$ld_desc,$ld_type,$ld_openreport,$ld_plyrmin,$ld_plyrmax,$ld_minmaps,$ld_maxmaps,$ld_inactvdaysa,$ld_inactvdrop,$ld_inactvdaysb,$ld_chal,$ld_chalranks,$ld_chalrespnd,$ld_chalplydays,$ld_chaltimea,$ld_chalmstart,$ld_chalmend,$ld_chalmtzone,$ld_chalmapsa,$ld_chalfinal,$ld_chalmapsb,$ld_skill){
    global $dir, $url, $out, $admn, $site;
    include("$dir[func]/checkdata.php");
    if (mysql_num_rows(mysql_query("select laddername from ladders where id='$ladderid'")) < 1){
        $errormessage=$errormessage."That Ladder ID is invalid.<br>";
        error_check($errormessage);
    }

    if (mysql_num_rows(mysql_query("select gamename from games where id='$ld_gameid'")) < 1){
        $errormessage=$errormessage."That game is invalid.<br>";
        error_check($errormessage);
    }

    if ((($ld_plyrmin > 1)||($ld_plyrmax > 1))&&($ld_type < 2)){
        $errormessage=$errormessage."Minimum and Maximum players must be set to one for 1v1 ladders.<br>";
        error_check($errormessage);
    }

    if ($ld_plyrmin > $ld_plyrmax){
        $errormessage=$errormessage."Maximum players must be equal to or greater than minimum players.<br>";
        error_check($errormessage);
    }

    if (($ld_minmaps < 1)&&($ld_maxmaps > 0)){
        $errormessage=$errormessage."Maps must be set to 'No Maps' for both fields.<br>";
        error_check($errormessage);
    }

    if ($ld_minmaps > $ld_maxmaps){
        $errormessage=$errormessage."Maximum maps must be equal to or greater than minimum maps.<br>";
        error_check($errormessage);
    }

    if (($ld_openreport < 1)&&($ld_chal < 1)){
        $errormessage=$errormessage."Challenges must be set to yes on 'Challenges Only' ladders.<br>";
        error_check($errormessage);
    }

    if (($ld_openreport > 0)&&($ld_openreport < 2)){
        if($ld_chal < 1) {
            $errormessage=$errormessage."Challenges must be set to yes on 'Challenges and Open Play' ladders.<br>";
            error_check($errormessage);
        }

    }

    if ($ld_openreport >1){
        if($ld_chal > 0) {
            $errormessage=$errormessage."Challenges must be set to no on 'Open Play Only' ladders.<br>";
            error_check($errormessage);
        }else{

            $ld_openreport = 1;
        }

    }

    if (($ld_chalmend <= $ld_chalmstart)&&($ld_chal)){
        $errormessage=$errormessage."The latest match start time must come after <br>the earliest match start time in the list.<br><br>For 24 hour challenging, set start time to 1am and end time to Midnight.<br>";
        error_check($errormessage);
    }

    if (($ld_chalmapsa < $ld_chalmapsb)&&($ld_chal)&&($ld_maxmaps >0)){
        $errormessage=$errormessage."Number of maps suggested must exceed or equal<br> number of maps to be chosen by challenger when finalizing.<br>";
        error_check($errormessage);
    }

    $ld_name=change_charecters($ld_name);
    $ld_abrv=change_charecters($ld_abrv);
    $ld_desc=change_charecters($ld_desc);
    $ld_type=change_numbersonly($ld_type);
    $ld_openreport=change_numbersonly($ld_openreport);
    $ld_plyrmin=change_numbersonly($ld_plyrmin);
    $ld_plyrmax=change_numbersonly($ld_plyrmax);
    $ld_minmaps=change_numbersonly($ld_minmaps);
    $ld_maxmaps=change_numbersonly($ld_maxmaps);
    $ld_inactvdaysa=change_numbersonly($ld_inactvdaysa);
    $ld_inactvdrop=change_numbersonly($ld_inactvdrop);
    $ld_inactvdaysb=change_numbersonly($ld_inactvdaysb);
    $ld_chal=change_numbersonly($ld_chal);
    $ld_chalranks=change_numbersonly($ld_chalranks);
    $ld_chalrespnd=change_numbersonly($ld_chalrespnd);
    $ld_chalplydays=change_numbersonly($ld_chalplydays);
    $ld_chaltimea=change_numbersonly($ld_chaltimea);
    $ld_chalmstart=change_numbersonly($ld_chalmstart);
    $ld_chalmend=change_numbersonly($ld_chalmend);
    $ld_chalmtzone=change_charecters($ld_chalmtzone);
    $ld_chalmapsa=change_numbersonly($ld_chalmapsa);
    $ld_chalfinal=change_numbersonly($ld_chalfinal);
    $ld_chalmapsb=change_numbersonly($ld_chalmapsb);
    $ld_skill=change_numbersonly($ld_skill);
    $errormessage=check_validlength($errormessage, $ld_name, "2", "50", "Your Ladder's Name Must be 2-25 Characters Long.");
    $errormessage=check_validlength($errormessage, $ld_abrv, "2", "25", "Your Ladders Abbreviation Must be 2-10 Characters Long.");
    $errormessage=check_validlength($errormessage, $ld_desc, "0", "250", "Your Ladders Description Must not Exceed 200 Characters.");
    $errormessage=check_validlength($errormessage, $ld_type, "1", "2", "Your Ladder Type is Invalid.");
    error_check($errormessage);
    //CHECK IF LADDER/ABBREVIATION EXIST HERE
    $errormessage=check_ladderexistother($errormessage, $ld_name, $ld_abrv, $ladderid);
    error_check($errormessage);
    //UPDATE LADDER
    $tday=date("Y-m-d H:i:s");
    mysql_query("UPDATE ladders SET
    gameon='$ld_gameid',
    laddername='$ld_name',
    abbreviation='$ld_abrv',
    type='$ld_type',
    description='$ld_desc',
    openplay='$ld_openreport',
    minmembers='$ld_plyrmin',
    maxmembers='$ld_plyrmax',
    minmaps='$ld_minmaps',
    maxmaps='$ld_maxmaps',
    inactivedaysa='$ld_inactvdaysa',
    inactivedrop='$ld_inactvdrop',
    inactivedaysb='$ld_inactvdaysb',
    challenge='$ld_chal',
    challranks='$ld_chalranks',
    respondhours='$ld_chalrespnd',
    playdays='$ld_chalplydays',
    timescangive='$ld_chaltimea',
    starthour='$ld_chalmstart',
    endhour='$ld_chalmend',
    timezone='$ld_chalmtzone',
    mapscangive='$ld_chalmapsa',
    finalizehours='$ld_chalfinal',
    mapscanaccept='$ld_chalmapsb',
    skillneeded='$ld_skill',
    lasteditby='$admn[id]',
    lastedit='$tday'
    WHERE id='$ladderid'");
    include("$dir[func]/admin_finishmessage.php");
    display_message("The ladder has been updated.<br>");
}

function delete_ladder($ladderid){
    //DELETE LADDER AND TOURNAMENTS ON LADDER
    mysql_query("DELETE FROM ladders WHERE id='$ladderid'");
    mysql_query("DELETE FROM tourneys WHERE ladderid='$ladderid'");
    //DROP LADDER TABLE
    mysql_query("DROP TABLE IF EXISTS ladder_$ladderid");
}

?>
