<?
if(!IsSet($indexloaded)){
    header("Location: ./index.php");
}

//CHECK ADMIN ACCESS LEVEL
if($admn[access] < 10){
    include("$dir[func]/error.php");
    display_error("You are not allowed to perform this function.<br>");
}

function admin_createteam(){
    global $dir, $url, $out, $site, $admn;
    //GET LADDERS
    $ladderslist=mysql_query("SELECT id,laddername  FROM ladders ORDER by laddername");
    while(list($id,$name)=mysql_fetch_row($ladderslist)){
        $theladders=$theladders."<option value='$id'>$name</option>";
        $howmanyeditable=1;
    }

    if(!$howmanyeditable){
        include("$dir[func]/error.php");
        display_error("There are no ladders.<br>");
    }

    //TABLE ATERNATING COLORS
    $altcolora="#000033";
    $altcolorb="#000020";
    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <strong>Create a team</strong></td>
    </tr>
    <tr>
    <td width='100%' valign='top' align='left'>
    <form method='post'>
    <table width='100%' border='0' cellspacing='0' cellpadding='0' valign='top' align='center'>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>* Ladder</font></td>
    <td width='50%' valign='center' align='center'>
    <select name='ladderid'>$theladders</select></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>* Team Leaders Player ID</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='playerid' value='' size='30' maxlength='25'></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>* Team Name</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='teamname' value='' size='30' maxlength='25'></td>
    </tr>
    </table>
    </td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='action' value='createteamb'>
    <input type='submit' name='' value='Create Team'></td>
    </form>
    </tr>
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function admin_createteamb($ladderid,$playerid,$teamname){
    global $dir, $file, $url, $out, $site, $admn;
    if(!$ladderid){
        include("$dir[func]/error.php");
        display_error("Invalid Ladder ID.<br>");
    }

    if(!$playerid){
        include("$dir[func]/error.php");
        display_error("Invalid Player ID.<br>");
    }

    if(!$teamname){
        include("$dir[func]/error.php");
        display_error("Invalid Team Name.<br>");
    }

    if($admn[access] < 99){
        if (mysql_num_rows(mysql_query("SELECT staffid FROM staffaccess WHERE staffid='$admn[id]' AND ladderid='$ladderid'")) < 1){
            include("$dir[func]/error.php");
            display_error("You are not allowed to create teams on this laddder.<br>");
        }

    }

    //CHECK LADDER EXISTS
    $thisladder=mysql_query("SELECT laddername,openplay,type FROM ladders WHERE id='$ladderid'");
    $ladderinfo=mysql_fetch_array($thisladder);
    if(!$ladderinfo[laddername]){
        include("$dir[func]/error.php");
        display_error("Invalid Ladder ID.<br>");
    }

    if($ladderinfo[type] ==1){
        $type =1;
    }else if($ladderinfo[type] ==3){

        $type = 3;
    }

    //CHECK PLAYER EXISTS
    $userinfo=mysql_query("SELECT * FROM users WHERE id='$playerid'");
    $uinfo=mysql_fetch_array($userinfo);
    if(!$uinfo[alias]){
        include("$dir[func]/error.php");
        display_error("Unknown Player.<br>");
    }

    //MAKE SURE PLAYER IS NOT ALREADY ON LADDER
    $pteams=mysql_query("SELECT teamid FROM teammembers WHERE playerid='$playerid'");
    while($row = mysql_fetch_array($pteams)){
        $check=mysql_query("SELECT id FROM ladder_$ladderid WHERE id='$row[teamid]'");
        $checkb=mysql_fetch_array($check);
        if($checkb){
            include("$dir[func]/error.php");
            display_error("This player is already on this ladder.<br>");
        }

    }

    include("$dir[func]/checkdata.php");
    $teamname=change_charecters($teamname);
    $errormessage=check_validlength($errormessage, $teamname, "3", "50", "Team names must be 3-25 characters");
    error_check($errormessage);
    $errormessage=check_ban($errormessage, $teamname, $email, $ip);
    error_check($errormessage);
    $tday=date("Y-m-d H:i:s");
    $ip=getenv("REMOTE_ADDR");
    mysql_query("INSERT INTO teams VALUES (
    NULL,
    '$teamname',
    '$uinfo[email]',
    '',
    '0',
    '',
    '',
    '',
    '$tday',
    '$playerid.$admin[id]',
    '$ip',
    '$type');");
    $getid=mysql_query("SELECT id FROM teams WHERE teamname='$teamname'");
    $newidnfo=mysql_fetch_array($getid);
    $teamid=$newidnfo["id"];
    mysql_query("INSERT INTO teammembers VALUES (
    '$teamid',
    '$playerid',
    '1',
    '$tday');");
    if($ladderinfo[openplay]){
        $nextrank="0";
    }else{

        $totalranked=mysql_query("SELECT COUNT(*) FROM ladder_$ladderid");
        $totalranked=mysql_fetch_array($totalranked);
        $nextrank=($totalranked[0] + 1);
    }

    mysql_query("INSERT INTO ladder_$ladderid VALUES (
    '$teamid',
    '$tday',
    '',
    '0',
    '0',
    '',
    '0',
    '$nextrank',
    '0',
    '0',
    '0',
    '0',
    '0',
    '0',
    '0',
    '0',
    '0',
    '0',
    '0',
    '0',
    '0',
    '0',
    '0');");
    include("$dir[func]/admin_finishmessage.php");
    display_message("The team was created.<br><br>Teamname: $teamname<br>Leader: <a href='$url[base]/$file[players]?playerid=$playerid'>$uinfo[alias]</a><br>Team ID: $teamid<br><a href='$url[base]/$file[teams]?teamid=$teamid'>View Team</a><br>");
}

?>
