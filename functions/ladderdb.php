<?
function ladder_database($page,$orderby){
    global $dir, $file, $url, $out, $misc, $site, $admn;
    $result=mysql_query("select COUNT(*) from ladders");
    $result=mysql_fetch_array($result);
    $totalladders="$result[0]";
    $maxresults=$misc[ladderdb];
    $stop="$maxresults";
    if(!$page){
        $start="1";
    }else{

        $start="$page";
    }

    if($start > $totalladders){
        $start=0;
    }

    $page="$start";
    $start=($start-1);
    $altcolora="#000033";
    $altcolorb="#000020";
    $altcolora="' class='altcolora";
    $altcolorb="' class='altcolorb";
    if($orderby == "id_up"){
        $order = "id";
    }else if ($orderby == "id_down"){

        $order = "id desc";
    }else if ($orderby == "laddername_down"){

        $order = "laddername desc";
    }else if ($orderby == "laddername_up"){

        $order = "laddername";
    }else if ($orderby == "type_down"){

        $order = "type desc";
    }else if ($orderby == "type_up"){

        $order = "type";
    }else if ($orderby == "gameon_down"){

        $order = "gameon desc";
    }else if ($orderby == "gameon_up"){

        $order = "gameon";

    }else if ($orderby == "min_down"){

        $order = "minmembers desc";
    }else if ($orderby == "min_up"){

        $order = "minmembers";

    }else if ($orderby == "max_down"){

        $order = "maxmembers desc";
    }else if ($orderby == "max_up"){

        $order = "maxmembers";

    }else{

        $order = "laddername";
    }

    $result=mysql_query("SELECT id,gameon,laddername,type,minmembers,maxmembers FROM ladders ORDER BY $order LIMIT $start,$stop");
    while(list($id,$gameon,$laddername,$type,$min,$max)=mysql_fetch_row($result)) {
        if($altladrow=="$altcolora"){
            $altladrow="$altcolorb";
        }else{

            $altladrow="$altcolora";
        }

        if($type == '1'){
            $laddertype = "1v1";
        }else if($type == '3'){

            $laddertype = "Team";
        }else{

            $laddertype = "";
        }
        
	  $lgame = mysql_fetch_array(mysql_query("SELECT gamename FROM games WHERE id = '$gameon'"));
        $count=mysql_fetch_array(mysql_query("SELECT count(*) AS laddercount FROM ladder_$id"));

        $playerlist=$playerlist."
        <tr bgcolor='$altladrow'>
        <td width='' valign='center' align='left'><a href='$url[base]/$file[ladder]?ladderid=$lid'>&nbsp;$laddername</a></td>
        <td width='' valign='center' align='center'>$id</td>
        <td width='' valign='center' align='left'>&nbsp;$lgame[gamename]</td>
        <td width='' valign='center' align='center'>$laddertype</td>
        <td width='' valign='center' align='center'>$count[laddercount]</td>	
        <td width='' valign='center' align='center'>$min</td>
        <td width='' valign='center' align='center'>$max</td>";

        $ladderlist=$ladderlist."</tr>";
        $counter++;
    }

    if($order == "laddername desc"){
        $laddername = "laddername_up";
    }else{

        $laddername = "laddername_down";
    }

    if($order == "id desc"){
        $id = "id_up";
    }else{

        $id = "id_down";
    }

    if($order == "type desc"){
        $type = "type_up";
    }else{

        $type = "type_down";
    }

    if($order == "gameon desc"){
        $gameon = "gameon_up";
    }else{

        $gameon = "gameon_down";
    }
	
    if($order == "minmembers desc"){
        $min = "min_up";
    }else{

        $min = "min_down";
    }
	
    if($order == "maxmembers desc"){
        $max = "max_up";
    }else{

        $max = "max_down";
    }
	
    $tablehead=table_head("show","100%","","left");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    <br>
    <center>
    $tablehead
    &nbsp; &nbsp;<strong><font class='catfont'>$site[shortname] Ladder Database</font></strong><br>
    <hr class='catfont' size='1'>
    <table width='100%' border='0' bordercolor='#000000' cellspacing='1' cellpadding='1'>
    <tr class='altcolor'>
    <td width='30%' valign='center' align='left'><strong><a href='$url[base]/$file[stats]?action=ladderdb&orderby=$laddername'>&nbsp;Ladder Name</a></strong></td>
    <td width='5%' valign='center' align='center'><strong><a href='$url[base]/$file[stats]?action=ladderdb&orderby=$id'>ID</a></strong></td>
    <td width='30%' valign='center' align='left'><strong><a href='$url[base]/$file[stats]?action=ladderdb&orderby=$gameon'>&nbsp;Game</a></strong></td>
    <td width='15%' valign='center' align='center'><strong><a href='$url[base]/$file[stats]?action=ladderdb&orderby=$type'>Type</a></strong></td>
    <td width='10%' valign='center' align='center'><strong>Size</strong></td>
    <td width='5%' valign='center' align='center'><strong><a href='$url[base]/$file[stats]?action=ladderdb&orderby=$min'>Min</a></strong></td>
    <td width='5%' valign='center' align='center'><strong><a href='$url[base]/$file[stats]?action=ladderdb&orderby=$max'>Max</a></strong></td>";
    
    $out[body]=$out[body]."</tr>
    $playerlist
    ";
    $pagenumb=1;
    if ($totalusers > $maxresults) {
        $totalpages=($totalusers / $maxresults);
        while($pagenum < $totalpages){
            $pagenum++;
            if($pagenumb=="$page"){
                $pages=$pages."[<a href='$url[base]/$file[stats]?action=ladderdb&orderby=$orderby&page=$pagenumb'>$pagenum</a>] ";
            }else{

                $pages=$pages."<a href='$url[base]/$file[stats]?action=ladderdb&orderby=$orderby&page=$pagenumb'>$pagenum</a> ";
            }

            $pagenumb=($maxresults + $pagenumb);
        }

    }

    if($pages){
        $out[body]=$out[body]."
        <tr class='altcolor'>
        <td width='100%' valign='center' align='center' colspan='8'><strong>$pages</strong></td>
        </tr>";
    }

    $out[body]=$out[body]."
    </table>
    $tablefoot";
    $tablehead=table_head("show","400","","left");
    $tablefoot=table_foot("show");
    include("$dir[curtheme]");
}

?>
