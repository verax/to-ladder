<?
if(!IsSet($indexloaded)){
    header("Location: ./index.php");
}

//CHECK ADMIN ACCESS LEVEL
if($admn[access] < 25){
    include("$dir[func]/error.php");
    display_error("You are not allowed to perform this function.<br>");
}

function admin_match(){
    global $dir, $url, $out, $site, $admn;
    //GET LADDERS
    $ladderslist=mysql_query("SELECT id,laddername  FROM ladders ORDER by laddername");
    while(list($id,$name)=mysql_fetch_row($ladderslist)){
        $theladders=$theladders."<option value='$id'>$name</option>";
        $foundladders=1;
    }

    if(!$foundladders){
        include("$dir[func]/error.php");
        display_error("There are no ladders.<br>");
    }

    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <form method='post'>
    <td width='100%' valign='top' align='center'>
    <strong>Edit a Match</strong><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <br>
    Select a ladder<br>
    <select name='ladderid'>$theladders</select><br>
    <br></td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='action' value='editmatchb'>
    <input type='submit' name='' value='Find Match'>
    </td>
    </form>
    </tr>
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function admin_matchb($ladderid){
    global $dir, $url, $out, $site, $admn;
    if(!$ladderid){
        include("$dir[func]/error.php");
        display_error("Invalid Ladder ID.<br>");
    }

    if($admn[access] < 99){
        if (mysql_num_rows(mysql_query("SELECT staffid FROM staffaccess WHERE staffid='$admn[id]' AND ladderid='$ladderid'")) < 1){
            include("$dir[func]/error.php");
            display_error("You are not allowed to manage matches on this laddder.<br>");
        }

    }

    //GET MATCHES
    $matchlist=mysql_query("SELECT matchid,winneralias,loseralias,DATE_FORMAT(confirmdate, '%M %d, %l:%i %p') FROM matchdb WHERE ladderid='$ladderid' ORDER by confirmdate");
    while(list($matchid,$winneralias,$loseralias,$confirmdate)=mysql_fetch_row($matchlist)){
        $thematches=$thematches."<option value='$matchid'>$winneralias vs $loseralias - $confirmdate</option>";
        $foundladders=1;
    }

    if(!$foundladders){
        include("$dir[func]/error.php");
        display_error("There are no matches on this ladder to edit.<br>");
    }

    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <form method='post'>
    <td width='100%' valign='top' align='center'>
    <strong>Edit a Match</strong><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <br>
    <select name='matchid'>$thematches</select><br>
    <br></td>
    </tr>
    <script language='javascript'>var confirmdelete='Are you sure you want to DELETE this match';</script>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='action' value='editmatchc'>
    <input type='submit' name='todo' value='Edit Match'>
    <input type='submit' name='todo' value='Delete Match' onClick='return confirm(confirmdelete);'>
    </td>
    </form>
    </tr>
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function admin_matchc($matchid,$todo){
    global $dir, $url, $out, $site, $admn;
    if(!$matchid){
        include("$dir[func]/error.php");
        display_error("Invalid Match ID.<br>");
    }

    if($todo=="Edit Match"){
        edit_match($matchid);
    }

    else if($todo=="Delete Match"){
        delete_match($matchid);
        include("$dir[func]/admin_finishmessage.php");
        display_message("The match was deleted.<br>");
    }

    else{
        include("$dir[func]/error.php");
        display_error("Unknown Command.<br>");
    }

}

function edit_match($matchid){
    global $dir, $url, $out, $site, $admn;
    $matchinfo=mysql_query("SELECT * FROM matchdb WHERE matchid='$matchid'");
    $minfo=mysql_fetch_array($matchinfo);
    if(!$minfo[laddername]){
        include("$dir[func]/error.php");
        display_error("Unknown Team.<br>");
    }

    //TABLE ATERNATING COLORS
    $altcolora="#000033";
    $altcolorb="#000020";
    if($minfo[tagplace]=="0"){
        $checkeda="CHECKED";
        $checkedb="";
    }else{

        $checkeda="";
        $checkedb="CHECKED";
    }

    //CHANGE SPECIAL CHARECTERS BACK
    include("$dir[func]/checkdata.php");
    $minfo[teamname]=unchange_charecters($minfo[teamname]);
    $minfo[tag]=unchange_charecters($minfo[tag]);
    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <font class='catfont'><strong>Edit a Match</strong></font></td>
    </tr>
    <tr>
    <td width='100%' valign='top' align='left'>
    <form method='post'>
    <table width='100%' border='0' cellspacing='0' cellpadding='0' valign='top' align='center'>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Winner Alias</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='m_walias' value='$minfo[winneralias]' size='30' maxlength='25'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Loser Alias</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='m_lalias' value='$minfo[loseralias]' size='30' maxlength='25'></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Winner Score</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='m_wscore' value='$minfo[winscore]' size='30' maxlength='10'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Loser Score</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='m_lscore' value='$minfo[losescore]' size='30' maxlength='10'></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Winner Comment</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='m_wcomment' value='$minfo[wincomment]' size='30' maxlength='250'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Loser Comment</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='m_lcomment' value='$minfo[losecomment]' size='30' maxlength='250'></td>
    </tr>
    <tr class='altcolora'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Report IP</font></td>
    <td width='50%' valign='center' align='center'>
    $minfo[reporterip]</td>
    </tr>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Confirm IP</font></td>
    <td width='50%' valign='center' align='center'>
    $minfo[confirmerip]</td>
    </tr>
    </table>
    </td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='action' value='editmatchd'>
    <input type='hidden' name='matchid' value='$matchid'>
    <input type='submit' name='' value='Update Match'>
    <input type='reset' name='' value='Reset'></td>
    </form>
    </tr>
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function admin_matchd($matchid,$m_walias,$m_lalias,$m_wscore,$m_lscore,$m_wcomment,$m_lcomment){
    global $dir, $url, $out, $site, $admn;
    if(!$matchid){
        include("$dir[func]/error.php");
        display_error("Invalid Match ID.<br>");
    }

    if(!$m_walias){
        include("$dir[func]/error.php");
        display_error("Invalid winner name.<br>");
    }

    if(!$m_lalias){
        include("$dir[func]/error.php");
        display_error("Invalid loser name.<br>");
    }

    include("$dir[func]/checkdata.php");
    $m_walias=change_charecters($m_walias);
    $m_lalias=change_charecters($m_lalias);
    $m_wscore=change_numbersonly($m_wscore);
    $m_lscore=change_numbersonly($m_lscore);
    $m_wcomment=change_charecters($m_wcomment);
    $m_lcomment=change_charecters($m_lcomment);
    $errormessage=check_validlength($errormessage, $m_walias, "3", "50", "Your winning team name be 3-25 Characters");
    $errormessage=check_validlength($errormessage, $m_lalias, "3", "50", "Your losing team name be 3-25 Characters");
    error_check($errormessage);
    mysql_query("UPDATE matchdb SET
    winneralias='$m_walias',
    loseralias='$m_lalias',
    winscore='$m_wscore',
    losescore='$m_lscore',
    wincomment='$m_wcomment',
    losecomment='$m_lcomment'
    WHERE matchid='$matchid'");
    include("$dir[func]/admin_finishmessage.php");
    display_message("The match was updated.<br>");
}

function delete_match($matchid){
    mysql_query("DELETE FROM matchdb WHERE matchid='$matchid'");
}

?>
