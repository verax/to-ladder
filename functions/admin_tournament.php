<?
if(!IsSet($indexloaded)){
    header("Location: ./index.php");
}

//CHECK ADMIN ACCESS LEVEL
if($admn[access] < 63){
    include("$dir[func]/error.php");
    display_error("You are not allowed to perform this function.<br>");
}

$admin=mysql_query("SELECT tid FROM tourneys WHERE tid='$tid'");
$admin=mysql_fetch_array($admin);
if($admin[tid]){
    $tinfo=mysql_query("SELECT * FROM tourneys WHERE tid='$admin[tid]'");
    $tinfo=mysql_fetch_array($tinfo);
}else{

    $defaultz=mysql_query("SELECT * FROM defaults2 WHERE line='1'");
    $tinfo=mysql_fetch_array($defaultz);
    $tinfo[name]="Tournament Admin";
}

function characters($text){
    $text=str_replace(";", "", $text);
    $text=htmlspecialchars("$text");
    $text=str_replace("#", "", $text);
    $text=str_replace("|", "", $text);
    $text=str_replace("'", "&acute;", $text);
    $text=eregi_replace("( ){2,}", " ", $text);

    $text=trim($text);
    return($text);
}

function madmin_manage($edit){
    global $config, $tinfo, $dir, $url, $out, $site, $misc;
    $tourneylist=mysql_query("SELECT tid,name FROM tourneys ORDER by name");
    while(list($tid,$name)=mysql_fetch_row($tourneylist)){
        $tourneylistid.="<option value='$tid'>$name</option>\n";
    }

    if($edit[tid]){
        $tourneyinfo=mysql_query("SELECT * FROM tourneys WHERE tid='$edit[tid]'");
        $adedt=mysql_fetch_array($tourneyinfo);
    }

    if($adedt[tid]){
        $actiontype="
        <input type='hidden' name='action' value='madmin_manage_update'>
        <input type='hidden' name='adedt[tid]' value='$adedt[tid]'>";
        $addeditword="Edit";
        $size="$adedt[size]";
        $selected_size[$size]="SELECTED";
        $active="$adedt[active]";
        $selected_active[$active]="SELECTED";
        $open="$adedt[open]";
        $selected_open[$open]="SELECTED";
        $adedt[password]=$adedt[password];
    }else{

        $defaultz=mysql_query("SELECT logo,bgcolor,tdcolor,fontcolor FROM defaults2 WHERE line='1'");
        $adedt=mysql_fetch_array($defaultz);
        $actiontype="
        <input type='hidden' name='action' value='madmin_manage_add'>";
        $addeditword="Create";
    }

    $tablehead=table_head("show","80%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br><center>
    $tablehead
    
    ";
    if(!$adedt[tid]){
        $out[body]=$out[body]."
        <table width='600' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    	<tr class='altcolor'>
    	<form method='post'>
    	<td width='100%' valign='top' align='center'>
    	<strong>Edit a Tournament</strong><br>
    	</td></tr>
        <tr>
        <form method='post'>
        <td align='center' width='600'><br>
        <select name='edit[tid]'>
        $tourneylistid</select><br><br>
	</td></tr>
        <tr class='altcolor'><td width='100%' valign='top' align='center'><input type='hidden' name='action' value='madmin_manage'>
        <input type='submit' value='Edit Tournament'>
        </td></form>
        </tr>
        </table><br>

        <table width='600' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
        <tr class='altcolor'>
    	<form method='post'>
    	<td width='100%' valign='top' align='center'>
    	<strong>Delete a Tournament</strong><br>
    	</td></tr>
        <tr><form method='post'>
        <td align='center' width='600'><br>
        <select name='delete[tid]'>
        $tourneylistid</select><br><br></td></tr>
        <tr class='altcolor'><td width='100%' valign='top' align='center'>
        <script language='javascript'>var confirmdelete='Are you 100% sure you want to DELETE this Tournament';</script>
        <input type='hidden' name='action' value='madmin_manage_delete'>
        <input type='submit' value='Delete Tournament' onClick='return confirm(confirmdelete);'>
        </td></form>
        </tr>
        </table><br>

        <table width='600' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
        <tr class='altcolor'>
        <td width='100%' valign='top' align='center'>
        <b>Reset a Tournament</b><br>
        </td></tr>
        <form method='post'>
        <tr bgcolor='$tinfo[tdcolor]'>
        <td align='center' width='600'><br>
	<select name='reset[tid]'>
        $tourneylistid</select><br><br>
        Brackets: <input type='radio' name='reset[players]' value='0' CHECKED>
        Brackets and Players: <input type='radio' name='reset[players]' value='1'><br></td></tr>
        <tr class='altcolor'><td width='100%' valign='top' align='center'>
        <script language='javascript'>var confirmreset='Are you 100% sure you want to RESET this Tournament? All tournament data WILL be lost!';</script>
        <input type='hidden' name='action' value='madmin_manage_reset'>
        <input type='submit' value='Reset Tournament' onClick='return confirm(confirmreset);'>
        </td></form>
        </tr>
        </table>
        <br>
        ";
    }

    $ladderslist=mysql_query("SELECT id,laddername FROM ladders ORDER by laddername");
    while(list($id,$name)=mysql_fetch_row($ladderslist)){
        $selectedladder="";
        if($ladderid=="$id"){
            $selectedladder="SELECTED";
        }

        $theladders.="<option value='$id' $selectedladder>$name</option>";
    }
    $out[body]=$out[body]."
    
    <table width='600' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center' colspan='2'>
    <b>$addeditword a Tournament</b><br>
    Required fields are marked with *
    </td></tr>
    <form method='post'>
    <tr class='altcolora'>
    <td align='left' width='300'>Tournament Name *</td>
    <td align='right'><input type='text' name='adedt[name]' value='$adedt[name]' maxlength='50'></td>
    </tr>";
    if($adedt[tid]){
        $ladname=mysql_fetch_array(mysql_query("SELECT laddername FROM ladders WHERE id='$adedt[ladderid]'"));
    	$out[body]=$out[body]."<tr class='altcolorb'>
    	<td align='left' width='300'>Ladder On</td>
    	<td align='right'>$ladname[laddername]</td>
        </tr><input type='hidden' name='adedt[ladderid]' value='$adedt[ladderid]'>";
    }else{
     	$out[body]=$out[body]."
    	<tr class='altcolorb'>
    	<td align='left' width='300'>Ladder On *</td>
    	<td align='right'><select name='adedt[ladderid]'><option value='$adedt[ladderid]'>Select a Ladder</option>$theladders</select></td>
    </tr>";
    }
    
    $out[body]=$out[body]."
    <tr class='altcolora'>
    <td align='left' width='250'>Tourney Size *</td>
    <td align='right'><select name='adedt[size]'>
    <option value='4' $selected_size[4]>4 Player/Team</option>
    <option value='8' $selected_size[8]>8 Player/Team</option>
    <option value='16' $selected_size[16]>16 Player/Team</option>
    <option value='32' $selected_size[32]>32 Player/Team</option>
    <option value='64' $selected_size[64]>64 Player/Team</option>
    <option value='128' $selected_size[128]>128Player/Team</option>
    <option value='256' $selected_size[256]>256Player/Team</option>
    <option value='512' $selected_size[512]>512Player/Team</option>
    </select></td>
    </tr>
    <tr class='altcolorb'>
    <td align='left' width='250'>Tourney Active *</td>
    <td align='right'><select name='adedt[active]'>
    <option value='1' $selected_active[1]>New Signups On</option>
    <option value='0' $selected_active[0]>New Signups Off</option>
    </select></td>
    </tr>";
    if($adedt[tid]){
    	$out[body]=$out[body]."
    	<tr class='altcolora'>
    	<td align='left' width='250'>Tourney Open *</td>
    	<td align='right'><select name='adedt[open]'>
    	<option value='1' $selected_open[1]>Tourney Open</option>
    	<option value='0' $selected_open[0]>Tourney Closed</option>
    	</select></td>
    	</tr>";
    }else{
	$out[body]=$out[body]."
    	<input type='hidden' name='adedt[open]' value='1'>";
    }
    $out[body]=$out[body]."
    <tr class='altcolora'>
    <td align='left' width='300'>Custom Signup Form Field 1</td>
    <td align='right'><input type='text' name='adedt[field1]' value='$adedt[field1]' maxlength='50'></td>
    </tr>
    <tr class='altcolorb'>
    <td align='left' width='300'>Custom Signup Form Field 2</td>
    <td align='right'><input type='text' name='adedt[field2]' value='$adedt[field2]' maxlength='50'></td>
    </tr>
    <tr class='altcolora'>
    <td align='left' width='300'>Custom Signup Form Field 3</td>
    <td align='right'><input type='text' name='adedt[field3]' value='$adedt[field3]' maxlength='50'></td>
    </tr>
    <tr class='altcolorb'>
    <td align='left' width='300'>Tournament Admins (Directors)</td>
    <td align='right'><input type='text' name='adedt[directors]' value='$adedt[directors]' maxlength='250'></td>
    </tr>
    <input type='hidden' name='adedt[password]' value='notneeded'>
    <input type='hidden' name='adedt[logo]' value='$adedt[logo]' maxlength='100'>
    <input type='hidden' name='adedt[bgcolor]' value='#000000'>
    <input type='hidden' name='adedt[tdcolor]' value='#808080'>
    <input type='hidden' name='adedt[fontcolor]' value='#ffffff'>
    <tr class='altcolora'>
    <td align='center' width='100%' colspan='2'>Tournament Description, Rules, etc.</td>
    </tr>
    <tr class='altcolorb'>
    <td align='center' width='100%' colspan='2'><textarea name='adedt[details]' rows='25' cols='90'>$adedt[details]</textarea></td>
    </tr>
    <tr class='altcolor'>
    <td align='center' width='600' colspan='2'>
    $actiontype
    <input type='submit' value='$addeditword Tournament'>
    </td></form>
    </tr>
    </table>
    </ul>
    $tablefoot";
    include("$dir[curtheme]");
}

function madmin_manage_add($adedt){
    global $config, $out, $site, $misc, $dir, $url, $file;
    while(list($varib,$value)=each($adedt)){
        $adedt[$varib]=characters($value);
    }

    if(!$adedt[name]){
        include("$dir[func]/error.php");
        display_error("Your tourney must have a name!");
    }

    if(!$adedt[size]){
        include("$dir[func]/error.php");
        display_error("Your tourney size is unknown");
    }

    if($adedt[ladderid] == ''){
        include("$dir[func]/error.php");
        display_error("You did not select a ladder.");
    }

    if((!$adedt[bgcolor])||(!$adedt[tdcolor])||(!$adedt[fontcolor])){
        include("$dir[func]/error.php");
        display_error("Your tourney must have a color scheme");
    }

    //TEMP
    $adedt[type]=1;
    mysql_query("INSERT INTO tourneys VALUES (
    'NULL',
    '$adedt[size]',
    '$adedt[type]',
    '$adedt[active]',
    '$adedt[name]',
    '$adedt[details]',
    '$adedt[field1]',
    '$adedt[field2]',
    '$adedt[field3]',
    '$adedt[directors]',
    '$adedt[password]',
    '$adedt[logo]',
    '$adedt[bgcolor]',
    '$adedt[tdcolor]',
    '$adedt[fontcolor]',
    '$adedt[ladderid]',
    '$adedt[open]');");
     include("$dir[func]/admin_finishmessage.php");
     display_message("The tournament has been created.<br>");
}

function madmin_manage_update($adedt){
    global $config, $out, $site, $misc, $dir, $url, $file;
    while(list($varib,$value)=each($adedt)){
        $adedt[$varib]=characters($value);
    }

    $adedt[password]=$adedt[password];
    if((!$adedt[name])||(!$adedt[password])){
        include("$dir[func]/error.php");
        display_error("<center>Your tourney must have a name and admin password");
    }

    if(!$adedt[size]){
        include("$dir[func]/error.php");
        display_error("<center>Your tourney size is unknown");
    }

    if($adedt[ladderid] == ''){
        include("$dir[func]/error.php");
        display_error("You did not select a ladder option");
    }

    if((!$adedt[bgcolor])||(!$adedt[tdcolor])||(!$adedt[fontcolor])){
        include("$dir[func]/error.php");
        display_error("<center>Your tourney must have a color scheme");
    }

    //TEMP
    $adedt[type]=1;
    mysql_query("UPDATE tourneys SET
    size='$adedt[size]',
    type='$adedt[type]',
    active='$adedt[active]',
    name='$adedt[name]',
    details='$adedt[details]',
    field1='$adedt[field1]',
    field2='$adedt[field2]',
    field3='$adedt[field3]',
    directors='$adedt[directors]',
    password='$adedt[password]',
    open='$adedt[open]',
    logo='$adedt[logo]',
    bgcolor='$adedt[bgcolor]',
    tdcolor='$adedt[tdcolor]',
    fontcolor='$adedt[fontcolor]'
    WHERE tid='$adedt[tid]'");
    include("$dir[func]/admin_finishmessage.php");
    display_message("The tournament has been updated.<br>");
}

function madmin_manage_delete($delete){
    global $config, $out, $site, $misc, $dir, $url, $file;
    mysql_query("DELETE FROM tourneys WHERE tid='$delete[tid]'");
    mysql_query("DELETE FROM players2 WHERE tid='$delete[tid]'");
    mysql_query("DELETE FROM brackets2 WHERE tid='$delete[tid]'");
    include("$dir[func]/admin_finishmessage.php");
    display_message("The tournament has been deleted.<br>");
}

function madmin_manage_reset($reset){
    global $config, $out, $site, $misc, $dir, $url, $file;
    if($reset[players]){
        mysql_query("DELETE FROM players2 WHERE tid='$reset[tid]'");
    }

    mysql_query("DELETE FROM brackets2 WHERE tid='$reset[tid]'");
    include("$dir[func]/admin_finishmessage.php");
    display_message("The tournament has been reset.<br>");
}

//START TOURNEY ADMIN FUNTIONS
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
function select_tournament(){
    global $config, $out, $site, $misc, $dir, $url;
    $tourneylist=mysql_query("SELECT tid,name FROM tourneys ORDER by name");
    while(list($tid,$name)=mysql_fetch_row($tourneylist)){
        $tourneylistid.="<option value='$tid'>$name</option>\n";
    }

    $tablehead=table_head("show","70%","","left");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot  <br>
    $tablehead &nbsp; &nbsp;<strong>
    <center>
    <table width='600' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <b>Select a Tournament</b><br>
    </td></tr>
    <tr><td align='center'>
    <form method='post'><br>
    <select name='tid'>$tourneylistid</select>
    <input type='hidden' name='action' value='edit_tournament'><br><br>
    <input type='submit' value='Submit'><br>
    </form></td></tr>
    <tr class='altcolor'><td><br></td></tr></table>
    $tablefoot";
    include("$dir[curtheme]");
}

function TourneyAdminHome($tid){
    global $config,$tinfo,$out,$url,$dir,$site;
    $check = mysql_fetch_array(mysql_query("SELECT open FROM tourneys WHERE tid='$tid'"));
    if($check[open] == '0'){
        include("$dir[func]/error.php");
        display_error("Tournament is closed.<br><br>It can only be re-opened via the admin control panel.");
    }

    $tablehead=table_head("show","70%","","left");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <script language='javascript'>var confirmdelete='This will delete all brackets details! Are you sure you wish to continue?';</script>
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot  <br>
    $tablehead &nbsp; &nbsp;<strong>
    <center>
    <table width='600' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class ='altcolor'>
    <td width='100%' valign='top' align='center' colspan='2'>
    <b>$tinfo[name]</b><br>
    </td></tr>
    <tr class='altcolorb'>
    <td align='left' width='300'><b>Manage Player Signups</b></td>
    <td align='center'><form method='post'>
    <input type='hidden' name='tid' value='$tid'>
    <input type='hidden' name='action' value='admin_players'>
    <input type='submit' value='Update'>
    </td></form>
    </tr>
    <tr class='altcolora'>
    <td align='left' width='300'><b>Manage Brackets with Drop Menus</b></td>
    <td align='center'><form method='post'>
    <input type='hidden' name='tid' value='$tid'>
    <input type='hidden' name='action' value='admin_brackets'>
    <input type='submit' value='Update'>
    </td></form>
    </tr>
    <tr class='altcolorb'>
    <td align='left' width='300'><b>Add Players to Brackets by Signup Order</b></td>
    <td align='center'><form method='post'>
    <input type='hidden' name='action' value='admin_autobracket'>
    <input type='hidden' name='tid' value='$tid'>
    <input type='submit' value='Update' onClick='return confirmdelete;'>
    </td></form>
    </tr>
    <tr class='altcolor'><td colspan='2'><br></td></tr>
    </table><br><br><br>
    </ul>
    $tablefoot";
    include("$dir[curtheme]");
}

function admin_players($edit,$tid){
    global $config,$tinfo,$admin,$out,$url,$dir;
    if($edit[pid]){
        $tourneyinfo=mysql_query("SELECT * FROM players2 WHERE pid='$edit[pid]' AND tid='$admin[tid]'");
        $adedt=mysql_fetch_array($tourneyinfo);
    }

    if($adedt[tid]){
        $actiontype="
        <input type='hidden' name='action' value='admin_players_update'>
        <input type='hidden' name='adedt[pid]' value='$adedt[pid]'>";
        $addeditword="Edit";
    }else{

        $actiontype="
        <input type='hidden' name='action' value='admin_players_add'>";
        $addeditword="Add";
    }

    $tablehead=table_head("show","70%","","left");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot  <br>
    $tablehead<strong>
    <br>
    ";
    if(!$adedt[pid]){
        $playerlist=mysql_query("SELECT pid,name FROM players2 WHERE tid='$admin[tid]' ORDER BY name");
        while(list($pid,$name)=mysql_fetch_row($playerlist)){
            $playerlistid.="<option value='$pid'>$name</option>\n";
        }

        $out[body]=$out[body]."
        <table width='600' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
        <tr class ='altcolor'>
        <td width='100%' valign='top' align='center' colspan='2'>
        <b>Delete a Team/Player</b>
        </td></tr>
        <tr class='altcolora'><td align='left' width='300'>
        <form method='post'>
        <b>Name</b></td>
        <td align='center' width='300'><select name='delete[pid]'>
        $playerlistid</select></td>
        </tr>
        <tr class='altcolor'>
        <td align='center' colspan='2' width='100%'>
        <script language='javascript'>var confirmdelete='Are you 100% sure you want to DELETE this Player';</script>
        <input type='hidden' name='action' value='admin_players_delete'>
        <input type='hidden' name='tid' value='$tid'>
        <input type='submit' value='Delete Team/Player' onClick='return confirm(confirmdelete);'>
        </td></form>
        </tr>
        </table>
        <br>
        ";
    }

    $out[body]=$out[body]."
    <table width='600' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class ='altcolor'>
    <td width='100%' valign='top' align='center' colspan='2'>
    <b>$addeditword a Team/Player</b>
    </td></tr>
    <tr class='altcolora'>
    <form method='post'>
    ";
    $ladderteams = mysql_query("SELECT id FROM ladder_$tinfo[ladderid]");
    while($ladderteam = mysql_fetch_array($ladderteams)){
        $teams = mysql_query("SELECT id,teamname FROM teams WHERE id ='$ladderteam[id]' ORDER by teamname");
        if($row=mysql_fetch_array($teams)){
            $theteams.="<option value='$row[teamname]' $selectedteam>$row[teamname]</option>";
        }

    }

    $out[body]=$out[body]."
    
    <form method='post'>
    <td align='left' width='300'><b>Name</b></td>
    <td align='center' width='300'><select name='adedt[name]'>$theteams</select></td>
    </tr>
    ";
    if($tinfo[field1]){
        $out[body]=$out[body]."
        <tr class='altcolorb'>
        <td align='left' width='300'>$tinfo[field1]</td>
        <td align='right'><input type='text' name='adedt[field1]' value='$adedt[field1]' maxlength='100'></td>
        </tr>
        ";
    }

    if($tinfo[field2]){
        $out[body]=$out[body]."
        <tr class='altcolora'>
        <td align='left' width='300'>$tinfo[field2]</td>
        <td align='right'><input type='text' name='adedt[field2]' value='$adedt[field2]' maxlength='100'></td>
        </tr>
        ";
    }

    if($tinfo[field1]){
        $out[body]=$out[body]."
        <tr class='altcolorb'>
        <td align='left' width='300'>$tinfo[field3]</td>
        <td align='right'><input type='text' name='adedt[field3]' value='$adedt[field3]' maxlength='100'></td>
        </tr>
        ";
    }

    $out[body]=$out[body]."
    <tr class='altcolor'>
    <td align='center' width='100%' colspan='2'>
    $actiontype
    <input type='hidden' name='tid' value='$tid'>
    <input type='submit' value='Add Team/Player'>
    </td></form>
    </tr>
    </table>
    </ul>
    $tablefoot";
    include("$dir[curtheme]");
}

function admin_players_add($adedt){
    global $config,$admin,$out,$dir,$url,$site,$file;
    while(list($varib,$value)=each($adedt)){
        $adedt[$varib]=characters($value);
        if(!$adedt[$varib]){
            include("$dir[func]/error.php");
            display_error("All fields are required");
        }

    }

    if(mysql_num_rows(mysql_query("SELECT pid FROM players2 WHERE tid='$admin[tid]' AND name='$adedt[name]'"))){
        include("$dir[func]/error.php");
        display_error("That Team Name is already in use on this tournament");
    }

    $teamid=mysql_query("SELECT id,teamemail FROM teams WHERE teamname='$adedt[name]'");
    $team=mysql_fetch_array($teamid);
    if(mysql_num_rows(mysql_query("SELECT pid FROM players2 WHERE tid='$admin[tid]' AND pid='$team[id]'"))){
        include("$dir[func]/error.php");
        display_error("That Team is already in use on this tournament");
    }

    mysql_query("INSERT INTO players2 VALUES (
    '$team[id]',
    '$admin[tid]',
    'Admin Add',
    '$config[stamp]',
    '$adedt[name]',
    '$team[teamemail]',
    '$adedt[field1]',
    '$adedt[field2]',
    '$adedt[field3]');");
     include("$dir[func]/admin_finishmessage.php");
     display_message("The player has been added.<br>");
}

function admin_players_update($adedt){
    global $config,$admin,$out,$url,$dir,$site,$file;
    while(list($varib,$value)=each($adedt)){
        $adedt[$varib]=characters($value);
        if(!$adedt[$varib]){
            include("$dir[func]/error.php");
            display_error("All fields are required");
        }

    }

    mysql_query("UPDATE players2 SET
    name='$adedt[name]',
    email='$adedt[email]',
    field1='$adedt[field1]',
    field2='$adedt[field2]',
    field3='$adedt[field3]'
    WHERE pid='$adedt[pid]' AND tid='$admin[tid]'");
    mysql_query("UPDATE brackets2 SET
    name='$adedt[name]'
    WHERE pid='$adedt[pid]' AND tid='$admin[tid]'");
    include("$dir[func]/admin_finishmessage.php");
    display_message("The player has been updated.<br>");
    include("$dir[curtheme]");
}

function admin_players_delete($delete){
    global $config,$admin,$out,$url,$dir,$site,$file;
    mysql_query("DELETE FROM players2 WHERE pid='$delete[pid]' AND tid='$admin[tid]'");
    mysql_query("DELETE FROM brackets2 WHERE pid='$delete[pid]' AND tid='$admin[tid]'");
    include("$dir[func]/admin_finishmessage.php");
    display_message("The player has been deleted.<br>");
    include("$dir[curtheme]");
}

function admin_brackets($btype,$tid){
    global $config,$tinfo,$admin,$out,$dir,$url;
    if($btype[text]){
        $brackets=mysql_query("SELECT bid,pid,name FROM brackets2 WHERE tid='$admin[tid]' ORDER BY bid");
        while(list($bid,$pid,$name)=mysql_fetch_row($brackets)){
            $bracket[$bid]="$name";
        }

    }else{

        $textedited=mysql_query("SELECT name FROM brackets2 WHERE pid='0' AND tid='$admin[tid]'ORDER BY name");
        while(list($name)=mysql_fetch_row($textedited)){
            $textplayers.="<option value='0|$name'>$name</option>";
        }

        if($textplayers){
            $textplayers="<option value=''>-TEXT FIELD ADDS-</option> $textplayers";
        }

        $players=mysql_query("SELECT pid,name FROM players2 WHERE tid='$admin[tid]' ORDER BY name");
        while(list($pid,$name)=mysql_fetch_row($players)){
            $playerlist.="<option value='$pid|$name'>$name</option>";
        }

        if($playerlist){
            $playerlist="<option value=''>-PLAYER SIGNUPS-</option> $playerlist";
        }

        $brackets=mysql_query("SELECT bid,pid,name FROM brackets2 WHERE tid='$admin[tid]' ORDER BY bid");
        while(list($bid,$pid,$name)=mysql_fetch_row($brackets)){
            $bracket[$bid]="<option value='$pid|$name' SELECTED>$name</option>";
        }

    }

    ////////////////////////////////
    $brackets="$tinfo[size]";
    //TEMP
    //$brackets="64";
    $spots=($brackets * 2 - 1);
    $temp_rows="$brackets";
    while($temp_rows > 1){
        $rowcount++;
        if($rowcount > 1){
            $temp_rows=($temp_rows / 2);
        }

        $listrows.="-> $temp_rows";
        $rows="$rowcount";
    }

    $rowwidths=round(100 / $rows - 0.5);
    $actualrows=($rows * 2);
    /*
    //UNCOMMENT SECTION FOR DEBUGGING
    echo"
    [Bracket Stats]<br>
    Brackets: $brackets<br>
    Spots: $spots<br>
    Game Rows: $rows<br>
    Actual Rows: $actualrows<br>
    Row Widths: $rowwidths<br>
    Pattern: $listrows<br>
    <hr>";
    */
    ////////////////////////////////
    $tablehead=table_head("show","70%","","left");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot  <br>
    $tablehead &nbsp; &nbsp;<strong>
    <table width='600' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class ='altcolor'>
    <td width='100%' valign='top' align='center'>
    <b>$tinfo[name] Brackets</b><br>
    </td></tr></table>
    <br>
    
    <br>
    <table border='0' width='100%' cellspacing='0' cellpadding='0'>
    <tr>
    <form method='post'>
    ";
    $counter="$brackets";
    while($counter > 1){
        if($round){
            $counter=($counter / 2);
        }

        $lastcb="$countbrackets";
        $countbrackets="";
        $game="";
        $round++;
        $finals=($rows - 1);
        $winner=($rows);
        $roundgamesay="Round $round Games";
        if($round=="$winner"){
            $roundgamesay="Winner!";
        }

        if($round=="$finals"){
            $roundgamesay="Finals";
        }

        $out[body]=$out[body]."
        <td align='center' valign='center' width='$rowwidths%' colspan='2'><b>$roundgamesay</b><br>
        <table border='0' width='100%' bordercolor='#FF0000' cellspacing='0' cellpadding='0'>
        ";
        $spacesetcount="";
        while($countbrackets < $counter){
            $countbrackets++;
            $spot++;
            $spacesetcount++;
            if($spacesetcount=="4"){
                $spacesetcount="";
            }

            $bracketid="$round"."_"."$countbrackets";
            if($btype[text]){
                $inputtype="<input type='text' name='brackets[$bracketid]' value='$bracket[$bracketid]' maxlength='20'>";
            }else{

                $inputtype="<select name='brackets[$bracketid]'>$bracket[$bracketid] $textplayers $playerlist</select>";
            }

            $out[body]=$out[body]."
            <tr class='altcolor'>
            <td align='left' valign='center' width='100%'>$inputtype</td>
            <td align='left' valign='center' width='100%'>&nbsp;</td>
            </tr>
            ";
            if($countbrackets < $counter){
                $spacecounter++;
                $spacecounter=($spacecounter + 1);
                if($spacecounter==2){
                    $roundx2p1=($lastroundx2p1 * 2 + 1);
                    if($round==1){
                        $roundx2p1="1";
                    }

                    while($countspaces < $roundx2p1){
                        $countspaces++;
                        $spacesetbgcolor="#$altcolor";
                        if((!$spacesetcount)||($spacesetcount=="2")){
                            $spacesetbgcolor="";
                        }

                        $spacecentercount++;
                        $spacecenterword="";
                        $alignspacer1="left";
                        $spacecentercountcheck=($lastroundx2p1 + 1);
                        if($spacecentercount=="$spacecentercountcheck"){
                            $spacecenterwordcount++;
                            if(($spacecenterwordcount==1)||($spacecenterwordcount==3)){
                                $actualgamecount++;
                                $spacecenterword="<b>Game $actualgamecount</b>";
                                $alignspacer1="center";
                                if($spacecenterwordcount==3){
                                    $spacecenterwordcount="";
                                    $spacecenterwordcount++;
                                }

                            }

                        }

                        $out[body]=$out[body]."
                        <tr>
                        <td align='$alignspacer1' valign='center'>&nbsp; $spacecenterword</td>
                        <td align='left' valign='center' bgcolor='$spacesetbgcolor'>&nbsp;</td>
                        </tr>
                        ";
                    }

                    $spacecounter="";
                    $countspaces="";
                    $spacecentercount="";
                }

            }

        }

        $spacecenterwordcount="";
        $actualgamecount="";
        $lastroundx2p1="$roundx2p1";
        $out[body]=$out[body]."
        </table>
        </td>
        ";
    }

    $out[body]=$out[body]."
    </tr>
    </table>
    <br>
    <br><br><br>
    <table width='600' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <input type='hidden' name='brackets[text]' value='$btype[text]'>
    <input type='hidden' name='action' value='admin_brackets_update'>
    <input type='hidden' name='tid' value='$tid'>
    <tr class ='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='submit' value='Update Brackets'>
    </td></tr></table></form>
    <br><br>
    $tablefoot";
    include("$dir[curtheme]");
}

function admin_brackets_update($brackets,$tid){
    global $config,$tinfo,$admin,$out,$url,$dir;
    mysql_query("DELETE FROM brackets2 WHERE tid='$admin[tid]'");
    while(list($bracketid,$playerinfo)=each($brackets)){
        if($brackets[text]){
            $nameid[1]="$playerinfo";
        }else{

            $nameid=split("\|",$playerinfo);
        }

        if(($nameid[1])&&($bracketid!="text")){
            mysql_query("INSERT INTO brackets2 VALUES (
            '$bracketid',
            '$admin[tid]',
            '$nameid[0]',
            '$nameid[1]');");
        }

    }

    $tablehead=table_head("show","70%","","left");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot  <br>
    $tablehead<strong>
    <center><br>
    <font class='catfont'><b>The Brackets have been updated!</b><br></font>
    </ul><table><tr><td>
    <form>
    <input type='hidden' name='action' value='admin_brackets'>
    <input type='hidden' name='tid' value='$tid'>
    <input type='submit' value='Return to Brackets'>
    </form></td><td>
    <form>
    <input type='hidden' name='action' value='admin_brackets_finish'>
    <input type='hidden' name='tid' value='$tid'>
    <input type='submit' value='Close Tournament'>
    </center>
    </form></td></tr></table>
    $tablefoot";
    include("$dir[curtheme]");
}

function admin_brackets_finish($tid){
    global $config,$tinfo,$admin,$out,$url,$dir;
    $i=0;
    $j=0;
    $p=0;
    $k=0;
    $winner = array();
    $team1 = array();
    $team2 = array();
    $report = array(id);
    $brackets=mysql_query("SELECT bid,pid,name FROM brackets2  WHERE tid='$admin[tid]' ORDER BY (SUBSTRING_INDEX(bid,'_',1)+0), (SUBSTRING_INDEX(bid,'_',-1)+0)");
    while($row= mysql_fetch_array($brackets) ){
        $bid = $row[bid];
        if(($bid[2]%2==0)&&($bid[2] == $game)&&($bid[0] ==$round)){
            $bracketid="$round"."_"."$countbrackets";
            $winner[$i] = ($round+1)."_".($bid[2]/2);
            $team1[$i] = $teama;
            $team2[$i] = $row[pid];
            $i++;
        }

        $teama = $row[pid];
        $round = $bid[0];
        $game  = $bid[2] + 1;
    }

    $tablehead=table_head("show","70%","","left");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot  <br>
    $tablehead<strong>
    <center>
    
    <b>Would you like to report the following matches played in the tournament?</b><br></font>
    </ul>
    <table width='400' cellspacing='1' cellpadding='1'>
    <tr class='altcolor'><td>#</td><td>Winner</td><td>Loser</td></tr>
    <form method=post>
    ";

    
    while($j<$i){
        $winningteam=mysql_fetch_array(mysql_query("SELECT pid,name FROM brackets2 WHERE bid='$winner[$j]' AND tid='$admin[tid]'"));
        if($winningteam[pid] == $team1[$j]){
            $losingteam = $team2[$j];
            $losingteamname=mysql_fetch_array(mysql_query("SELECT teamname FROM teams WHERE id='$losingteam'"));
            $k = $j + 1;

	    $out[body]=$out[body]." <tr class=altcolora'><td><b>$k</b></td><td><b>$winningteam[name]</b></td><td><b>$losingteamname[0]</b></td></tr>";
        
	}else if($winningteam[pid] == $team2[$j]){

            $losingteam = $team1[$j];
            $losingteamname=mysql_fetch_array(mysql_query("SELECT teamname FROM teams WHERE id='$losingteam'"));
            $k = $j + 1;
            $out[body]=$out[body]." <tr class=altcolora'><td><b>$k</b></td><td><b>$winningteam[name]</b></td><td><b>$losingteamname[0]</b></td></tr>";
        }

        $report[$p] = $winningteam[pid];
        $out[body]=$out[body]." <input type='hidden' name='report[$p]' value = '$report[$p]'>";
        $p++;
        $report[$p] = $losingteam;
        $out[body]=$out[body]." <input type='hidden' name='report[$p]' value = '$report[$p]'>";
        $p++;
        $j++;
    }

    $out[body]=$out[body]."
    </table>
    <br><br>
    <input type='hidden' name='action' value='admin_brackets_report'>
    <input type='hidden' name='tid' value='$tid'>
    <input type='submit' value='Close Tournament and Report Matches'>
    </form>
    <form>
    <input type='hidden' name='action' value='admin_brackets_close'>
    <input type='hidden' name='tid' value='$tid'>
    <input type='submit' value='Close Tournament Without Reporting Matches'>
    </form>
    </center>
    $tablefoot";
    include("$dir[curtheme]");
}

function admin_brackets_report($report){
    global $config,$tinfo,$admin,$out,$url,$dir,$site,$file;
    $size=count($report);
    $i=0;
    $ladderid = $tinfo[ladderid];
    if(!$ladderid){
        include("$dir[func]/error.php");
        display_error("Invalid Ladder ID<br>");
    }

    // ERROR CHECK
    while($i<$size){
        $wteamid = $report[$i];
        $i++;
        $lteamid = $report[$i];
        $i++;
        if((!$wteamid) || (!$lteamid)){
            include("$dir[func]/error.php");
            display_error("<b>No Winning/Losing Team For One Of The Matches. <br> All Reports Aborted. Edit Brackets And Try Again.</b><br>");
        }

        if($wteamid==$lteamid){
            include("$dir[func]/error.php");
            display_error("<b>Winning Team And Losing Team The Same For One Of The Matches.<br> All Reports Aborted. Edit Brackets And Try Again.</b><br>");
        }

    }

    $i=0;
    include("$dir[func]/rankadjust.php");
    //REPORT
    while($i<$size){
        $wteamid = $report[$i];
        $i++;
        $lteamid = $report[$i];
        $i++;
        $ladderinfo=mysql_query("SELECT laddername FROM ladders WHERE id='$ladderid'");
        $ladder=mysql_fetch_array($ladderinfo);
        //GET WINNER STATS
        $winnerteam=mysql_query("SELECT teamname FROM teams WHERE id='$wteamid'");
        $winner=mysql_fetch_array($winnerteam);
        $winteaminfo=mysql_query("SELECT status,rank,games,percent FROM ladder_$ladderid WHERE id='$wteamid'");
        $wtinfo=mysql_fetch_array($winteaminfo);
        //GET LOSER STATS
        $loserteam=mysql_query("SELECT teamname FROM teams WHERE id='$lteamid'");
        $loser=mysql_fetch_array($loserteam);
        $losteaminfo=mysql_query("SELECT status,rank,games,percent FROM ladder_$ladderid WHERE id='$lteamid'");
        $ltinfo=mysql_fetch_array($losteaminfo);
        // GET SKILL INFO
        if($wtinfo[rank] > $ltinfo[rank]){
            $rankdifference=($wtinfo[rank] - $ltinfo[rank]);
        }else{

            $rankdifference="1";
        }

        if($ltinfo[percent]){
            $percentoratio=($wtinfo[percent] / $ltinfo[percent]);
        }else{

            $percentoratio="1";
        }

        $skilladjust=($rankdifference * $percentoratio / 10);
        $skillfactor=($skilladjust + $wtinfo[games] / 100);
        if($skillfactor > 20){
            $gainedskill="20";
        }else{

            $gainedskill=round($skillfactor);
        }

        //IF BOTH RANKED AND LOSER WAS RANKED BETTER THAN WINNER, WINNER GETS EXTRA POINTS
        if(($wtinfo[rank] > 0) && ($ltinfo[rank] > 0) && ($wtinfo[rank] > $ltinfo[rank])){
            $trankedplayers=mysql_query("SELECT COUNT(*) FROM ladder_$ladderid WHERE rank > '0'");
            $trplayers=mysql_fetch_array($trankedplayers);
            $rankedplayers="$rplayers[0]";
            $gainedpoints=($rankedplayers - $ltinfo[rank]);
            if($gainedpoints < 10){
                $gainedpoints=($gainedpoints + 10);
            }

        }else{

            //GET FLAT RATE OF 10 POINTS
            $gainedpoints="10";
        }

        //ADD TO UNVALIDATED MATCHES
        $tday=date("Y-m-d H:i:s");
        $mcode=md5(uniqid(microtime()));
        $mcode="$mcode";
        $ip=getenv("REMOTE_ADDR");
        mysql_query("INSERT INTO matchdb VALUES (
        '$mcode',
        '$wteamid',
        '$lteamid',
        '$winner[teamname]',
        '$loser[teamname]',
        '$ladderid',
        '$ladder[laddername]',
        '$wtinfo[rank]',
        '$ltinfo[rank]',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '0',
        '$tday',
        '$ip',
        '0',
        '$tday',
        '$ip');");
        $getcode=mysql_query("SELECT matchid FROM matchdb WHERE winnerid='$wteamid' AND loserid='$lteamid'");
        $match=mysql_fetch_array($getcode);
        $matchcode="$match[matchid]";
        //GET WINNERS LADDER STATS
        $winnerladder=mysql_query("SELECT * FROM ladder_$ladderid WHERE id='$wteamid'");
        $wlinfo=mysql_fetch_array($winnerladder);
        //GET LOSERS LADDER STATS
        $loserladder=mysql_query("SELECT * FROM ladder_$ladderid WHERE id='$lteamid'");
        $llinfo=mysql_fetch_array($loserladder);
        //GET TOTAL RANKERS
        $totalranked=mysql_query("SELECT COUNT(*) FROM ladder_$ladderid WHERE rank > '0'");
        $totalranks=mysql_fetch_array($totalranked);
        $totalranks="$totalranks[0]";
        //UPDATE LADDER RANKS
        if(($wtinfo[rank] > 0) && ($ltinfo[rank] > 0)){
            $wtempnewrank=($wtinfo[rank] + $ltinfo[rank]);
            $wtempnewrank=($wtempnewrank / 2 - 0.5);
            if($wtempnewrank > $totalranks){
                $wtempnewrank="$totalranks";
            }

        }

        else if(($wtinfo[rank] < 1) && ($ltinfo[rank] > 0)){
            $wtempnewrank=($totalranks + 1);
        }

        else if(($wtinfo[rank] < 1) && ($ltinfo[rank] < 1)){
            $wtempnewrank=($totalranks + 1);
        }else{

            $wtempnewrank="";
        }

        $wtempnewrank=round($wtempnewrank);
        //IF RANK IS BETTER THAN CURRENT RANK UPDATE LADDER
        if(($wtempnewrank) && ($wtempnewrank > 0)){
            if(($wtempnewrank < $wlinfo[rank]) || ($wlinfo[rank] < 1)){
                update_teamranks($ladderid,$wteamid,$wtempnewrank);
                $updatedrank="$wtempnewrank";
            }

        }

        //UPDATE WINNERS LADDER STATS
        if($updatedrank){
            $newlastrank="$wlinfo[rank]";
            if(($updatedrank < $wlinfo[bestrank]) || ($wlinfo[bestrank] < 1)){
                $newbestrank="$updatedrank";
            }else{

                $newbestrank="$wlinfo[bestrank]";
            }

        }else{

            $newlastrank="$wlinfo[lastrank]";
            $newbestrank="$wlinfo[bestrank]";
        }

        $newwins=($wlinfo[wins] + 1);
        $newgames=($newwins + $wlinfo[losses]);
        $newpercent=round($newwins / $newgames * 100);
        $newstreak=($wlinfo[streak] + 1);
        if($newstreak < 1){
            $newstreak="1";
        }

        if($newstreak > $wlinfo[beststreak]){
            $newbeststreak="$newstreak";
        }else{

            $newbeststreak="$wlinfo[beststreak]";
        }

        if($llinfo[rank] > 0){
            $newpoints=($totalranks - $llinfo[rank]);
            $newpoints=round($newpoints / 10);
        }

        if($newpoints < 10){
            $newpoints="10";
        }

        $newskill=($wlinfo[skill] + $totalkills);
        $newpoints=($newpoints + $wlinfo[points]);
        mysql_query("UPDATE ladder_$ladderid SET
        lastmatch='$tday',
        lastplayed='$lteamid',
        statusdisplay='',
        lastrank='$newlastrank',
        bestrank='$newbestrank',
        wins='$newwins',
        games='$newgames',
        percent='$newpercent',
        streak='$newstreak',
        beststreak='$newbeststreak',
        points='$newpoints',
        skill='$newskill'
        WHERE id='$wteamid'");
        //UPDATE LOSERS LADDER STATS
        $newlastrank="$llinfo[rank]";
        if(($llinfo[rank] < $llinfo[bestrank]) || ($llinfo[bestrank]=="0")){
            $newbestrank="$llinfo[rank]";
        }else{

            $newbestrank="$llinfo[bestrank]";
        }

        if($llinfo[rank] > $llinfo[worstrank]){
            $newworstrank="$llinfo[rank]";
        }else{

            $newworstrank="$llinfo[worstrank]";
        }

        $newlosses=($llinfo[losses] + 1);
        $newgames=($newlosses + $llinfo[wins]);
        $newpercent=round($llinfo[wins] / $newgames * 100);
        $newstreak=($llinfo[streak] - 1);
        if($newstreak > 0){
            $newstreak="-1";
        }

        if($newstreak < $llinfo[worststreak]){
            $newworststreak="$newstreak";
        }else{

            $newworststreak="$llinfo[worststreak]";
        }

        $newskill=($llinfo[skill] + $match[skill]);
        mysql_query("UPDATE ladder_$ladderid SET
        lastmatch='$tday',
        lastplayed='$wteamid',
        statusdisplay='',
        lastrank='$newlastrank',
        bestrank='$newbestrank',
        worstrank='$newworstrank',
        losses='$newlosses',
        games='$newgames',
        percent='$newpercent',
        streak='$newstreak',
        worststreak='$newworststreak',
        skill='$newskill'
        WHERE id='$lteamid'");
        $ranksadjusted=rank_checkadjust($ladderid);
    }

    mysql_query("UPDATE tourneys SET open='0' WHERE tid='$admin[tid]'");
    include("$dir[func]/admin_finishmessage.php");
    display_message("All Matches Have Been Reported. <br><br>Tournament is now closed and cannot be edited further without re-opening.<br>");
    include("$dir[curtheme]");
}

function admin_autobracket(){
    global $config,$tinfo,$admin,$out,$url,$dir,$site,$file;
    mysql_query("DELETE FROM brackets2 WHERE tid='$admin[tid]'");
    $players=mysql_query("SELECT pid,name FROM players2 WHERE tid='$admin[tid]' ORDER BY joined LIMIT $tinfo[size]");
    while(list($pid,$name)=mysql_fetch_row($players)){
        $count++;
        mysql_query("INSERT INTO brackets2 VALUES (
        '1_$count',
        '$admin[tid]',
        '$pid',
        '$name');");
    }
    include("$dir[func]/admin_finishmessage.php");
    display_message("The brackets have been automatically set.<br>");
    include("$dir[curtheme]");
}

function close_tournament(){
    global $config,$tinfo,$admin,$out,$url,$dir,$site,$file;
    mysql_query("UPDATE tourneys SET open='0' WHERE tid='$admin[tid]'");
    include("$dir[func]/admin_finishmessage.php");
    display_message("Tournament is now closed and cannot be edited further.<br>The results were not reported.<br>");
    include("$dir[curtheme]");
}

?>
