<?PHP
if(!IsSet($indexloaded)){
    header("Location: ./index.php");
}

if($admn[access] < 85){
    include("$dir[func]/error.php");
    display_error("You are not allowed to perform this function.<br>");
}

function admin_settings(){
    global $dir, $url, $out, $site, $admn, $theme;

    $settings = mysql_fetch_array(mysql_query("SELECT * FROM settings"));
    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <form method='post'>
    <td width='100%' valign='top' align='center' colspan='3'>
    <strong>Configuracion del Sitio</strong><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center' colspan='3'>
    <br></td>
    </tr>
    <tr class='altcolor'><td colspan='3' align='center'><b>Configuracion General</b></td></tr>
    <tr class='altcolora'>
    <td width='25%' valign='center' align='left'>
    Email del sitio</td>
    <td width='25%' valign='center' align='left'>
    <input type='text' name='email' value='$settings[email]' size='30' maxlength='40'></td>
    <td width='50%'>Email</td>
    </tr>
    <tr class='altcolorb'>
    <td width='25%' valign='center' align='left'>
    Nombre Corto del sitio</td>
    <td width='25%' valign='center' align='left'>
    <input type='text' name='shortname' value='$settings[shortname]' size='5' maxlength='5'></td>
    <td width='50%'>Nombre Corto del sitio</td>
    </tr>
    <tr class='altcolora'>
    <td width='25%' valign='center' align='left'>
    Nombre Largo del sitio</td>
    <td width='25%' valign='center' align='left'>
    <input type='text' name='longname' value='$settings[longname]' size='25' maxlength='25'></td>
    <td width='50%'>Nombre Largo del sitio</td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$settings[theme];
    $selected[$value]="SELECTED";
    $themeoption1 = $theme[option1];
    $themeoption2 = $theme[option2];
    $themeoption3 = $theme[option3];
    $themeoption4 = $theme[option4];
    $themeoption5 = $theme[option5];
    $out[body]=$out[body]."
    <tr class='altcolorb'>
    <td width='20%' valign='center' align='left'>
    Theme por defecto</td>
    <td width='20%' valign='center' align='left'>
    <select name='default_theme'><option value='$theme[option1]' $selected[$themeoption1]>$theme[option1]</option><option value='$theme[option2]' $selected[$themeoption2]>$theme[option2]</option><option value='$theme[option3]' $selected[$themeoption3]>$theme[option3]</option><option value='$theme[option4]' $selected[$themeoption4]>$theme[option4]</option><option value='$theme[option5]' $selected[$themeoption5]>$theme[option5]</option></select>
    </td> <td>Theme por defecto</td>
    </tr>
    <tr class='altcolora'>
    <td width='20%' valign='center' align='left'>
    URL Foro</td>
    <td width='20%' valign='center' align='left'>
    <input type='text' name='forumurl' value='$settings[forumurl]' size='30' maxlength='65'></td>
    <td>URL Foro (Opcional)</td>
    </tr>
    <tr class='altcolorb'>
    <td width='20%' valign='center' align='left'>
    URL Imagen del Banner</td>
    <td width='20%' valign='center' align='left'>
    <input type='text' name='bannerloc' value='$settings[bannerloc]' size='30' maxlength='100'></td>
    <td>URL Imagen del Banner</td>
    </tr>
    <tr class='altcolora'>
    <td width='20%' valign='center' align='left'>
    Direccion del Banner</td>
    <td width='20%' valign='center' align='left'>
    <input type='text' name='bannerurl' value='$settings[bannerurl]' size='30' maxlength='65'></td>
    <td>Direccion del Banner</td>
    </tr>
    <tr class='altcolor'><td colspan='3' align='center'><b>Advanced Settings</b></td></tr>
    <tr class='altcolorb'>
    <td width='20%' valign='center' align='left'>
    Edad minima</td>
    <td width='20%' valign='center' align='left'>
    <input type='text' name='joinage' value='$settings[joinage]' size='2' maxlength='40'></td>
    <td> Minima edad permitida</td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$settings[multipleip];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolora'>
    <td width='20%' valign='center' align='left'>
    Multiples IP</td>
    <td width='20%' valign='center' align='left'>
    <select name='multipleip'><option value='yes' $selected[yes]>Si</option><option value='no' $selected[no]>No</option></select>
    </td> <td>Permitir la creaci&oacute;n de m&uacute;ltiples cuentas desde la misma ip</td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$settings[tourneyip];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolorb'>
    <td width='20%' valign='center' align='left'>
    IP Torneo</td>
    <td width='20%' valign='center' align='left'>
    <select name='tourneyip'><option value='yes' $selected[yes]>Si</option><option value='no' $selected[no]>No</option></select>
    </td> <td>Permitir la inscripci&oacute;n a m&uacute;ltiples torneos desde la misma ip</td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$settings[changeplyrname];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolora'>
    <td width='20%' valign='center' align='left'>
    Cambiar nombre del jugador</td>
    <td width='20%' valign='center' align='left'>
    <select name='changeplyrname'><option value='yes' $selected[yes]>Si</option><option value='no' $selected[no]>No</option></select>
    </td><td>Permitir al jugador cambiar su nombre</td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$settings[changeteamname];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolorb'>
    <td width='20%' valign='center' align='left'>
    Cambiar nombre del clan</td>
    <td width='20%' valign='center' align='left'>
    <select name='changeteamrname'><option value='yes' $selected[yes]>Si</option><option value='no' $selected[no]>No</option></select>
    </td><td>Permitir al clan cambiar su nombre</td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$settings[deleteteam];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolora'>
    <td width='20%' valign='center' align='left'>
    Borrar clan</td>
    <td width='20%' valign='center' align='left'>
    <select name='deleteteam'><option value='yes' $selected[yes]>Si</option><option value='no' $selected[no]>No</option></select>
    </td><td>Permitir al clan ser borrado. Es muy recomendable que la respuesta sea 'no', ya que puede causar problemas.</td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$settings[leaveladder];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolorb'>
    <td width='20%' valign='center' align='left'>
    Dejar un ladder</td>
    <td width='20%' valign='center' align='left'>
    <select name='leaveladder'><option value='yes' $selected[yes]>Si</option><option value='no' $selected[no]>No</option></select>
    </td><td>Permitir al clan salirse de un ladder.</td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$settings[challengedown];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolora'>
    <td width='20%' valign='center' align='left'>
    Rechazar desaf&iacute;o</td>
    <td width='20%' valign='center' align='left'>
    <select name='challengedown'><option value='yes' $selected[yes]>Si</option><option value='no' $selected[no]>No</option></select>
    </td><td>Permitir rechazar desaf&iacute;os</td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$settings[losercomment];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolorb'>
    <td width='20%' valign='center' align='left'>
    Comentario del perdedor</td>
    <td width='20%' valign='center' align='left'>
    <select name='losercomment'><option value='yes' $selected[yes]>Si</option><option value='no' $selected[no]>No</option></select>
    </td><td>Permitir al perdedor comentar en el reporte del partido</td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$settings[winnercomment];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolora'>
    <td width='20%' valign='center' align='left'>
    Comentario del ganador</td>
    <td width='20%' valign='center' align='left'>
    <select name='winnercomment'><option value='yes' $selected[yes]>Si</option><option value='no' $selected[no]>No</option></select>
    </td><td>Permitir al ganador comentar en el reporte del partido</td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$settings[invitemembers];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolorb'>
    <td width='20%' valign='center' align='left'>
    Invitar miembros</td>
    <td width='20%' valign='center' align='left'>
    <select name='invitemembers'><option value='yes' $selected[yes]>Si</option><option value='no' $selected[no]>No</option></select>
    </td><td>Si = invitar ; No = a&nacute;adir sin invitar</td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$settings[allow];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolora'>
    <td width='20%' valign='center' align='left'>
    Permitir nformaci&oacute;n de Usuario</td>
    <td width='20%' valign='center' align='left'>
    <select name='allow'><option value='yes' $selected[yes]>Si</option><option value='no' $selected[no]>No</option></select>
    </td><td>Preguntar por m&aacute;s informaci&oacute;n al usuario al momento del registro</td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$settings[uforce];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolorb'>
    <td width='20%' valign='center' align='left'>
    Forzar nformaci&oacute;n de Usuario</td>
    <td width='20%' valign='center' align='left'>
    <select name='uforce'><option value='yes' $selected[yes]>Si</option><option value='no' $selected[no]>No</option></select>
    </td><td> Requerir m&aacute;s informaci&oacute;n al usuario al momento del registro</td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$settings[edit];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolora'>
    <td width='20%' valign='center' align='left'>
    Editar Informaci&oacute;n de Usuario</td>
    <td width='20%' valign='center' align='left'>
    <select name='edit'><option value='yes' $selected[yes]>Si</option><option value='no' $selected[no]>No</option></select>
    </td><td> Permitir al jugar editar su informaci&oacute;n despu&eacute;s del registro</td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$settings[newplayer];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolorb'>
    <td width='20%' valign='center' align='left'>
    Nuevo Jugador</td>
    <td width='20%' valign='center' align='left'>
    <select name='newplayer'><option value='yes' $selected[yes]>Si</option><option value='no' $selected[no]>No</option></select>
    </td><td> Permitir registros, no se desactivar&aacute;n las inscripciones</td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$settings[dobans];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolora'>
    <td width='20%' valign='center' align='left'>
    Bans</td>
    <td width='20%' valign='center' align='left'>
    <select name='dobans'><option value='yes' $selected[yes]>Si</option><option value='no' $selected[no]>No</option></select>
    </td><td> Permitir a los admins banear jugadores</td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$settings[docensors];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolorb'>
    <td width='20%' valign='center' align='left'>
    Censuras</td>
    <td width='20%' valign='center' align='left'>
    <select name='docensors'><option value='yes' $selected[yes]>Si</option><option value='no' $selected[no]>No</option></select>
    </td><td> Permitir censurar algunas palabras</td>
    </tr>
    <tr class='altcolora'>
    <td width='20%' valign='center' align='left'>
    Caracter de censura</td>
    <td width='20%' valign='center' align='left'>
    <input type='text' name='censorschar' value='$settings[censorschar]' size='1' maxlength='1'></td>
    <td>Caracter utilizado para censurar palabras</td>
    </tr>
    <tr class='altcolorb'>
    <td width='20%' valign='center' align='left'>
    Sesi&oacute;n</td>
    <td width='20%' valign='center' align='left'>
    <input type='text' name='default_session' value='$settings[session]' size='3' maxlength='3'></td>
    <td>Tiempo de espera en segundos para usuarios logeados. Ingresar un 0 para no tener tiempo de espera</td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$settings[passrequests];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolora'>
    <td width='20%' valign='center' align='left'>
    Solicitudes de Contrase&nacute;a</td>
    <td width='20%' valign='center' align='left'>
    <select name='passrequests'><option value='yes' $selected[yes]>Si</option><option value='no' $selected[no]>No</option></select>
    </td><td>Permitir la retribuci&oacute;n de contrase&nacute;as olvidadas</td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$settings[flags];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolorb'>
    <td width='20%' valign='center' align='left'>
    Banderas</td>
    <td width='20%' valign='center' align='left'>
    <select name='flags'><option value='yes' $selected[yes]>Si</option><option value='no' $selected[no]>No</option></select>
    </td>
    <td>Permitir banderas para usuarios y clanes</td>
    </tr>
    <tr class='altcolora'>
    <td width='20%' valign='center' align='left'>
    Base de Datos de Usuario</td>
    <td width='20%' valign='center' align='left'>
    <input type='text' name='playerdb' value='$settings[playerdb]' size='3' maxlength='3'></td>
    <td>Numero de jugadores por p&aacute;gina en la base de datos de jugador.</td>
    </tr>
    <tr class='altcolorb'>
    <td width='20%' valign='center' align='left'>
    Base de Datos de clan</td>
    <td width='20%' valign='center' align='left'>
    <input type='text' name='teamdb' value='$settings[teamdb]' size='3' maxlength='3'></td>
    <td>Numero de clanes por p&aacute;gina en la base de datos de clan.</td>
    </tr>
    <tr class='altcolora'>
    <td width='20%' valign='center' align='left'>
    Base de Datos de Ladder</td>
    <td width='20%' valign='center' align='left'>
    <input type='text' name='ladderdb' value='$settings[ladderdb]' size='3' maxlength='3'></td>
    <td>Numero de clanes por p&aacute;gina  en la base de datos de ladder.</td>
    </tr>
    <tr class='altcolorb'>
    <td width='20%' valign='center' align='left'>
    Posiciones</td>
    <td width='20%' valign='center' align='left'>
    <input type='text' name='standings' value='$settings[standings]' size='3' maxlength='3'></td>
    <td>Numero de clanes por p&aacute;gina en la base de datos de posiciones.</td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$settings[uservalidate];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolora'>
    <td width='20%' valign='center' align='left'>
    Validaci&oacute;n de Usuario</td>
    <td width='20%' valign='center' align='left'>
    <select name='uservalidate'><option value='yes' $selected[yes]>Si</option><option value='no' $selected[no]>No</option></select>
    </td><td>Si= Usuario valida su propia cuenta, No = Administrador valida la cuenta de usuario antes de su uso</td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$settings[headline];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolorb'>
    <td width='20%' valign='center' align='left'>
    Titular</td>
    <td width='20%' valign='center' align='left'>
    <select name='headline'><option value='yes' $selected[yes]>Si</option><option value='no' $selected[no]>No</option></select>
    </td><td>Si= Titular solo en la p&aacute;gina principal. No = Art&iacute;culo y titular en la p&aacute;gina principal</td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$settings[ranking];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolora'>
    <td width='20%' valign='center' align='left'>
    Ranking</td>
    <td width='20%' valign='center' align='left'>
    <select name='ranking'><option value='1' $selected[1]>1</option><option value='0' $selected[0]>2</option></select>
    </td><td>Partidos no desafiados: 1 = Ganador sube la mitad de la diferencia de posiciones con el perdedor (si es mayor). 2 = Ganador toma la posici&oacute;n del perdedor (si es mayor)</td>
    </tr>
    ";
    $selected="";
    $value="";
    $value=$settings[challranking];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolorb'>
    <td width='20%' valign='center' align='left'>
     Ranking de desaf&iacute;os</td>
    <td width='20%' valign='center' align='left'>
    <select name='challranking'><option value='1' $selected[1]>1</option><option value='0' $selected[0]>2</option></select>
    </td><td>Partidos desafiados: 1= Ganador toma la posici&oacute;n del perdedor (si es mayor). 2=  Ganador sube la mitad de la diferencia de posiciones con el perdedor (si es mayor)</td>
    </tr>
    <tr class='altcolor'><td colspan='3' align='center'><b>Configuraci&oacute;n SMTP: No editar a menos que se este usando un servidor SMTP.</b></td></tr>
    ";
    $selected="";
    $value="";
    $value=$settings[localhost];
    $selected[$value]="SELECTED";
    $out[body]=$out[body]."
    <tr class='altcolora'>
    <td width='20%' valign='center' align='left'>
    Localhost</td>
    <td width='20%' valign='center' align='left'>
    <select name='localhost'><option value='yes' $selected[yes]>Si</option><option value='no' $selected[no]>No</option></select>
    </td><td>Dejar en no si se esta usando SMTP; de otra manera, no modificar</td>
    </tr>
    <tr class='altcolorb'>
    <td width='20%' valign='center' align='left'>
    Servidor SMTP</td>
    <td width='20%' valign='center' align='left'>
    <input type='text' name='smtp_sever' value='$settings[smtp_server]' size='30' maxlength='55'></td>
    <td>URL &oacute; IP para el servidor SMTP</td>
    </tr>
    <tr class='altcolora'>
    <td width='20%' valign='center' align='left'>
    Usuario SMTP</td>
    <td width='20%' valign='center' align='left'>
    <input type='text' name='username' value='$settings[username]' size='25' maxlength='25'></td>
    <td>Usuario para el servidor SMTP</td>
    </tr>
    <tr class='altcolorb'>
    <td width='20%' valign='center' align='left'>
    Contrase&nacute;a SMTP</td>
    <td width='20%' valign='center' align='left'>
    <input type='text' name='password' value='$settings[password]' size='25' maxlength='25'></td>
    <td>Contrase&nacute;a para el servidor SMTP</td>
    </tr>


    <tr class='altcolora'>
    <td valign='center' align='center' colspan='3'>
    <input type='hidden' name='action' value='settings_update'>
    <input type='submit' name='' value='Submit'><td>
    </tr> </td
    </form>
    </table>

    $tablefoot";
    include("$dir[curtheme]");
}

function settings_update($email,$shortname,$longname,$default_theme,$forumurl,$bannerloc,$bannerurl,$joinage,$multipleip,$tourneyip,$changeplyrname,$changeteamname,$deleteteam,$leaveladder,$challengedown,$losercomment,$winnercomment,$invitemembers,$allow,$uforce,$edit,$newplayer,$dobans,$docensors,$censorschar,$default_session,$passrequests,$flags,$playerdb,$teamdb,$ladderdb,$standings,$uservalidate,$headline,$ranking,$challranking,$localhost,$smtp_server,$username,$password){
    global $dir, $url, $out, $admn, $site;
    include("$dir[func]/checkdata.php");

    if($joinage < 1){
	  $errormessage=$errormessage."Edad m&iacute;nima debe ser mayor que 0.<br>";
        error_check($errormessage);
    }

    if(($censorschar =='')&&($docensors=="yes")){
	  $errormessage=$errormessage."No seleccionaste un caracter de censura.<br>";
        error_check($errormessage);
    }

    if($session < 0){
	  $errormessage=$errormessage."Sesi&oacute;n debe ser un valor positivo.<br>";
        error_check($errormessage);
    }

    if($playerdb < 1){
	  $errormessage=$errormessage."Base de datos del jugador debe ser mayor que 0.<br>";
        error_check($errormessage);
    }

    if($teamdb < 1){
	  $errormessage=$errormessage."Base de datos del clan debe ser mayor que 0.<br>";
        error_check($errormessage);
    }

    if($ladderdb < 1){
	  $errormessage=$errormessage."Base de datos del ladder debe ser mayor que 0.<br>";
        error_check($errormessage);
    }

    mysql_query("UPDATE settings SET
    email = '$email',
    shortname='$shortname',
    longname='$longname',
    theme = '$default_theme',
    forumurl = '$forumurl',
    bannerloc = '$bannerloc',
    bannerurl = '$bannerurl',
    joinage='$joinage',
    multipleip = '$multipleip',
    tourneyip = '$tourneyip',
    changeplyrname = '$changeplyrname',
    changeteamname='$changeteamname',
    deleteteam='$deleteteam',
    leaveladder='$leaveladder',
    challengedown='$challengedown',
    losercomment='$losercomment',
    winnercomment='$winnercomment',
    invitemembers='$invitemembers',
    allow='$allow',
    uforce='$uforce',
    edit='$edit',
    newplayer='$newplayer',
    dobans='$dobans',
    docensors = '$docensors',
    censorschar='$censorschar',
    session='$default_session',
    passrequests='$passrequests',
    flags='$flags',
    playerdb='$playerdb',
    teamdb='$teamdb',
    ladderdb='$ladderdb',
    standings='$standings',
    uservalidate='$uservalidate',
    headline='$headline',
    ranking='$ranking',
    challranking='$challranking',
    localhost='$localhost',
    smtp_server='$smtp_server',
    username='$username',
    password='$password'");

    include("$dir[func]/admin_finishmessage.php");
    display_message("La configuraci&oacute;n ha sido actualizada.<br>");
}

?>
