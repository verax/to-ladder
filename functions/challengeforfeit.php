<?
function challenge_forfeit($challid){
    global $dir, $file, $url, $out, $plyr;
    if(!$challid){
        include("$dir[func]/error.php");
        display_error("Invalid Challenge ID.<br>");
    }

    $challengeinfo=mysql_query("SELECT * FROM challenges WHERE challid='$challid'");
    $chall=mysql_fetch_array($challengeinfo);
    if(!$chall[challenger]){
        include("$dir[func]/error.php");
        display_error("Unknown Challenge ID.<br>");
    }

    if($chall[finalizedby] > 0){
        include("$dir[func]/error.php");
        display_error("This match has already been finalized.<br>");
    }

    $teamembers=mysql_query("SELECT teamid FROM teammembers WHERE teamid='$chall[challenger]' AND playerid='$plyr[id]' AND status <= '3' OR teamid='$chall[challenged]' AND playerid='$plyr[id]' AND status <= '3'");
    $tmem=mysql_fetch_array($teamembers);
    if($tmem[teamid]=="$chall[challenger]"){
        //CHALLENGER FORFEIT
        $winnerid="$chall[challenged]";
        $winrank="$chall[challengedrank]";
        $loserid="$chall[challenger]";
        $loserank="$chall[challengerrank]";
        $ladderid="$chall[ladderid]";
    }

    else if($tmem[teamid]=="$chall[challenged]"){
        //CHALLENGED FORFEIT
        $winnerid="$chall[challenger]";
        $winrank="$chall[challengerrank]";
        $loserid="$chall[challenged]";
        $loserank="$chall[challengedrank]";
        $ladderid="$chall[ladderid]";
    }

    else{
        include("$dir[func]/error.php");
        display_error("You cannot forfeit challenges for this team.<br>");
    }

    create_forfeit($challid,$ladderid,$winnerid,$loserid,$winrank,$loserank);
    $out[body]=$out[body]."
    <br><br>
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <strong>Forfeited</strong><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='left'>
    <br><ul>
    The challenge was forfeited.</ul></td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    &nbsp;</td>
    </tr>
    </table>";
    include("$dir[curtheme]");
}

//NEXT 2 FUNCTIONS DELETES AND AWARDS WINS FOR UNRESPONDED OR UNFINALIZED CHALLENGES
function check_unrespondedchallenges(){
    $challengeladders=mysql_query("SELECT id,respondhours,finalizehours FROM ladders WHERE challenge='1'");
    while(list($id,$respondhours,$finalizehours)=mysql_fetch_row($challengeladders)){
        $laterespond=date("Y-m-d H:i:s",time()-60*60*$respondhours);
        $latefinalize=date("Y-m-d H:i:s",time()-60*60*$finalizehours);
        $challengeinfo=mysql_query("SELECT challid,challenger,challenged,ladderid,challengerrank,challengedrank,respondedby,finalizedby FROM challenges WHERE
        ladderid='$id' AND respondedby='0' AND finalizedby='0' AND challengedtime < '$laterespond' OR
        ladderid='$id' AND respondedby > '0' AND finalizedby='0' AND respondedtime < '$latefinalize'");
        while(list($challid,$challenger,$challenged,$ladderid,$challengerrank,$challengedrank,$respondedby,$finalizedby)=mysql_fetch_row($challengeinfo)){
            //DIDNT RESPOND IN TIME
            if(($respondedby=="0") && ($finalizedby=="0")){
                create_forfeit($challid,$id,$challenger,$challenged,$challengerrank,$challengedrank);
            }

            //DIDNT FINALIZE IN TIME
            if (($respondedby > 0) && ($finalizedby=="0")){
                create_forfeit($challid,$id,$challenged,$challenger,$challengedrank,$challengerrank);
            }

        }

    }

}

function create_forfeit($challid,$ladderid,$winnerid,$loserid,$winrank,$loserank){
    global $dir, $file, $url, $out, $plyr;
    $loserteaminfo=mysql_query("SELECT teamname FROM teams WHERE id='$loserid'");
    $lteam=mysql_fetch_array($loserteaminfo);
    $winnerteaminfo=mysql_query("SELECT teamname FROM teams WHERE id='$winnerid'");
    $wteam=mysql_fetch_array($winnerteaminfo);
    $ladderinfo=mysql_query("SELECT laddername FROM ladders WHERE id='$ladderid'");
    $linfo=mysql_fetch_array($ladderinfo);
    $ip=getenv("REMOTE_ADDR");
    $tday=date("Y-m-d H:i:s");
    if(!$wteam[teamname]){
        $wteam[teamname]="[DELETED]";
    }

    if(!$lteam[teamname]){
        $lteam[teamname]="[DELETED]";
    }

    /*
    //REPORT MATCH TO MATCH DATABASE
    mysql_query("INSERT INTO matchdb VALUES (
    '$challid',
    '$winnerid',
    '$loserid',
    '$wteam[teamname]',
    '$lteam[teamname]',
    '$ladderid',
    '$linfo[laddername]',
    '$winrank',
    '$loserank',
    '0',
    '0',
    '',
    '',
    '',
    '',
    'Win by forfeit.',
    'Loss by forfeit.',
    '0',
    '0',
    '$plry[id]',
    '$tday',
    '0.0.0.0',
    '$plry[id]',
    '$tday',
    '$ip');");
    */
    //GET WINNERS LADDER STATS
    $winnerladder=mysql_query("SELECT * FROM ladder_$ladderid WHERE id='$winnerid'");
    $wlinfo=mysql_fetch_array($winnerladder);
    //GET LOSERS LADDER STATS
    $loserladder=mysql_query("SELECT * FROM ladder_$ladderid WHERE id='$loserid'");
    $llinfo=mysql_fetch_array($loserladder);
    include("$dir[func]/rankadjust.php");
    if(($loserank) && ($winrank)){
        if(($loserank < $winrank) && ($loserank < $wlinfo[rank])){
            update_teamranks($ladderid,$winnerid,$loserank);
        }

    }

    $tommorrow=date("Y-m-d H:i:s",time()+60*60*24*1);
    //UPDATE WINNERS LADDER STATS
    if($winrank < $loserank){
        $newlastrank="$wlinfo[rank]";
        if(($winrank < $wlinfo[bestrank]) || ($wlinfo[bestrank] < 1)){
            $newbestrank="$loserank";
        }else{

            $newbestrank="$wlinfo[bestrank]";
        }

    }else{

        $newlastrank="$wlinfo[lastrank]";
        $newbestrank="$wlinfo[bestrank]";
    }

    $newwins=($wlinfo[wins] + 1);
    $newgames=($newwins + $wlinfo[losses]);
    $newpercent=round($newwins / $newgames * 100);
    $newstreak=($wlinfo[streak] + 1);
    if($newstreak=="0"){
        $newstreak="1";
    }

    if($newstreak > $wlinfo[beststreak]){
        $newbeststreak="$newstreak";
    }else{

        $newbeststreak="$wlinfo[beststreak]";
    }

    mysql_query("UPDATE ladder_$ladderid SET
    lastmatch='$tday',
    lastplayed='$loserid',
    status='20',
    statusdate='$tommorrow',
    statusdisplay='Win by forfeit',
    lastrank='$newlastrank',
    bestrank='$newbestrank',
    wins='$newwins',
    games='$newgames',
    percent='$newpercent',
    streak='$newstreak',
    beststreak='$newbeststreak'
    WHERE id='$winnerid'");
    //UPDATE LOSERS LADDER STATS
    if($llinfo[rank] < $llinfo[bestrank]){
        $newbestrank="$llinfo[rank]";
    }else{

        $newbestrank="$llinfo[bestrank]";
    }

    if($llinfo[rank] > $llinfo[worstrank]){
        $newworstrank="$llinfo[rank]";
    }else{

        $newworstrank="$llinfo[worstrank]";
    }

    $newlosses=($llinfo[losses] + 1);
    $newgames=($newlosses + $llinfo[wins]);
    $newpercent=round($llinfo[wins] / $newgames * 100);
    $newstreak=($llinfo[streak] - 1);
    if($newstreak=="0"){
        $newstreak="-1";
    }

    if($newstreak < $llinfo[worststreak]){
        $newworststreak="$newstreak";
    }else{

        $newworststreak="$llinfo[worststreak]";
    }

    mysql_query("UPDATE ladder_$ladderid SET
    lastmatch='$tday',
    status='6',
    statusdate='$tommorrow',
    statusdisplay='Loss by forfeit',
    lastplayed='$winnerid',
    bestrank='$newbestrank',
    worstrank='$newworstrank',
    losses='$newlosses',
    games='$newgames',
    percent='$newpercent',
    streak='$newstreak',
    worststreak='$newworststreak'
    WHERE id='$loserid'");
    mysql_query("DELETE FROM challenges WHERE challid='$challid'");
    //CHECK FOR DUPLICATE RANKS AND ADJUST RANKS
    rank_checkadjust($ladderid);
}

?>
