<?
function confirm_win(){
    global $dir, $file, $url, $out, $plyr;
    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    <center>
    <table width='100%' border='0' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center' colspan='6'>
    <strong>Unconfirmed Wins on your teams - Unconfirmed matches expire in 24 hours</strong><br>
    </td>
    </tr>
    <tr>
    <td width='15%' valign='center' align='center'>Confirm</td>
    <td width='15%' valign='center' align='center'>Losing Team</td>
    <td width='15%' valign='center' align='center'>Winning Team</td>
    <td width='15%' valign='center' align='center'>Ladder</td>
    <td width='15%' valign='center' align='center'>Reported By</td>
    <td width='25%' valign='center' align='center'>Reported Time</td>
    </tr>";
    $teamson=mysql_query("SELECT teamid,status FROM teammembers WHERE playerid='$plyr[id]'");
    while(list($teamid,$status)=mysql_fetch_row($teamson)){
        $unconfirmedwins=mysql_query("SELECT matchid,ladderid,loserid,loseralias,reportedby,reportedip,DATE_FORMAT(reporteddate, '%M %d, %l:%i %p') FROM matchdbval WHERE winnerid='$teamid' ORDER by reporteddate");
        while(list($matchid,$ladderid,$loserid,$loseralias,$reportedby,$reportedip,$reporteddate)=mysql_fetch_row($unconfirmedwins)){
            if($ladderid!="$lastladderid"){
                $ladinfo=mysql_query("SELECT laddername FROM ladders WHERE id='$ladderid'");
                $linfo=mysql_fetch_array($ladinfo);
                $teaminfo=mysql_query("SELECT teamname FROM teams WHERE id='$teamid'");
                $tinfo=mysql_fetch_array($teaminfo);
                $out[body]=$out[body]."
                <tr class='altcolor'>
                </tr>";
            }

            if($reportedby=="0"){
                $reporter[alias]="Unknown";
            }else{

                $reported=mysql_query("SELECT alias FROM users WHERE id='$reportedby'");
                $reporter=mysql_fetch_array($reported);
            }

            $out[body]=$out[body]."
            <tr>
            <td width='15%' valign='center' align='center' class='catfont'>[<a href='?action=confirmwin&matchid=$matchid'><b>Confirm</b></a>]</td>
            <td width='15%' valign='center' align='center'><b><a href='$url[base]/$file[teams]?teamid=$loserid'>$loseralias</a></b></td>
            <td width='15%' valign='center' align='center'><b><a href='$url[base]/$file[teams]?teamid=$teamid'>$tinfo[teamname]</a></b></td>
            <td width='15%' valign='center' align='center'><b><a href='$url[base]/$file[ladder]?ladderid=$ladderid'>$linfo[laddername]</a></b></td>
            <td width='15%' valign='center' align='center'><b><a href='$url[base]/$file[players]?playerid=$reportedby'>$reporter[alias]</a></b></td>
            <td width='25%' valign='center' align='center'><b>$reporteddate</b></td>
            </tr>";
            $foundwins=1;
        }

        $lastladderid="$ladderid";
    }

    if(!$foundwins){
        $out[body]=$out[body]."
        <tr>
        <td width='100%' valign='center' align='center' colspan='6'><b>There are no unconfirmed wins</b></td>
        </tr>";
    }

    $out[body]=$out[body]."</table>
    $tablefoot
    <br>";
    // UNCONFIRMED LOSSES
    $out[body]=$out[body]."
    $tablehead
    <center>
    <table width='100%' border='0' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center' colspan='5'>
    <strong>Unconfirmed Losses on your teams - Unconfirmed matches expire in 24 hours</strong><br>
    </td>
    </tr>
    <tr>
    <td width='20%' valign='center' align='center'>Winning Team</td>
    <td width='20%' valign='center' align='center'>Losing Team</td>
    <td width='20%' valign='center' align='center'>Ladder</td>
    <td width='20%' valign='center' align='center'>Reported By</td>
    <td width='20%' valign='center' align='center'>Reported Time</td>
    </tr>";
    $lastladderid="";
    $teamsonb=mysql_query("SELECT teamid,status FROM teammembers WHERE playerid='$plyr[id]'");
    while(list($teamid,$status)=mysql_fetch_row($teamsonb)){
        $unconfirmedlosses=mysql_query("SELECT matchid,ladderid,winnerid,reportedby,reportedip,DATE_FORMAT(reporteddate, '%M %d, %l:%i %p') FROM matchdbval WHERE loserid='$teamid' ORDER by reporteddate");
        while(list($matchid,$ladderid,$winnerid,$reportedby,$reportedip,$reporteddate)=mysql_fetch_row($unconfirmedlosses)){
            if($ladderid!="$lastladderid"){
                $ladinfo=mysql_query("SELECT laddername FROM ladders WHERE id='$ladderid'");
                $linfo=mysql_fetch_array($ladinfo);
                $teaminfo=mysql_query("SELECT teamname FROM teams WHERE id='$teamid'");
                $tinfo=mysql_fetch_array($teaminfo);
                $out[body]=$out[body]."
                <tr class='altcolor'>
                </tr>";
            }

            $loserteaminfo=mysql_query("SELECT teamname FROM teams WHERE id='$winnerid'");
            $ltinfo=mysql_fetch_array($loserteaminfo);
            if($reportedby=="0"){
                $reporter[alias]="Unknown";
            }else{

                $reported=mysql_query("SELECT alias FROM users WHERE id='$reportedby'");
                $reporter=mysql_fetch_array($reported);
            }

            $out[body]=$out[body]."
            <tr>
            <td width='20%' valign='center' align='center'><a href='$url[base]/$file[teams]?teamid=$winnerid'><b>$ltinfo[teamname]</a></b></td>
            <td width='20%' valign='center' align='center'><a href='$url[base]/$file[teams]?teamid=$teamid'><b>$tinfo[teamname]</a></b></td>
            <td width='20%' valign='center' align='center'><a href='$url[base]/$file[ladder]?ladderid=$ladderid'><b>$linfo[laddername]</a></b></td>
            <td width='20%' valign='center' align='center'><a href='$url[base]/$file[players]?playerid=$reportedby'><b>$reporter[alias]</a></b></td>
            <td width='20%' valign='center' align='center'><b>$reporteddate</b></td>
            </tr>";
            $foundlosses=1;
        }

        $lastladderid="$ladderid";
    }

    if(!$foundlosses){
        $out[body]=$out[body]."
        <tr>
        <td width='100%' valign='center' align='center' colspan='5'><b>There are no unconfirmed losse</b>s</td>
        </tr>";
    }

    $out[body]=$out[body]."</table>
    $tablefoot";
    include("$dir[curtheme]");
}

function confirm_winb($matchid){
    global $dir, $file, $url, $out, $plyr;
    $matchinfo=mysql_query("SELECT *,DATE_FORMAT(reporteddate, '%M %d, %l:%i %p') FROM matchdbval WHERE matchid='$matchid'");
    $match=mysql_fetch_array($matchinfo);
    if(!$match[matchid]){
        include("$dir[func]/error.php");
        display_error("Unknown Match ID.<br>");
    }

    //CHECK IF CAN CONFIRM/REPORT
    if (mysql_num_rows(mysql_query("SELECT status FROM teammembers WHERE teamid='$match[winnerid]' AND playerid='$plyr[id]' AND status <='4'")) < 1){
        include("$dir[func]/error.php");
        display_error("You are not allowed to report losses for this team.<br>");
    }

    $ladderinfo=mysql_query("SELECT laddername FROM ladders WHERE id='$match[ladderid]'");
    $linfo=mysql_fetch_array($ladderinfo);
    if(!$linfo[laddername]){
        include("$dir[func]/error.php");
        display_error("Unknown ladder.<br>");
    }

    $winnerinfo=mysql_query("SELECT teamname FROM teams WHERE id='$match[winnerid]'");
    $wtinfo=mysql_fetch_array($winnerinfo);
    if(!$wtinfo[teamname]){
        include("$dir[func]/error.php");
        display_error("Unable to find your team.<br>");
    }

    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    <center>
    <table width='100%' border='0' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <strong>Confirm this win - $linfo[laddername]</strong><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    Winner: <a href='$url[base]/$file[teams]?teamid=$match[winnerid]'>$wtinfo[teamname]</a><br>
    Loser: <a href='$url[base]/$file[teams]?teamid=$match[loserid]'>$match[loseralias]</a><br>
    Winners Rank: $match[wrank]<br>
    Losers Rank: $match[lrank]<br>
    <br>
    Losers Comments:<br>$match[lcomment]<br>
    <br>
    Match was reported $match[15]</td>
    </tr>
    <form method='post'>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <input type='hidden' name='matchid' value='$match[matchid]'>
    <input type='hidden' name='action' value='confirmwinc'>
    <input type='submit' name='' value='Confirm Win'>
    </td></form>
    </tr>
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function confirm_winc($matchid){
    global $dir, $file, $url, $out, $plyr, $misc;
    $matchinfo=mysql_query("SELECT * FROM matchdbval WHERE matchid='$matchid'");
    $match=mysql_fetch_array($matchinfo);
    if(!$match[matchid]){
        include("$dir[func]/error.php");
        display_error("Unknown Match ID.<br>");
    }

    //CHECK IF CAN CONFIRM/REPORT
    if (mysql_num_rows(mysql_query("SELECT status FROM teammembers WHERE teamid='$match[winnerid]' AND playerid='$plyr[id]' AND status <='4'")) < 1){
        include("$dir[func]/error.php");
        display_error("You are not allowed to report losses for this team.<br>");
    }

    $ladderinfo=mysql_query("SELECT laddername FROM ladders WHERE id='$match[ladderid]'");
    $linfo=mysql_fetch_array($ladderinfo);
    if(!$linfo[laddername]){
        include("$dir[func]/error.php");
        display_error("Unknown ladder.<br>");
    }

    $winnerinfo=mysql_query("SELECT teamname FROM teams WHERE id='$match[winnerid]'");
    $wtinfo=mysql_fetch_array($winnerinfo);
    if(!$wtinfo[teamname]){
        include("$dir[func]/error.php");
        display_error("Unable to find your team.<br>");
    }

    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    <center>
    <table width='100%' border='0' bordercolor='#ffffff' cellspacing='0' cellpadding='2'>
    <form method='post'>
    <tr class='altcolorc'>
    <td width='100%' align='center' border='0' bordercolor='#ffffff'>
    <strong>Confirm this win</strong><br>
    </td>
    </tr>
    <tr><td>
    <table width=80% align='center' class='altcolorc'>
    ";
    $altcolora="#000033";
    $altcolorb="#000020";
    $winnersmembers=mysql_query("SELECT playerid,status FROM teammembers WHERE teamid='$match[winnerid]' ORDER by status");
    while(list($playerid,$status)=mysql_fetch_row($winnersmembers)){
        $memberinfo=mysql_query("SELECT alias FROM users WHERE id='$playerid'");
        $minfo=mysql_fetch_array($memberinfo);
        if($altladrow=="$altcolora"){
            $altladrow="$altcolorb";
        }else{

            $altladrow="$altcolora";
        }

        $out[body]=$out[body]."
        <tr bgcolor='$altladrow'>
        <td width='50%' align='left'>
        <a href='$url[base]/$file[players]?playerid=$playerid' target='new'>
        $minfo[alias]</a></td>
        <td width='50%' valign='center' align='right'>";
        if($status < 6){
            $out[body]=$out[body]."Played in Match:
            <input type='checkbox' name='member[$playerid]' value='1'>
            <input type='hidden' name='kills[$playerid]' value='0' maxlength='2' size='5'>";
        }else{

            $status=member_status($status);
            $out[body]=$out[body]."$status";
        }

        $out[body]=$out[body]."
        </td>
        </tr>";
    }

    $out[body]=$out[body]."
    </table>";
    if($match[maps]){
        $out[body]=$out[body]."
        <tr><td>
        <table  width=80% align='center' class='altcolorc'>
        <tr class='altcolorc'>
        <td width='100%' valign='top' align='center' colspan='3'>
        <strong>Map Results</strong><br>
        </td>
        </tr>
        ";
        $mapsplayed=split(",",$match[maps]);
        while(list($mapnum,$mapid)=each($mapsplayed)){
            $mapnum=($mapnum+1);
            if($mapid){
                $mapplayedinfo=mysql_query("SELECT mapname FROM maps WHERE id='$mapid'");
                $mapinfo=mysql_fetch_array($mapplayedinfo);
                if($altladrow=="$altcolora"){
                    $altladrow="$altcolorb";
                }else{

                    $altladrow="$altcolora";
                }

                if(!$mapinfo[mapname]){
                    $mapinfo[mapname]="Unknown";
                }

                $out[body]=$out[body]."
                <tr bgcolor=$altladrow>
                <td width='33%' valign='center' align='left'>
                #$mapnum Map Played</td>
                <td width='33%' valign='center' align='center'>$mapinfo[mapname]</td>
                <td width='33%' valign='center' align='right'>Won: <input type='checkbox' name='map[$mapnum]' value='1'></td>
                </tr>
                ";
            }

        }

        $out[body]=$out[body]."
        </table></td></tr>
        ";
    }

    $out[body]=$out[body]."
    <tr class='altcolorc'>
    <td width='100%' valign='top' align='center'>
    <strong>Final Score</strong><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <table width='100%' border='0'  cellspacing='0' cellpadding='0'>
    <tr>
    <td width='50%' valign='center' align='center'>$wtinfo[teamname] <input type='text' name='wfscore' value='' size='3' maxlength='3'>&nbsp;</td>
    <td width='50%' valign='center' align='center'>$match[loseralias] <input type='text' name='lfscore' value='' size='3' maxlength='3'> $ltinfo[teamname]</td>
    </tr>
    </table>";
    if($misc[winnercomment]=="yes"){
        $out[body]=$out[body]."</td>
        </tr>
        <tr class='altcolorc'>
        <td width='100%' valign='top' align='center'>
        <strong>Comment on this match</strong><br>
        </td>
        </tr>
        <tr>
        <td width='100%' valign='center' align='center'>
        <tr>
        <td width='100%' valign='center' align='center'>
        <input type='text' name='wcomment' maxlength='50' size='50'><br>
        <small>50 Characters Max</small>
        </td>
        </tr>";
    }

    $out[body]=$out[body]."
    <tr>
    <td width='100%' valign='top' align='center'>
    <BR>
    <input type='hidden' name='matchid' value='$match[matchid]'>
    <input type='hidden' name='action' value='confirmwind'>
    <input type='submit' name='' value='Confirm Win'>
    </td></form>
    </tr>
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function confirm_wind($matchid,$member,$map,$wfscore,$lfscore,$wcomment,$kills){
    global $dir, $file, $url, $out, $plyr, $misc;
    if(!$matchid){
        include("$dir[func]/error.php");
        display_error("Invalid Match ID.<br>");
    }

    if((!$wfscore) || ($wfscore <= $lfscore)){
        include("$dir[func]/error.php");
        display_error("Invalid Final Score.<br>");
    }

    $matchinfo=mysql_query("SELECT * FROM matchdbval WHERE matchid='$matchid'");
    $match=mysql_fetch_array($matchinfo);
    if(!$match[matchid]){
        include("$dir[func]/error.php");
        display_error("Unknown Match ID.<br>");
    }

    //CHECK IF CAN CONFIRM/REPORT
    if (mysql_num_rows(mysql_query("SELECT status FROM teammembers WHERE teamid='$match[winnerid]' AND playerid='$plyr[id]' AND status <='4'")) < 1){
        include("$dir[func]/error.php");
        display_error("You are not allowed to report losses for this team.<br>");
    }

    $ladderinfo=mysql_query("SELECT * FROM ladders WHERE id='$match[ladderid]'");
    $linfo=mysql_fetch_array($ladderinfo);
    if(!$linfo[laddername]){
        include("$dir[func]/error.php");
        display_error("Unknown ladder.<br>");
    }

    $winnerinfo=mysql_query("SELECT teamname FROM teams WHERE id='$match[winnerid]'");
    $wtinfo=mysql_fetch_array($winnerinfo);
    if(!$wtinfo[teamname]){
        include("$dir[func]/error.php");
        display_error("Unable to find your team.<br>");
    }

    if(!$member){
        include("$dir[func]/error.php");
        display_error("You must select the members that played.<br>");
    }

    include("$dir[func]/checkdata.php");
    $wfscore=change_numbersonly($wfscore);
    if($lfscore==""){
        $lfscore=0;
    }else{

        $lfscore=change_numbersonly($lfscore);
    }

    if((!$wfscore) || (!$lfscore) && ($lfscore!="0")){
        include("$dir[func]/error.php");
        display_error("Invalid Final Score.<br>");
    }

    if($wcomment){
        $wcomment=wordwrap($wcomment,20," ",1);
        $wcomment=change_censor($wcomment);
        $wcomment=change_charecters($wcomment);
    }

    while(list($memberid,$played)=each($member)){
        if($played){
            $memberid=change_numbersonly($memberid);
            $kills[$memberid]=change_numbersonly($kills[$memberid]);
            $membersplayed=$membersplayed."$memberid|$kills[$memberid],";
            $totalkills=($totalkills + $kills[$memberid]);
        }

    }

    if($map){
        while(list($maporder,$wonmap)=each($map)){
            if($wonmap){
                $mapsplayed=$mapsplayed."$maporder,";
            }

        }

    }

    if(!$membersplayed){
        include("$dir[func]/error.php");
        display_error("You must select the members that played.<br>");
    }

    //CHECK FOR CHALLENGE OR IF OPEN PLAY IS ALLOWED
    $thechallenge=mysql_query("SELECT * FROM challenges WHERE challenger='$match[winnerid]' AND challenged='$match[loserid]' AND finalizedby > '0' OR challenger='$match[loserid]' AND challenged='$match[winnerid]' AND finalizedby > '0'");
    $chall=mysql_fetch_array($thechallenge);
    if($chall[challid]){
        if($misc[challranking] < 2){

        	if($match[winnerid]=="$chall[challenger]"){
            		$wtempnewrank="$chall[challengedrank]";
        	}else{

            		$wtempnewrank=0;
        	}
	}
	else if($misc[challranking] > 1){

		if($match[winnerid]=="$chall[challenger]"){
            		$wtempnewrank=($chall[challengedrank] / 2 - 0.5);
        	}else{

            		$wtempnewrank=0;
        	}

	}

	else{
		$wtempnewrank=0;
	}

    }else{

        if($linfo[openplay] < 1){
            include("$dir[func]/error.php");
            display_error("Open play is not allowed on this ladder.<br>All matches must be scheduled challenges.<br>");
        }

    }

    //CLEAN MATCH DB
    clean_matchdb($matchid);
    $tday=date("Y-m-d H:i:s");
    $ip=getenv("REMOTE_ADDR");
    //REPORT MATCH TO MATCH DATABASE
    mysql_query("INSERT INTO matchdb VALUES (
    '$match[matchid]',
    '$match[winnerid]',
    '$match[loserid]',
    '$wtinfo[teamname]',
    '$match[loseralias]',
    '$match[ladderid]',
    '$linfo[laddername]',
    '$match[wrank]',
    '$match[lrank]',
    '$wfscore',
    '$lfscore',
    '$membersplayed',
    '$match[losersmembers]',
    '$match[maps]',
    '$mapsplayed',
    '$wcomment',
    '$match[lcomment]',
    '$match[points]',
    '$totalkills|$match[skill]',
    '$match[reportedby]',
    '$match[reporteddate]',
    '$match[reportedip]',
    '$plyr[id]',
    '$tday',
    '$ip');");
    //UPDATE WINNER TEAM PLAYER SCORES
    $membersplayed=split(",",$membersplayed);
    while(list($num,$playerid)=each($membersplayed)){
        $playerid=split("\|",$playerid);
        if($playerid[0]){
            mysql_query("UPDATE users SET
            matcheswon=matcheswon+1,
            points=points+$match[points],
            skill=skill+$playerid[1]
            WHERE id='$playerid[0]'");
        }

    }

    //UPDATE LOSER TEAM PLAYER SCORES
    $losersmembers=split(",",$match[losersmembers]);
    while(list($num,$losersid)=each($losersmembers)){
        $losersid=split("\|",$losersid);
        if($losersid[0]){
            mysql_query("UPDATE users SET
            matcheslost=matcheslost+1,
            skill=skill+$losersid[1]
            WHERE id='$losersid[0]'");
        }

    }

    //GET WINNERS LADDER STATS
    $winnerladder=mysql_query("SELECT * FROM ladder_$match[ladderid] WHERE id='$match[winnerid]'");
    $wlinfo=mysql_fetch_array($winnerladder);
    //GET LOSERS LADDER STATS
    $loserladder=mysql_query("SELECT * FROM ladder_$match[ladderid] WHERE id='$match[loserid]'");
    $llinfo=mysql_fetch_array($loserladder);
    //GET TOTAL RANKERS
    $totalranked=mysql_query("SELECT COUNT(*) FROM ladder_$match[ladderid] WHERE rank > '0'");
    $totalranks=mysql_fetch_array($totalranked);
    $totalranks="$totalranks[0]";
    if(!$chall[challid]){

        //Winner moves half way up to losers position
        if($misc[ranking] < 2){
        	//UPDATE LADDER RANKS
        	//IF BOTH TEAMS ARE ALREADY RANKED
        	if(($match[wrank] > 0) && ($match[lrank] > 0)){
            		$wtempnewrank=($match[wrank] + $match[lrank]);
            		$wtempnewrank=($wtempnewrank / 2 - 0.5);
            		if($wtempnewrank > $totalranks){
                		$wtempnewrank="$totalranks";
            		}

        	}

        	else if($match[wrank] < 1){
            		$wtempnewrank=($totalranks + 1);
        	}else{
            		$wtempnewrank="";
        	}

        //Winner takes losers position
        }else if($misc[ranking] > 1){

 		//UPDATE LADDER RANKS
        	//IF BOTH TEAMS ARE ALREADY RANKED
        	if(($match[wrank] > 0) && ($match[lrank] > 0)){
            		$wtempnewrank=$match[lrank];         
        	}

        	else if($match[wrank] < 1){
            		$wtempnewrank=($totalranks + 1);
        	}else{
            		$wtempnewrank="";
        	}

	}else{
		$wtempnewrank="";
	}

    }

    $wtempnewrank=round($wtempnewrank);
    //IF RANK IS BETTER THAN CURRENT RANK UPDATE LADDER
    include("$dir[func]/rankadjust.php");
    if(($wtempnewrank) && ($wtempnewrank > 0)){
        if(($wtempnewrank < $wlinfo[rank]) || ($wlinfo[rank] < 1)){
            update_teamranks($match[ladderid],$match[winnerid],$wtempnewrank);
            $updatedrank="$wtempnewrank";
        }

    }

    //UPDATE WINNERS LADDER STATS
    if($updatedrank){
        $newlastrank="$wlinfo[rank]";
        if(($updatedrank < $wlinfo[bestrank]) || ($wlinfo[bestrank] < 1)){
            $newbestrank="$updatedrank";
        }else{

            $newbestrank="$wlinfo[bestrank]";
        }

    }else{

        $newlastrank="$wlinfo[lastrank]";
        $newbestrank="$wlinfo[bestrank]";
    }

    $newwins=($wlinfo[wins] + 1);
    $newgames=($newwins + $wlinfo[losses]);
    $newpercent=round($newwins / $newgames * 100);
    $newstreak=($wlinfo[streak] + 1);
    if($newstreak < 1){
        $newstreak="1";
    }

    if($newstreak > $wlinfo[beststreak]){
        $newbeststreak="$newstreak";
    }else{

        $newbeststreak="$wlinfo[beststreak]";
    }

    if($llinfo[rank] > 0){
        $newpoints=($totalranks - $llinfo[rank]);
        $newpoints=round($newpoints / 10);
    }

    if($newpoints < 10){
        $newpoints="10";
    }

    //CHANGED SKILL TO KILLS
    //$newskill=($wlinfo[skill] + $match[skill]);
    $newskill=($wlinfo[skill] + $totalkills);
    $newpoints=($newpoints + $wlinfo[points]);
    mysql_query("UPDATE ladder_$match[ladderid] SET
    lastmatch='$tday',
    lastplayed='$match[loserid]',
    statusdisplay='',
    lastrank='$newlastrank',
    bestrank='$newbestrank',
    wins='$newwins',
    games='$newgames',
    percent='$newpercent',
    streak='$newstreak',
    beststreak='$newbeststreak',
    points='$newpoints',
    skill='$newskill'
    WHERE id='$match[winnerid]'");
    //UPDATE LOSERS LADDER STATS
    $newlastrank="$llinfo[rank]";
    if(($llinfo[rank] < $llinfo[bestrank]) || ($llinfo[bestrank]=="0")){
        $newbestrank="$llinfo[rank]";
    }else{

        $newbestrank="$llinfo[bestrank]";
    }

    if($llinfo[rank] > $llinfo[worstrank]){
        $newworstrank="$llinfo[rank]";
    }else{

        $newworstrank="$llinfo[worstrank]";
    }

    $newlosses=($llinfo[losses] + 1);
    $newgames=($newlosses + $llinfo[wins]);
    $newpercent=round($llinfo[wins] / $newgames * 100);
    $newstreak=($llinfo[streak] - 1);
    if($newstreak > 0){
        $newstreak="-1";
    }

    if($newstreak < $llinfo[worststreak]){
        $newworststreak="$newstreak";
    }else{

        $newworststreak="$llinfo[worststreak]";
    }

    $newskill=($llinfo[skill] + $match[skill]);
    mysql_query("UPDATE ladder_$match[ladderid] SET
    lastmatch='$tday',
    lastplayed='$match[winnerid]',
    statusdisplay='',
    lastrank='$newlastrank',
    bestrank='$newbestrank',
    worstrank='$newworstrank',
    losses='$newlosses',
    games='$newgames',
    percent='$newpercent',
    streak='$newstreak',
    worststreak='$newworststreak',
    skill='$newskill'
    WHERE id='$match[loserid]'");
    //DELETE UNVALIDATED MATCH
    mysql_query("DELETE FROM matchdbval WHERE matchid='$matchid'");
    if($chall[challid]){
        //DELETE CHALLENGE
        mysql_query("DELETE FROM challenges WHERE challid='$chall[challid]'");
    }

    //CLEAN LADDER - INACTIVITY
    if($linfo[inactivedrop] > 0){
        //SECOND WARNING - DELETE TEAMS
        if($linfo[inactivedaysb] > 0){
            $dropdateb=date("Y-m-d H:i:s",time()-60*60*24*$linfo[inactivedaysb]);
            $inactivedropb=mysql_query("SELECT id FROM ladder_$match[ladderid] WHERE joindate < '$dropdateb' AND lastmatch < '$dropdateb'");
            while(list($dropidb)=mysql_fetch_row($inactivedropb)){
                // REMOVE ALL RECORDS OF TEAM FROM LADDER SYSTEM
                mysql_query("DELETE FROM teammembers WHERE teamid='$dropidb'");
                mysql_query("DELETE FROM teams WHERE id='$dropidb'");
                mysql_query("DELETE FROM ladder_$match[ladderid] WHERE id='$dropidb'");
                mysql_query("DELETE FROM challenges WHERE challenger='$dropidb'");
                mysql_query("DELETE FROM challenges WHERE challenged='$dropidb'");
                mysql_query("DELETE FROM teammembersinv WHERE teamid='$dropidb'");
            }

        }

        //FIRST DROP TO (admin set amount) RANKS
        if($linfo[inactivedaysa] > 0){
            $dropdatea=date("Y-m-d H:i:s",time()-60*60*24*$linfo[inactivedaysa]);
            $inactivedropa=mysql_query("SELECT id,rank FROM ladder_$match[ladderid] WHERE joindate < '$dropdatea' AND statusdate < '$dropdatea' AND lastmatch < '$dropdatea' AND rank > '0' ORDER by rank");
            while(list($dropida,$dropranka)=mysql_fetch_row($inactivedropa)){
                $teamsdropped++;
                if($teamsdropped < 25){
                    $droptoa=($dropranka + $linfo[inactivedrop]);
                    if($droptoa > $totalranks){
                        $droptoa="$totalranks";
                    }

                    mysql_query("UPDATE ladder_$match[ladderid] SET rank='$droptoa' WHERE id='$dropida'");
                    mysql_query("UPDATE ladder_$match[ladderid] SET status='69', statusdisplay='Dropped for Inactivity', statusdate='$tday' WHERE id='$dropida'");
                }

            }

        }

    }

    //CHECK FOR DUPLICATE RANKS AND ADJUST RANKS
    //RETURN (1) IF ADJUSTED
    $ranksadjusted=rank_checkadjust($match[ladderid]);
    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    <table width='100%' border='0' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <strong>Your win has been recorded</strong><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='left'>
    <br><ul>
    Ladder: <a href='$url[base]/$file[ladder]?ladderid=$match[ladderid]'>$linfo[laddername]</a><br>
    Winner: <a href='$url[base]/$file[teams]?teamid=$match[winnerid]'>$wtinfo[teamname]</a><br>
    Loser: <a href='$url[base]/$file[teams]?teamid=$match[loserid]'>$match[loseralias]</a>
    </ul></td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <strong><a href='$url[base]/$file[match]?matchid=$match[matchid]'>View the Match</a></strong><br>
    </td>
    </tr>
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function clean_matchdb($matchid){
    $deletedate=date("Y-m-d H:i:s",time()-60*60*24*30);
    mysql_query("DELETE FROM matchdb WHERE reportdate < '$deletedate' OR matchid='$matchid'");
}

function member_status($status){
    if($status=="1"){ $status="Leader"; }

    if($status=="2"){ $status="Co-Leader"; }

    if($status=="3"){ $status="Captain"; }

    if($status=="4"){ $status="Co-Captain"; }

    if($status=="5"){ $status="Member"; }

    if($status=="6"){ $status="Training"; }

    if($status=="7"){ $status="Inactive"; }

    if($status=="8"){ $status="Suspended"; }

    return($status);
}

?>
