<?
if(!IsSet($indexloaded)){
    header("Location: ./index.php");
}

function admin_cpdisplay(){
    global $dir, $file, $url, $out, $site, $admn;
    if($admn[access] < 5){
        include("$dir[func]/error.php");
        display_error("You are not allowed to perform this function.<br>");
    }

    //TABLE ATERNATING COLORS
    $altcolora="#101010";
    $altcolorb="#202020";
    $tablehead=table_head("show","100%","","center");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    <br>
    $tablehead
    <table width='100%' border='1' bordercolor='#000000' cellspacing='0' cellpadding='2'>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <font class='catfont'><strong>$site[shortname] Admin</strong></font><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <table width='100%' border='0' cellspacing='0' cellpadding='0'>
    <tr class='altcolora'>
    <form method='post'>
    <td width='80%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    <strong>
    Validation Codes
    </strong></font></td>
    <td width='20%' valign='center' align='center'>
    <form method='post'>
    <input type='hidden' name='action' value='validate'>
    <input type='submit' name='' value='Update >>'>
    </td>
    </form>
    </tr>
    <tr class='altcolorb'>
    <form method='post'>
    <td width='80%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    <strong>
    Create Team
    </strong></font></td>
    <td width='20%' valign='center' align='center'>
    <form method='post'>
    <input type='hidden' name='action' value='createteam'>
    <input type='submit' name='' value='Update >>'>
    </td>
    </form>
    </tr>
    <tr class='altcolora'>
    <form method='post'>
    <td width='80%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    <strong>
    Edit a Team
    </strong></font></td>
    <td width='20%' valign='center' align='center'>
    <form method='post'>
    <input type='hidden' name='action' value='editateam'>
    <input type='submit' name='' value='Update >>'>
    </td>
    </form>
    </tr>
    <tr class='altcolorb'>
    <form method='post'>
    <td width='80%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    <strong>
    Edit a Player
    </strong></font></td>
    <td width='20%' valign='center' align='center'>
    <form method='post'>
    <input type='hidden' name='action' value='editplayer'>
    <input type='submit' name='' value='Update >>'>
    </td>
    </form>
    </tr>
    <tr class='altcolora'>
    <form method='post'>
    <td width='80%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    <strong>
    Admin Comments
    </strong></font></td>
    <td width='20%' valign='center' align='center'>
    <form method='post'>
    <input type='hidden' name='action' value='admincomments'>
    <input type='submit' name='' value='Update >>'>
    </td>
    </form>
    </tr>
    </table>
    </td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <font class='catfont'><strong>Match Functions</strong></font><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <table width='100%' border='0' cellspacing='0' cellpadding='0'>
    <tr class='altcolora'>
    <form method='post'>
    <td width='80%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    <strong>
    Report Match
    </strong></font></td>
    <td width='20%' valign='center' align='center'>
    <form method='post'>
    <input type='hidden' name='action' value='reportmatch'>
    <input type='submit' name='' value='Update >>'>
    </td>
    </form>
    </tr>
    <tr class='altcolorb'>
    <form method='post'>
    <td width='80%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    <strong>
    Edit Match
    </strong></font></td>
    <td width='20%' valign='center' align='center'>
    <form method='post'>
    <input type='hidden' name='action' value='editmatch'>
    <input type='submit' name='' value='Update >>'>
    </td>
    </form>
    </tr>
    <tr class='altcolora'>
    <form method='post'>
    <td width='80%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    <strong>
    Edit Challenge
    </strong></font></td>
    <td width='20%' valign='center' align='center'>
    <form method='post'>
    <input type='hidden' name='action' value='editchallenge'>
    <input type='submit' name='' value='Update >>'>
    </td>
    </form>
    </tr>
    </table>
    </td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <font class='catfont'><strong>Staff Functions</strong></font><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <table width='100%' border='0' cellspacing='0' cellpadding='0'>
    <tr class='altcolora'>
    <form method='post'>
    <td width='80%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    <strong>
    Add Staff
    </strong></font></td>
    <td width='20%' valign='center' align='center'>
    <form method='post'>
    <input type='hidden' name='action' value='addstaff'>
    <input type='submit' name='' value='Update >>'>
    </td>
    </form>
    </tr>
    <tr class='altcolorb'>
    <form method='post'>
    <td width='80%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    <strong>
    Edit Staff
    </strong></font></td>
    <td width='20%' valign='center' align='center'>
    <form method='post'>
    <input type='hidden' name='action' value='editstaff'>
    <input type='submit' name='' value='Update >>'>
    </td>
    </form>
    </tr>
    </table>
    </td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <font class='catfont'><strong>Game and Ladder Functions</strong></font><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <table width='100%' border='0' cellspacing='0' cellpadding='0'>
    <tr class='altcolora'>
    <form method='post'>
    <td width='80%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    <strong>
    Manage Games
    </strong></font></td>
    <td width='20%' valign='center' align='center'>
    <form method='post'>
    <input type='hidden' name='action' value='editgames'>
    <input type='submit' name='' value='Update >>'>
    </td>
    </form>
    </tr>
    <tr class='altcolorb'>
    <form method='post'>
    <td width='80%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    <strong>
    Manage Ladders
    </strong></font></td>
    <td width='20%' valign='center' align='center'>
    <form method='post'>
    <input type='hidden' name='action' value='editladders'>
    <input type='submit' name='' value='Update >>'>
    </td>
    </form>
    </tr>
    <tr class='altcolora'>
    <form method='post'>
    <td width='80%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    <strong>
    Manage Rules
    </strong></font></td>
    <td width='20%' valign='center' align='center'>
    <form method='post'>
    <input type='hidden' name='action' value='editrules'>
    <input type='submit' name='' value='Update >>'>
    </td>
    </form>
    </tr>
    <tr class='altcolorb'>
    <form method='post'>
    <td width='80%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    <strong>
    Manage Maps
    </strong></font></td>
    <td width='20%' valign='center' align='center'>
    <form method='post'>
    <input type='hidden' name='action' value='editmaps'>
    <input type='submit' name='' value='Update >>'>
    </td>
    </form>
    </tr>
    </table>
    </td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <font class='catfont'><strong>Tournament Functions</strong></font><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <table width='100%' border='0' cellspacing='0' cellpadding='0'>
    <tr class='altcolora'>
    <form method='post'>
    <td width='80%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    <strong>
    Manage Tournaments
    </strong></font></td>
    <td width='20%' valign='center' align='center'>
    <form method='post'>
    <input type='hidden' name='action' value='madmin_manage'>
    <input type='submit' name='' value='Update >>'>
    </td>
    </form>
    </tr>
    <tr class='altcolorb'>
    <form method='post'>
    <td width='80%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    <strong>
    Run Tournaments
    </strong></font></td>
    <td width='20%' valign='center' align='center'>
    <form method='post'>
    <input type='hidden' name='action' value='select_tournament'>
    <input type='submit' name='' value='Update >>'>
    </td>
    </form>
    </tr>
    </table>
    </td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <font class='catfont'><strong>Site Functions</strong></font><br>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='center' align='center'>
    <table width='100%' border='0' cellspacing='0' cellpadding='0'>
    <tr class='altcolora'>
    <form method='post'>
    <td width='80%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    <strong>
    Ban and Censor
    </strong></font></td>
    <td width='20%' valign='center' align='center'>
    <form method='post'>
    <input type='hidden' name='action' value='censor'>
    <input type='submit' name='' value='Update >>'>
    </td>
    </form>
    </tr>
    <tr class='altcolorb'>
    <form method='post'>
    <td width='80%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    <strong>
    Link Manager
    </strong></font></td>
    <td width='20%' valign='center' align='center'>
    <form method='post'>
    <input type='hidden' name='action' value='linkman'>
    <input type='submit' name='' value='Update >>'>
    </td>
    </form>
    </tr>
    <tr class='altcolora'>
    <form method='post'>
    <td width='80%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    <strong>
    News Manager
    </strong></font></td>
    <td width='20%' valign='center' align='center'>
    <form method='post'>
    <input type='hidden' name='action' value='news'>
    <input type='submit' name='' value='Update >>'>
    </td>
    </form>
    </tr>
    <tr class='altcolorb'>
    <form method='post'>
    <td width='80%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    <strong>
    Send Newsletter
    </strong></font></td>
    <td width='20%' valign='center' align='center'>
    <form method='post'>
    <input type='hidden' name='action' value='newsletter'>
    <input type='submit' name='' value='Update >>'>
    </td>
    </form>
    </tr>
    <tr class='altcolora'>
    <form method='post'>
    <td width='80%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>
    <strong>
    Site Settings
    </strong></font></td>
    <td width='20%' valign='center' align='center'>
    <form method='post'>
    <input type='hidden' name='action' value='settings'>
    <input type='submit' name='' value='Update >>'>
    </td>
    </form>
    </tr>
    </table>
    </td>
    </tr>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <strong><a href='$url[base]/$file[login]?action=adminlogout'>Admin Logout</a></strong><br>
    </td>
    </tr>
    </table>
    $tablefoot
    </center>";
    include("$dir[curtheme]");
}

?>
