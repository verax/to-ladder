# phpMyAdmin SQL Dump
# version 2.5.4
# http://www.phpmyadmin.net
#
# Host: localhost
# Generation Time: Nov 7, 2005 at 03:05 PM
# Server version: 4.0.16
# PHP Version: 4.3.10
#
#

# --------------------------------------------------------

#
# Table structure for table `bancensor`
#

DROP TABLE IF EXISTS `bancensor`;
CREATE TABLE `bancensor` (
  `id` int(10) NOT NULL auto_increment,
  `text` varchar(100) NOT NULL default '',
  `type` int(2) NOT NULL default '0',
  `reason` varchar(250) NOT NULL default '',
  `displayreason` varchar(250) NOT NULL default '',
  `addedby` int(10) NOT NULL default '0',
  `addeddate` datetime NOT NULL default '0000-00-00 00:00:00',
  `lasteditby` int(10) NOT NULL default '0',
  `lastedit` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM PACK_KEYS=1 AUTO_INCREMENT=1 ;

# --------------------------------------------------------

#
# Table structure for table `brackets2`
#

DROP TABLE IF EXISTS `brackets2`;
CREATE TABLE `brackets2` (
  `bid` varchar(10) NOT NULL default '0',
  `tid` int(10) NOT NULL default '0',
  `pid` int(10) NOT NULL default '0',
  `name` varchar(25) NOT NULL default ''
) ENGINE=MyISAM;

# --------------------------------------------------------

#
# Table structure for table `challenges`
#

DROP TABLE IF EXISTS `challenges`;
CREATE TABLE `challenges` (
  `challid` varchar(50) NOT NULL default '',
  `challenger` int(10) NOT NULL default '0',
  `challenged` int(10) NOT NULL default '0',
  `challengeralias` varchar(50) NOT NULL default '',
  `challengedalias` varchar(50) NOT NULL default '',
  `ladderid` int(10) NOT NULL default '0',
  `challengerrank` int(10) NOT NULL default '0',
  `challengedrank` int(10) NOT NULL default '0',
  `challengercomment` varchar(250) NOT NULL default '',
  `challengedby` int(10) NOT NULL default '0',
  `challengedtime` datetime NOT NULL default '0000-00-00 00:00:00',
  `matchtimes` varchar(250) NOT NULL default '',
  `matchmaps` varchar(250) NOT NULL default '',
  `challengedcomment` varchar(250) NOT NULL default '',
  `respondedby` int(10) NOT NULL default '0',
  `respondedtime` datetime NOT NULL default '0000-00-00 00:00:00',
  `finaltime` varchar(250) NOT NULL default '',
  `finalmaps` varchar(250) NOT NULL default '',
  `finalizedby` int(10) NOT NULL default '0',
  `finalizedtime` datetime NOT NULL default '0000-00-00 00:00:00',
  KEY `challid` (`challid`),
  KEY `challenger` (`challenger`),
  KEY `challenged` (`challenged`),
  KEY `ladderid` (`ladderid`),
  KEY `finaltime` (`finaltime`)
) ENGINE=MyISAM PACK_KEYS=1;

# --------------------------------------------------------

#
# Table structure for table `default`
#

DROP TABLE IF EXISTS `default`;
CREATE TABLE `default` (
  `val` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`val`)
) ENGINE=MyISAM;

# --------------------------------------------------------

#
# Table structure for table `defaults2`
#

DROP TABLE IF EXISTS `defaults2`;
CREATE TABLE `defaults2` (
  `line` tinyint(1) NOT NULL default '0',
  `password` varchar(10) NOT NULL default '',
  `logo` varchar(100) NOT NULL default '',
  `bgcolor` varchar(6) NOT NULL default '',
  `tdcolor` varchar(6) NOT NULL default '',
  `fontcolor` varchar(6) NOT NULL default ''
) ENGINE=MyISAM;

INSERT INTO defaults2 VALUES (1, 'admin1', '', '000050', '808080', 'ffffff');

# --------------------------------------------------------

#
# Table structure for table `files`
#

DROP TABLE IF EXISTS `files`;
CREATE TABLE `files` (
  `id` int(10) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL default '',
  `link` varchar(250) NOT NULL default '',
  `count` int(10) NOT NULL default '0',
  `lastdlby` int(10) NOT NULL default '0',
  `lastdl` datetime NOT NULL default '0000-00-00 00:00:00',
  `lasteditby` int(10) NOT NULL default '0',
  `lastedit` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM PACK_KEYS=1 AUTO_INCREMENT=1 ;

# --------------------------------------------------------

#
# Table structure for table `games`
#

DROP TABLE IF EXISTS `games`;
CREATE TABLE `games` (
  `id` int(10) NOT NULL auto_increment,
  `gamename` varchar(50) NOT NULL default '0',
  `abbreviation` varchar(25) NOT NULL default '',
  `description` varchar(250) NOT NULL default '',
  `status` tinyint(1) NOT NULL default '0',
  `forumid` int(10) NOT NULL default '0',
  `lasteditby` int(10) NOT NULL default '0',
  `lastedit` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM PACK_KEYS=1 AUTO_INCREMENT=2 ;

# --------------------------------------------------------

#
# Table structure for table `ladders`
#

DROP TABLE IF EXISTS `ladders`;
CREATE TABLE `ladders` (
  `id` int(10) NOT NULL auto_increment,
  `gameon` int(10) NOT NULL default '0',
  `laddername` varchar(50) NOT NULL default '',
  `abbreviation` varchar(25) NOT NULL default '',
  `type` tinyint(2) NOT NULL default '0',
  `description` varchar(250) NOT NULL default '',
  `openplay` tinyint(1) NOT NULL default '0',
  `minmembers` tinyint(3) NOT NULL default '0',
  `maxmembers` tinyint(3) NOT NULL default '0',
  `minmaps` tinyint(2) NOT NULL default '0',
  `maxmaps` tinyint(2) NOT NULL default '0',
  `inactivedaysa` int(2) NOT NULL default '0',
  `inactivedrop` tinyint(2) NOT NULL default '0',
  `inactivedaysb` int(2) NOT NULL default '0',
  `challenge` tinyint(1) NOT NULL default '0',
  `challranks` int(10) NOT NULL default '0',
  `respondhours` int(3) NOT NULL default '0',
  `playdays` tinyint(2) NOT NULL default '0',
  `timescangive` tinyint(2) NOT NULL default '0',
  `starthour` tinyint(2) NOT NULL default '0',
  `endhour` tinyint(2) NOT NULL default '0',
  `timezone` varchar(25) NOT NULL default '',
  `mapscangive` tinyint(2) NOT NULL default '0',
  `finalizehours` int(1) NOT NULL default '0',
  `mapscanaccept` tinyint(2) NOT NULL default '0',
  `skillneeded` int(10) NOT NULL default '0',
  `createdby` int(10) NOT NULL default '0',
  `created` datetime NOT NULL default '0000-00-00 00:00:00',
  `lasteditby` int(10) NOT NULL default '0',
  `lastedit` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`),
  KEY `gameon` (`gameon`),
  KEY `laddername` (`laddername`)
) ENGINE=MyISAM PACK_KEYS=1 AUTO_INCREMENT=3 ;

# --------------------------------------------------------

#
# Table structure for table `links`
#

DROP TABLE IF EXISTS `links`;
CREATE TABLE `links` (
  `id` int(10) NOT NULL auto_increment,
  `name` varchar(250) NOT NULL default '',
  `linkurl` varchar(250) NOT NULL default '',
  `imageurl` varchar(250) NOT NULL default '',
  `count` tinyint(10) NOT NULL default '0',
  `lasteditby` int(10) NOT NULL default '0',
  `lastedit` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM PACK_KEYS=1 AUTO_INCREMENT=1 ;

# --------------------------------------------------------

#
# Table structure for table `maps`
#

DROP TABLE IF EXISTS `maps`;
CREATE TABLE `maps` (
  `id` int(10) NOT NULL auto_increment,
  `ladderid` int(10) NOT NULL default '0',
  `mapname` varchar(50) NOT NULL default '',
  `mapfilename` varchar(50) NOT NULL default '',
  `mapinfolink` varchar(100) NOT NULL default '',
  `imageurl` varchar(100) NOT NULL default '',
  `editedby` int(10) NOT NULL default '0',
  `edited` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM PACK_KEYS=1 AUTO_INCREMENT=1 ;

# --------------------------------------------------------

#
# Table structure for table `matchdb`
#

DROP TABLE IF EXISTS `matchdb`;
CREATE TABLE `matchdb` (
  `matchid` varchar(50) NOT NULL default '',
  `winnerid` int(10) NOT NULL default '0',
  `loserid` int(10) NOT NULL default '0',
  `winneralias` varchar(50) NOT NULL default '',
  `loseralias` varchar(50) NOT NULL default '',
  `ladderid` int(10) NOT NULL default '0',
  `laddername` varchar(50) NOT NULL default '',
  `winrank` int(10) NOT NULL default '0',
  `loserank` int(10) NOT NULL default '0',
  `winscore` int(10) NOT NULL default '0',
  `losescore` int(10) NOT NULL default '0',
  `winmembers` varchar(250) NOT NULL default '',
  `losemembers` varchar(250) NOT NULL default '',
  `mapsplayed` varchar(250) NOT NULL default '',
  `mapswon` varchar(250) NOT NULL default '',
  `wincomment` varchar(250) NOT NULL default '',
  `losecomment` varchar(250) NOT NULL default '',
  `wonpoints` int(10) NOT NULL default '0',
  `wonskill` varchar(10) NOT NULL default '0',
  `reporterid` int(10) NOT NULL default '0',
  `reportdate` datetime NOT NULL default '0000-00-00 00:00:00',
  `reporterip` varchar(16) NOT NULL default '',
  `confirmerid` int(10) NOT NULL default '0',
  `confirmdate` datetime NOT NULL default '0000-00-00 00:00:00',
  `confirmerip` varchar(16) NOT NULL default '',
  KEY `matchid` (`matchid`),
  KEY `winnerid` (`winnerid`),
  KEY `loserid` (`loserid`),
  KEY `ladderid` (`ladderid`),
  KEY `reportdate` (`reportdate`),
  KEY `confirmdate` (`confirmdate`)
) ENGINE=MyISAM PACK_KEYS=1;

# --------------------------------------------------------

#
# Table structure for table `matchdbval`
#

DROP TABLE IF EXISTS `matchdbval`;
CREATE TABLE `matchdbval` (
  `matchid` varchar(50) NOT NULL default '',
  `winnerid` int(10) NOT NULL default '0',
  `loserid` int(10) NOT NULL default '0',
  `loseralias` varchar(50) NOT NULL default '',
  `ladderid` int(10) NOT NULL default '0',
  `losersmembers` varchar(250) NOT NULL default '',
  `maps` varchar(250) NOT NULL default '',
  `lcomment` varchar(250) NOT NULL default '',
  `wrank` int(10) NOT NULL default '0',
  `lrank` int(10) NOT NULL default '0',
  `points` int(10) NOT NULL default '0',
  `skill` int(10) NOT NULL default '0',
  `reportedby` int(10) NOT NULL default '0',
  `reporteddate` datetime NOT NULL default '0000-00-00 00:00:00',
  `reportedip` varchar(16) NOT NULL default '',
  PRIMARY KEY  (`matchid`),
  KEY `winnerid` (`winnerid`),
  KEY `loserid` (`loserid`),
  KEY `ladderid` (`ladderid`)
) ENGINE=MyISAM PACK_KEYS=1;

# --------------------------------------------------------

#
# Table structure for table `news`
#

DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `id` int(10) NOT NULL auto_increment,
  `headline` varchar(250) NOT NULL default '',
  `content` mediumtext NOT NULL,
  `lasteditedby` int(10) NOT NULL default '0',
  `lastedit` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`),
  KEY `headline` (`headline`)
) ENGINE=MyISAM PACK_KEYS=1 AUTO_INCREMENT=2 ;

# --------------------------------------------------------

#
# Table structure for table `passrequests`
#

DROP TABLE IF EXISTS `passrequests`;
CREATE TABLE `passrequests` (
  `id` int(10) NOT NULL auto_increment,
  `requestdate` datetime default '0000-00-00 00:00:00',
  `ipaddress` varchar(16) NOT NULL default '0.0.0.0',
  `playerid` int(10) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM PACK_KEYS=1 AUTO_INCREMENT=4 ;

# --------------------------------------------------------

#
# Table structure for table `players2`
#

DROP TABLE IF EXISTS `players2`;
CREATE TABLE `players2` (
  `pid` int(10) NOT NULL default '0',
  `tid` int(10) NOT NULL default '0',
  `ipaddress` varchar(15) NOT NULL default '',
  `joined` varchar(14) NOT NULL default '',
  `name` varchar(50) NOT NULL default '',
  `email` varchar(100) NOT NULL default '',
  `field1` varchar(100) NOT NULL default '',
  `field2` varchar(100) NOT NULL default '',
  `field3` varchar(100) NOT NULL default ''
) ENGINE=MyISAM;

# --------------------------------------------------------

#
# Table structure for table `referer`
#

DROP TABLE IF EXISTS `referer`;
CREATE TABLE `referer` (
  `date` timestamp NOT NULL,
  `referer` text
) ENGINE=MyISAM;

# --------------------------------------------------------

#
# Table structure for table `rules`
#

DROP TABLE IF EXISTS `rules`;
CREATE TABLE `rules` (
  `id` int(10) NOT NULL auto_increment,
  `ladderon` int(10) NOT NULL default '0',
  `ruletitle` varchar(250) NOT NULL default '',
  `rulebody` mediumtext NOT NULL,
  `lasteditby` int(10) NOT NULL default '0',
  `lastedit` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`),
  KEY `ladderon` (`ladderon`),
  KEY `ruletitle` (`ruletitle`)
) ENGINE=MyISAM PACK_KEYS=1 AUTO_INCREMENT=1 ;

# --------------------------------------------------------

#
# Table structure for table `sessions`
#

DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions` (
  `sessionid` varchar(50) NOT NULL default '',
  `ipaddress` varchar(50) NOT NULL default '',
  `userid` int(10) NOT NULL default '0',
  `useralias` varchar(50) NOT NULL default '',
  `usertype` int(1) NOT NULL default '0',
  `lastactive` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`sessionid`,`ipaddress`)
) ENGINE=MyISAM PACK_KEYS=1;

# --------------------------------------------------------

#
# Table structure for table `sessions2`
#

DROP TABLE IF EXISTS `sessions2`;
CREATE TABLE `sessions2` (
  `sessionid` varchar(50) NOT NULL default '',
  `ipaddress` varchar(50) NOT NULL default '',
  `tid` int(10) NOT NULL default '0',
  `password` varchar(10) NOT NULL default '',
  `lastactive` timestamp NOT NULL,
  PRIMARY KEY  (`sessionid`)
) ENGINE=MyISAM;

# --------------------------------------------------------

#
# Table structure for table `staff`
#

DROP TABLE IF EXISTS `staff`;
CREATE TABLE `staff` (
  `id` int(10) NOT NULL auto_increment,
  `displayname` varchar(25) NOT NULL default '0',
  `pass` varchar(10) NOT NULL default '0',
  `email` varchar(50) NOT NULL default '0',
  `title` varchar(25) NOT NULL default '0',
  `country` TINYINT(2) NOT NULL default '0',
  `access` varchar(10) NOT NULL default '0',
  `contact` varchar(50) NOT NULL default '',
  `addeddate` datetime NOT NULL default '0000-00-00 00:00:00',
  `addedby` varchar(30) NOT NULL default '',
  `lastlogin` datetime NOT NULL default '0000-00-00 00:00:00',
  `lasteditby` int(10) NOT NULL default '0',
  `lastedit` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM PACK_KEYS=1 AUTO_INCREMENT=2 ;

INSERT INTO staff VALUES ( '1', 'admin1', 'admin1', '', '', '', '99', '', '', '', '', '', '');

# --------------------------------------------------------

#
# Table structure for table `staffaccess`
#

DROP TABLE IF EXISTS `staffaccess`;
CREATE TABLE `staffaccess` (
  `staffid` int(10) NOT NULL default '0',
  `ladderid` int(10) NOT NULL default '0',
  `status` tinyint(1) NOT NULL default '0',
  `joindate` datetime NOT NULL default '0000-00-00 00:00:00',
  KEY `staffid` (`staffid`),
  KEY `ladderid` (`ladderid`)
) ENGINE=MyISAM PACK_KEYS=1;

# --------------------------------------------------------

#
# Table structure for table `staffcomments`
#

DROP TABLE IF EXISTS `staffcomments`;
CREATE TABLE `staffcomments` (
  `commentid` int(10) NOT NULL auto_increment,
  `id` int(10) NOT NULL default '0',
  `isateam` tinyint(1) NOT NULL default '0',
  `comment` varchar(250) NOT NULL default '',
  `display` tinyint(1) NOT NULL default '0',
  `addedby` int(10) NOT NULL default '0',
  `added` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`commentid`),
  KEY `id` (`id`)
) ENGINE=MyISAM PACK_KEYS=1 AUTO_INCREMENT=4 ;

# --------------------------------------------------------

#
# Table structure for table `teammembers`
#

DROP TABLE IF EXISTS `teammembers`;
CREATE TABLE `teammembers` (
  `teamid` int(10) NOT NULL default '0',
  `playerid` int(10) NOT NULL default '0',
  `status` tinyint(1) NOT NULL default '0',
  `joindate` datetime NOT NULL default '0000-00-00 00:00:00',
  KEY `teamid` (`teamid`),
  KEY `playerid` (`playerid`)
) ENGINE=MyISAM PACK_KEYS=1;

# --------------------------------------------------------

#
# Table structure for table `teammembersinv`
#

DROP TABLE IF EXISTS `teammembersinv`;
CREATE TABLE `teammembersinv` (
  `teamid` int(10) NOT NULL default '0',
  `playerid` int(10) NOT NULL default '0',
  `inviterid` int(10) NOT NULL default '0',
  `invitedate` datetime NOT NULL default '0000-00-00 00:00:00',
  KEY `teamid` (`teamid`),
  KEY `playerid` (`playerid`),
  KEY `inviterid` (`inviterid`)
) ENGINE=MyISAM PACK_KEYS=1;

# --------------------------------------------------------

#
# Table structure for table `teams`
#

DROP TABLE IF EXISTS `teams`;
CREATE TABLE `teams` (
  `id` int(10) NOT NULL auto_increment,
  `teamname` varchar(50) NOT NULL default '0',
  `teamemail` varchar(100) NOT NULL default '',
  `tag` varchar(25) NOT NULL default '0',
  `tagplace` tinyint(1) NOT NULL default '0',
  `website` varchar(100) NOT NULL default '0',
  `logo` varchar(100) NOT NULL default '0',
  `contact` varchar(100) NOT NULL default '',
  `joindate` datetime NOT NULL default '0000-00-00 00:00:00',
  `idcreate` int(10) NOT NULL default '0',
  `ipcreate` varchar(16) NOT NULL default '0.0.0.0',
  `type` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `teamname` (`teamname`)
) ENGINE=MyISAM PACK_KEYS=1 AUTO_INCREMENT=11 ;

# --------------------------------------------------------

#
# Table structure for table `tourneys`
#

DROP TABLE IF EXISTS `tourneys`;
CREATE TABLE `tourneys` (
  `tid` int(10) NOT NULL auto_increment,
  `size` tinyint(2) NOT NULL default '0',
  `type` tinyint(2) NOT NULL default '0',
  `active` tinyint(1) NOT NULL default '0',
  `name` varchar(50) NOT NULL default '',
  `details` text NOT NULL,
  `field1` varchar(50) NOT NULL default '',
  `field2` varchar(50) NOT NULL default '',
  `field3` varchar(50) NOT NULL default '',
  `directors` varchar(250) NOT NULL default '',
  `password` varchar(10) NOT NULL default '',
  `logo` varchar(100) NOT NULL default '',
  `bgcolor` varchar(6) NOT NULL default '',
  `tdcolor` varchar(6) NOT NULL default '',
  `fontcolor` varchar(6) NOT NULL default '',
  `ladderid` int(10) NOT NULL default '0',
  `open` tinyint(4) NOT NULL default '1',
  PRIMARY KEY  (`tid`)
) ENGINE=MyISAM AUTO_INCREMENT=11 ;

# --------------------------------------------------------

#
# Table structure for table `users`
#

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) NOT NULL auto_increment,
  `alias` varchar(50) NOT NULL default '0',
  `pass` varchar(25) NOT NULL default '0',
  `email` varchar(50) NOT NULL default '0',
  `icq` varchar(10) NOT NULL default '0',
  `aim` varchar(25) NOT NULL default '0',
  `yahoo` varchar(35) NOT NULL default '0',
  `msn` varchar(35) NOT NULL default '0',
  `website` varchar(100) NOT NULL default '0',
  `logo` varchar(100) NOT NULL default '0',
  `newsletter` tinyint(1) NOT NULL default '0',
  `joindate` datetime default '0000-00-00 00:00:00',
  `ipaddress` varchar(16) NOT NULL default '0.0.0.0',
  `matcheswon` int(10) NOT NULL default '0',
  `matcheslost` int(10) NOT NULL default '0',
  `points` int(10) NOT NULL default '0',
  `skill` int(10) NOT NULL default '0',
  `money` int(10) NOT NULL default '0',
  `lastlogin` datetime NOT NULL default '0000-00-00 00:00:00',
  `theme` varchar(30) NOT NULL default '',
  `country` char(2) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `alias` (`alias`)
) ENGINE=MyISAM PACK_KEYS=1 AUTO_INCREMENT=7 ;

# --------------------------------------------------------

#
# Table structure for table `usersinfo`
#

DROP TABLE IF EXISTS `usersinfo`;
CREATE TABLE `usersinfo` (
  `id` int(10) NOT NULL default '0',
  `firstname` varchar(50) NOT NULL default '0',
  `middlename` varchar(50) NOT NULL default '0',
  `lastname` varchar(50) NOT NULL default '0',
  `address` varchar(50) NOT NULL default '0',
  `city` varchar(50) NOT NULL default '0',
  `state` char(2) NOT NULL default '0',
  `zipcode` varchar(10) NOT NULL default '0',
  `phone` bigint(10) NOT NULL default '0',
  `birthday` date NOT NULL default '0000-00-00',
  `occupation` char(2) NOT NULL default '0',
  `required` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM PACK_KEYS=1;

# --------------------------------------------------------

#
# Table structure for table `validate`
#

DROP TABLE IF EXISTS `validate`;
CREATE TABLE `validate` (
  `vcode` varchar(50) NOT NULL default '',
  `alias` varchar(50) NOT NULL default '0',
  `pass` varchar(25) NOT NULL default '0',
  `email` varchar(50) NOT NULL default '0',
  `icq` varchar(10) NOT NULL default '0',
  `aim` varchar(25) NOT NULL default '0',
  `yahoo` varchar(50) NOT NULL default '0',
  `msn` varchar(35) NOT NULL default '0',
  `website` varchar(100) NOT NULL default '0',
  `logo` varchar(100) NOT NULL default '0',
  `newsletter` tinyint(1) NOT NULL default '0',
  `joindate` datetime NOT NULL default '0000-00-00 00:00:00',
  `ipaddress` varchar(16) NOT NULL default '0.0.0.0',
  `firstname` varchar(50) NOT NULL default '0',
  `middlename` varchar(50) NOT NULL default '0',
  `lastname` varchar(50) NOT NULL default '0',
  `address` varchar(50) NOT NULL default '0',
  `city` varchar(50) NOT NULL default '0',
  `state` char(2) NOT NULL default '0',
  `zipcode` varchar(10) NOT NULL default '0',
  `phone` bigint(10) NOT NULL default '0',
  `birthday` date NOT NULL default '0000-00-00',
  `occupation` char(2) NOT NULL default '0',
  `theme` varchar(30) NOT NULL default '0',
  `country` char(2) NOT NULL default '',
  PRIMARY KEY  (`vcode`)
) ENGINE=MyISAM PACK_KEYS=1;

#
# Table structure for table `settings`
#

DROP TABLE IF EXISTS `settings`;
CREATE TABLE settings (
  email varchar(35) NOT NULL default '',
  theme varchar(15) NOT NULL default '',
  shortname varchar(5) NOT NULL default '',
  longname varchar(25) NOT NULL default '',
  forumurl varchar(65) NOT NULL default '',
  bannerloc varchar(120) NOT NULL default '',
  bannerurl varchar(65) NOT NULL default '',
  joinage tinyint(2) NOT NULL default '0',
  multipleip char(3) NOT NULL default '',
  tourneyip char(3) NOT NULL default '',
  changeplyrname char(3) NOT NULL default '',
  changeteamname char(3) NOT NULL default '',
  deleteteam char(3) NOT NULL default '',
  leaveladder char(3) NOT NULL default '',
  challengedown char(3) NOT NULL default '',
  losercomment char(3) NOT NULL default '',
  winnercomment char(3) NOT NULL default '',
  invitemembers char(3) NOT NULL default '',
  allow char(3) NOT NULL default '',
  uforce char(3) NOT NULL default '',
  edit char(3) NOT NULL default '',
  newplayer char(3) NOT NULL default '',
  dobans char(3) NOT NULL default '',
  docensors char(3) NOT NULL default '',
  censorschar char(1) NOT NULL default '',
  session smallint(3) NOT NULL default '0',
  passrequests char(3) NOT NULL default '',
  flags char(3) NOT NULL default '',
  playerdb tinyint(3) NOT NULL default '0',
  teamdb tinyint(3) NOT NULL default '0',
  ladderdb tinyint(3) NOT NULL default '0',
  standings tinyint(3) NOT NULL default '0',
  uservalidate char(3) NOT NULL default '',
  headline char(3) NOT NULL default '',
  ranking tinyint(1) NOT NULL default '0',
  challranking tinyint(1) NOT NULL default '0',
  localhost char(3) NOT NULL default '',
  smtp_server varchar(65) NOT NULL default '',
  username varchar(25) NOT NULL default '',
  password varchar(25) NOT NULL default ''
) ENGINE=MyISAM;


INSERT INTO settings VALUES ('to@v2g.cl', 'Blue', 'TO Ladder', 'Tactical Ops Ladder', 'http://www.v2gamers.cl/foro/index.php?/forum/1213-tactical-ops-35/', 'http://eslgfx.net/media/de/news/2009/toaot/allgemein/toaot_banner_g1.jpg', 'http://www.toladder.tk', 14, 'yes', 'yes', 'yes', '', 'no', 'yes', 'no', 'yes', 'yes', 'yes', 'no', 'no', 'yes', 'yes', 'yes', 'yes', '*', 300, 'yes', 'yes', 25, 25, 25, 25, 'yes', 'yes', 1, 1, 'yes', '', '', '');


