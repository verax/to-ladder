<?
$indexloaded=1;
include("config/config.php");
include("$dir[func]/global.php");
if($gameid){
    display_gamepanel($gameid,$orderby);
}

else if($ladderid){
    display_ladderpanel($ladderid);
}

else{
    header("Location: $url[base]/$file[main]");
}

function display_gamepanel($gameid,$orderby){
    global $dir, $file, $url, $out, $misc;
    $gameinfo=mysql_query("SELECT * FROM games WHERE id='$gameid'");
    $game=mysql_fetch_array($gameinfo);
    if(!$game[gamename]){
        include("$dir[func]/error.php");
        display_error("Unknown Game ID.<br>");
    }

    if(!$game[status]){
        include("$dir[func]/error.php");
        display_error("This Ladder is temporarily closed.<br>");
    }

    $tablehead=table_head("show","","","left");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");

    if($orderby == "id_up"){
        $order = "id";
    }else if ($orderby == "id_down"){

        $order = "id desc";
    }else if ($orderby == "laddername_down"){

        $order = "laddername desc";
    }else if ($orderby == "laddername_up"){

        $order = "laddername";
    }else if ($orderby == "type_down"){

        $order = "type desc";
    }else if ($orderby == "type_up"){

        $order = "type";
    }else if ($orderby == "gameon_down"){

        $order = "gameon desc";
    }else if ($orderby == "gameon_up"){

        $order = "gameon";

    }else if ($orderby == "min_down"){

        $order = "minmembers desc";
    }else if ($orderby == "min_up"){

        $order = "minmembers";

    }else if ($orderby == "max_down"){

        $order = "maxmembers desc";
    }else if ($orderby == "max_up"){

        $order = "maxmembers";

    }else{

        $order = "laddername";
    }
    // GET LADDERS
    $getladders=mysql_query("SELECT id,laddername,type,minmembers,maxmembers,challenge,openplay FROM ladders WHERE gameon='$gameid' ORDER by $order");
    while(list($lid,$laddername,$type,$minmembers,$maxmembers,$challenge,$openplay)=mysql_fetch_row($getladders)){
	  $ladderdetails=mysql_fetch_array(mysql_query("SELECT count(*) AS laddercount FROM ladder_$lid"));
	  if($type < 3){
		$laddertype= "1v1";
	  }else{
		$laddertype= "Team";
	  }
	  if($openplay < 1){
		$ladderopen= "1v1";
	  }else{
		$ladderopen= "Team";
	  }

        if($altcolorw == "altcolora"){
		$altcolorw = "altcolorb";
	  }else{
		$altcolorw = "altcolora";
	  }
	  $thisladder=$thisladder."
        <tr class=$altcolorw>
        <td valign='top' align='center'>$lid</td>
	  <td valign='top' align='left'><a href='$url[base]/$file[ladder]?ladderid=$lid'>&nbsp $laddername</a></td>
	  <td valign='top' align='center'>$laddertype</td>
	  <td valign='top' align='center'>$ladderdetails[laddercount]</td>
        <td valign='top' align='center'>$minmembers</td>
        <td valign='top' align='center'>$maxmembers</td>
        <td valign='top' align='center'><a href='$url[base]/$file[ladder]?ladderid=$lid'>View</a></td>

        </tr>
        ";
    }

    if($order == "laddername desc"){
        $laddername = "laddername_up";
    }else{

        $laddername = "laddername_down";
    }

    if($order == "id desc"){
        $id = "id_up";
    }else{

        $id = "id_down";
    }

    if($order == "type desc"){
        $type = "type_up";
    }else{

        $type = "type_down";
    }

    if($order == "gameon desc"){
        $gameon = "gameon_up";
    }else{

        $gameon = "gameon_down";
    }
	
    if($order == "minmembers desc"){
        $min = "min_up";
    }else{

        $min = "min_down";
    }
	
    if($order == "maxmembers desc"){
        $max = "max_up";
    }else{

        $max = "max_down";
    }


    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <table width='80%' align='center' border='0' cellspacing='0' cellpadding='5'>
    <tr>
    <td width='100%' valign='top' align='left'>
    <table width='100%' border='0' cellspacing='0' cellpadding='0'>
    <tr>
    <td width='100%' valign='top' align='left'><strong>$game[gamename]</strong> : $game[description]</td>
    </tr>
    </table>
    <br>
    <table width='100%' border='0' cellspacing='0' cellpadding='0'>
    <tr>
    <td valign='top' align='center'>";
    //LADDERS
    if($thisladder){
        $out[body]=$out[body]."
        $tablehead
        &nbsp; &nbsp;<strong><font class='catfont'>Ladders</font></strong>
        <hr class='catfont' size='1'>
        <table width='100%' border='0' cellspacing='1' cellpadding='0'>
        <tr class='altcolor'>
        <td align='center' colspan='2' width='33%'><b><font class='catfont'>Ladder Details</font></b></td>
        <td align='center' colspan='1'><b><font class='catfont'>Ladder Type</font></b></td>
        <td align='center' colspan='1'><b><font class='catfont'>Number</font></b></td>
        <td align='center' colspan='2'><b><font class='catfont'>Members</font></b></td>
        <td align='center' colspan='1'><b><font class='catfont'>Ladder</font></b></td>
        </tr>
	  <tr class='altcolor'>
        <td align='center'> <a href='$url[base]/$file[game]?gameid=$gameid&orderby=$id'>ID</a></td>
	  <td align='left' width='35%'><a href='$url[base]/$file[game]?gameid=$gameid&orderby=$laddername'>&nbsp Ladder Name</a></td>
	  <td align='center'><a href='$url[base]/$file[game]?gameid=$gameid&orderby=$type'>Type</a></td>
        <td align='center'>Players/Teams</td>
	  <td align='center'><a href='$url[base]/$file[game]?gameid=$gameid&orderby=$min'>Min</a></td>
	  <td align='center'><a href='$url[base]/$file[game]?gameid=$gameid&orderby=$max'>Max</a></td>
	  <td align='center'>Standings</td>
        $thisladder
	  <tr class='altcolor'><td colspan='7' width='100%'><br></td></tr>
        </table>
        $tablefoot
        <br>";
    }else{
	$out[body]=$out[body]."
        $tablehead
        &nbsp; &nbsp;<strong><font class='catfont'>Ladders</font></strong>
        <hr class='catfont' size='1'>
        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
          There are no ladders set up for this game at this moment.<br>  Only ladder administrators can add new ladders.
        </table>
        $tablefoot
        <br>";
    }

    $out[body]=$out[body]."
    $thisnews
    </td>
    </tr>
    </table>
    <br>
    </td>
    </tr>
    </table>";
    include("$dir[curtheme]");
}

// LADDER PANEL
function display_ladderpanel($ladderid){
    global $dir, $file, $url, $out, $misc, $site;
    $ladderinfo=mysql_query("SELECT * FROM ladders WHERE id='$ladderid'");
    $linfo=mysql_fetch_array($ladderinfo);
    if(!$linfo[laddername]){
        $out[body]=$out[body]."Unknown Ladder ID.<br>";
        include("$dir[curtheme]");
        exit;
    }

    $gameinfo=mysql_query("SELECT gamename FROM games WHERE id='$linfo[gameon]'");
    $game=mysql_fetch_array($gameinfo);
    if(!$game[gamename]){
        $out[body]=$out[body]."Unknown Game ID.<br>";
        include("$dir[curtheme]");
        exit;
    }

    $tablehead=table_head("show","","","left");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $recentmatches=$recentmatches."
    $tablehead
    &nbsp; &nbsp;<strong><font class='catfont'>Recent Matches</font></strong>
    <hr class='catfont' size='1'>";
    $lastmatches=mysql_query("SELECT matchid,winneralias,loseralias FROM matchdb WHERE ladderid='$ladderid' ORDER by confirmdate DESC LIMIT 5");
    while(list($matchid,$winneralias,$loseralias)=mysql_fetch_row($lastmatches)){
        $recentmatches=$recentmatches."$out[bulletleft] <a href='$url[base]/$file[match]?matchid=$matchid'>$winneralias vs $loseralias</a><br>";
    }

    $recentmatches=$recentmatches."
    $tablefoot";
    $upcomingmatches=$upcomingmatches."
    $tablehead
    &nbsp; &nbsp;<strong><font class='catfont'>Upcoming Matches</font></strong>
    <hr class='catfont' size='1'>";
    $challenges=mysql_query("SELECT challid,challenger,challenged FROM challenges WHERE ladderid='$ladderid' AND finalizedby > '0' ORDER by finalizedtime DESC LIMIT 5");
    while(list($challid,$challenger,$challenged)=mysql_fetch_row($challenges)){
        $challenger=mysql_query("SELECT teamname FROM teams WHERE id='$challenger'");
        $cgr=mysql_fetch_array($challenger);
        $challenged=mysql_query("SELECT teamname FROM teams WHERE id='$challenged'");
        $cgd=mysql_fetch_array($challenged);
        $upcomingmatches=$upcomingmatches."$out[bulletleft] <a href='$url[base]/$file[match]?challid=$challid'>$cgr[teamname] vs $cgd[teamname]</a><br>";
    }

    $upcomingmatches=$upcomingmatches."
    $tablefoot";
    $overview=$overview."
    $tablehead
    &nbsp; &nbsp;<strong><font class='catfont'>Ladder Information</font></strong>
    <hr class='catfont' size='1'>";
    if($linfo[skillneeded] > 0){
        $overview=$overview."$out[bulletleft] This is an advanced ladder.<br>
        You must have a skill rating of at least $linfo[skillneeded] to join.<br>";
    }

    if($linfo[openplay]=="1"){
        $overview=$overview."$out[bulletleft] Non-Scheduled Matches are allowed.<br>";
    }else{

        $overview=$overview."$out[bulletleft] Non-Scheduled Matches are not allowed.<br>";
    }

    if($linfo[challenge] > 0){
        $overview=$overview."$out[bulletleft] Challenging is allowed.<br>";
    }else{

        $overview=$overview."$out[bulletleft] Challenging is not allowed.<br>";
    }

    if($linfo[minmembers]=="1"){
        $overview=$overview."$out[bulletleft] A team must have at least $linfo[minmembers] member before playing matches.<br>";
    }else{

        $overview=$overview."$out[bulletleft] A team must have at least $linfo[minmembers] members before playing matches.<br>";
    }

    if($linfo[maxmembers]=="1"){
        $overview=$overview."$out[bulletleft] Teams can have no more than 1 member.<br>";
    }else{

        $overview=$overview."$out[bulletleft] Teams can have up to $linfo[maxmembers] members.<br>";
    }

    if(($linfo[inactivedaysa] == '500000')||($linfo[inactivedaysb] == '500000'))
    {
        $overview=$overview."";
    }

    else if($linfo[inactivedaysa] * 2 < $linfo[inactivedaysb])
    {
        $overview=$overview."$out[bulletleft] For every $linfo[inactivedaysa] days you do not play a match you will be dropped $linfo[inactivedrop] ranks.<br>
        If you continue to stay inactive for $linfo[inactivedaysb] days your team will be deleted.<br>
        This is to keep inactive players from clogging the ranks.<br>";
    }

    else if($linfo[inactivedaysa]=="$linfo[inactivedaysb]")
    {
        $overview=$overview."$out[bulletleft] If you do not play a match for $linfo[inactivedaysb] days you will be deleted.<br> This is to stop inactvite teams from clogging up the ranks.";
    }

    else{
        $overview=$overview."$out[bulletleft] If you do not play a match for $linfo[inactivedaysa] days you will be dropped $linfo[inactivedrop] ranks.<br>
        If you continue to stay inactive for $linfo[inactivedaysb] days you will be deleted.<br>
        This is to keep inactive players from clogging the ranks.<br>";
    }

    $overview=$overview."
    $tablefoot";
    if ($linfo[challranks] == '1000'){
        $linfo[challranks] = "any";
    }

    if( $linfo[respondhours] == '500000'){
        $linfo[respondhours] = "any";
    }

    $challenginginfo=$challenginginfo."
    $tablehead
    &nbsp; &nbsp;<strong><font class='catfont'>Challenge Information</font></strong>
    <hr class='catfont' size='1'>
    $out[bulletleft] Challenging is allowed.<br>
    $out[bulletleft] You can challenge teams up to $linfo[challranks] ranks above your team if they are free for challenging.<br>
    $out[bulletleft] A team has $linfo[respondhours] hours to respond to your challenge.<br>
    $out[bulletleft] A challenged team must then respond to your challenge selecting $linfo[timescangive] time/dates and $linfo[mapscangive] maps.<br>
    $out[bulletleft] The earliest a match can be scheduled for is 12 hours after the latest finalizing time.<br>
    $out[bulletleft] The latest a match can be scheduled for is $linfo[playdays] days after the the latest finalizing time.<br>";
    $timeearliest=date("g a", mktime($linfo[starthour],1,1,1,1,1));
    $timelatest=date("g a", mktime($linfo[endhour],1,1,1,1,1));
    if( $linfo[finalizehours] == '500000'){
        $linfo[finalizehours] = "any";
    }

    $challenginginfo=$challenginginfo."
    $out[bulletleft] Match times can be between $timeearliest and $timelatest $linfo[timezone].<br>
    $out[bulletleft] You must then finalize the challenge within $linfo[finalizehours] hours of the challenged team accepting.<br>
    You will then need to select $linfo[mapscanaccept] of the maps and one of the times to have the match.<br>
    $out[bulletleft] The maps you pick must be played in the order the challenged team selected them.<br>
    If for some reason a match goes beyond the maps selected before the match ends the team who is winning will select the next map and then alternate.<br>
    $out[bulletleft] The match must be played at the final selected time by the challenger.<br>
    $out[bulletleft] If a team does not show up 10 minutes after the match is to start it is considered a forfeit to the non-showing team.<br>
    make sure to save a record of having the match either by screenshots or icq conversation logs,
    as you will be contacted via email by an admin if you are acccused of forfeiting.<br>
    If you cannot prove you played the accusing team you they will be awarded the match and you will be givin a forfeit to your record.<br>
    $tablefoot";
    if($linfo[challenge] > 0){
        $lefttoppanel="$upcomingmatches";
        $righttoppanel="$recentmatches";
        $leftbotpanel="$overview";
        $rightbotpanel="$challenginginfo";
    }else{

        $lefttoppanel="$overview";
        $righttoppanel="$recentmatches";
        $leftbotpanel="";
        $rightbotpanel="";
    }

    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <table width='100%' align='center' border='0' cellspacing='0' cellpadding='5'>
    <tr>
    <td width='100%' valign='top' align='left'>
    <table width='100%' border='0' cellspacing='0' cellpadding='0'>
    <tr>
    <td width='250' valign='top' align='left'><strong>
    <a href='$url[base]/$file[game]?gameid=$linfo[gameon]'>$game[gamename]</a>
    &#155; <a href='$url[base]/$file[ladder]?ladderid=$ladderid'>
    $linfo[laddername]</a></strong></td>
    <td width='5' valign='top' align='left'>&nbsp;</td>
    <td valign='top' align='left'>$linfo[description]</td>
    <td width='5' valign='top' align='right'>&nbsp;</td>
    </tr>
    </table>
    <br>
    <center><strong>
    <a href='$url[base]/$file[ladder]?ladderid=$ladderid'>View Ladder</a>
    </strong></center>
    <br>
    <table width='100%' border='0' cellspacing='0' cellpadding='0'>
    <tr>
    <td width='50%' valign='top' align='center'>
    $lefttoppanel
    </td>
    <td width='10' valign='top' align='center'>&nbsp;</td>
    <td width='50%' valign='top' align='center'>
    $righttoppanel
    </td>
    </tr>
    </table>
    <br>
    <table width='100%' border='0' cellspacing='0' cellpadding='0'>
    <tr>
    <td width='50%' valign='top' align='center'>
    $leftbotpanel
    </td>
    <td width='10' valign='top' align='center'>&nbsp;</td>
    <td width='50%' valign='top' align='center'>
    $rightbotpanel
    </td>
    </tr>
    </table>
    <br>
    </td>
    </tr>
    </table>";

    include("$dir[curtheme]");
}

?>
