<?PHP
if(!$indexloaded){
    header("Location: ./index.php");
}

function main_page(){
    global $dir, $url, $file, $out, $misc;
    include("$dir[func]/userstats.php");
    $stats=user_stats();
    $tablehead=table_head("show","100%","","left");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    //GET NEWS HEADLINES
    $query = "SELECT * FROM news ORDER by id DESC LIMIT 7";
    $getnews = mysql_query($query);
    if($misc[headline] == "yes"){
    		while(list($id,$headline)=mysql_fetch_row($getnews)){
        		$headlines = $headlines."$out[bulletleft] <a href='$url[base]/$file[help]?newsid=$id'>$headline</a><br>";
    		}
		if(!$headlines){
        		$headlines = "$out[bulletleft] No News Headlines";
   		}

		$newsout = "<br>$tablehead&nbsp; &nbsp;<strong><font class='catfont'>Headlines</font></strong><br><hr class='catfont' size='1'>$headlines $tablefoot<br>";


     }else{
		$newsout = "<br>";
		while(list($id,$headline,$content,$reportedby,$reporteddate)=mysql_fetch_row($getnews)){
        		$reportername = mysql_fetch_array(mysql_query("SELECT displayname FROM staff WHERE id = '$reportedby'"));
			$newsout = $newsout."$tablehead<strong><font class='catfont'>$out[bulletleft] $headline</font></strong><br><i><small>Posted $reporteddate by $reportername[displayname] </small></i><br><hr class='catfont' size='1'>$content $tablefoot<br>";
		}
    }

    //GET RECENT MATCHES
    $query = "SELECT matchid,winneralias,loseralias FROM matchdb ORDER by confirmdate DESC LIMIT 5";
    $lastmatches = mysql_query($query);
    while(list($matchid,$winneralias,$loseralias)=mysql_fetch_row($lastmatches)){
        $recentmatches=$recentmatches."$out[bulletleft] <a href='$url[base]/$file[match]?matchid=$matchid'>$winneralias vs $loseralias</a><br>";
    }

    if(!$recentmatches){
        $recentmatches = "$out[bulletleft] No Recent Matches";
    }

    $date = date('Y-m-d H:i:s', mktime(0, 0, 0, date("m") , date("d") + 7, date("Y")));
    //GET UPCOMING MATCHES
    $query = "SELECT challid,challengeralias,challengedalias FROM challenges WHERE finalizedby > '0' AND  finalizedtime < '$date' ORDER by finalizedtime DESC LIMIT 5";
    $comingmatches=mysql_query($query);
    while(list($matchid,$winneralias,$loseralias)=mysql_fetch_row($comingmatches)){
        $upcomingmatches=$upcomingmatches."$out[bulletleft] <a href='$url[base]/$file[match]?challid=$matchid'>$winneralias vs $loseralias</a><br>";
    }

    if(!$upcomingmatches){
        $upcomingmatches="$out[bulletleft] No Upcoming Matches";
    }

    //GET LINKS
    $query = "SELECT id,name,linkurl,imageurl FROM links ORDER by id";
    $topfiles = mysql_query($query);
    while(list($linkid,$linkname,$linkurl,$linkimage)=mysql_fetch_row($topfiles)){
        if(!$linkimage){
            $linklist=$linklist."$out[bulletleft] <a href='$linkurl' target='top'>$linkname</a><br>";
        }else{

            $linklistb=$linklistb."<br><center><a href='$linkurl' target='top'><img src='$linkimage' border='0' alt='$linkname'></a></center>";
        }

        $linkimage="";
    }

    $linklist="$linklist $linklistb";
    //OUTPUT DATA
    $out[body].="
    <table width='100%' height='100%' border='0' cellspacing='0' cellpadding='0'>
    <tr>
    <td width='100%' valign='top' align='left' colspan='5'>
    <table width='100%' height='100%' border='0' cellspacing='0' cellpadding='0'>
    <tr>
    <td width='100%' height='62' valign='top' align='center'>
    $bannerhead
    $out[banner]
    $bannerfoot
    </td>
    </tr>
    </table>
    </td>
    </tr>
    <tr>
    <td width='5%' valign='top' align='left'>&nbsp;</td>
    <td width='52%' valign='top' align='left'>
    <table width='100%' height='100%' border='0' cellspacing='0' cellpadding='0'>
    <tr><td width='100%' valign='top' align='center'>
    $newsout
    $tablehead
    &nbsp; &nbsp;<strong><font class='catfont'>Recent Matches</font></strong><br>
    <hr class='catfont' size='1'>
    $recentmatches
    $tablefoot
    <br>
    $tablehead
    &nbsp; &nbsp;<strong><font class='catfont'>Upcoming Matches</font></strong><br>
    <hr class='catfont' size='1'>
    $upcomingmatches
    $tablefoot
    </td></tr>
    </table>
    </td>
    <td width='5%' valign='top'>&nbsp;</td><td width='20%' valign='top'>
    <table width='100%' height='100%' border='0' cellspacing='0' cellpadding='0'>
    <tr><td width='100%' valign='top' align='center'>
    <br>
    $tablehead
    &nbsp; &nbsp;<strong><font class='catfont'>Stats</font></strong><br>
    <hr class='catfont' size='1'>
    $stats
    $tablefoot
    <br>
    $tablehead
    &nbsp; &nbsp;<strong><font class='catfont'>Links</font></strong><br>
    <hr class='catfont' size='1'>
    $linklist
    $tablefoot
    </td></tr>
    </table>
    </td>
    <td width='18%' valign='top' align='right'>&nbsp;</td>
    </tr>
    </table>";
    include("$dir[curtheme]");
}

?>
