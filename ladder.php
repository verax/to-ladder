<?
$indexloaded=1;
include("config/config.php");
include("$dir[func]/global.php");
if($ladderid){
    ladder_home($ladderid,$rank,$cid,$sort,$unranked,$view);
}

else{
    header("Location: $url[base]/$file[main]");
}

function ladder_home($ladderid,$rank,$cid,$sort,$unranked){
    global $dir, $file, $url, $out, $plyr, $misc;
    $altcolora="' class='altcolora";
    $altcolorb="' class='altcolorb";
    //LADDER COLORS
    //$altcolora="#000033";
    //$altcolorb="#000020";
    $centerrowcolor="3a4466";
    $sortbycolumncolor="#3a4466";
    $tday=date("Y-m-d H:i:s");
    $thisladder=mysql_query("SELECT * FROM ladders WHERE id='$ladderid'");
    $linfo=mysql_fetch_array($thisladder);
    if(!$linfo[laddername]){
        include("$dir[func]/error.php");
        display_error("Unknown Ladder ID. This ladder may have been deleted.<br>");
    }

    $gameinfo=mysql_query("SELECT gamename,abbreviation,status,forumid FROM games WHERE id='$linfo[gameon]'");
    $game=mysql_fetch_array($gameinfo);
    if(!$game[gamename]){
        include("$dir[func]/error.php");
        display_error("Unknown Game ID.<br>");
    }

    if(!$game[status]){
        include("$dir[func]/error.php");
        display_error("This Ladder is temporarily closed.<br>");
    }

    //CHECK FOR LOGGED IN PLAYER*
    if($plyr[id]){
        //CHECK IF PLAYER IS ON THIS LADDER
        $playerteaminfo=mysql_query("SELECT teamid,status FROM teammembers WHERE playerid='$plyr[id]'");
        while($row=mysql_fetch_array($playerteaminfo)){
            if(mysql_fetch_array(mysql_query("SELECT * FROM ladder_$ladderid WHERE id='$row[teamid]'"))){
                $ptinfo = $row;
            }

        }

    }

    if($ptinfo[teamid]){
        $teaminfo=mysql_query("SELECT teamname FROM teams WHERE id='$ptinfo[teamid]'");
        $team=mysql_fetch_array($teaminfo);
        $teamladderinfo=mysql_query("SELECT status,rank FROM ladder_$ladderid WHERE id='$ptinfo[teamid]'");
        $tlinfo=mysql_fetch_array($teamladderinfo);
    }

    // SORT TYPES
    if($sort=="id_up"){
        $sortby="id desc";
    }

    else if($sort=="rank_up"){
        $sortby="rank DESC";
    }

    else if($sort=="wins_up"){
        $sortby="wins DESC";
    }

    else if($sort=="losses_up"){
        $sortby="losses DESC";
    }

    else if($sort=="percent_up"){
        $sortby="percent";
    }

    else if($sort=="streak_up"){
        $sortby="streak DESC";
    }

    else if($sort=="id_down"){
        $sortby="id";
    }

    else if($sort=="rank_down"){
        $sortby="rank";
    }

    else if($sort=="wins_down"){
        $sortby="wins";
    }

    else if($sort=="losses_down"){
        $sortby="losses";
    }

    else if($sort=="percent_down"){
        $sortby="percent desc";
    }

    else if($sort=="streak_down"){
        $sortby="streak";
    }

    else if($sort=="idle_down"){
        $sortby="lastmatch";
    }

    else if($sort=="idle_up"){
        $sortby="lastmatch desc";
    }

    else{
        $sortby="rank";
    }

    $tdsortby=split(" ", $sortby);
    $tdsortby="$tdsortby[0]";
    $td[$tdsortby]="bgcolor='$sortbycolumncolor'";
    $totalranked=mysql_query("SELECT COUNT(*) FROM ladder_$ladderid WHERE rank > '0'");
    $totalranked=mysql_fetch_array($totalranked);
    $totalranked="$totalranked[0]";
    $totalunranked=mysql_query("SELECT COUNT(*) FROM ladder_$ladderid WHERE rank < '1'");
    $totalunranked=mysql_fetch_array($totalunranked);
    $totalunranked="$totalunranked[0]";
    if($totalranked < 1){
        $unranked=1;
    }else{

        $unranked="$unranked";
    }

    $maxresults=$misc[standings];
    $stop=$maxresults;
    if($cid){
        $cteamladderinfo=mysql_query("SELECT rank FROM ladder_$ladderid WHERE id='$cid'");
        $ctlinfo=mysql_fetch_array($cteamladderinfo);
        $crank="$ctlinfo[rank]";
        $halfmaxresults=($maxresults / 2);
        $startresult=($crank - $halfmaxresults);
        if($startresult < 1){
            $rank=0;
        }else{

            $rank=$startresult;
        }

    }

    $rank=round($rank);
    if(!$rank){
        $start="0";
    }else{

        $start=round($rank-1);
    }

    $tablehead=table_head("show","","","left");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    if($rank){
        $rankcount=$rank;
    }else{

        $rankcount=1;
    }

    if($unranked){
        $ladusers=mysql_query("SELECT id,joindate,lastmatch,lastplayed,status,statusdate,statusdisplay,rank,lastrank,bestrank,worstrank,wins,losses,games,percent,streak,beststreak,worststreak,points,skill,money,forfeits,icon,DATE_FORMAT(statusdate, '%M %d, %l:%i %p') FROM ladder_$ladderid WHERE rank < '1' ORDER by $sortby limit $start,$stop");
    }else{

        $ladusers=mysql_query("SELECT id,joindate,lastmatch,lastplayed,status,statusdate,statusdisplay,rank,lastrank,bestrank,worstrank,wins,losses,games,percent,streak,beststreak,worststreak,points,skill,money,forfeits,icon,DATE_FORMAT(statusdate, '%M %d, %l:%i %p') FROM ladder_$ladderid WHERE rank > '0' ORDER by $sortby limit $start,$stop");
    }

    while(list($id,$joindate,$lastmatch,$lastplayed,$status,$statusdate,$statusdisplay,$currank,$lastrank,$bestrank,$worstrank,$wins,$losses,$games,$percent,$streak,$beststreak,$worststreak,$points,$skill,$money,$forfeits,$icon,$statusdateb)=mysql_fetch_row($ladusers)){
        $teaminfo=mysql_query("SELECT teamname,idcreate FROM teams WHERE id='$id'");
        $tinfo=mysql_fetch_array($teaminfo);
        $name="$tinfo[teamname]";
        $flag=mysql_fetch_array(mysql_query("SELECT country FROM users WHERE id='$tinfo[idcreate]'"));
        if($flag[0] == ""){
            $flag[0] = "";
        }

        if(!$name){
            $name="Unknown [$id]";
        }

        if($linfo[challenge]){
            $statusmessage="$statusdisplay";
        }

        if(!$statusmessage){
            $lastplayedteam=mysql_query("SELECT teamname FROM teams WHERE id='$lastplayed'");
            $lptinfo=mysql_fetch_array($lastplayedteam);
            if($lptinfo[teamname]){
                if($streak > 0){
                    $statusmessage="<a href='$url[base]/$file[teams]?teamid=$lastplayed'>Defeated $lptinfo[teamname]</a>";
                }else{

                    $statusmessage="<a href='$url[base]/$file[teams]?teamid=$lastplayed'>Lost to $lptinfo[teamname]</a>";
                }

            }else{

                $statusmessage="N/A";
            }

        }

        if($unranked){
            $centerahref="";
        }else{

            $centerahref="<a href='$url[base]/$file[ladder]?ladderid=$ladderid&cid=$id'>";
        }

        $games=($wins+$losses);
        if($games){
            $percent=round($wins/$games*100);
        }else{

            $percent="0";
        }

        if($lastmatch=="0000-00-00 00:00:00"){
            $lastmatch="$joindate";
        }else{

            $lastmatch="$lastmatch";
        }

        $lastmatch=split(" ", $lastmatch);
        $lastmatch=strtotime( "$lastmatch[0] 00:00" );
        $today=date("Y-m-d 00:00");
        $today=strtotime($today);
        $idle=(($today - $lastmatch)/86400);
        $idle=intval($idle);
        if($altladrow=="$altcolora"){
            $altladrow="$altcolorb";
        }else{

            $altladrow="$altcolora";
        }

        //UP-DOWN ARROWS RANK STATUS ARROWS
        if(($currank > 0) && (!$unranked)){
            $addthes="";
            if(($currank > $lastrank) && ($lastrank > 0)){
                $movedifference=($currank - $lastrank);
                if($movedifference > 1){
                    $addthes="s";
                }

                $arrowrankstatus="<ACRONYM TITLE='Down $movedifference Rank$addthes'><img  src='$url[themes]/images/arrow_dn.gif' style='cursor:hand;'></ACRONYM>";
            }

            else if(($currank < $lastrank) && ($lastrank > 0) || ($lastrank=="0") && ($currank > 0)){
                $movedifference=($lastrank - $currank);
                if($movedifference > 1){
                    $addthes="s";
                }

                if($lastrank=="0"){
                    $upmessage="Up from unranked";
                }else{

                    $upmessage="Up $movedifference Rank$addthes";
                }

                $arrowrankstatus="<ACRONYM TITLE='$upmessage'><img src='$url[themes]/images/arrow_up.gif' style='cursor:hand;'></ACRONYM>";
            }

            else{
                $arrowrankstatus="&nbsp; &nbsp;";
            }

        }

        //INACTIVITY WARNING
        if(($idle+2) >= $linfo[inactivedaysb]){
            $idle="<font color='FF00'>$idle</font>";
        }

        else if(($idle+2) >= $linfo[inactivedaysa]){
            $idle="<font color='FFFF00'>$idle</font>";
        }

        if($currank=="$crank"){
            $altladrow="$centerrowcolor";
            $td[$tdsortby]="";
        }else{

            $altladrow="$altladrow";
            $td[$tdsortby]="";
        }

        //START ICONS AND LOCKS
        $iconlist="";
        if(($status=="10") && ($statusdate > $tday)){
            $iconlist=$iconlist."<img src='$url[themes]/images/lock.gif' alt='Locked Untill $statusdateb' style='cursor:hand;'> ";
        }

        if($icon){
            $iconlist=$iconlist."<img src='$url[themes]/icons/$icon.gif' alt='' style='cursor:hand;'> ";
        }

        if($streak > 9){
            $iconlist=$iconlist."<img src='$url[themes]/images/hot.gif' alt='$streak game winning streak' style='cursor:hand;'> ";
        }

        if($streak < "-4"){
            $iconlist=$iconlist."<img src='$url[themes]/images/cold.gif' alt='$streak game losing streak' style='cursor:hand;'> ";
        }

        //Calculate Winning Percentage
        if(($wins+$losses) > '0'){
            $per = ($wins/($wins+$losses));
            $percentage = round($per*100,0 );
        }else{

            $percentage = '0';
        }

        $ladderdisplay=$ladderdisplay."<tr bgcolor='$altladrow' cellpadding='10'>
        <td width='' valign='center' align='center' $td[rank] bgcolor='$altladrow'>
        <font size='1' color='#0099FF'>
        $centerahref $currank</a></font> $arrowrankstatus</td>
        <td width=''  valign='center' align='left' $td[id] $td[joindate] $td[icon] $td[lastrank] $td[worstrank] $td[worststreak] $td[money] $td[forfeits]>
        <font color='#0099FF'>
        <a href='$url[base]/$file[teams]?teamid=$id'> &nbsp;&nbsp; $name</a></font> $iconlist</td>
        <td width='' valign='center' align='center' $td[wins] $td[games] $td[percent]>
        <font size='1' color='#0099FF'>$wins</font></td>
        <td width='' valign='center' align='center' $td[losses] $td[games] $td[percent]>
        <font size='1' color='#0099FF'>$losses</font></td>
        <td width='' valign='center' align='center' $td[streak]>
        <font size='1' color='#0099FF'>$streak</font></td>
        <td width='' valign='center' align='center' $td[beststreak]>
        <font size='1' color='#0099FF'>$percentage%</font></td>
        <td width='' valign='center' align='center' $td[lastplayed] $td[status] $td[statusdate] $td[statusdisplay]>
        <font size='1' color='#0099FF'>$statusmessage</font></td>
        <td width='' valign='center' align='center'  $td[lastmatch]>
        <font size='1' color='#0099FF'>$idle</font></td>
        <td width='' valign='center' align='center'  $td[id]>
        <font size='1' color='#0099FF'>$id</font></td>
        ";
        if($misc[flags] == "yes"){
            if($flag[0]){
                $ladderdisplay=$ladderdisplay."
                <td width='' valign='center' align='center'>
                <img width='20' height='14'  src='$url[themes]/images/flags/$flag[0]_small.gif'>
                </td>";
            }else{

                $ladderdisplay=$ladderdisplay."
                <td width='' valign='center' align='center'>
                </td>";
            }

        }

        $ladderdisplay=$ladderdisplay."</tr>";
        $statusmessage="";
        $rankcount++;
    }

    mysql_free_result($ladusers);
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <table width='100%' align='center' border='0' cellspacing='0' cellpadding='5'>
    <tr>
    <td width='100%' valign='top' align='left'>
    <table width='100%' border='0' cellspacing='0' cellpadding='0'>
    <tr>
    <td width='50%' valign='top' align='center'>
    $tablehead
    &nbsp; &nbsp;<strong><!--<a href='$url[base]/$file[game]?gameid=$linfo[gameon]'>
    <font class='catfont'>[$game[abbreviation]]</font></a> -->
    <a href='$url[base]/$file[game]?ladderid=$ladderid'>
    <font class='catfont'>$linfo[laddername]</font></a></strong>
    <hr class='catfont' size='1'>";
    if($team[teamname]){
        if($tlinfo[rank] > 0){
            $out[body]=$out[body]."$out[bulletleft] <a href='$url[base]/$file[ladder]?ladderid=$ladderid&cid=$ptinfo[teamid]'>$team[teamname]</a><br>";
        }else{

            $out[body]=$out[body]."$out[bulletleft] <a href='$url[base]/$file[teams]?teamid=$ptinfo[teamid]'>$team[teamname]</a><br>";
        }

        $out[body]=$out[body]."$out[bulletleft] <a href='$url[base]/$file[join]?action=leaveladdera&ladderid=$ladderid'>Leave this Ladder</a><br>";
        if($linfo[challenge]){
            $challengeinfo=mysql_query("SELECT challid,challenger,challenged,respondedby,respondedby,finalizedby FROM challenges WHERE (challenger='$ptinfo[teamid]' OR challenged='$ptinfo[teamid]') AND ladderid='$ladderid'");
            $chall=mysql_fetch_array($challengeinfo);
            if($ptinfo[teamid]=="$chall[challenger]"){
                if(($chall[respondedby] > 0) && ($chall[finalizedby] > 0)){
                    $out[body]=$out[body]."$out[bulletleft] <a href='$url[base]/$file[match]?challid=$chall[challid]'>Match Set</a><br>";
                }else if(($chall[respondedby] > 0) && ($chall[finalizedby]=="0")){

                    $out[body]=$out[body]."$out[bulletleft] <a href='$url[base]/$file[match]?action=finalize&challid=$chall[challid]'>Finalize Challenge</a><br>";
                }else{

                    $out[body]=$out[body]."$out[bulletleft] <a href='$url[base]/$file[match]?challid=$chall[challid]'>Awaiting Response</a><br>";
                }

            }else if($ptinfo[teamid]=="$chall[challenged]"){

                if(($chall[respondedby] > 0) && ($chall[finalizedby] > 0)){
                    $out[body]=$out[body]."$out[bulletleft] <a href='$url[base]/$file[match]?challid=$chall[challid]'>Match Set</a><br>";
                }else if(($chall[respondedby] > 0) && ($chall[finalizedby]=="0")){

                    $out[body]=$out[body]."$out[bulletleft] <a href='$url[base]/$file[match]?challid=$chall[challid]'>Awaiting Finalization</a><br>";
                }else{

                    $out[body]=$out[body]."$out[bulletleft] <a href='$url[base]/$file[match]?action=respond&challid=$chall[challid]'>Respond to Challenge</a><br>";
                }

            }else{

                $out[body]=$out[body]."$out[bulletleft] <a href='$url[base]/$file[match]?action=challenge&ladderid=$ladderid'>Challenge</a><br>";
            }

        }else{

            $out[body]=$out[body]."$out[bulletleft] <a href='$url[base]/$file[match]?action=challenge&ladderid=$ladderid'>Challenge</a><br>";
        }

    }else{

        $out[body]=$out[body]."$out[bulletleft] <a href='$url[base]/$file[join]?action=joinladdera&ladderid=$ladderid'>Join this Ladder</a><br>";
    }

    $out[body]=$out[body]."
    $out[bulletleft] <a href='$url[base]/$file[game]?ladderid=$ladderid'>Ladder Rules</a><br>";
    if(($linfo[minmaps] >0)&&($linfo[maxmaps] >0)){
        $out[body]=$out[body]."
        $out[bulletleft] <a href='$url[base]/$file[maps]?ladderid=$ladderid'>Map List</a><br>
        ";
    }

    $out[body]=$out[body]."
    $tablefoot
    </td>
    <td width='5' valign='top' align='center'>&nbsp;</td>
    <td width='50%' valign='top' align='center'>";
    //RANK
    //Wins
    $lhofwins=mysql_query("SELECT id FROM ladder_$ladderid WHERE rank > '0' ORDER by wins DESC limit 1");
    $lhwins=mysql_fetch_array($lhofwins);
    $thofwins=mysql_query("SELECT teamname FROM teams WHERE id='$lhwins[id]'");
    $thwins=mysql_fetch_array($thofwins);
    $hofwins="<a href='$url[base]/$file[teams]?teamid=$lhwins[id]'>$thwins[teamname]</a>";
    //Losses
    $lhoflosses=mysql_query("SELECT id FROM ladder_$ladderid WHERE rank > '0' ORDER by losses DESC limit 1");
    $lhlosses=mysql_fetch_array($lhoflosses);
    $thoflosses=mysql_query("SELECT teamname FROM teams WHERE id='$lhlosses[id]'");
    $thlosses=mysql_fetch_array($thoflosses);
    $hoflosses="<a href='$url[base]/$file[teams]?teamid=$lhlosses[id]'>$thlosses[teamname]</a>";
    //Streak
    $lhofstreak=mysql_query("SELECT id FROM ladder_$ladderid WHERE rank > '0' ORDER by streak DESC limit 1");
    $lhstreak=mysql_fetch_array($lhofstreak);
    $thofstreak=mysql_query("SELECT teamname FROM teams WHERE id='$lhstreak[id]'");
    $thstreak=mysql_fetch_array($thofstreak);
    $hofstreak="<a href='$url[base]/$file[teams]?teamid=$lhstreak[id]'>$thstreak[teamname]</a>";
    //Days Idle
    $lhofidle=mysql_query("SELECT id FROM ladder_$ladderid WHERE rank > '0' ORDER by lastmatch DESC limit 1");
    $lhidle=mysql_fetch_array($lhofidle);
    $thofidle=mysql_query("SELECT teamname FROM teams WHERE id='$lhidle[id]'");
    $thidle=mysql_fetch_array($thofidle);
    $hofidle="<a href='$url[base]/$file[teams]?teamid=$lhidle[id]'>$thidle[teamname]</a>";
    $out[body]=$out[body]."
    $tablehead
    &nbsp; &nbsp;<strong><font class='catfont'>Ladder Hall Of Fame</font></strong>
    <hr class='catfont' size='1'>
    <a href='$url[base]/$file[ladder]?ladderid=$ladderid&sort=wins'>
    Most Wins</a>  $out[bulletleft] $hofwins<br>
    <a href='$url[base]/$file[ladder]?ladderid=$ladderid&sort=losses'>
    Most Losses</a>  $out[bulletleft] $hoflosses<br>
    <a href='$url[base]/$file[ladder]?ladderid=$ladderid&sort=streak'>
    Best Streak</a> $out[bulletleft] $hofstreak<br>
    <a href='$url[base]/$file[ladder]?ladderid=$ladderid&sort=idle'>
    Longest Idle</a> $out[bulletleft] $hofidle<br>
    </font>
    $tablefoot";
    $out[body]=$out[body]."</td>
    </tr>
    </table>
    <br>";
    if($sortby == "id"){
        $id = "id_up";
    }else{

        $id = "id_down";
    }

    if($sortby == "rank"){
        $rank = "rank_up";
    }else{

        $rank = "rank_down";
    }

    if($sortby == "wins"){
        $wins = "wins_up";
    }else{

        $wins = "wins_down";
    }

    if($sortby == "losses"){
        $losses = "losses_up";
    }else{

        $losses = "losses_down";
    }

    if($sortby == "streak"){
        $streak = "streak_up";
    }else{

        $streak = "streak_down";
    }

    if($sortby == "percent"){
        $percent = "percent_up";
    }else{

        $percent = "percent_down";
    }

    if($sortby == "lastmatch"){
        $idlea = "idle_up";
    }else{

        $idlea = "idle_down";
    }

    $out[body]=$out[body]."
    $tablehead
    <table width='100%' border='0' cellspacing='1' cellpadding='1'>
    <tr class='altcolor'>
    <td width='' valign='center' align='center' colspan='1'>
    <font size='1' class='catfont'><strong>Rank</strong></font></td>
    <td width='20%' valign='center' align='center'>
    <a href='$url[base]/$file[ladder]?ladderid=$ladderid'>
    <font size='1' class='catfont'><strong>Account</strong></font></a></td>
    <td width='' valign='center' align='center' colspan='4'>
    <font size='1' class='catfont'><strong>Record</strong></font></td>
    <td width='' valign='center' align='center' colspan='1'>
    <font size='1' class='catfont'><strong>Latest</strong></font></td>
    <td width='' valign='center' align='center' colspan='1'>
    <font size='1' class='catfont'><strong>Days</strong></font></td>
    <td width='' valign='center' align='center' colspan='1'>
    <font size='1' class='catfont'><strong>Team</strong></font></td>
    ";
    if($misc[flags] == "yes"){
        $out[body]=$out[body]."
        <td width='' valign='center' align='center' colspan='1'>
        <font size='1' class='catfont'><strong>Team</strong></font></td>
        ";
    }

    $out[body]=$out[body]."
    </tr>
    <tr class='altcolor'>
    <td width='' valign='center' align='Center'>
    <font size='1' color='#FFFFFF'><strong><a href='$url[base]/$file[ladder]?ladderid=$ladderid&sort=$rank'>Rank</a></strong></font></td>
    <td width='' valign='center' align='center'>
    <font size='1' color='#FFFFFF'><strong>Name</strong></font></td>
    <td width='' valign='center' align='center'>
    <font size='1' color='#FFFFFF'><strong><a href='$url[base]/$file[ladder]?ladderid=$ladderid&sort=$wins'>Wins</a></strong></font></td>
    <td width='' valign='center' align='center'>
    <font size='1' color='#FFFFFF'><strong><a href='$url[base]/$file[ladder]?ladderid=$ladderid&sort=$losses'>Loss</a></strong></font></td>
    <td width='' valign='center' align='center'>
    <font size='1' color='#FFFFFF'><strong><a href='$url[base]/$file[ladder]?ladderid=$ladderid&sort=$streak'>Streak</a></strong></font></td>
    <td width='' valign='center' align='center'>
    <font size='1' color='#FFFFFF'><strong><a href='$url[base]/$file[ladder]?ladderid=$ladderid&sort=$percent'>Win%</a></strong></font></td>
    <td width='' valign='center' align='center'>
    <font size='1' color='#FFFFFF'><strong>Information</strong></font></td>
    <td width='' valign='center' align='center'>
    <font size='1' color='#FFFFFF'><strong><a href='$url[base]/$file[ladder]?ladderid=$ladderid&sort=$idlea'>Idle</a></strong></font></td>
    <td width='' valign='center' align='center'>
    <font size='1' color='#FFFFFF'><strong><a href='$url[base]/$file[ladder]?ladderid=$ladderid&sort=$id'>Id</a></strong></font></td>
    ";
    if($misc[flags] == "yes"){
        $out[body]=$out[body]."
        <td width='' valign='center' align='center'>
        <font size='1' color='#FFFFFF'><strong>Flag</strong></font>
        </td>
        ";
    }

    $out[body]=$out[body]."
    </tr>";
    $out[body]=$out[body]."$ladderdisplay";
    $out[body]=$out[body]."
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center' colspan='11'>
    <font size='1' class='catfont'>
    There are $totalranked Ranked and $totalunranked Unranked Teams on this ladder
    </font></td>
    </tr>
    </table>
    $tablefoot
    <br>";
    // START GET COUNTS/RANKS
    $drops=1;
    $tdcount=1;
    if($unranked){
        $usercount="$totalunranked";
        $unrankedvari="&unranked=1";
    }else{

        $usercount="$totalranked";
        $unrankedvari="";
    }

    if($usercount > $maxresults){
        if($rank < $maxresults){
            $pages=$pages."[<a href='$url[base]/$file[ladder]?ladderid=$ladderid$unrankedvari'>1-$maxresults</a>]<br>";
        }else{

            $pages=$pages."<a href='$url[base]/$file[ladder]?ladderid=$ladderid$unrankedvari'>1-$maxresults</a><br>";
        }

        while($enddrop <= $usercount) {
            $drops=($drops+$maxresults);
            $tdrops=($drops+$maxresults);
            if($tdrops > $usercount){
                $enddrops=$usercount;
            }else{

                $enddrops=($drops+$maxresults-1);
            }

            if(($rank=="$drops") && ($ranked!="un")){
                $pages=$pages."[<a href='$url[base]/$file[ladder]?ladderid=$ladderid$unrankedvari&rank=$drops'>$drops-$enddrops</a>]<br>";
            }else{

                $pages=$pages."<a href='$url[base]/$file[ladder]?ladderid=$ladderid$unrankedvari&rank=$drops'>$drops-$enddrops</a><br>";
            }

            if(!$nexttd){
                $nexttd=($tdcount + 5);
            }

            if($nexttd=="$tdcount"){
                $nexttd=($tdcount + 7);
                $pages=$pages."</strong></font></td><td width='200' valign='top' align='left'><font size='1'><strong>";
            }

            $tdcount++;
            $enddrop="$tdrops";
        }

    }

    $out[body]=$out[body]."
    $tablehead
    <table width='100%' border='0' cellspacing='0' cellpadding='0'>
    <tr>
    <td width='100%' height='100%' valign='top' align='left'>
    <table border='0' cellspacing='0' cellpadding='0'>
    <tr>
    <td width='200' valign='top' align='left'>
    <font size='1'><strong>$pages</strong></font>
    <td>
    </tr>
    </table>
    </td>
    </tr>
    <tr>
    <td width='100%' valign='top' align='center'>
    <hr class='catfont' size='1'>
    <strong>";
    if(($totalranked < 1) && ($totalunranked < 1)){
        $out[body]=$out[body]."There are no Teams on this ladder";
    }else{

        if($unranked){
            if($totalranked < 1){
                $out[body]=$out[body]."There are no Ranked Teams";
            }else{

                $out[body]=$out[body]."<a href='$url[base]/$file[ladder]?ladderid=$ladderid'>Click Here</a> to view Ranked Teams";
            }

        }else{

            if($totalunranked < 1){
                $out[body]=$out[body]."There are no Unranked Teams";
            }else{

                $out[body]=$out[body]."<a href='$url[base]/$file[ladder]?ladderid=$ladderid&unranked=1'>Click Here</a> to view Unranked Teams";
            }

        }

    }

    $out[body]=$out[body]."</strong>
    </td>
    </tr>
    </table>
    $tablefoot
    <br>";
    $out[body]=$out[body]."
    </td>
    </tr>
    </table>";
    include("$dir[curtheme]");
}

?>
