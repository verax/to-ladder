<?
$indexloaded=1;
include("config/config.php");
include("$dir[func]/global.php");
$altcolora="#000033";
$altcolorb="#000020";
$altcolora="' class='altcolora";
$altcolorb="' class='altcolorb";
if(!$playerid){
    include("$dir[func]/error.php");
    display_error("Unknown Player ID.<br>");
}

$playerinfo=mysql_query("SELECT * FROM users WHERE id='$playerid'");
$myinfo=mysql_fetch_array($playerinfo);
if(!$myinfo[alias]){
    include("$dir[func]/error.php");
    display_error("Unknown User.<br>");
}

$playerinfo=mysql_query("SELECT *,DATE_FORMAT(joindate, '%M %d, %Y') FROM users WHERE id='$playerid'");
$myinfo=mysql_fetch_array($playerinfo);
if(!$myinfo[alias]){
    include("$dir[func]/error.php");
    display_error("Unknown Player.<br>");
}

if($myinfo[country] == ""){
    $country = "";
}else{

    $country = "<img width='20' height='14' src='$url[themes]/images/flags/$myinfo[country]_small.gif'>";
}

$tablehead=table_head("show","100%","","left");
$tablefoot=table_foot("show");
$bannerhead=table_head("show","488","80","center");
$bannerfoot=table_foot("show");
$out[body]=$out[body]."
<center>
$bannerhead
$out[banner]
$bannerfoot
</center>
<br>
$tablehead
&nbsp; &nbsp;<strong><font class='catfont'>Player Information</font></strong><br>
<hr class='catfont' size='1'>
<table width='100%' border='0' bordercolor='#000000' cellspacing='0' cellpadding='0'>
<tr>
<td width='50%' valign='top' align='left'>
<!-- PLAYER INFO -->
<table width='100%' border='0' bordercolor='#000000' cellspacing='1' cellpadding='1'>
<tr class='altcolor'>
<td width='100%' valign='center' align='center' colspan='2'>
<strong>Player Information</font></strong>
</tr>
<tr class='altcolorb'>
<td width='40%' valign='center' align='left'>ID</td>
<td width='60%' valign='center' align='right'>$playerid</td>
</tr>
<tr class='altcolorb'>
<td width='40%' valign='center' align='left'>Name</td>
<td width='60%' valign='center' align='right'>$myinfo[alias]</td>
</tr>
<tr class='altcolorb'>
<td width='40%' valign='center' align='left'>Joindate</td>
<td width='60%' valign='center' align='right'>$myinfo[21]</td>
</tr>
<tr class='altcolorb'>
<td width='40%' valign='center' align='left'>Icq Number</td>
<td width='60%' valign='center' align='right'>$myinfo[icq]</td>
</tr>
<tr class='altcolorb'>
<td width='40%' valign='center' align='left'>Aim Name</td>
<td width='60%' valign='center' align='right'>$myinfo[aim]</td>
</tr>
<tr class='altcolorb'>
<td width='40%' valign='center' align='left'>MSN Messenger</td>
<td width='60%' valign='center' align='right'>$myinfo[msn]</td>
</tr>
<tr class='altcolorb'>
<td width='40%' valign='center' align='left'>Yahoo Messenger</td>
<td width='60%' valign='center' align='right'>$myinfo[yahoo]</td>
</tr>
<tr class='altcolorb'>
<td width='40%' valign='center' align='left'>Total Wins</td>
<td width='60%' valign='center' align='right'>$myinfo[matcheswon]</td>
</tr>
<tr class='altcolorb'>
<td width='40%' valign='center' align='left'>Total Losses</td>
<td width='60%' valign='center' align='right'>$myinfo[matcheslost]</td>
</tr>
";
if($misc[flags] == "yes"){
    $out[body]=$out[body]."
    <tr class='altcolorb'>
    <td width='40%' valign='center' align='left'>Country</td>
    <td width='60%' valign='center' align='right'>$country</td>
    </tr>
    ";
}

$out[body]=$out[body]."
<tr class='altcolorb'>
<td width='100%' valign='bottom' align='center' colspan='2'>
Email Player <a href='mailto:$myinfo[email]'><img src='$url[themes]/images/email.gif' border='0'></a></td>
</tr>
<tr class='altcolorb'>
<td width='100%' valign='bottom' align='center' colspan='2'><a href='$myinfo[website]' target='up'>Launch Player Website</a></td>
</tr>
</table>
<!-- END PLAYER INFO -->
</td>
<td width='50%' height='100%' valign='top' align='right'>
<!-- PLAYER LOGO -->
<table width='100%' height='100%' border='0' bordercolor='#000000' cellspacing='1' cellpadding='1'>
<tr class='altcolor'>
<td width='100%' valign='top' align='center'>
<strong>Player Logo</strong></td>
</tr>
<tr>
<td width='100%' height='100%' valign='center' align='center'>";
if(($myinfo[logo]) && ($myinfo[logo]!="http://")){
    $out[body]=$out[body]."<a href='$myinfo[website]' target='up'><img src='$myinfo[logo]' border='0'></a>";
}else{

    $out[body]=$out[body]."<a href='$myinfo[website]' target='up'><img src='$misc[playerlogo]' border='0'></a>";
}

$out[body]=$out[body]."</td>
</tr>
</table>
<!-- END PLAYER LOGO -->
</td>
</tr>
</table>
$tablefoot
<br>
$tablehead
&nbsp; &nbsp;<strong><font class='catfont'>Team Information</font></strong><br>
<hr class='catfont' size='1'>
<table width='100%' border='0' bordercolor='#000000' cellspacing='0' cellpadding='0'>
<table width='100%' border='0' bordercolor='#000000' cellspacing='1' cellpadding='2' bgcolor=''>
<tr class='altcolor'>
<td width='30%' valign='center' align='left'><strong>Team Name</strong></td>
<td width='20%' valign='center' align='center'><strong>Team Type</strong></td>
<td width='20%' valign='center' align='center'><strong>Status</strong></td>
<td width='20%' valign='center' align='center'><strong>Joindate</strong></td>
</tr>";
$teamson=mysql_query("SELECT teamid,status,DATE_FORMAT(joindate, '%M %d, %Y') FROM teammembers WHERE playerid='$playerid' ORDER by joindate");
while(list($team,$status,$joindate)=mysql_fetch_row($teamson)){
    $teaminfo=mysql_query("SELECT teamname,type FROM teams WHERE id='$team'");
    $tinfo=mysql_fetch_array($teaminfo);
    if($tinfo[type] == '1'){
        $teamtype = "Singles";
    }else if($tinfo[type] == '3'){

        $teamtype = "Team";
    }else{

        $teamtype = "";
    }

    $status=member_status($status);
    $out[body]=$out[body]."
    <tr class='altcolorb'><a name='$teamid'></a>
    <td width='30%' valign='center' align='left'><a href='$url[base]/$file[teams]?teamid=$team'>$tinfo[teamname]</a></td>
    <td width='20%' valign='center' align='center'>$teamtype</td>
    <td width='20%' valign='center' align='center'>$status</td>
    <td width='20%' valign='center' align='center'>$joindate</td>
    </tr>";
    $onteam=1;
}

if(!$onteam){
    $out[body]=$out[body]."
    <tr class='altcolorb'>
    <td width='100%' valign='center' align='center' colspan='4'>Not currently on any teams</td>
    </tr>";
}

$out[body]=$out[body]."</table>
$tablefoot
<br>
$tablehead
&nbsp; &nbsp;<strong><font class='catfont'>Admin Comments</font></strong><br>
<hr class='catfont' size='1'>
<table width='100%' border='0' bordercolor='#000000' cellspacing='1' cellpadding='2' bgcolor=''>
<tr class='altcolor'>
<td width='' valign='center' align='left'><strong>Comment</strong></td>
<td width='150' valign='center' align='center'><strong>Added</strong></td>
<td width='150' valign='center' align='center'><strong>By</strong></td>
</tr>";
//GET COMMENTS
$getcomments=mysql_query("SELECT comment,addedby,DATE_FORMAT(added, '%M %d, %l:%i %p') FROM staffcomments WHERE id='$playerid' AND isateam='0' AND display='1' ORDER by added");
while(list($comment,$addedby,$added)=mysql_fetch_row($getcomments)){
    $staffmember=mysql_query("SELECT displayname FROM staff WHERE id='$addedby'");
    $staff=mysql_fetch_array($staffmember);
    if($altladrow=="$altcolora"){
        $altladrow="$altcolorb";
    }else{

        $altladrow="$altcolora";
    }

    $out[body]=$out[body]."
    <tr bgcolor='$altladrow'>
    <td width='' valign='center' align='left'>$comment</td>
    <td width='150' valign='center' align='center'>$added</td>
    <td width='150' valign='center' align='center'>$staff[displayname]</td>
    </tr>";
}

//Show Admin Comments Only Visible To Staff
if ($admn[id]){
    $getcomments=mysql_query("SELECT comment,addedby,DATE_FORMAT(added, '%M %d, %l:%i %p') FROM staffcomments WHERE id='$playerid' AND isateam='0' AND display='0' ORDER by added");
    while(list($comment,$addedby,$added)=mysql_fetch_row($getcomments)){
        if($altladrow=="$altcolora"){
            $altladrow="$altcolorb";
        }else{

            $altladrow="$altcolora";
        }

        $staffmember=mysql_query("SELECT displayname FROM staff WHERE id='$addedby'");
        $staff=mysql_fetch_array($staffmember);
        $out[body]=$out[body]."
        <tr bgcolor='$altladrow'>
        <td width='' valign='center' align='left'>[Admin Visible Comment] $comment</td>
        <td width='150' valign='center' align='center'>$added</td>
        <td width='150' valign='center' align='center'>$staff[displayname]</td>
        </tr>
        ";
    }

}

$out[body]=$out[body]."
</table>
$tablefoot";
include("$dir[curtheme]");
function member_status($status){
    if($status=="1"){ $status="Leader"; }

    if($status=="2"){ $status="Co-Leader"; }

    if($status=="3"){ $status="Captain"; }

    if($status=="4"){ $status="Co-Captain"; }

    if($status=="5"){ $status="Member"; }

    if($status=="6"){ $status="Training"; }

    if($status=="7"){ $status="Inactive"; }

    if($status=="8"){ $status="Suspended"; }

    return($status);
}

?>
