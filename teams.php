<?
$indexloaded=1;
include("config/config.php");
include("$dir[func]/global.php");
if($teamid){
    display_thisteam($teamid);
}

else{
    include("$dir[func]/error.php");
    display_error("Unknown Team.<br>");
}

function display_thisteam($teamid){
    global $dir, $file, $url, $out, $misc, $admn;
    $teaminfo=mysql_query("SELECT *,DATE_FORMAT(joindate, '%M %d, %Y') FROM teams WHERE id='$teamid'");
    $tinfo=mysql_fetch_array($teaminfo);
    if(!$tinfo[teamname]){
        include("$dir[func]/error.php");
        display_error("Unknown Team ID.<br>");
    }

    $ladders=mysql_query("SELECT id,laddername FROM ladders");
    while($row = mysql_fetch_array($ladders)){
        $teams=mysql_query("SELECT id FROM ladder_$row[id] WHERE id='$teamid'");
        if(mysql_fetch_array($teams)){
            $count++;
            if($count >1){
                $ladderlist = "".$ladderlist.",";
            }

            $ladderlist = "".$ladderlist."<a href='$url[base]/$file[game]?ladderid=$row[id]'> ".$row[laddername]."</a>";
            $count = 1;
        }

    }

    $matches=mysql_query("SELECT DATE_FORMAT(reportdate, '%M %d, %l:%i %p') FROM matchdb WHERE winnerid='$teamid' OR loserid='$teamid' ORDER by reportdate DESC");
    $lastmatch = mysql_fetch_array($matches);
    $country = mysql_fetch_array(mysql_query("SELECT country FROM users WHERE id='$tinfo[idcreate]'"));
    if($country[0] == ""){
        $flag = "";
    }else{

        $flag = "<img width='20' height='14'  src='$url[themes]/images/flags/$country[0]_small.gif'>";
    }

    $tablehead=table_head("show","100%","","left");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot
    </center>
    <br>
    $tablehead
    &nbsp; &nbsp;<strong><font class='catfont'>Team Information</font></strong><br>
    <hr class='catfont' size='1'>
    <table width='100%' border='0' bordercolor='#000000' cellspacing='0' cellpadding='0'>
    <tr>
    <td width='50%' valign='top' align='left'>
    <!-- TEAM INFO -->
    <table width='100%' border='0' bordercolor='#000000' cellspacing='1' cellpadding='1'>
    <tr class='altcolor'>
    <td width='100%' valign='center' align='center' colspan='2'>
    <strong>Team Information</strong></td>
    </tr>
    <tr class='altcolorb'>
    <td width='40%' valign='center' align='left'>Id #</td>
    <td width='60%' valign='center' align='right'>$tinfo[id]</td>
    </tr>
    <tr class='altcolorb'>
    <td width='40%' valign='center' align='left'>Name</td>
    <td width='60%' valign='center' align='right'>$tinfo[teamname]</td>
    </tr>
    <tr class='altcolorb'>
    <td width='40%' valign='center' align='left'>Tag</td>
    <td width='60%' valign='center' align='right'>$tinfo[tag]</td>
    </tr>
    <tr class='altcolorb'>
    <td width='40%' valign='center' align='left'>Ladders</td>
    <td width='60%' valign='center' align='right'>$ladderlist</td>
    </tr>
    <tr class='altcolorb'>
    <td width='40%' valign='center' align='left'>Joindate</td>
    <td width='60%' valign='center' align='right'>$tinfo[12]</td>
    </tr>
    <tr class='altcolorb'>
    <td width='40%' valign='center' align='left'>Last Match</td>
    <td width='60%' valign='center' align='right'>$lastmatch[0]</td>
    </tr>";
    if($misc[flags] == "yes"){
        $out[body]=$out[body]."
        <tr class='altcolorb'>
        <td width='40%' valign='center' align='left'>Country</td>
        <td width='60%' valign='center' align='right'>$flag</td>
        </tr>";
    }

    $out[body]=$out[body]."
    <tr class='altcolorb'>
    <td width='40%' valign='center' align='left'><br></td>
    <td width='60%' valign='center' align='right'></td>
    </tr>
    <tr class='altcolorb'>
    <td width='100%' valign='bottom' align='center' colspan='2'>
    Email Team <a href='mailto:$tinfo[teamemail]'><img src='$url[themes]/images/email.gif' border='0'></a></td>
    </tr>
    <tr class='altcolorb'>
    <td width='100%' valign='bottom' align='center' colspan='2'><a href='$tinfo[website]' target='up'>Launch Teams Website</a></td>
    </tr>
    </table>
    <!-- END PLAYER INFO -->
    </td>
    <td width='50%' height='100%' valign='top' align='right'>
    <!-- PLAYER LOGO -->
    <table width='100%' height='100%' border='0' bordercolor='#000000' cellspacing='1' cellpadding='1' bgcolor=''>
    <tr class='altcolor'>
    <td width='100%' valign='top' align='center'>
    <strong>Team Logo</strong></td>
    </tr>
    <tr>
    <td width='100%' height='100%' valign='top' align='center'>";
    if(($tinfo[logo]) && ($tinfo[logo]!="http://")){
        $out[body]=$out[body]."<a href='$tinfo[website]' target='up'><img src='$tinfo[logo]' border='0'></a>";
    }else{

        $out[body]=$out[body]."<a href='$tinfo[website]' target='up'><img src='$misc[teamlogo]' border='0'></a>";
    }

    $out[body]=$out[body]."</td>
    </tr>
    </table>
    <!-- END PLAYER LOGO -->
    </td>
    </tr>
    </table>
    $tablefoot
    <br>
    $tablehead
    &nbsp; &nbsp;<strong><font class='catfont'>Team Members</font></strong><br>
    <hr class='catfont' size='1'>
    <table width='100%' border='0' bordercolor='#000000' cellspacing='1' cellpadding='2' bgcolor=''>
    <tr class='altcolor'>
    <td width='' valign='center' align='left'><strong>Name</strong></td>
    <td width='' valign='center' align='center'><strong>Joined</strong></td>
    <td width='' valign='center' align='center'><strong>Rank</strong></td>
    <td width='' valign='center' align='center'><strong>Wins</strong></td>
    <td width='' valign='center' align='center'><strong>Losses</strong></td>
    <td width='' valign='center' align='center'><strong>Email</strong></td>
    <td width='' valign='center' align='center'><strong>Website</strong></td>";
    if($admn[id]){
        $out[body]=$out[body]."
        <td width='' valign='center' align='center'><strong>Ip</strong></td>";
    }

    $out[body]=$out[body]."</tr>";
    if($tinfo[tagplace] > 0){
        $tagleft="";
        $tagright="$tinfo[tag]";
    }else{

        $tagleft="$tinfo[tag]";
        $tagright="";
    }

    $teammembers=mysql_query("SELECT playerid,status,DATE_FORMAT(joindate,'%M %d, %Y') FROM teammembers WHERE teamid='$teamid' ORDER by status");
    while(list($player,$status,$joined)=mysql_fetch_row($teammembers)){
        $tmemberinfo=mysql_query("SELECT alias,email,website,matcheswon,matcheslost,skill,ipaddress FROM users WHERE id='$player'");
        $member=mysql_fetch_array($tmemberinfo);
        $status=member_status($status);
        /*
        if($member[icq] > 0){
            $icq="<a href='http://wwp.icq.com/$member[icq]#pager' target=_blank><img src='http://online.mirabilis.com/scripts/online.dll?icq=$member[icq]&img=5' border='0'></a>
            <a href='http://wwp.icq.com/scripts/search.dll?to=$member[icq]'><img src='$url[themes]/images/icq_addme.gif' border='0'></a>";
        }else{

            $icq="";
            //$icq="<img src='$url[themes]/images/icq_noicq.gif' border='0'>";
        }

        if($member[aim]){
            $aim="<a href='aim:addbuddy?screenname=$member[aim]'><img src='$url[themes]/images/aim.gif' border='0'></a>";
        }else{

            $aim="";
        }

        if($member[yahoo]){
            $yahoo="<a href='http://edit.yahoo.com/config/send_webmesg?.target=$member[yahoo]&.src=pg' target='top'>
            <img src='http://opi.yahoo.com/online?u=$member[yahoo]&m=g&t=1' border='0'></a>";
        }else{

            $yahoo="";
        }

        */
        if($member[website]){
            $website="<a href='$member[website]' target='top'><img src='$url[themes]/images/home.gif' border='0'></a>";
        }else{

            $website="";
        }

        if($member[ipaddress]){
            $ip="<img src='$url[themes]/images/ip.gif' border='0' alt='$member[ipaddress]' style='cursor:hand;'>";
        }else{

            $ip="?";
        }

        $out[body]=$out[body]."
        <tr class='altcolorb'><a name='$teamid'></a>
        <td width='' valign='center' align='left'><a href='$url[base]/$file[players]?playerid=$player'>$tagleft $member[alias] $tagright</a></td>
        <td width='' valign='center' align='center'>$joined</td>
        <td width='' valign='center' align='center'>$status</td>
        <td width='' valign='center' align='center'>$member[matcheswon]</td>
        <td width='' valign='center' align='center'>$member[matcheslost]</td>
        <td width='' valign='center' align='center'><a href='mailto:$member[email]'><img src='$url[themes]/images/email.gif' border='0'></a></td>
        <td width='' valign='center' align='center'>$website</td>";
        if($admn[id]){
            $out[body]=$out[body]."
            <td width='' valign='center' align='center'>$ip</td>";
        }

        $out[body]=$out[body]."</tr>";
        $teamemails=$teamemails."$member[email];";
    }

    $out[body]=$out[body]."</table>
    <center>Email all team members <a href='mailto:$teamemails'><img src='$url[themes]/images/email.gif' border='0'></a></center>
    $tablefoot
    <br>
    $tablehead
    &nbsp; &nbsp;<strong><font class='catfont'>Ladder Stats</font></strong><br>
    <hr class='catfont' size='1'>
    <table width='100%' border='0' bordercolor='#000000' cellspacing='1' cellpadding='2' bgcolor=''>
    <tr class='altcolor'>
    <td width='' valign='center' align='center' colspan='1'>
    <font size='1'><strong>Ladder</strong></font></td>
    <td width='' valign='center' align='center' colspan='4'>
    <font size='1'><strong>Rank History</strong></font></td>
    <td width='' valign='center' align='center' colspan='4'>
    <font size='1'><strong>Record Status</strong></font></td>
    <td width='' valign='center' align='center' colspan='3'>
    <font size='1'><strong>Streaks</strong></font></td>
    <td width='' valign='center' align='center'>
    <font size='1'><strong>Days</strong></font></td>
    </tr>
    <tr class='altcolor'>
    <td width='' valign='center' align='left'>
    <font size='1'><strong>
    &nbsp;Name</strong></font></td>
    <td width='' valign='center' align='center'>
    <font size='1'><strong>
    Now</strong></font></td>
    <td width='' valign='center' align='center'>
    <font size='1'><strong>
    Last</strong></font></td>
    <td width='' valign='center' align='center'>
    <font size='1'><strong>
    Best</strong></font></td>
    <td width='' valign='center' align='center'>
    <font size='1'><strong>
    Worst</strong></font></td>
    <td width='' valign='center' align='center'>
    <font size='1'><strong>
    Wins</strong></font></td>
    <td width='' valign='center' align='center'>
    <font size='1'><strong>
    Losses</strong></font></td>
    <td width='' valign='center' align='center'>
    <font size='1'><strong>
    Games</strong></font></td>
    <td width='' valign='center' align='center'>
    <font size='1'><strong>
    Win%</strong></font></td>
    <td width='' valign='center' align='center'>
    <font size='1'><strong>
    Now</strong></font></td>
    <td width='' valign='center' align='center'>
    <font size='1'><strong>
    Best</strong></font></td>
    <td width='' valign='center' align='center'>
    <font size='1'><strong>
    Worst</strong></font></td>
    <td width='' valign='center' align='center'>
    <font size='1'><strong>
    Idle</strong></font></td>
    </tr>";
    $ladders=mysql_query("SELECT id,laddername FROM ladders ORDER BY laddername");
    while($row = mysql_fetch_array($ladders)){
        $teams=mysql_query("SELECT id FROM ladder_$row[id] WHERE id='$teamid'");
        if($teaminfor = mysql_fetch_array($teams)){
            $linfo = mysql_fetch_array(mysql_query("SELECT * FROM ladder_$row[id] WHERE id='$teaminfor[id]'"));
            $laddername = mysql_fetch_array(mysql_query("SELECT laddername,id FROM ladders WHERE id='$row[id]'"));
            if($linfo[lastmatch]=="0000-00-00 00:00:00"){
                $lastmatch="$linfo[joindate]";
            }else{

                $lastmatch="$linfo[lastmatch]";
            }

            $lastmatch=split(" ", $lastmatch);
            $lastmatch=strtotime( "$lastmatch[0] 00:00" );
            $today=date("Y-m-d 00:00");
            $today=strtotime($today);
            $idle=(($today-$lastmatch)/86400);
            $out[body]=$out[body]."<tr class='altcolorb'>
            <td width='' valign='center' align='left'>
            &nbsp;<a href='$url[base]/$file[ladder]?ladderid=$laddername[id]'>$laddername[laddername]</a></td>
            <td width='' valign='center' align='center'>
            $linfo[rank]</td>
            <td width='' valign='center' align='center'>
            $linfo[lastrank]</td>
            <td width='' valign='center' align='center'>
            $linfo[bestrank]</td>
            <td width='' valign='center' align='center'>
            $linfo[worstrank]</td>
            <td width='' valign='center' align='center'>
            $linfo[wins]</td>
            <td width='' valign='center' align='center'>
            $linfo[losses]</td>
            <td width='' valign='center' align='center'>
            $linfo[games]</td>
            <td width='' valign='center' align='center'>
            $linfo[percent]%</td>
            <td width='' valign='center' align='center'>
            $linfo[streak]</td>
            <td width='' valign='center' align='center'>
            $linfo[beststreak]</td>
            <td width='' valign='center' align='center'>
            $linfo[worststreak]</td>
            <td width='' valign='center' align='center'>
            $idle</td>
            </tr>
            ";
        }

    }

    $out[body]=$out[body]."
    </table>
    $tablefoot
    <br>
    $tablehead
    &nbsp; &nbsp;<strong><font class='catfont'>Recent Matches</font></strong><br>
    <hr class='catfont' size='1'>
    <table width='100%' border='0' bordercolor='#000000' cellspacing='1' cellpadding='1'>
    <tr class='altcolor'>
    <td width='20%' valign='center' align='left'><strong>&nbsp;Ladder</strong></td>
    <td width='22%' valign='center' align='center'><strong>Winner</strong></td>
    <td width='22%' valign='center' align='center'><strong>Loser</strong></td>
    <td width='21%' valign='center' align='center'><strong>Match Date</strong></td>
    <td width='15%' valign='center' align='center'><strong>Stats</strong></td>";
    if($admn[id]){
        $out[body]=$out[body]."
        <td width='' valign='center' align='center'><strong>Wip</strong></td>
        <td width='' valign='center' align='center'><strong>Lip</strong></td>";
    }

    $out[body]=$out[body]."
    </tr>";
    $maxresults="25";
    $stop="$maxresults";
    $altcolora="#000033";
    $altcolorb="#000020";
    $altcolora="' class='altcolora";
    $altcolorb="' class='altcolorb";
    $result=mysql_query("SELECT matchid,winnerid,loserid,winneralias,loseralias,laddername,ladderid,DATE_FORMAT(reportdate, '%M %d, %l:%i %p'),reporterip,confirmerip from matchdb WHERE winnerid='$teamid' OR loserid='$teamid' ORDER BY reportdate desc LIMIT 10");
    while(list($matchid,$winnerid,$loserid,$winneralias,$loseralias,$laddername,$ladderid,$reportdate,$reporterip,$confirmerip)=mysql_fetch_row($result)) {
        if($altladrow=="$altcolora"){
            $altladrow="$altcolorb";
        }else{

            $altladrow="$altcolora";
        }

        if($reporterip){
            $reporterip="<img src='$url[themes]/images/ip.gif' border='0' alt='$reporterip' style='cursor:hand;'>";
        }else{

            $reporterip="?";
        }

        if($confirmerip){
            $confirmerip="<img src='$url[themes]/images/ip.gif' border='0' alt='$confirmerip' style='cursor:hand;'>";
        }else{

            $confirmerip="?";
        }

        $out[body]=$out[body]."
        <tr bgcolor='$altladrow'>
        <td width='' valign='center' align='left'>&nbsp;<a href='$url[base]/$file[ladder]?ladderid=$ladderid'>$laddername</a></td>
        <td width='' valign='center' align='center'><a href='$url[base]/$file[teams]?teamid=$winnerid'>$winneralias</a></td>
        <td width='' valign='center' align='center'><a href='$url[base]/$file[teams]?teamid=$loserid'>$loseralias</a></td>
        <td width='' valign='center' align='center'>$reportdate</td>
        <td width='' valign='center' align='center'><a href='$url[base]/$file[match]?matchid=$matchid'><img src='$url[themes]/images/stats.gif' border='0'></a></td>";
        if($admn[id]){
            $out[body]=$out[body]."
            <td width='' valign='center' align='center'>$confirmerip</td>
            <td width='' valign='center' align='center'>$reporterip</td>";
        }

        $out[body]=$out[body]."</tr>";
        $counter++;
    }

    $out[body]=$out[body]."
    </table>
    $tablefoot
    <br>
    $tablehead
    &nbsp; &nbsp;<strong><font class='catfont'>Admin Comments</font></strong><br>
    <hr class='catfont' size='1'>
    <table width='100%' border='0' bordercolor='#000000' cellspacing='1' cellpadding='2' bgcolor=''>
    <tr class='altcolor'>
    <td width='' valign='center' align='left'><strong>Comment</strong></td>
    <td width='150' valign='center' align='center'><strong>Added</strong></td>
    <td width='150' valign='center' align='center'><strong>By</strong></td>
    </tr>";
    //Show Admin Comments
    $getcomments=mysql_query("SELECT comment,addedby,DATE_FORMAT(added, '%M %d, %l:%i %p') FROM staffcomments WHERE id='$teamid' AND isateam='1' AND display='1' ORDER by added");
    while(list($comment,$addedby,$added)=mysql_fetch_row($getcomments)){
        $staffmember=mysql_query("SELECT displayname FROM staff WHERE id='$addedby'");
        $staff=mysql_fetch_array($staffmember);
        if($altladrow=="$altcolora"){
            $altladrow="$altcolorb";
        }else{

            $altladrow="$altcolora";
        }

        $out[body]=$out[body]."
        <tr bgcolor='$altladrow'>
        <td width='' valign='center' align='left'>$comment</td>
        <td width='150' valign='center' align='center'>$added</td>
        <td width='150' valign='center' align='center'>$staff[displayname]</td>
        </tr>
        ";
    }

    //Show Admin Comments Only Visible To Staff
    If ($admn[id]){
        $getcomments=mysql_query("SELECT comment,addedby,DATE_FORMAT(added, '%M %d, %l:%i %p') FROM staffcomments WHERE id='$teamid' AND isateam='1' AND display='0' ORDER by added");
        while(list($comment,$addedby,$added)=mysql_fetch_row($getcomments)){
            $staffmember=mysql_query("SELECT displayname FROM staff WHERE id='$addedby'");
            $staff=mysql_fetch_array($staffmember);
            if($altladrow=="$altcolora"){
                $altladrow="$altcolorb";
            }else{

                $altladrow="$altcolora";
            }

            $out[body]=$out[body]."
            <tr bgcolor='$altladrow'>
            <td width='' valign='center' align='left'>[Admin Visible Comment] $comment</td>
            <td width='150' valign='center' align='center'>$added</td>
            <td width='150' valign='center' align='center'>$staff[displayname]</td>
            </tr>
            ";
        }

    }

    $out[body]=$out[body]."
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function member_status($status){
    if($status=="1"){ $status="Leader"; }

    if($status=="2"){ $status="Co-Leader"; }

    if($status=="3"){ $status="Captain"; }

    if($status=="4"){ $status="Co-Captain"; }

    if($status=="5"){ $status="Member"; }

    if($status=="6"){ $status="Training"; }

    if($status=="7"){ $status="Inactive"; }

    if($status=="8"){ $status="Suspended"; }

    return($status);
}

?>
