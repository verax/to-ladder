<?
//////////////////////////////////////////////////////////////////////////
/////////////////// REQUIRED INFORMATION ////////////////////////////////
////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////



//DATABASE INFORMATION
$db[host]		='localhost:8889'; //usually localhost
$db[uname]	='root';
$db[pass]		='root';
$db[name]		='tacops';





///////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////EVERYTHING BELOW SHOULD NOT NEED BE EDITED////////////////////
///THESE VARIABLES ARE ALL CONTROLLED FROM THE ADMIN CONTROL PANEL NOW//
///////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////






//DATABASE ACCESS

mysql_connect($db[host], $db[uname], $db[pass]);
@mysql_select_db("$db[name]") or die ("<b><center>Unable to access database. Please notify administrator.<br><br>Software by http://www.mygamingladder.com</center></b>");

$settings=mysql_fetch_array(mysql_query("SELECT * FROM settings"));

/////////////////////////////////////////////////////////////////////
///////////////// THEME NAMES //////////////////////////////////////
// THEMES THAT USERS CAN CHOOSE FOR THEIR ACCOUNT
$theme[option1]	        = 'Black';
$theme[option2]	        = 'Gray';
$theme[option3]	        = 'Red';
$theme[option4]	        = 'Blue';
$theme[option5]	        = 'AngryBlue';
/////////CHOICES ARE GRAY, BLUE, RED, ANGRYBLUE AND BLACK/////////
/////////////YOU CAN ALSO MAKE YOUR OWN)/////////////////////////

$email[sendaccount]	=$settings[email];
$theme[normal]		="Blue";             //Default Themes
$site[shortname]	      =$settings[shortname];
$site[longname]		=$settings[longname];
$url[forums]		=$settings[forumurl];
$out[banner] 		=$settings[bannerloc];
$out[bannerurl] 	      =$settings[bannerurl];
$misc[joinage]		=$settings[joinage];           //Minimum age permitted.
$misc[multipleip]		=$settings[multipleip];        //Allow multiple signups from the same ip.
$misc[tourneyip]		=$settings[tourneyip];         //Allow multiple signups on tournament from the same ip.
$misc[changeplyrname]	=$settings[changeplyrname];    //Allow player name to be changed.
$misc[changeteamname]	=$settings[changeteamname];    //Allow team name to be changed.
$misc[deleteteam]		=$settings[deleteteam];        //Allow teams to be deleted. We STRONGLY recommend this to be left on 'no'.
$misc[leaveladder]      =$settings[leaveladder];       //Allow teams to leave ladders.
$misc[challengedown]    =$settings[challengedown];     //Allow teams to challenge other teams below them on the ladder.
$misc[losercomment]	=$settings[losercomment];      //Allow loser comment in match reporting.
$misc[winnercomment]	=$settings[winnercomment];     //Allow winner comment in match reporting.
$misc[invitemembers]    =$settings[invitemembers];     // Yes = invite ; No = Add without invite.
$uinfo[allow]		=$settings[allow];            //Ask for more user information on signup.
$uinfo[force]		=$settings[uforce];             //Require the extra information on signup.
$uinfo[edit]		=$settings[edit];              //Allow extra information to be edited after signup.
$misc[newplayer]		=$settings[newplayer];         //Allow signups, setting no will disable signups.
$misc[dobans]		=$settings[dobans];            //Allow admins to ban players.
$misc[docensors]		=$settings[docensors];         //Allow certain words to be censored.
$misc[censorschar]	=$settings[censorschar];       //Censor character.
$timeout[session]		=$settings[session];           //Timeout time in seconds for logged in sessions.
$misc[passrequests] 	=$settings[passrequests];      //Allow forgotton password retrieval
$misc[flags]            =$settings[flags];      	 //Allow flags for user and team accounts
$misc[playerdb]		=$settings[playerdb]; 		 //Number of players shown per page on player database.
$misc[teamdb]		=$settings[teamdb];      	 //Number of teams shown per page on player database.
$misc[ladderdb]		=$settings[ladderdb];       	 //Number of ladders shown per page on ladder database.
$misc[standings]		=$settings[standings];         //Number of players shown per page in ladder standings.
$misc[uservalidate]     =$settings[uservalidate];      // Yes= User validates own account, No = Admin validates user account before use
$misc[headline]         =$settings[headline];          // Yes= Headline only. No = Story and headline.
$misc[ranking]          =$settings[ranking];        	 // Non challenge Matches: 1 = Winner moves half way up to losers position(if higher). 2 = Winner takes losers position(if higher)
$misc[challranking]     =$settings[challranking];      // Challenge Matches: 1= Winner takes losers position(if higher). 2= Winner moves half way up to losers position(if higher).

$site[homeurl]		=  ("http://".$HTTP_HOST."".dirname($SCRIPT_NAME));
$dir[base]			=  getcwd();

$url[base]			="$site[homeurl]";
$url[themes]		="$site[homeurl]/themes";
$dir[func]			="$dir[base]/functions";
$dir[themes]		="$dir[base]/themes";

$misc[teamlogo]		="$url[themes]/images/defaultteamlogo.jpg";
$misc[playerlogo]		="$url[themes]/images/defaultplayerlogo.jpg";
$misc[mapimage]		="$url[themes]/images/defaultmaplogo.jpg";

$email[localhost]		=$settings[localhost];  //set to no if using SMTP; leave unchanged otherwise
$email[smtp_server]	=$settings[smtp_server]; //url or IP for SMTP server
$email[username]		=$settings[username];   //username for SMTP authentication; leave blank if not needed
$email[password]		=$settings[password]; 	//password for SMTP authentication; leave blank if not needed
//////////////////////////////////////////////////////////////////////////

$email[genhelp]		="$email[sendaccount]";
$email[banhelp]		="$email[sendaccount]";
$email[techhelp]		="$email[sendaccount]";

$file[main]			='index.php';
$file[stats]		='stats.php';
$file[admin]		='adminhome.php';
$file[login]		='login.php';
$file[game]			='game.php';
$file[staff]		='staff.php';
$file[rules]		='rules.php';
$file[help]			='news.php';
$file[join]			='join.php';
$file[phome]		='playerhome.php';
$file[edit]			='edit.php';
$file[players]		='players.php';
$file[teams]		='teams.php';
$file[match]		='match.php';
$file[ladder]		='ladder.php';
$file[maps]			='maps.php';
$file[search]		='search.php';
$file[files]		='files.php';
$file[links]		='links.php';
$file[tournament]		='tournament.php';


//Emulate Register Globals

if (!ini_get('register_globals')) {
   $superglobals = array($_SERVER, $_ENV,
       $_FILES, $_COOKIE, $_POST, $_GET);
   if (isset($_SESSION)) {
       array_unshift($superglobals, $_SESSION);
   }
   foreach ($superglobals as $superglobal) {
       extract($superglobal, EXTR_SKIP);
   }
}

?>
