<?
$indexloaded=1;
include("config/config.php");
include("$dir[func]/global.php");
if(!$plyr[id]){
    header("Location: $url[base]/$file[main]");
}

include("$dir[func]/loginforms.php");
force_login();
$playerinfo=mysql_query("SELECT *,DATE_FORMAT(joindate, '%M %d, %Y') FROM users WHERE id='$plyr[id]'");
$myinfo=mysql_fetch_array($playerinfo);
if(!$myinfo[alias]){
    include("$dir[func]/error.php");
    display_error("Unknown User.<br>");
}

$altcolora="' class='altcolora";
$altcolorb="' class='altcolorb";
$tablehead=table_head("show","100%","","left");
$tablefoot=table_foot("show");
$bannerhead=table_head("show","488","80","center");
$bannerfoot=table_foot("show");
$out[body]=$out[body]."
<center>
$bannerhead
$out[banner]
$bannerfoot
</center>
<br>
$tablehead
&nbsp; &nbsp;<strong><font class='catfont'>Jugador</font></strong><br>
<hr class='catfont' size='1'>
<table width='100%' border='0' bordercolor='#000000' cellspacing='0' cellpadding='0'>
<tr>
<td width='50%' valign='top' align='left'>
<!-- PLAYER INFO -->
<table width='100%' border='0' bordercolor='#000000' cellspacing='1' cellpadding='1'>
<tr class='altcolor'>
<td width='100%' valign='center' align='center' colspan='2'>
<strong>Informaci&oacute;n del jugador</strong>&nbsp;&nbsp;<a href='$url[base]/$file[edit]?action=player' class='btn btn-mini'>Editar Informaci&oacute;n</a></td>
</tr>
<tr class='altcolorb'>
<td width='40%' valign='center' align='left'>Name</td>
<td width='60%' valign='center' align='right'>$myinfo[alias]</td>
</tr>
<tr class='altcolorb'>
<td width='40%' valign='center' align='left'>Email</td>
<td width='60%' valign='center' align='right'>$myinfo[email]</td>
</tr>
<tr class='altcolorb'>
<td width='40%' valign='center' align='left'>Joindate</td>
<td width='60%' valign='center' align='right'>$myinfo[joindate]</td>
</tr>
<tr class='altcolorb'>
<td width='40%' valign='center' align='left'>Total Wins</td>
<td width='60%' valign='center' align='right'>$myinfo[matcheswon]</td>
</tr>
<tr class='altcolorb'>
<td width='40%' valign='center' align='left'>Total Losses</td>
<td width='60%' valign='center' align='right'>$myinfo[matcheslost]</td>
</tr>
";
$out[body]=$out[body]."</tr>
<tr class='altcolorb'>
<td width='100%' valign='bottom' align='center' colspan='2'><a href='$myinfo[website]' target='up'>Launch Player Website</a></td>
</tr>
</table>
<!-- END PLAYER INFO -->
</td>
<td width='50%' height='100%' valign='top' align='right'>
<!-- PLAYER LOGO -->
<table width='100%' height='100%' border='0' bordercolor='#000000' cellspacing='1' cellpadding='1'>
<tr class=''>
<td width='100%' valign='center' align='center'>
</td>
</tr>
<tr>
<td width='100%' height='100%' valign='center' align='center'>";
if(($myinfo[logo]) && ($myinfo[logo]!="http://")){
    $out[body]=$out[body]."<img src='$myinfo[logo]' class='img-polaroid'>";
}else{

    $out[body]=$out[body]."<img src='$misc[playerlogo]' class='img-polaroid'>";
}

$out[body]=$out[body]."</td>
</tr>
</table>
<!-- END PLAYER LOGO -->
</td>
</tr>
</table>
$tablefoot
<br>
$tablehead
&nbsp; &nbsp;<strong><font class='catfont'>Clan</font></strong><br>
<hr class='catfont' size='1'>
<table width='100%' border='0' bordercolor='#000000' cellspacing='1' cellpadding='1'>
<tr class='altcolor'>
<td width='25%' valign='center' align='left'><strong>Nombre</strong></td>
<td width='75%' valign='center' align='left'><strong>Editar</strong></td>
</tr>";
$teamson=mysql_query("SELECT teamid,status,DATE_FORMAT(joindate, '%M %d, %Y') FROM teammembers WHERE playerid='$plyr[id]' ORDER by joindate");
while(list($team,$status,$joindate)=mysql_fetch_row($teamson)){
    $teaminfo=mysql_query("SELECT teamname,type FROM teams WHERE id='$team'");
    $tinfo=mysql_fetch_array($teaminfo);
    $teams=mysql_query("SELECT teamid FROM teammembers WHERE playerid='$plyr[id]'");
    $status=member_status($status);
    $out[body]=$out[body]."
    <tr class='altcolorb'><a name='$teamid'></a>
    <td width='25%' valign='center' align='left'><a href='$url[base]/$file[teams]?teamid=$team'><b>$tinfo[teamname]</b></a></td>
    ";
    //if($teamid=="$team"){
    $out[body]=$out[body]."
    <!-- TEAM MANAGE OPTIONS-->
    <td width='75%' valign='center' align='left'>
    <//strong><a href='$url[base]/$file[edit]?action=team&teamid=$team'><button class='btn btn-mini' style='color: #000 !important;'>Editar Clan</button></a> &nbsp;</strong></font>";
    if($tinfo[type] > 1){
        $out[body]=$out[body]."<//strong><a href='$url[base]/$file[edit]?action=members&teamid=$team' class='btn btn-mini'>Editar Miembros</a> &nbsp;</strong></font>";
        if($misc[invitemembers] == 'yes'){
            $out[body]=$out[body]."<//strong><a href='$url[base]/$file[edit]?action=invite&teamid=$team' class='btn btn-mini btn-info'>Invitar Miembro</a> &nbsp;</strong></font>";
        }else if($misc[invitemembers] == 'no'){

            $out[body]=$out[body]."<//strong><a href='$url[base]/$file[edit]?action=addmember&teamid=$team' class='btn btn-mini btn-success'>Agregar un Miembro</a>]&nbsp;</strong></font>";
        }

    }

    $out[body]=$out[body]."
    <//strong><a href='$url[base]/$file[edit]?action=leave&teamid=$team' class='btn btn-mini btn-danger'>Dejar el clan</a>&nbsp;</strong></font></td>
    <!-- END TEAM MANAGE OPTIONS-->
    </tr>";
    //}

    $onteam=1;
}

if(!$onteam){
    $out[body]=$out[body]."
    <td width='75%' valign='center' align='center' colspan='5'>Not currently on any teams</td>
    ";
}

$out[body]=$out[body]."
</table>
$tablefoot
<br>
$tablehead
&nbsp; &nbsp;<strong><font class='catfont'>Invitaciones</font></strong><br>
<hr class='catfont' size='1'>
<table width='100%' border='0' bordercolor='#000000' cellspacing='1' cellpadding='1'>
<tr class='altcolor'>
<td width='15%' valign='center' align='center'><strong>Join</strong></td>
<td width='20%' valign='center' align='left'><strong>Team Name</strong></td>
<td width='20%' valign='center' align='center'><strong>Ladder</strong></td>
<td width='20%' valign='center' align='center'><strong>Inviter</strong></td>
<td width='25%' valign='center' align='center'><strong>Date</strong></td>
</tr>";
$invitations=mysql_query("SELECT teamid,playerid,inviterid,DATE_FORMAT(invitedate, '%M %d, %Y') FROM teammembersinv WHERE playerid='$plyr[id]' ORDER by invitedate");
while(list($team,$playerid,$inviterid,$invitedate)=mysql_fetch_row($invitations)){
    $teaminfo=mysql_query("SELECT teamname FROM teams WHERE id='$team'");
    $tinfo=mysql_fetch_array($teaminfo);
    $inviterinfo=mysql_query("SELECT alias FROM users WHERE id='$inviterid'");
    $inviter=mysql_fetch_array($inviterinfo);
    $status=member_status($status);
    $out[body]=$out[body]."
    <tr class='altcolorb'><a name='$teamid'></a>
    <td width='20%' valign='center' align='center'>[<a href='$url[base]/$file[edit]?action=accept&teamid=$team&inviteid=$plyr[id]'>Join</a>]</td>
    <td width='25%' valign='center' align='left'><a href='$url[base]/$file[teams]?teamid=$team'>$tinfo[teamname]</a></td>
    <td width='25%' valign='center' align='center'><a href='$url[base]/$file[players]?playerid=$inviterid'>$inviter[alias]</a></td>
    <td width='30%' valign='center' align='center'>$invitedate</td>
    </tr>";
    $invites=1;
}

if(!$invites){
    $out[body]=$out[body]."
    <tr class='altcolorb'>
    <td width='100%' valign='center' align='center' colspan='5'>No Current Invitations</td>
    </tr>";
}

$out[body]=$out[body]."
</table>
$tablefoot";
include("$dir[curtheme]");
function member_status($status){
    if($status=="1"){ $status="Leader"; }

    if($status=="2"){ $status="Co-Leader"; }

    if($status=="3"){ $status="Captain"; }

    if($status=="4"){ $status="Co-Captain"; }

    if($status=="5"){ $status="Member"; }

    if($status=="6"){ $status="Training"; }

    if($status=="7"){ $status="Inactive"; }

    if($status=="8"){ $status="Suspended"; }

    return($status);
}

?>
