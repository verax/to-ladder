<?
$now = gmdate('D, d M Y H:i:s') . ' GMT';
header('Expires: ' . $now);
header('Last-Modified: ' . $now);
header('Cache-Control: no-store, no-cache, must-revalidate'); // HTTP/1.1
header('Cache-Control: pre-check=0, post-check=0, max-age=0'); // HTTP/1.1
header('Pragma: no-cache'); // HTTP/1.0
$indexloaded=1;
include("config/config.php");
include("$dir[func]/global.php");
include("$dir[func]/loginforms.php");
force_adminlogin();
switch($action){
    //VALIDATE
    case "validate":
    include("$dir[func]/admin_validate.php");
    admin_validate();
    break;
    case "validateb":
    include("$dir[func]/admin_validate.php");
    admin_validateb($vcode,$todo);
    break;
    //TEAM
    case "createteam":
    include("$dir[func]/admin_createteam.php");
    admin_createteam();
    break;
    case "createteamb":
    include("$dir[func]/admin_createteam.php");
    admin_createteamb($ladderid,$playerid,$teamname);
    break;
    case "editateam":
    include("$dir[func]/admin_editteam.php");
    admin_editateam();
    break;
    case "editateamb":
    include("$dir[func]/admin_editteam.php");
    admin_editateamb($ladderid);
    break;
    case "editateamc":
    include("$dir[func]/admin_editteam.php");
    admin_editateamc($ladderid,$teamid,$todo);
    break;
    case "editateamd":
    include("$dir[func]/admin_editteam.php");
    admin_editateamd($teamid,$ladderid,$t_alias,$t_email,$t_tag,$t_tagplace,$t_url,$t_logo,$t_contact,$t_rank,$t_lrank,$t_brank,$t_wrank,$t_wins,$t_loss,$t_streak,$t_bstreak,$t_wstreak,$t_points,$t_skill,$t_money,$t_forfeits,$t_icon,$t_lock);
    break;
    case "editateame":
    include("$dir[func]/admin_editteam.php");
    admin_editateame($teamid,$ladderid,$memberrank);
    break;
    case "editateamf":
    include("$dir[func]/admin_editteam.php");
    admin_editateamf($teamid,$ladderid,$playerid);
    break;
    //PLAYER
    case "editplayer":
    include("$dir[func]/admin_editplayer.php");
    admin_editplayer();
    break;
    case "editplayerb":
    include("$dir[func]/admin_editplayer.php");
    admin_editplayerb($playerid,$todo);
    break;
    case "editplayerc":
    include("$dir[func]/admin_editplayer.php");
    admin_editplayerc($playerid,$p_alias,$p_pass,$p_email,$p_icq,$p_aim,$p_msn,$p_yahoo,$p_url,$p_logo,$p_won,$p_loss,$p_points,$p_skill,$p_money,$p_newsl,$p_theme,$p_country);
    break;
    //ADD STAFF
    case "addstaff":
    include("$dir[func]/admin_addstaff.php");
    admin_addstaff();
    break;
    case "addstaffb":
    include("$dir[func]/admin_addstaff.php");
    admin_addstaffb($s_name,$s_pass,$s_email,$s_title,$s_country,$s_contact,$s_access,$ladders);
    break;
    //EDIT STAFF
    case "editstaff":
    include("$dir[func]/admin_editstaff.php");
    admin_editstaff();
    break;
    case "editstaffb":
    include("$dir[func]/admin_editstaff.php");
    admin_editstaffb($staffid,$todo);
    break;
    case "editstaffc":
    include("$dir[func]/admin_editstaff.php");
    admin_editstaffc($staffid,$s_name,$s_pass,$s_email,$s_title,$s_country,$s_contact,$s_access,$ladders);
    break;
    //MANAGE GAMES
    case "editgames":
    include("$dir[func]/admin_games.php");
    admin_editgames();
    break;
    case "editgamesb":
    include("$dir[func]/admin_games.php");
    admin_editgamesb($gameid,$todo);
    break;
    case "editgamesc":
    include("$dir[func]/admin_games.php");
    admin_editgamesc($g_name,$g_abrv,$g_desc,$g_active,$g_fid);
    break;
    case "editgamesd":
    include("$dir[func]/admin_games.php");
    admin_editgamesd($gameid,$g_name,$g_abrv,$g_desc,$g_active,$g_fid);
    break;
    //MANAGE LADDERS
    case "editladders":
    include("$dir[func]/admin_ladders.php");
    admin_editladders();
    break;
    case "editladdersb":
    include("$dir[func]/admin_ladders.php");
    admin_editladdersb($ladderid,$todo);
    break;
    case "editladdersc":
    include("$dir[func]/admin_ladders.php");
    admin_editladdersc($ld_gameid,$ld_name,$ld_abrv,$ld_desc,$ld_type,$ld_openreport,$ld_plyrmin,$ld_plyrmax,$ld_minmaps,$ld_maxmaps,$ld_inactvdaysa,$ld_inactvdrop,$ld_inactvdaysb,$ld_chal,$ld_chalranks,$ld_chalrespnd,$ld_chalplydays,$ld_chaltimea,$ld_chalmstart,$ld_chalmend,$ld_chalmtzone,$ld_chalmapsa,$ld_chalfinal,$ld_chalmapsb,$ld_skill);
    break;
    case "editladdersd":
    include("$dir[func]/admin_ladders.php");
    admin_editladdersd($ladderid,$ld_gameid,$ld_name,$ld_abrv,$ld_desc,$ld_type,$ld_openreport,$ld_plyrmin,$ld_plyrmax,$ld_minmaps,$ld_maxmaps,$ld_inactvdaysa,$ld_inactvdrop,$ld_inactvdaysb,$ld_chal,$ld_chalranks,$ld_chalrespnd,$ld_chalplydays,$ld_chaltimea,$ld_chalmstart,$ld_chalmend,$ld_chalmtzone,$ld_chalmapsa,$ld_chalfinal,$ld_chalmapsb,$ld_skill);
    break;
    //RULES
    case "editrules":
    include("$dir[func]/admin_rules.php");
    admin_editrules();
    break;
    case "editrulesb":
    include("$dir[func]/admin_rules.php");
    admin_editrulesb($ladderid);
    break;
    case "editrulesc":
    include("$dir[func]/admin_rules.php");
    admin_editrulesc($ladderid,$ruleid,$todo);
    break;
    case "editrulesd":
    include("$dir[func]/admin_rules.php");
    admin_editrulesd($ladderid,$title,$body);
    break;
    case "editrulese":
    include("$dir[func]/admin_rules.php");
    admin_editrulese($ruleid,$title,$body);
    break;
    case "editrulesf":
    include("$dir[func]/admin_rules.php");
    admin_editrulesf($ruleid,$ladderid);
    break;
    //NEWS
    case "news":
    include("$dir[func]/admin_news.php");
    admin_news();
    break;
    case "newsb":
    include("$dir[func]/admin_news.php");
    admin_newsb($newsid,$todo);
    break;
    case "newsc":
    include("$dir[func]/admin_news.php");
    admin_newsc($headline,$content);
    break;
    case "newsd":
    include("$dir[func]/admin_news.php");
    admin_newsd($newsid,$headline,$content);
    break;
    //NEWSLETTER
    case "newsletter":
    include("$dir[func]/admin_news.php");
    admin_newsletter();
    break;
    case "newsletterb":
    include("$dir[func]/admin_news.php");
    admin_newsletterb($subject,$message);
    break;
    case "newsletterc":
    include("$dir[func]/admin_news.php");
    admin_newsletterc($subject,$message,$sendto);
    break;
    //MAPS
    case "editmaps":
    include("$dir[func]/admin_maps.php");
    admin_maps();
    break;
    case "editmapsb":
    include("$dir[func]/admin_maps.php");
    admin_mapsb($ladderid);
    break;
    case "editmapsc":
    include("$dir[func]/admin_maps.php");
    admin_mapsc($ladderid,$mapid,$todo);
    break;
    case "editmapsd":
    include("$dir[func]/admin_maps.php");
    admin_mapsd($ladderid,$m_name,$m_fname,$m_url,$m_image);
    break;
    case "editmapse":
    include("$dir[func]/admin_maps.php");
    admin_mapse($mapid,$m_name,$m_fname,$m_url,$m_image);
    break;
    case "editmapsf":
    include("$dir[func]/admin_maps.php");
    admin_mapsf($mapid,$ladderid);
    break;
    //MATCH
    case "editmatch":
    include("$dir[func]/admin_match.php");
    admin_match();
    break;
    case "editmatchb":
    include("$dir[func]/admin_match.php");
    admin_matchb($ladderid);
    break;
    case "editmatchc":
    include("$dir[func]/admin_match.php");
    admin_matchc($matchid,$todo);
    break;
    case "editmatchd":
    include("$dir[func]/admin_match.php");
    admin_matchd($matchid,$m_walias,$m_lalias,$m_wscore,$m_lscore,$m_wcomment,$m_lcomment);
    break;
    //REPORT MATCH
    case "reportmatch":
    include("$dir[func]/admin_reportmatch.php");
    admin_reportmatch();
    break;
    case "reportmatchb":
    include("$dir[func]/admin_reportmatch.php");
    admin_reportmatchb($ladderid);
    break;
    case "reportmatchc":
    include("$dir[func]/admin_reportmatch.php");
    admin_reportmatchc($ladderid,$wteamid,$lteamid);
    break;
    //CHALLENGE
    case "editchallenge":
    include("$dir[func]/admin_challenge.php");
    admin_challenge();
    break;
    case "editchallengeb":
    include("$dir[func]/admin_challenge.php");
    admin_challengeb($ladderid);
    break;
    case "editchallengec":
    include("$dir[func]/admin_challenge.php");
    admin_challengec($challid,$todo);
    break;
    //FILE MANAGER
    case "fileman":
    include("$dir[func]/admin_filemanager.php");
    admin_fileman();
    break;
    case "filemanb":
    include("$dir[func]/admin_filemanager.php");
    admin_filemanb($fileid,$todo);
    break;
    case "filemanc":
    include("$dir[func]/admin_filemanager.php");
    admin_filemanc($f_name,$f_link);
    break;
    case "filemand":
    include("$dir[func]/admin_filemanager.php");
    admin_filemand($fileid,$f_name,$f_link);
    break;
    //LINKS
    case "linkman":
    include("$dir[func]/admin_linkmanager.php");
    admin_links();
    break;
    case "linkmanb":
    include("$dir[func]/admin_linkmanager.php");
    admin_linksb($linkid,$todo);
    break;
    case "linkmanc":
    include("$dir[func]/admin_linkmanager.php");
    admin_linksc($l_name,$l_url,$l_img);
    break;
    case "linkmand":
    include("$dir[func]/admin_linkmanager.php");
    admin_linksd($linkid,$l_name,$l_url,$l_img);
    break;
    // BAN AND CENSOR
    case "censor":
    include("$dir[func]/admin_ban.php");
    admin_censor();
    break;
    case "censorb":
    include("$dir[func]/admin_ban.php");
    admin_censorb($b_text,$b_type,$b_reason,$b_displayreason);
    break;
    case "censorc":
    include("$dir[func]/admin_ban.php");
    admin_censorc($banid);
    break;
    case "censord":
    include("$dir[func]/admin_ban.php");
    admin_censord($b_text,$b_type,$b_reason,$b_displayreason,$banid);
    break;
    case "censore":
    include("$dir[func]/admin_ban.php");
    admin_censore($banid);
    break;
    //ADMIN COMMENTS
    // PLAYER COMMENTS
    case "admincomments":
    include("$dir[func]/admin_comments.php");
    admin_comments();
    break;
    case "admincommentsb":
    include("$dir[func]/admin_comments.php");
    admin_commentsb();
    break;
    case "admincommentsc":
    include("$dir[func]/admin_comments.php");
    admin_commentsc($playerid);
    break;
    case "admincommentsd":
    include("$dir[func]/admin_comments.php");
    admin_commentsd($playerid,$c_comment,$c_display);
    break;
    case "admincommentse":
    include("$dir[func]/admin_comments.php");
    admin_commentse($commentid);
    break;
    //TEAM COMMENTS
    case "admincommentsf":
    include("$dir[func]/admin_comments.php");
    admin_commentsf();
    break;
    case "admincommentsg":
    include("$dir[func]/admin_comments.php");
    admin_commentsg($ladderid);
    break;
    case "admincommentsh":
    include("$dir[func]/admin_comments.php");
    admin_commentsh($teamid);
    break;
    case "admincommentsi":
    include("$dir[func]/admin_comments.php");
    admin_commentsi($teamid,$c_comment,$c_display);
    break;
    //TOURNAMENT
    case "select_tournament":
    include("$dir[func]/admin_tournament.php");
    select_tournament();
    break;
    case "edit_tournament":
    include("$dir[func]/admin_tournament.php");
    TourneyAdminHome($tid);
    break;
    case "admin_players":
    include("$dir[func]/admin_tournament.php");
    admin_players($edit,$tid);
    break;
    case "admin_players_add":
    include("$dir[func]/admin_tournament.php");
    admin_players_add($adedt);
    break;
    case "admin_players_update":
    include("$dir[func]/admin_tournament.php");
    admin_players_update($adedt);
    break;
    case "admin_players_delete":
    include("$dir[func]/admin_tournament.php");
    admin_players_delete($delete);
    break;
    case "admin_brackets":
    include("$dir[func]/admin_tournament.php");
    admin_brackets($btype,$tid);
    break;
    case "admin_brackets_update":
    include("$dir[func]/admin_tournament.php");
    admin_brackets_update($brackets,$tid);
    break;
    case "admin_brackets_finish":
    include("$dir[func]/admin_tournament.php");
    admin_brackets_finish($tid);
    break;
    case "admin_brackets_report":
    include("$dir[func]/admin_tournament.php");
    admin_brackets_report($report);
    break;
    case "admin_brackets_close":
    include("$dir[func]/admin_tournament.php");
    close_tournament();
    break;
    case "admin_autobracket":
    include("$dir[func]/admin_tournament.php");
    admin_autobracket();
    break;
    case "madmin_manage":
    include("$dir[func]/admin_tournament.php");
    madmin_manage($edit);
    break;
    case "madmin_manage_add":
    include("$dir[func]/admin_tournament.php");
    madmin_manage_add($adedt);
    break;
    case "madmin_manage_update":
    include("$dir[func]/admin_tournament.php");
    madmin_manage_update($adedt);
    break;
    case "madmin_manage_delete":
    include("$dir[func]/admin_tournament.php");
    madmin_manage_delete($delete);
    break;
    case "madmin_manage_reset":
    include("$dir[func]/admin_tournament.php");
    madmin_manage_reset($reset);
    break;
    case "madmin_reports":
    include("$dir[func]/admin_tournament.php");
    madmin_reports($report);
    break;
    //SETTINGS
    case "settings":
    include("$dir[func]/admin_settings.php");
    admin_settings();
    break;
    case "settings_update":
    include("$dir[func]/admin_settings.php");
    settings_update($email,$shortname,$longname,$default_theme,$forumurl,$bannerloc,$bannerurl,$joinage,$multipleip,$tourneyip,$changeplyrname,$changeteamname,$deleteteam,$leaveladder,$challengedown,$losercomment,$winnercomment,$invitemembers,$allow,$uforce,$edit,$newplayer,$dobans,$docensors,$censorschar,$default_session,$passrequests,$flags,$playerdb,$teamdb,$ladderdb,$standings,$uservalidate,$headline,$ranking,$challranking,$localhost,$smtp_server,$username,$password);
    break;
 //ADMIN DISPLAY
    default:
    include("$dir[func]/admin.php");
    admin_cpdisplay();
    break;
}

?>
