<?PHP
$indexloaded=1;
include("config/config.php");
include("$dir[func]/global.php");
include("$dir[func]/loginforms.php");
switch($action){
    case "join":
    include("$dir[func]/newplayer.php");
    form_newplayer();
    break;
    case "joinb":
    include("$dir[func]/newplayer.php");
    form_newplayerb($n_alias,$n_pass,$n_passa,$n_email,$n_emaila,$n_icq,$n_aim,$n_yahoo,$n_msn,$n_url,$n_logo,$n_newsletter,$n_fname,$n_mname,$n_lname,$n_addrs,$n_city,$n_state,$n_zip,$n_phonea,$n_phoneb,$n_bdaym,$n_bdayd,$n_bdayy,$n_occup,$agree_age,$agree_policy,$n_country);
    break;
    case "joinladdera":
    force_login();
    join_ladder($ladderid);
    break;
    case "joinladderb":
    force_login();
    join_ladderb($ladderid,$teamid);
    break;
    case "leaveladdera":
    force_login();
    leaveladdera($ladderid);
    break;
    case "leaveladderb":
    force_login();
    leaveladderb($ladderid,$teamid);
    break;
    case "chooseladder":
    force_login();
    select_ladder();
    break;
    case "createteam":
    force_login();
    create_team($ladderid);
    break;
    case "createteamb":
    force_login();
    create_teamb($ladderid,$t_alias,$t_email,$t_tag,$t_tagplace,$t_url,$t_logo,$t_contact);
    break;
    case "team":
    force_login();
    include("$dir[func]/jointeam.php");
    join_team($teamid);
    break;
    case "teamb":
    force_login();
    include("$dir[func]/jointeam.php");
    join_teamb($teamid);
    break;
    case "validate":
    include("$dir[func]/validate.php");
    validate_player($vc);
    break;
    case "validateb":
    include("$dir[func]/validate.php");
    validate_playerb($code);
    break;
    case "lostpass":
    include("$dir[func]/validate.php");
    lost_password();
    break;
    case "lostpassb":
    include("$dir[func]/validate.php");
    lost_passwordb($playerid,$ip);
    break;
    default:
    header("Location: $url[base]/$file[main]");
    break;
}

function select_ladder(){
    global $dir, $url, $out, $plyr, $site, $uinfo, $misc;
    $ladderslist=mysql_query("SELECT id,laddername FROM ladders ORDER by laddername");
    while(list($id,$name)=mysql_fetch_row($ladderslist)){
        $selectedladder="";
        if($ladderid=="$id"){
            $selectedladder="SELECTED";
        }

        $theladders.="<option value='$id' $selectedladder>$name</option>";
    }

    $playerinfo=mysql_query("SELECT * FROM users WHERE id='$plyr[id]'");
    $player=mysql_fetch_array($playerinfo);
    $altcolora="#000033";
    $altcolorb="#000020";
    $tablehead=table_head("show","100%","","left");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot  <br>
    $tablehead &nbsp; &nbsp;<strong>
    <font class='catfont'>Unirse a un ladder</font></strong><br>
    <hr class='catfont' size='1'>  <table width='100%' border='0' cellspacing='1' cellpadding='1'>
    <form method='post'>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>Elige un ladder para unirte</font>
    </td>
    <td width='50%' valign='center' align='center'>
    <select name='ladderid'>$theladders</select>
    </td>
    </tr>
    <tr class='altcolorb'>
    <td width='100%' valign='center' align='left' colspan='2'>
    <input type='hidden' name='action' value='joinladdera'>
    <center><button type='submit' name='' value='Join Ladder' class='btn btn-mini btn-success'>Unirse</button></center>
    </td>
    </tr> </form> </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function join_ladder($ladderid){
    global $dir, $url, $out, $plyr, $site, $uinfo, $misc;
    if(!$ladderid){
        include("$dir[func]/error.php");
        display_error("Invalid Ladder ID.<br>");
    }

    //MAKE SURE PLAYER IS NOT ALREADY ON LADDER
    $pteams=mysql_query("SELECT teamid FROM teammembers WHERE playerid='$plyr[id]'");
    while($row = mysql_fetch_array($pteams)){
        $check=mysql_query("SELECT id FROM ladder_$ladderid WHERE id='$row[teamid]'");
        $checkb=mysql_fetch_array($check);
        if($checkb){
            include("$dir[func]/error.php");
            display_error("Ya estas participando de este ladder.<br>");
        }

    }

    // CHECK IF SINGLES LADDER
    $ladderinfo=mysql_query("SELECT type FROM ladders WHERE id='$ladderid'");
    $laddertype=mysql_fetch_array($ladderinfo);
    if($laddertype[type] == 1){
        joinladder_single($ladderid);
        exit();
    }

    // GET LIST OF TEAMS FOR WHICH PLAYER IS LEADER
    $teamid=mysql_query("SELECT teamid FROM teammembers WHERE playerid='$plyr[id]' AND status = '1'");
    while($row = mysql_fetch_array($teamid)){
        $teamsinfo=mysql_query("SELECT teamname,id FROM teams WHERE id='$row[teamid]' AND type='3'");
        $teamslist=mysql_fetch_array($teamsinfo);
        if($teamslist){
            $teams.="<option value='$teamslist[id]'>$teamslist[teamname]</option>";
        }

    }

    $altcolora="#000033";
    $altcolorb="#000020";
    $tablehead=table_head("show","100%","","left");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot  <br>
    $tablehead &nbsp; &nbsp;<strong>
    <font class='catfont'>Unirse a un ladder</font></strong><br>
    <hr class='catfont' size='1'>  <table width='100%' border='0' cellspacing='1' cellpadding='1'>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='verdana,arial' size='2' color='#FFFFFF'>Unirse con el clan actual</font>
    </td>";
    if($teams){
        $out[body]=$out[body]."
        <form method='post'>
        <td width='50%' valign='center' align='center'>
        <select name='teamid'>$teams</select>
        <input type='hidden' name='ladderid' value='$ladderid'>
        <input type='hidden' name='action' value='joinladderb'>
        <center><button type='submit' name='' value='Join Ladder' class='btn btn-mini btn-success'>Unirse</button></center></td></form>";
    }else{

        $out[body]=$out[body]."
        <td width='50%' valign='center' align='center'>
        <center>No eres lider de ning&uacute;n clan</center>
        </td></tr>";
    }

    $out[body]=$out[body]."
    <form method='post'>
    <tr class='altcolorb'>
    <td width='50%' valign='center' align='left'>
    <font face='verdana,arial' size='2' color='#FFFFFF'>Create New Team</font>
    </td>
    <td width='50%' valign='center' align='center'>
    <input type='hidden' name='ladderid' value='$ladderid'>
    <input type='hidden' name='action' value='createteam'> <br>
    <center><input type='submit' name='' value='Create Team'></center></form>
    </td>
    </tr>
    </table>
    $tablefoot";
    include("$dir[curtheme]");
}

function create_team($ladderid){
    global $dir, $url, $out, $plyr, $site, $uinfo, $misc;
    if(!$ladderid){
        include("$dir[func]/error.php");
        display_error("Invalid Ladder ID.<br>");
    }

    $thisladder=mysql_query("SELECT * FROM ladders WHERE id='$ladderid'");
    $ladderinfo=mysql_fetch_array($thisladder);
    if(!$ladderinfo[laddername]){
        include("$dir[func]/error.php");
        display_error("Invalid Ladder ID.<br>");
    }

    $playerinfo=mysql_query("SELECT * FROM users WHERE id='$plyr[id]'");
    $player=mysql_fetch_array($playerinfo);
    $altcolora="#000033";
    $altcolorb="#000020";
    $tablehead=table_head("show","100%","","left");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    $out[body]=$out[body]."
    <center>
    $bannerhead
    $out[banner]
    $bannerfoot  <br>
    $tablehead &nbsp; &nbsp;<strong>
    <font class='catfont'>Create a New Team</font></strong><br>
    <hr class='catfont' size='1'>  <table width='100%' border='0' cellspacing='1' cellpadding='1'>
    <form method='post'>
    <input type='hidden' name='ladderid' value='$ladderinfo[laddername]'>
    <tr class='altcolorb'> <td width='50%' valign='center' align='left'> <font face='veradna,arial' size='2' color='#FFFFFF'>* Ladder</font>
    </td>
    <td width='50%' valign='center' align='center'> $ladderinfo[laddername]</td> </tr>
    <tr class='altcolora'> <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>* Team Name</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='t_alias' value='' size='30' maxlength='25'></td> </tr>
    <tr class='altcolorb'> <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'>* Team Email</font></td>
    <td width='50%' valign='center' align='center'>
    <input type='text' name='t_email' value='$player[email]' size='30' maxlength='50'></td> </tr>
    <tr class='altcolora'> <td width='50%' valign='center' align='left'> <font face='veradna,arial' size='2' color='#FFFFFF'> Tag</font></td>
    <td width='50%' valign='center' align='center'> <input type='text' name='t_tag' value='' size='30' maxlength='10'></td> </tr>
    <tr class='altcolorb'> <td width='50%' valign='center' align='left'> <font face='veradna,arial' size='2' color='#FFFFFF'> Tag Placement</font></td>
    <td width='50%' valign='center' align='center'> Before Name: <input type='radio' name='t_tagplace' value='0' CHECKED>  After: <input type='radio' name='t_tagplace' value='1'></td></tr>
    <tr class='altcolora'> <td width='50%' valign='center' align='left'> <font face='veradna,arial' size='2' color='#FFFFFF'> Team Website</font></td> <td width='50%' valign='center' align='center'>
    <input type='text' name='t_url' value='$player[website]' size='30' maxlength='100'></td> </tr>
    <tr class='altcolorb'> <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'> Team Logo</font></td> <td width='50%' valign='center' align='center'>
    <input type='text' name='t_logo' value='$player[logo]' size='30' maxlength='100'></td> </tr>
    <tr class='altcolora'> <td width='50%' valign='center' align='left'>
    <font face='veradna,arial' size='2' color='#FFFFFF'> Contact Method (Irc Channel@Server / AIM / MSN)</font></td> <td width='50%' valign='center' align='center'>
    <input type='text' name='t_contact' value='' size='30' maxlength='50'></td>
    </tr>  <tr> <td width='100%' valign='top' align='center' colspan='2'> <br>
    <input type='hidden' name='ladderid' value='$ladderid'>
    <input type='hidden' name='action' value='createteamb'>
    <button type='submit' name='' value='Join Ladder' class='btn btn-mini btn-success'>Unirse al ladder</button></td> </tr> </form> </table> $tablefoot";
    include("$dir[curtheme]");
}

function leaveladdera($ladderid){
    global $dir, $url, $out, $plyr, $site, $uinfo, $misc;
    if(!$ladderid){
        include("$dir[func]/error.php");
        display_error("Invalid Ladder ID.<br>");
    }

    $teams = mysql_query("SELECT teamid FROM teammembers WHERE playerid='$plyr[id]' AND status='1'");
    while($row=mysql_fetch_array($teams)){
        $teamid = mysql_query("SELECT id FROM ladder_$ladderid WHERE id='$row[teamid]'");
        if($rowb = mysql_fetch_array($teamid)){
            $tid = $rowb[id];
        }

    }

    if($misc[leaveladder] == "no"){
        include("$dir[func]/error.php");
        display_error("Leaving ladders is not permitted.<br>");
    }

    $ladder = mysql_fetch_array(mysql_query("SELECT type FROM ladders WHERE id='$ladderid'"));
    if($ladder[type] == '1'){
        include("$dir[func]/error.php");
        display_error("Leaving 1v1 ladders is not permitted.<br>");
    }

    if(!$tid){
        include("$dir[func]/error.php");
        display_error("You are not a team leader on this team.<br>");
    }

    //CHECK HOW MANY LADDERS TEAM IS ON
    $count = 0;
    $ladders = mysql_query("SELECT id FROM ladders WHERE type='3'");
    while($row=mysql_fetch_array($ladders)){
        $ladderon = mysql_query("SELECT id FROM ladder_$row[id] WHERE id='$tid'");
        if(mysql_fetch_array($ladderon)){
            $count++;
        }

    }

    $altcolora="#000033";
    $altcolorb="#000020";
    $tablehead=table_head("show","70%","","left");
    $tablefoot=table_foot("show");
    $bannerhead=table_head("show","488","80","center");
    $bannerfoot=table_foot("show");
    if($count >1){
        $out[body]=$out[body]."
        <center>
        $bannerhead
        $out[banner]
        $bannerfoot  <br>
        $tablehead &nbsp; &nbsp;<strong>
        <font class='catfont'>Leave Ladder</font></strong></font>
        <hr class='catfont' size='1'>  <center><br>Are you sure you wish to leave this ladder? <br><br>All stats for your team on this ladder will be lost. </center><br>
        <form method='post'>
        <input type='hidden' name='ladderid' value='$ladderid'>
        <input type='hidden' name='teamid' value='$tid'>
        <input type='hidden' name='action' value='leaveladderb'>
        <center><button type='submit' value='Leave Ladder' class='btn btn-mini btn-danger'>Dejar el ladder</button></center>
        </form>
        $tablefoot";
        include("$dir[curtheme]");
    }else{

        include("$dir[func]/error.php");
        display_error("This team is not on any other ladders. <br>Each team must remain on at least one ladder. <br><br>If you wish to leave the team, please use the <br>'leave team' option in the account manager.<br>");
    }

}

?>
