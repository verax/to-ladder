<?PHP
$indexloaded=1;
include("config/config.php");
include("$dir[func]/global.php");
include("$dir[func]/loginforms.php");
force_login();
switch($action){
    case "player":
    include("$dir[func]/editplayer.php");
    edit_player();
    break;
    case "editplayerb":
    include("$dir[func]/editplayer.php");
    edit_playerb($n_alias,$n_pass,$n_passa,$n_email,$n_emaila,$n_fname,$n_mname,$n_lname,$n_addrs,$n_city,$n_state,$n_zip,$n_phonea,$n_phoneb,$n_bdaym,$n_bdayd,$n_bdayy,$n_occup,$n_icq,$n_aim,$n_yahoo,$n_msn,$n_url,$n_logo,$n_newsletter,$n_theme,$n_country);
    break;
    case "team":
    include("$dir[func]/editteam.php");
    edit_team($teamid);
    break;
    case "teamb":
    include("$dir[func]/editteam.php");
    edit_teamb($teamid,$t_alias,$t_email,$t_tag,$t_tagplace,$t_url,$t_logo,$t_contact);
    break;
    case "members":
    include("$dir[func]/manageteam.php");
    manage_members($teamid);
    break;
    case "membersb":
    include("$dir[func]/manageteam.php");
    manage_membersb($teamid,$memberrank);
    break;
    case "invite":
    include("$dir[func]/invitemember.php");
    invite_member($teamid);
    break;
    case "inviteb":
    include("$dir[func]/invitemember.php");
    invite_memberb($teamid,$inviteid);
    break;
    case "addmember":
    include("$dir[func]/addmember.php");
    invite_member($teamid);
    break;
    case "addmemberb":
    include("$dir[func]/addmember.php");
    invite_memberb($teamid,$inviteid);
    break;
    case "accept":
    include("$dir[func]/invitemember.php");
    accept_invite($teamid,$inviteid);
    break;
    case "leave":
    include("$dir[func]/leaveteam.php");
    leave_team($teamid);
    break;
    case "leaveb":
    include("$dir[func]/leaveteam.php");
    leave_teamb($teamid);
    break;
    default:
    header("Location: $url[base]/$file[main]");
    break;
}

?>
